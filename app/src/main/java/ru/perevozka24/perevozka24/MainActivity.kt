package ru.perevozka24.perevozka24

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.core.view.GravityCompat
import androidx.lifecycle.Observer
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.include_main_bottom_menu.*
import ru.perevozka24.perevozka24.ui.base.BaseUpdateActivity
import ru.perevozka24.perevozka24.ui.common.DrawerHolder
import ru.perevozka24.perevozka24.ui.main.MainScreenMode
import ru.perevozka24.perevozka24.ui.main.MainViewModel
import ru.perevozka24.perevozka24.ui.main.orders.detailed.OfferDetailedActivityDirections
import ru.perevozka24.perevozka24.ui.viewModel
import ru.perevozka24.perevozka24.utils.trackEvent
import timber.log.Timber

class MainActivity : BaseUpdateActivity(), DrawerHolder {

    private val viewModel: MainViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val navController = findNavController(R.id.nav_host_fragment)
        AppBarConfiguration(navController.graph, drawerLayout)

        bottomMenuEquipment.setOnClickListener { viewModel.changeMode(MainScreenMode.Equipment) }
        bottomMenuCargos.setOnClickListener { viewModel.changeMode(MainScreenMode.Cargo) }
        bottomMenuOrders.setOnClickListener { viewModel.changeMode(MainScreenMode.Order) }
        bottomMenuOpenMenu.setOnClickListener {
            openLeftDrawer()
            trackEvent("show_main_menu", mapOf("type" to "bottom-menu"))
        }
        bottomMenuProfiles.setOnClickListener { viewModel.openProfile() }

        viewModel.openProfileEvent.observe(this, Observer {
            navController.navigate(it)
        })

        intent.getIntExtra(REDIRECT, -1).takeIf { it > 0 }?.let {
            intent.removeExtra(REDIRECT)

            when (it) {
                VIEW_CARGO -> viewModel.changeMode(MainScreenMode.Cargo)
                VIEW_ORDER -> viewModel.changeMode(MainScreenMode.Order)
                MY_ORDERS -> viewModel.changeMode(MainScreenMode.MyOrders)
            }
        }

        intent?.let {
            openFromPush(it)
            Timber.e(it.extras.toString())
        }
    }

    override fun openLeftDrawer() {
        drawerLayout?.openDrawer(GravityCompat.START)
    }

    override fun onBackPressed() {
        if (drawerLayout.isOpen) {
            drawerLayout.closeDrawers()
        } else {
            super.onBackPressed()
        }
    }

    override fun onNewIntent(intent: Intent) {
        super.onNewIntent(intent)
        openFromPush(intent)
    }

    private fun openFromPush(intent: Intent) {
        val pushOfferId = intent.getStringExtra("offerId") ?: intent.getStringExtra("offer_id")

        if (pushOfferId != null) {
            findNavController(R.id.nav_host_fragment).navigate(
                OfferDetailedActivityDirections.actionGlobalOfferDetailedActivity(offerId = pushOfferId)
            )
            intent.removeExtra("offerId")
            intent.removeExtra("offer_id")
        }
    }

    override fun closeLeftDrawer() {
        drawerLayout?.closeDrawer(GravityCompat.START)
    }

    companion object {
        private const val REDIRECT = "redirect"
        const val VIEW_CARGO = 4
        const val VIEW_ORDER = 6
        const val MY_ORDERS = 5

        fun create(context: Context, redirect: Int) =
            Intent(context, MainActivity::class.java).apply {
                putExtra(REDIRECT, redirect)
            }
    }
}
