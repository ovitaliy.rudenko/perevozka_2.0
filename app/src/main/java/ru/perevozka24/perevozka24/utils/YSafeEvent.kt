package ru.perevozka24.perevozka24.utils

import com.yandex.metrica.YandexMetrica
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import ru.perevozka24.perevozka24.ui.common.ext.safeSuspended

fun trackEvent(event: String, params: Map<String, Any>? = null) {
    GlobalScope.launch {
        safeSuspended {
            withContext(Dispatchers.IO) {
                if (params == null) YandexMetrica.reportEvent(event)
                else YandexMetrica.reportEvent(event, params)
            }
        }
    }
}
