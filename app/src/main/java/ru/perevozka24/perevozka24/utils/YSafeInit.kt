package ru.perevozka24.perevozka24.utils

import android.content.Context
import com.yandex.mapkit.MapKitFactory
import com.yandex.mapkit.directions.DirectionsFactory
import ru.perevozka24.perevozka24.R

object YSafeInit {
    fun map(context: Context) {
        try {
            MapKitFactory.setApiKey(context.getString(R.string.yandex_key))
        } catch (e: AssertionError) {
            e.printStackTrace()
        }
        try {
            MapKitFactory.initialize(context)
        } catch (e: AssertionError) {
            e.printStackTrace()
        }
    }

    fun directions(context: Context) {
        try {
            DirectionsFactory.initialize(context)
        } catch (e: AssertionError) {
            e.printStackTrace()
        }
    }
}
