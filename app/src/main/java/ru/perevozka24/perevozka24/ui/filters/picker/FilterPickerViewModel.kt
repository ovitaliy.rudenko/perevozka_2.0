package ru.perevozka24.perevozka24.ui.filters.picker

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.map
import ru.perevozka24.perevozka24.data.ResourceProvider
import ru.perevozka24.perevozka24.data.Result
import ru.perevozka24.perevozka24.data.features.filter.FilterLocationPickerRepository
import ru.perevozka24.perevozka24.data.features.filter.FilterPickerRepository
import ru.perevozka24.perevozka24.ui.base.BaseViewModel
import ru.perevozka24.perevozka24.ui.common.livedata.Combined2LiveData
import ru.perevozka24.perevozka24.ui.filters.models.PickerModel
import ru.perevozka24.perevozka24.ui.filters.picker.loader.PickerLoader
import ru.perevozka24.perevozka24.ui.filters.picker.loader.PickerLoaderFactory

open class FilterPickerViewModel(
    private val pickerRepository: FilterPickerRepository,
    private val pickerLocationRepository: FilterLocationPickerRepository,
    resourceProvider: ResourceProvider
) : BaseViewModel(resourceProvider) {
    lateinit var loader: PickerLoader<PickerModel>

    val result: LiveData<Result<List<PickerModel>>> = MutableLiveData()
    private val items = result.map {
        (it as? Result.Success<List<PickerModel>>)?.data ?: emptyList()
    }

    private val filter = MutableLiveData<String>()

    private val applyFilter = fun(items: List<PickerModel>?, filter: String?): List<PickerModel> {
        return if (filter == null) items ?: emptyList()
        else loader.filter(items ?: emptyList(), filter.trim())
    }

    val filtered =
        Combined2LiveData<List<PickerModel>, String, List<PickerModel>>(
            items,
            filter,
            applyFilter
        )

    fun <T : PickerModel> init(current: T, vararg extra: Any) {
        loader = PickerLoaderFactory.create(
            current,
            pickerRepository,
            pickerLocationRepository,
            extra.clone()
        )

        doWork(result) {
            loader.load()
        }
    }

    fun filterChanged(newFilter: String) {
        filter.postValue(newFilter)
    }
}
