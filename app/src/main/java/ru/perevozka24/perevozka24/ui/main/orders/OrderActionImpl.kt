package ru.perevozka24.perevozka24.ui.main.orders

import android.view.View
import androidx.fragment.app.Fragment
import ru.perevozka24.perevozka24.ui.main.orders.base.BaseOrderContentFragment
import ru.perevozka24.perevozka24.ui.main.orders.detailed.OfferDetailedActivity
import ru.perevozka24.perevozka24.ui.main.orders.models.OfferModel

class OrderActionImpl(
    private val fragment: Fragment,
    private val addToFavorite: AddToFavoriteOrderDelegate
) : OrderAction {
    override fun addToFavorite(model: OfferModel, view: View) {
        addToFavorite.addToFavorite(model)
    }

    override fun openDetailed(model: OfferModel, view: View) {
        fragment.startActivityForResult(
            OfferDetailedActivity.create(fragment.requireContext(), model),
            BaseOrderContentFragment.REQUEST_CODE
        )
    }
}
