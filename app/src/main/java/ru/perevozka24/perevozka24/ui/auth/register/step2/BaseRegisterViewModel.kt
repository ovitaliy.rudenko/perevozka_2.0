package ru.perevozka24.perevozka24.ui.auth.register.step2

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import ru.perevozka24.perevozka24.R
import ru.perevozka24.perevozka24.data.ResourceProvider
import ru.perevozka24.perevozka24.data.Result
import ru.perevozka24.perevozka24.ui.auth.models.LoginModel
import ru.perevozka24.perevozka24.ui.base.BaseViewModel
import ru.perevozka24.perevozka24.ui.common.livedata.postValue
import ru.perevozka24.perevozka24.ui.common.widgets.form.CheckedValidator
import ru.perevozka24.perevozka24.ui.common.widgets.form.EmptyFieldValidator
import ru.perevozka24.perevozka24.ui.common.widgets.form.InputField
import ru.perevozka24.perevozka24.utils.trackEvent

abstract class BaseRegisterViewModel(
    resourceProvider: ResourceProvider
) : BaseViewModel(resourceProvider) {

    protected lateinit var contact: String

    val usernameFieldError: LiveData<String> = MutableLiveData()
    protected val usernameField =
        MutableLiveData(
            InputField(
                "",
                EmptyFieldValidator(),
                { usernameFieldError.postValue(it) }
            )
        )

    val passwordError: LiveData<String> = MutableLiveData()
    protected val passwordField = MutableLiveData(
        InputField(
            "",
            EmptyFieldValidator(),
            { passwordError.postValue(it) }
        ))
    val agreementError: LiveData<String> = MutableLiveData()
    protected val agreementCheckedField = MutableLiveData(
        InputField(
            true,
            CheckedValidator(
                resourceProvider.getString(R.string.auth_registration_agreement_not_checked)
            ),
            { agreementError.postValue(it) }
        )
    )

    val registering: LiveData<Result<Unit>> = MutableLiveData()

    fun init(value: String) {
        contact = value
    }

    fun userNameChange(username: String) {
        usernameField.value?.value = username
    }

    fun passwordChange(password: String) {
        passwordField.value?.value = password
    }

    fun agreementCheckChanged(checked: Boolean) {
        agreementCheckedField.value?.value = checked
    }

    fun register() {
        val fields = arrayOf(
            usernameField,
            passwordField,
            agreementCheckedField
        )

        val isValid = fields.map {
            it.value?.validate()
            (it.value?.hasError ?: true).not()
        }
            .all { it }

        if (isValid) {
            doWork(registering) {
                registerDo()
                trackEvent(
                    "reg_successful",
                    mapOf("email" to (usernameField.value?.value ?: ""))
                )
            }
        }
    }

    protected abstract suspend fun registerDo(): LoginModel
}
