package ru.perevozka24.perevozka24.ui.common.widgets.form

class MinLengthValidator(private val minLength: Int) : InputFieldValidator<String> {
    override fun validate(input: String): String? {
        if (input.length < minLength) {
            return "Минимальная длина поля $minLength"
        }
        return null
    }
}
