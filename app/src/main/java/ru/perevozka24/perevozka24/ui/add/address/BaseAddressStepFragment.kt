package ru.perevozka24.perevozka24.ui.add.address

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import kotlinx.android.synthetic.main.fragment_add_order_step_address.*
import kotlinx.android.synthetic.main.item_filter_location.view.*
import ru.perevozka24.perevozka24.R
import ru.perevozka24.perevozka24.data.features.location.entity.SimpleAddress
import ru.perevozka24.perevozka24.ui.add.BaseCreateOrderStepFragment
import ru.perevozka24.perevozka24.ui.common.ext.toast
import ru.perevozka24.perevozka24.ui.common.ext.visible
import ru.perevozka24.perevozka24.ui.filters.models.CityModel
import ru.perevozka24.perevozka24.ui.filters.models.CountryModel
import ru.perevozka24.perevozka24.ui.filters.models.RegionModel
import ru.perevozka24.perevozka24.ui.filters.picker.AddressLinePickerActivity
import ru.perevozka24.perevozka24.ui.filters.picker.FilterPickerActivity
import ru.perevozka24.perevozka24.ui.picker.map.MapPickerActivity
import ru.perevozka24.perevozka24.ui.viewModel

@SuppressWarnings("TooManyFunctions")
abstract class BaseAddressStepFragment : BaseCreateOrderStepFragment() {

    protected val viewModel: AddressStepViewModel by viewModel()

    protected open val needDetectLocation = true

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_add_order_step_address, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.initCurrentLocation(needDetectLocation)

        initInputViews()

        addOrderSelectOnMap.setOnClickListener {
            startActivityForResult(
                MapPickerActivity.create(requireContext(), viewModel.simpleAddress), REQUEST_ADDRESS
            )
        }

        bindNextButton()
    }

    private fun initInputViews() {
        initAddressCountry()
        initAddressRegion()
        initAddressCity()
        initAddressView()
    }

    private fun initAddressCountry() {
        with(addOrderCountry) {
            setBackgroundResource(R.drawable.underlined_fa)
            locationIcon.setImageResource(R.drawable.ic_word)
            val defaultTitle = context.getString(R.string.filter_country_info_default)
            locationTitle.text = defaultTitle
            viewModel.country.observe(viewLifecycleOwner, Observer {
                locationTitle.text = it?.name ?: defaultTitle
                addOrderRegion.visible = it?.name != null
            })
            setOnClickListener { startPickingCountry() }
        }
    }

    private fun initAddressRegion() {
        with(addOrderRegion) {
            setBackgroundResource(R.drawable.underlined_fa)
            locationIcon.setImageResource(R.drawable.ic_location)
            val defaultTitle = context.getString(R.string.filter_region_info_default)
            locationTitle.text = defaultTitle
            viewModel.region.observe(viewLifecycleOwner, {
                locationTitle.text = it?.name ?: defaultTitle
                addOrderCity.visible = it?.name != null
            })
            setOnClickListener { startPickingRegion() }
        }
    }

    protected open fun initAddressCity() {
        with(addOrderCity) {
            setBackgroundResource(R.drawable.underlined_fa)
            locationIcon.setImageResource(R.drawable.ic_city)
            val defaultTitle = context.getString(R.string.filter_city_info_default)
            locationTitle.text = defaultTitle
            viewModel.city.observe(viewLifecycleOwner, {
                locationTitle.text = it?.name ?: defaultTitle
                addOrderAddress.visible = it?.name != null
            })
            setOnClickListener { startPickingCity() }
        }
    }

    protected open fun initAddressView() {
        with(addOrderAddress) {
            setBackgroundResource(R.drawable.underlined_fa)
            locationIcon.setImageResource(R.drawable.ic_address_line)
            locationTitle.text = context.getString(R.string.filter_line_default)
            viewModel.address.observe(viewLifecycleOwner, Observer {
                locationTitle.text = it?.takeIf { it.isNotEmpty() }
                    ?: context.getString(R.string.filter_line_default)
            })
            setOnClickListener { startPickingLine() }
        }
    }

    open fun submit() {
        addOrderViewModel.locationSelected(
            checkNotNull(viewModel.country.value),
            checkNotNull(viewModel.region.value),
            checkNotNull(viewModel.city.value),
            checkNotNull(viewModel.address.value)
        )
    }

    private fun bindNextButton() {
        addOrderAddMoveNext.setOnClickListener { submit() }
        viewModel.nextEnabled.observe(viewLifecycleOwner, Observer {
            addOrderAddMoveNext.isEnabled = it
        })
    }

    private fun startPickingCountry() {
        val intent = FilterPickerActivity.create(
            requireContext(),
            checkNotNull(viewModel.country.value),
            0,
        )
        startActivityForResult(intent, REQUEST_COUNTRY)
    }

    private fun startPickingRegion() {
        val region = checkNotNull(viewModel.region.value)
        if (region.countryId == null) {
            toast(getString(R.string.filter_select_country_at_first))
        } else {
            val intent = FilterPickerActivity.create(
                requireContext(),
                region,
                0,
            )
            startActivityForResult(intent, REQUEST_REGION)
        }
    }

    protected fun startPickingCity() {
        val city = checkNotNull(viewModel.city.value)
        if (city.regionId == null) {
            toast(getString(R.string.filter_select_region_at_first))
        } else {
            val intent = FilterPickerActivity.create(
                requireContext(),
                city,
                0,
            )
            startActivityForResult(intent, REQUEST_CITY)
        }
    }

    private fun startPickingLine() {
        val city = viewModel.region.value
        if (city?.id == null) {
            toast(getString(R.string.filter_select_city_at_first))
        } else {
            val intent = AddressLinePickerActivity.create(
                requireContext(),
                viewModel.address.value ?: ""
            )
            startActivityForResult(intent, REQUEST_ADDRESS_LINE)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUEST_COUNTRY && resultCode == Activity.RESULT_OK) {
            val value = data?.getParcelableExtra<CountryModel>(FilterPickerActivity.MODEL)
            viewModel.countryChanged(checkNotNull(value))
        }
        if (requestCode == REQUEST_REGION && resultCode == Activity.RESULT_OK) {
            val value = data?.getParcelableExtra<RegionModel>(FilterPickerActivity.MODEL)
            viewModel.regionChanged(checkNotNull(value))
        }
        if (requestCode == REQUEST_CITY && resultCode == Activity.RESULT_OK) {
            val value = data?.getParcelableExtra<CityModel>(FilterPickerActivity.MODEL)
            viewModel.cityChanged(checkNotNull(value))
        }
        if (requestCode == REQUEST_ADDRESS && resultCode == Activity.RESULT_OK) {
            val value = data?.getParcelableExtra<SimpleAddress>(MapPickerActivity.ADDRESS)
            viewModel.addressSelected(checkNotNull(value))
        }
        if (requestCode == REQUEST_ADDRESS_LINE && resultCode == Activity.RESULT_OK) {
            val value = data?.getStringExtra(AddressLinePickerActivity.LINE)
            viewModel.addressChanged(checkNotNull(value))
        }
    }

    private companion object {
        private const val REQUEST_COUNTRY = 10001
        private const val REQUEST_REGION = 10002
        private const val REQUEST_CITY = 10003
        private const val REQUEST_ADDRESS_LINE = 10004
        private const val REQUEST_ADDRESS = 10005
    }
}
