package ru.perevozka24.perevozka24.ui.main.orders

import android.view.View
import ru.perevozka24.perevozka24.ui.main.orders.models.OfferModel

interface OrderAction {
    fun addToFavorite(model: OfferModel, view: View)
    fun openDetailed(model: OfferModel, view: View)
}
