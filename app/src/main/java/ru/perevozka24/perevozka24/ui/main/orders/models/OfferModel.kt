package ru.perevozka24.perevozka24.ui.main.orders.models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize
import org.threeten.bp.LocalDateTime
import ru.perevozka24.perevozka24.ui.filters.models.CategoryModel

@Parcelize
data class OfferModel(
    val id: String,
    val name: String,
    val shipment: Long?,
    val category: CategoryModel,
    val categoryId: Long?,
    val offer: String?,
    val dateAdded: LocalDateTime?,
    val code: OfferStatus,
    val contacts: String?,
    val country: String?,
    val message: String?,
    val abuseCount: Int,
    val categoryImage: String?,
    val remainingTime: Long,
    val price: Long,
    val payRate: Double?,
    val payType: String?,
    val favorite: Boolean,
    val cityId: Long?,
    val cityName: String?,
    val cityNameTo: String?,
    val status: OfferProgressStatus,
    val gallery: List<ImageModel>,
    val address: String?,
    val addressTo: String?,
    val companyUrl: String?,
    val companyName: String?,
    val showContact: Boolean,
    val performTime: PerformTime
) : Parcelable
