package ru.perevozka24.perevozka24.ui.picker.map

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import com.yandex.mapkit.geometry.Geometry
import com.yandex.mapkit.geometry.Point
import com.yandex.mapkit.map.CameraPosition
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import ru.perevozka24.perevozka24.data.ResourceProvider
import ru.perevozka24.perevozka24.data.features.location.LocationParams.Companion.ZOOM_ADDRESS
import ru.perevozka24.perevozka24.data.features.location.LocationRepository
import ru.perevozka24.perevozka24.data.features.location.entity.SimpleAddress
import ru.perevozka24.perevozka24.ui.LiveEvent
import ru.perevozka24.perevozka24.ui.base.BaseViewModel
import ru.perevozka24.perevozka24.ui.common.livedata.postValue

class MapPickerViewModel(
    private val locationRepository: LocationRepository,
    resourceProvider: ResourceProvider
) : BaseViewModel(resourceProvider) {

    val address: LiveData<SimpleAddress?> = MutableLiveData(null)
    val submitEnabled: LiveData<Boolean> = Transformations.map(address) { it != null }

    val cameraPosition = LiveEvent<CameraPosition?>()

    val suggestions: LiveData<List<SimpleAddress>> = MutableLiveData()
    val updateQuery = LiveEvent<String>()

    fun init(newAddress: SimpleAddress) {
        address.postValue(newAddress)
        updateQuery.postValue(newAddress.toText())
        cameraPosition.postValue(newAddress.toCameraPosition())
    }

    fun queryChanged(geometry: Geometry, query: String) {
        if (query.length < MIN_QUERY_LENGTH) {
            return
        }

        GlobalScope.launch {
            safe {
                val parts = listOfNotNull(
                    address.value?.country,
                    address.value?.region,
                    address.value?.city,
                    query
                ).joinToString(", ")
                if (parts.isNotEmpty()) {
                    val list = locationRepository.getAddress(geometry, query)
                    suggestions.postValue(list)
                }
            }
        }
    }

    fun setLocation(point: Point, zoom: Float) {
        cameraPosition.postValue(point.toCameraPosition())

        GlobalScope.launch {
            safe {
                val list = locationRepository.getAddress(point, zoom)
                list.minBy {
                    distance(
                        point.latitude,
                        point.latitude,
                        it.latitude ?: Double.MAX_VALUE,
                        it.latitude ?: Double.MAX_VALUE
                    )
                }?.let {
                    suggestions.postValue(emptyList())
                    updateQuery.postValue(it.toText())
                    address.postValue(it)
                }
            }
        }
    }

    fun addressSelected(address: SimpleAddress) {
        this.cameraPosition.postValue(address.toCameraPosition())
        this.address.postValue(address)
        updateQuery.postValue(address.toText())
    }

    fun clearQuery() {
        this.address.postValue(null)
        updateQuery.postValue("")
        cameraPosition.postValue(null)
        suggestions.postValue(emptyList())
    }

    fun Point.toCameraPosition() =
        CameraPosition(this, ZOOM_ADDRESS, 0f, 0f)

    private companion object {
        const val MIN_QUERY_LENGTH = 3

        const val EARTH_RADIUS = 6378137.0

        fun distance(
            fromLat: Double,
            fromLon: Double,
            toLat: Double,
            toLon: Double
        ): Double {
            val radius = EARTH_RADIUS
            val deltaLat = toLat - fromLat
            val deltaLon = toLon - fromLon
            val angle = 2 * Math.asin(
                Math.sqrt(
                    Math.pow(Math.sin(deltaLat / 2), 2.0) +
                            Math.cos(fromLat) * Math.cos(toLat) *
                            Math.pow(Math.sin(deltaLon / 2), 2.0)
                )
            )
            return radius * angle
        }
    }
}
