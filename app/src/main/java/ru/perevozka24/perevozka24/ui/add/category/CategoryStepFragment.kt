package ru.perevozka24.perevozka24.ui.add.category

import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.GroupieViewHolder
import kotlinx.android.synthetic.main.fragment_add_order_step_category.*
import ru.perevozka24.perevozka24.R
import ru.perevozka24.perevozka24.ui.add.AddOrderActivity
import ru.perevozka24.perevozka24.ui.add.AddOrderViewModel
import ru.perevozka24.perevozka24.ui.add.ICreateOrderStep
import ru.perevozka24.perevozka24.ui.add.photo.items.GridSpacingItemDecoration
import ru.perevozka24.perevozka24.ui.base.BaseContentLoaderFragment
import ru.perevozka24.perevozka24.ui.common.ext.updateOrReplace
import ru.perevozka24.perevozka24.ui.filters.models.CategoryModel
import ru.perevozka24.perevozka24.ui.filters.picker.loader.PickerMapper
import ru.perevozka24.perevozka24.ui.viewModel

class CategoryStepFragment : BaseContentLoaderFragment(), ICreateOrderStep {
    override val title: Int = R.string.create_order_step_category_type

    private val addOrderViewModel: AddOrderViewModel by lazy {
        (activity as AddOrderActivity).viewModel
    }
    private val stepViewModel: CategoryStepViewModel by viewModel()

    override val contentLayoutId: Int = R.layout.fragment_add_order_step_category
    private val adapter = GroupAdapter<GroupieViewHolder>()

    private val onClickListener = View.OnClickListener { v ->
        addOrderViewModel.categorySelected(v.tag as CategoryModel)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        subscribe(stepViewModel.result)
        filterPickerList.adapter = adapter
        filterPickerList.addItemDecoration(GridSpacingItemDecoration(1, resources.getDimensionPixelOffset(R.dimen.dp8)))
        stepViewModel.filtered.observe(viewLifecycleOwner, Observer { list ->
            list ?: return@Observer
            val mapped = PickerMapper.mapAddNewOrder(list, onClickListener)
            adapter.updateOrReplace(mapped)
        })
    }
}
