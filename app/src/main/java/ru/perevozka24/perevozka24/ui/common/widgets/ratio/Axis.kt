package ru.perevozka24.perevozka24.ui.common.widgets.ratio

enum class Axis(val id: Int) {
    HORIZONTAL(0), VERTICAL(1)
}

internal fun findAxis(id: Int) = Axis.values().find { it.id == id }
    ?: throw IllegalArgumentException("id must not exceed 1")
