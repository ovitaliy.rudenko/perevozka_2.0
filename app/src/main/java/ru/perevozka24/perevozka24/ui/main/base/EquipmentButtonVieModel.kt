package ru.perevozka24.perevozka24.ui.main.base

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.map
import ru.perevozka24.perevozka24.R
import ru.perevozka24.perevozka24.data.ResourceProvider
import ru.perevozka24.perevozka24.data.features.auth.AuthRepository
import ru.perevozka24.perevozka24.ui.base.BaseViewModel

class EquipmentButtonVieModel(
    authRepository: AuthRepository,
    resourceProvider: ResourceProvider
) : BaseViewModel(resourceProvider) {
    val equipmentLink: LiveData<Int> = MutableLiveData(authRepository.isAuthorized())
        .map {
            if (it) R.string.link_add_equipment_logged_in
            else R.string.link_add_equipment_not_logged_in
        }
}
