package ru.perevozka24.perevozka24.ui.filters.models

import android.os.Parcelable

interface FilterModel : Parcelable {
    fun updated(value: PickerModel): FilterModel
    fun updated(type: Class<out PickerModel>, value: PickerModel?): FilterModel = this
}
