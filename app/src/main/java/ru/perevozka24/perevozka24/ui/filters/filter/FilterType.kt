package ru.perevozka24.perevozka24.ui.filters.filter

import ru.perevozka24.perevozka24.R

enum class FilterType {
    EQUIPMENT {
        override val title: Int
            get() = R.string.filter_equipment_title
        override val button: Int
            get() = R.string.filter_equipment_button
    },

    CARGO {
        override val title: Int
            get() = R.string.filter_cargo_title
        override val button: Int
            get() = R.string.filter_cargo_button
    },

    ORDER {
        override val title: Int
            get() = R.string.filter_order_title
        override val button: Int
            get() = R.string.filter_order_button
    };

    abstract val title: Int
    abstract val button: Int
}
