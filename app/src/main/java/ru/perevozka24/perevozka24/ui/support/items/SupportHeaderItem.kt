package ru.perevozka24.perevozka24.ui.support.items

import com.xwray.groupie.kotlinandroidextensions.GroupieViewHolder
import com.xwray.groupie.kotlinandroidextensions.Item
import kotlinx.android.synthetic.main.item_support_content.view.*
import kotlinx.android.synthetic.main.item_support_header.view.*
import ru.perevozka24.perevozka24.R

data class SupportHeaderItem(val title: Int) : Item() {
    override fun getId() = 0L
    override fun getLayout() = R.layout.item_support_header
    override fun bind(viewHolder: GroupieViewHolder, position: Int) {
        with(viewHolder.itemView) {
            itemSupportHeaderTitle.text = resources.getString(title)
        }
    }
}
