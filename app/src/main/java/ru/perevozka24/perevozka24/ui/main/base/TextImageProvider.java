package ru.perevozka24.perevozka24.ui.main.base;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.graphics.Paint.Style;
import android.util.DisplayMetrics;
import android.view.WindowManager;

import com.yandex.runtime.image.ImageProvider;

public class TextImageProvider extends ImageProvider {
    private static final float FONT_SIZE = 15;

    private final Context context;
    private final String text;
    private final int size;

    public TextImageProvider(Context context, String text, int size) {
        this.context = context;
        this.text = text;
        this.size = size;
    }

    @Override
    public String getId() {
        return "text_" + text;
    }

    @Override
    public Bitmap getImage() {
        final DisplayMetrics metrics = new DisplayMetrics();
        final WindowManager manager =
                (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        manager.getDefaultDisplay().getMetrics(metrics);

        final Paint textPaint = new Paint();
        textPaint.setTextSize(FONT_SIZE * metrics.density);
        textPaint.setTextAlign(Align.CENTER);
        textPaint.setStyle(Style.FILL);
        textPaint.setAntiAlias(true);
        final Paint.FontMetrics textMetrics = textPaint.getFontMetrics();
        final float sizeHalf = size / 2f;
        final Bitmap bitmap = Bitmap.createScaledBitmap(
                MapPinTransformation.Companion.getClusterBg(),
                size, size,
                true);
        final Canvas canvas = new Canvas(bitmap);
        canvas.drawText(
                text,
                sizeHalf,
                sizeHalf - (textMetrics.ascent + textMetrics.descent) / 2,
                textPaint);

        return bitmap;
    }
}
