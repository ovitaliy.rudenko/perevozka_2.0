package ru.perevozka24.perevozka24.ui.notification

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import ru.perevozka24.perevozka24.data.ResourceProvider
import ru.perevozka24.perevozka24.data.Result
import ru.perevozka24.perevozka24.data.features.notification.NotificationRepository
import ru.perevozka24.perevozka24.ui.base.BaseViewModel
import ru.perevozka24.perevozka24.ui.common.livedata.postValue
import ru.perevozka24.perevozka24.ui.filters.models.NotificationModel
import ru.perevozka24.perevozka24.ui.filters.models.PickerModel

class NotificationViewModel(
    private val repository: NotificationRepository,
    resourceProvider: ResourceProvider
) : BaseViewModel(resourceProvider) {

    val result: LiveData<Result<NotificationModel>> = MutableLiveData()

    init {
        doWork(result) {
            repository.getNotificationSetting()
        }
    }

    fun enabledChange(value: Boolean) {
        val model = (result.value as? Result.Success)?.data ?: return
        val updated = model.copy(enabled = value)
        saveUpdated(updated)
    }

    fun enabledSoundChange(value: Boolean) {
        val model = (result.value as? Result.Success)?.data ?: return
        val updated = model.copy(sounds = value)
        saveUpdated(updated)
    }

    fun pickerValueChanged(value: PickerModel) {
        val model = (result.value as? Result.Success)?.data ?: return
        val updated = model.updated(value) as NotificationModel
        saveUpdated(updated)
    }

    private fun saveUpdated(updated: NotificationModel) {
        GlobalScope.launch {
            val response = safeExecute { repository.save(updated) }
            if (response is Result.Success) {
                result.postValue(Result.Success(updated))
            } else if (response is Result.Error) {
                error.postValue(response.message)
            }
        }
    }
}
