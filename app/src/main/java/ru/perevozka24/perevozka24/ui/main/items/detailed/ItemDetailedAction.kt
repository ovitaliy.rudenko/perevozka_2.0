package ru.perevozka24.perevozka24.ui.main.items.detailed

import ru.perevozka24.perevozka24.ui.main.items.models.ItemModel

interface ItemDetailedAction {
    fun addToFavorite(item: ItemModel)
    fun addAbuse(item: ItemModel)
    fun block(item: ItemModel)
    fun call(item: ItemModel)
}
