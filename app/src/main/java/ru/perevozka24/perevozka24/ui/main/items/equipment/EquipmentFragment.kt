package ru.perevozka24.perevozka24.ui.main.items.equipment

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.commit
import androidx.lifecycle.Observer
import kotlinx.android.synthetic.main.include_filter_equipment.*
import org.kodein.di.Kodein
import org.kodein.di.android.x.closestKodein
import ru.perevozka24.perevozka24.R
import ru.perevozka24.perevozka24.ui.base.BaseFragment
import ru.perevozka24.perevozka24.ui.common.ext.toast
import ru.perevozka24.perevozka24.ui.filters.filter.FilterActivity
import ru.perevozka24.perevozka24.ui.filters.filter.FilterSelectionFragment
import ru.perevozka24.perevozka24.ui.filters.filter.FilterType
import ru.perevozka24.perevozka24.ui.filters.models.FilterModel
import ru.perevozka24.perevozka24.ui.main.ViewMode
import ru.perevozka24.perevozka24.ui.main.base.ISearchContainer
import ru.perevozka24.perevozka24.ui.viewModel
import ru.perevozka24.perevozka24.utils.trackEvent

class EquipmentFragment : BaseFragment() {
    override val kodein: Kodein by closestKodein()
    val viewModel: EquipmentViewModel by viewModel()

    val mapViewModel: EquipmentMapViewModel by viewModel()
    val listViewModel: EquipmentListViewModel by viewModel()

    private val mapFragment by lazy(LazyThreadSafetyMode.NONE) {
        EquipmentMapFragment()
    }
    private val listFragment by lazy(LazyThreadSafetyMode.NONE) {
        EquipmentListFragment()
    }

    private val children: Array<ISearchable> by lazy(LazyThreadSafetyMode.NONE) {
        arrayOf(
            mapFragment as ISearchable,
            listFragment as ISearchable
        )
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(R.layout.fragment_tab_equipment, container, false)

    private fun createFilterIntent() =
        FilterActivity.create(requireContext(), FilterType.EQUIPMENT)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        trackEvent("visited_main_screen")
    }

     fun openFilter() {
        startActivityForResult(
            createFilterIntent(),
            CODE
        )
        trackEvent(
            "filter_pressed",
            mapOf("type" to "main_screen")
        )
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        (parentFragment as? ISearchContainer)?.subscribeSearch(viewModel.query) { query ->
            queryChanged(query)
        }

        contentFilter.setOnClickListener { openFilter() }
        contentMapPin.setOnClickListener { viewModel.switchMode(ViewMode.MAP) }
        contentListPin.setOnClickListener { viewModel.switchMode(ViewMode.LIST) }

        childFragmentManager.commit {
            add(R.id.mainContentContainer, mapFragment)
            add(R.id.mainContentContainer, listFragment)
        }

        viewModel.mode.observe(viewLifecycleOwner, Observer {
            val mode = it ?: return@Observer

            contentMapPin.isSelected = mode == ViewMode.MAP
            contentListPin.isSelected = mode == ViewMode.LIST

            val show: Fragment
            val hide: Fragment

            when (mode) {
                ViewMode.MAP -> {
                    show = mapFragment
                    hide = listFragment
                }
                ViewMode.LIST -> {
                    show = listFragment
                    hide = mapFragment
                }
            }
            childFragmentManager.commit {
                show(show)
                hide(hide)
            }
        })
        viewModel.error.observe(viewLifecycleOwner, {
            toast(it)
        })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == CODE && resultCode == Activity.RESULT_OK) {
            queryChanged("")
            (parentFragment as? ISearchContainer)?.clearSearch()
            refreshAfterFilterChange(
                data?.getParcelableExtra(FilterSelectionFragment.INITIAL_VALUE) as? FilterModel,
                data?.getParcelableExtra(FilterSelectionFragment.CHANGED_VALUE) as? FilterModel
            )
        }
    }

    private fun queryChanged(q: String) {
        children.forEach { it.search(q) }
    }

    private fun refreshAfterFilterChange(old: FilterModel?, new: FilterModel?) {
        children.forEach { it.refreshAfterFilterChange(old, new) }
    }

    companion object {
        private const val CODE = 1002
    }
}
