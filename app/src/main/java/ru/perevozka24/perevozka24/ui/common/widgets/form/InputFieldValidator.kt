package ru.perevozka24.perevozka24.ui.common.widgets.form

interface InputFieldValidator<T> {

    /**
     * Validates input value and returns error if value is not valid
     */
    fun validate(input: T): String?
}
