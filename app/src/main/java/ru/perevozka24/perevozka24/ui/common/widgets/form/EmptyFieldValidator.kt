package ru.perevozka24.perevozka24.ui.common.widgets.form

class EmptyFieldValidator : InputFieldValidator<String> {

    private val emptyFieldError = "Поле должно быть заполнено"

    override fun validate(input: String): String? {
        if (input.isEmpty()) {
            return emptyFieldError
        }
        return null
    }
}
