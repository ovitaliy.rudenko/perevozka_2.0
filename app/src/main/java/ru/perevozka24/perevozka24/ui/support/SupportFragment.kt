package ru.perevozka24.perevozka24.ui.support

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.GroupieViewHolder
import kotlinx.android.synthetic.main.fragment_list.*
import ru.perevozka24.perevozka24.R
import ru.perevozka24.perevozka24.ui.base.BaseFragment
import ru.perevozka24.perevozka24.ui.common.ext.call
import ru.perevozka24.perevozka24.ui.common.ext.email
import ru.perevozka24.perevozka24.ui.common.ext.openLink
import ru.perevozka24.perevozka24.ui.support.items.SupportItemBuilder
import ru.perevozka24.perevozka24.utils.trackEvent

class SupportFragment : BaseFragment() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        trackEvent("visited_support")
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(R.layout.fragment_list, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        list.adapter = GroupAdapter<GroupieViewHolder>().apply {
            addAll(SupportItemBuilder.build(call, email, site))
        }
    }

    private val call: SupportItemAction = object : SupportItemAction {
        override fun action(resId: Int) {
            context?.call(getString(resId))
        }
    }

    private val email: SupportItemAction = object : SupportItemAction {
        override fun action(resId: Int) {
            context?.email(getString(resId))
        }
    }

    private val site: SupportItemAction = object : SupportItemAction {
        override fun action(resId: Int) {
            context?.openLink(getString(resId))
        }
    }
}
