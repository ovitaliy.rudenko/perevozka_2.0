package ru.perevozka24.perevozka24.ui.distance.input

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.liveData
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import ru.perevozka24.perevozka24.R
import ru.perevozka24.perevozka24.data.ResourceProvider
import ru.perevozka24.perevozka24.data.Result
import ru.perevozka24.perevozka24.data.features.location.CalculateLocationParams
import ru.perevozka24.perevozka24.data.features.location.LocationRepository
import ru.perevozka24.perevozka24.ui.LiveEvent
import ru.perevozka24.perevozka24.ui.base.BaseViewModel
import ru.perevozka24.perevozka24.ui.common.livedata.postValue
import ru.perevozka24.perevozka24.ui.distance.result.DistanceCalculationResult
import ru.perevozka24.perevozka24.ui.filters.models.Direction
import ru.perevozka24.perevozka24.ui.filters.models.FilterDistanceCalculatorModel
import ru.perevozka24.perevozka24.ui.filters.models.PickerModel
import ru.perevozka24.perevozka24.ui.filters.models.RegionCityModel

class DistanceInputViewModel(
    private val locationRepository: LocationRepository,
    resourceProvider: ResourceProvider
) : BaseViewModel(resourceProvider) {

    val selection: LiveData<FilterDistanceCalculatorModel> = liveData {
        emit(locationRepository.getSelection())
    }
    val showCalculation = LiveEvent<DistanceCalculationResult>()

    val inputError: LiveData<String?> = MutableLiveData<String>()

    var consumption: Int? = resourceProvider.getString(R.string.calculate_distance_hint)
        .toIntOrNull()
    var price: Int? = resourceProvider.getString(R.string.calculate_distance_fuel_price_hint)
        .toIntOrNull()

    val calculation: LiveData<Result<Unit>> = MutableLiveData()

    fun pickerValueChanged(value: PickerModel) {
        val updated = checkNotNull(selection.value).updated(value) as FilterDistanceCalculatorModel
        selection.postValue(updated)
        inputError.postValue(null)
    }

    fun consumptionChange(value: Int?) {
        consumption = value
        inputError.postValue(null)
    }

    fun priceChange(value: Int?) {
        price = value
        inputError.postValue(null)
    }

    fun calculate() {
        val errors = mutableListOf<Int>()

        val value = checkNotNull(selection.value)
        if (value.city.id == null) {
            errors.add(R.string.calculate_distance_error_missed_from)
        }
        if (value.cityTo.id == null) {
            errors.add(R.string.calculate_distance_error_missed_to)
        }

        if (errors.isEmpty()) {
            doWork(calculation) {
                val params = CalculateLocationParams(value.getFromParams(), value.getToParams())
                val route = locationRepository.getRoute(params).first()
                locationRepository.setSelection(checkNotNull(selection.value))
                withContext(Dispatchers.Main) {
                    showCalculation.postValue(
                        DistanceCalculationResult.map(
                            checkNotNull(value.country),
                            RegionCityModel(
                                Direction.FROM,
                                checkNotNull(value.region),
                                checkNotNull(value.city)
                            ),
                            checkNotNull(value.countryTo),
                            RegionCityModel(
                                Direction.TO,
                                checkNotNull(value.regionTo),
                                checkNotNull(value.cityTo)
                            ),
                            consumption?.toDouble(),
                            price?.toDouble(),
                            route
                        )
                    )
                }
            }
        } else {
            error.postValue(errors.joinToString("\n") { resourceProvider.getString(it) })
        }
    }
}
