package ru.perevozka24.perevozka24.ui.main.toolbar

import android.annotation.SuppressLint
import android.content.Context
import androidx.annotation.StringRes
import androidx.core.content.res.ResourcesCompat
import kotlinx.android.synthetic.main.toolbar_main_search.view.*
import ru.perevozka24.perevozka24.R

@SuppressLint("ViewConstructor")
class ToolbarSearchView(context: Context, @StringRes hint: Int) : ToolbarView(context) {
    init {
        setBackgroundColor(ResourcesCompat.getColor(resources, R.color.yellow, null))
        inflate(context, R.layout.toolbar_main_search, this)
        mainTopBarSearchFiled.setHint(hint)
    }

    override fun setupOnDrawerClick(callback: () -> Unit) {
        openDrawerView.setOnClickListener { callback() }
    }
}
