package ru.perevozka24.perevozka24.ui.auth.register.step1

import android.text.InputType
import androidx.navigation.fragment.findNavController
import ru.perevozka24.perevozka24.R
import ru.perevozka24.perevozka24.ui.viewModel

class RegisterEmailFragment : BaseRegisterContactFragment<RegisterEmailViewModel>() {
    override val viewModel: RegisterEmailViewModel by viewModel()

    override fun getContactViewHint(): Int = R.string.auth_hint_email

    override fun getContactViewTitle(): Int = R.string.auth_input_title_email

    override fun getOtherWayButtonTitle(): Int = R.string.auth_using_phone

    override fun getOtherWayClickAction() {
        findNavController().navigate(R.id.action_registerEmailFragment_to_registerPhoneFragment)
    }

    override fun getProceedClickAction(contact: String) {
        findNavController().navigate(
            RegisterEmailFragmentDirections.actionRegisterEmailFragmentToEmailRegisterFragment(
                contact
            )
        )
    }

    override fun getInputType(): Int =
        InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS
}
