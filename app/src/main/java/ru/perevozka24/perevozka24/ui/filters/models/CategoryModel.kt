package ru.perevozka24.perevozka24.ui.filters.models

import androidx.annotation.DrawableRes
import kotlinx.android.parcel.Parcelize

@Parcelize
data class CategoryModel(
    val id: String?,
    val name: String?,
    val sale: Int,
    val shipment: Int,
    @DrawableRes val icon: Int
) : PickerModel {
    companion object {
        fun default(id: String? = null) = CategoryModel(
            id = id,
            name = null,
            shipment = 0,
            sale = 0,
            icon = 0
        )
    }
}
