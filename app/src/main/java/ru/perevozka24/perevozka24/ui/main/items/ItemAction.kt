package ru.perevozka24.perevozka24.ui.main.items

import android.view.View
import ru.perevozka24.perevozka24.ui.main.items.models.ItemModel

interface ItemAction {
    fun addToFavorite(itemModel: ItemModel, view: View)
    fun showMore(itemModel: ItemModel, view: View)
    fun openDetailed(itemModel: ItemModel, view: View)
    fun openAbuser(itemModel: ItemModel, view: View)
    fun addToBlackList(itemModel: ItemModel, view: View)
}
