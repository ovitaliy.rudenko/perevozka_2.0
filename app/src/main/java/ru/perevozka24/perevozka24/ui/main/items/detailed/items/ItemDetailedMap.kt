package ru.perevozka24.perevozka24.ui.main.items.detailed.items

import com.xwray.groupie.kotlinandroidextensions.GroupieViewHolder
import com.xwray.groupie.kotlinandroidextensions.Item
import com.yandex.mapkit.geometry.Point
import com.yandex.mapkit.map.CameraPosition
import com.yandex.runtime.image.ImageProvider
import kotlinx.android.synthetic.main.item_detailed_map.view.*
import ru.perevozka24.perevozka24.R
import ru.perevozka24.perevozka24.data.features.location.LocationParams.Companion.ZOOM_CITY

data class ItemDetailedMap(
    private val latitude: Double,
    private val longitude: Double
) : Item() {
    override fun getId() = 0L

    override fun getLayout(): Int = R.layout.item_detailed_map

    override fun bind(viewHolder: GroupieViewHolder, position: Int) {
        with(viewHolder.itemView) {
            itemDetailedMap.onStart()
            val p = Point(latitude, longitude)
            with(itemDetailedMap.map) {
                move(CameraPosition(p, ZOOM_CITY, 0f, 0f))
                mapObjects.addPlacemark(
                    p,
                    ImageProvider.fromResource(context, R.drawable.ic_detailed_map_pin)
                )
                isRotateGesturesEnabled = false
                isZoomGesturesEnabled = false
                isScrollGesturesEnabled = false
                isTiltGesturesEnabled = false
            }
        }
    }

    override fun unbind(viewHolder: GroupieViewHolder) {
        super.unbind(viewHolder)
        with(viewHolder.itemView) {
            itemDetailedMap.onStop()
        }
    }
}
