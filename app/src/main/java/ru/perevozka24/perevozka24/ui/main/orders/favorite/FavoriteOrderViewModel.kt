package ru.perevozka24.perevozka24.ui.main.orders.favorite

import ru.perevozka24.perevozka24.data.ResourceProvider
import ru.perevozka24.perevozka24.data.features.offers.OffersRepository
import ru.perevozka24.perevozka24.ui.common.livedata.postValue
import ru.perevozka24.perevozka24.ui.main.orders.base.BaseOrdersViewModel

class FavoriteOrderViewModel(
    private val repository: OffersRepository,
    resourceProvider: ResourceProvider
) : BaseOrdersViewModel(repository, resourceProvider) {
    override val searchReportName: String
        get() = error("not used")

    init {
        load()
    }

    override fun refresh() = load()

    private fun load() {
        doWork(items) {
            val result = repository.getFavorites()
            itemsList.postValue(result)
            result
        }
    }
}
