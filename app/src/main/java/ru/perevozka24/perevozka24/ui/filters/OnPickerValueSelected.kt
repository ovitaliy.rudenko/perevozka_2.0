package ru.perevozka24.perevozka24.ui.filters

import ru.perevozka24.perevozka24.ui.filters.models.PickerModel

interface OnPickerValueSelected {
    fun onSelectPickerValue(value: PickerModel)
}
