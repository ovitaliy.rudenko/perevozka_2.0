package ru.perevozka24.perevozka24.ui.filters.models

import ru.perevozka24.perevozka24.data.features.filter.FilterPickerDataSource.Companion.CARGO_TYPE
import ru.perevozka24.perevozka24.data.features.filter.entities.FilterCargoEntity
import ru.perevozka24.perevozka24.data.features.filter.entities.FilterDistanceCalculatorEntity
import ru.perevozka24.perevozka24.data.features.filter.entities.FilterEntity
import ru.perevozka24.perevozka24.data.features.filter.entities.FilterItemsEquipmentEntity
import ru.perevozka24.perevozka24.data.features.filter.entities.FilterOfferEntity
import ru.perevozka24.perevozka24.ui.common.Mapper

object FilterModelMapper : Mapper<FilterEntity, FilterModel> {
    override fun map(e: FilterEntity): FilterModel =
        when (e) {
            is FilterItemsEquipmentEntity -> FilterItemsEquipmentMapper.map(e)
            is FilterCargoEntity -> FilterCargoMapper.map(e)
            is FilterOfferEntity -> FilterOfferMapper.map(e)
            is FilterDistanceCalculatorEntity -> FilterDistanceCalculatorMapper.map(e)
            else -> error("unsupported type")
        }

    private object FilterItemsEquipmentMapper :
        Mapper<FilterItemsEquipmentEntity, FilterItemsEquipmentModel> {
        override fun map(e: FilterItemsEquipmentEntity) =
            FilterItemsEquipmentModel(
                category = e.category?.let { CategoryMapper.map(it) } ?: CategoryModel.default(),
                subCategory = e.subCategory?.let { SubCategoryMapper.map(it) }
                    ?: SubCategoryModel.default(),
                country = e.country?.let { CountryMapper.map(it) } ?: CountryModel.default(),
                region = e.region?.let { RegionMapper.map(it).copy(countryId = e.country?.code) }
                    ?: RegionModel.default(countryId = e.country?.code),
                city = (e.city?.let { CityMapper.map(it) } ?: CityModel.default()).copy(
                    countryId = e.country?.code,
                    regionId = e.region?.id
                ),
            )
    }

    private object FilterCargoMapper : Mapper<FilterCargoEntity, FilterCargoModel> {
        override fun map(e: FilterCargoEntity) =
            FilterCargoModel(
                category = CategoryModel.default(id = CARGO_TYPE),
                subCategory = e.subCategory?.takeIf { !it.categoryId.isNullOrBlank() }
                    ?.let { SubCategoryMapper.map(it) }
                    ?: SubCategoryModel.default(categoryId = CARGO_TYPE),
                country = e.country?.let { CountryMapper.map(it).copy(direction = Direction.FROM) }
                    ?: CountryModel.default(direction = Direction.FROM),
                region = e.region?.let {
                    RegionMapper.map(it)
                        .copy(direction = Direction.FROM)
                }
                    ?: RegionModel.default(
                        direction = Direction.FROM,
                        countryId = e.country?.code
                    ),
                countryTo = e.countryTo?.let {
                    CountryMapper.map(it).copy(direction = Direction.TO)
                }
                    ?: CountryModel.default(direction = Direction.TO),
                regionTo = e.regionTo?.let {
                    RegionMapper.map(it)
                        .copy(direction = Direction.TO, countryId = e.countryTo?.code)
                } ?: RegionModel.default(direction = Direction.TO, countryId = e.countryTo?.code),
                cargoBody = e.cargoBody?.let { CargoBodyModel.Mapper.map(it) }
                    ?: CargoBodyModel.default(),
                status = e.status?.let { OfferStatusModel.Mapper.map(it) }
            )
    }

    private object FilterOfferMapper : Mapper<FilterOfferEntity, FilterOfferModel> {
        override fun map(e: FilterOfferEntity) =
            FilterOfferModel(
                category = e.category?.let { CategoryMapper.map(it) } ?: CategoryModel.default(),
                subCategory = e.subCategory?.let { SubCategoryMapper.map(it) }
                    ?: SubCategoryModel.default(),
                country = e.country?.let { CountryMapper.map(it) } ?: CountryModel.default(),
                region = e.region?.let { RegionMapper.map(it) }
                    ?: RegionModel.default(e.country?.code),
                city = (e.city?.let { CityMapper.map(it) } ?: CityModel.default()).copy(
                    countryId = e.country?.code,
                    regionId = e.region?.id,
                ),
                status = e.status?.let { OfferStatusModel.Mapper.map(it) }
            )
    }

    private object FilterDistanceCalculatorMapper :
        Mapper<FilterDistanceCalculatorEntity, FilterDistanceCalculatorModel> {
        override fun map(e: FilterDistanceCalculatorEntity) =
            FilterDistanceCalculatorModel(
                country = e.country?.let {
                    CountryMapper.map(it).copy(direction = Direction.FROM)
                } ?: CountryModel.default(Direction.FROM),
                region = e.region?.let {
                    RegionMapper.map(it).copy(direction = Direction.FROM)
                } ?: RegionModel.default(e.country?.code, direction = Direction.FROM),
                city = (e.city?.let {
                    CityMapper.map(it).copy(direction = Direction.FROM)
                } ?: CityModel.default(direction = Direction.FROM)).copy(
                    countryId = e.country?.code,
                    regionId = e.region?.id,
                ),
                countryTo = e.countryTo?.let {
                    CountryMapper.map(it).copy(direction = Direction.TO)
                } ?: CountryModel.default(Direction.TO),
                regionTo = e.regionTo?.let {
                    RegionMapper.map(it).copy(direction = Direction.TO)
                } ?: RegionModel.default(e.countryTo?.code, direction = Direction.TO),
                cityTo = (e.cityTo?.let {
                    CityMapper.map(it).copy(direction = Direction.TO)
                } ?: CityModel.default(direction = Direction.TO)).copy(
                    countryId = e.country?.code,
                    regionId = e.region?.id,
                ),
            )
    }
}
