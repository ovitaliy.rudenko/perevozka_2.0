package ru.perevozka24.perevozka24.ui.filters.models

import ru.perevozka24.perevozka24.data.features.rate.RateTypeEntity

data class RateTypeModel(
    val id: String,
    var name: String
) {
    object Mapper : ru.perevozka24.perevozka24.ui.common.Mapper<RateTypeEntity, RateTypeModel> {
        override fun map(e: RateTypeEntity): RateTypeModel {
            return RateTypeModel(
                id = e.id,
                name = e.name
            )
        }
    }
}
