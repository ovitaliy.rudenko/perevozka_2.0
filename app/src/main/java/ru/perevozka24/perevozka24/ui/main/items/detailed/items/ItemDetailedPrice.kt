package ru.perevozka24.perevozka24.ui.main.items.detailed.items

import com.xwray.groupie.kotlinandroidextensions.GroupieViewHolder
import com.xwray.groupie.kotlinandroidextensions.Item
import kotlinx.android.synthetic.main.item_detailed_price.view.*
import ru.perevozka24.perevozka24.R
import ru.perevozka24.perevozka24.ui.common.ext.setVisibleText

data class ItemDetailedPrice(
    private val cost: String
) : Item() {
    override fun getId(): Long = 0
    override fun getLayout(): Int = R.layout.item_detailed_price

    override fun bind(viewHolder: GroupieViewHolder, position: Int) {
        with(viewHolder.itemView) {
            val values = cost.split("/")
            itemDetailedPriceTitle.text = values[0]

            val value = values.takeIf { it.size >= 2 }?.get(1)
            itemDetailedPriceData.setVisibleText(value)
        }
    }
}
