package ru.perevozka24.perevozka24.ui.filters.filter

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.commit
import kotlinx.android.synthetic.main.activity_fragment_container.*
import ru.perevozka24.perevozka24.R
import ru.perevozka24.perevozka24.ui.base.BaseFragmentActivity
import ru.perevozka24.perevozka24.ui.viewModel

class FilterActivity : BaseFragmentActivity() {

    val viewModel: FilterSelectionViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val type = checkNotNull(intent.getSerializableExtra(TYPE)) as FilterType
        toolbarTitleView.setText(type.title)
        title = ""
        viewModel.init(type)
        supportFragmentManager.commit {
            replace(R.id.container, FilterSelectionFragment.create(type))
        }
    }

    companion object {
        private const val TYPE = "type"

        fun create(context: Context, type: FilterType): Intent {
            return Intent(context, FilterActivity::class.java).apply {
                putExtra(TYPE, type)
            }
        }
    }
}
