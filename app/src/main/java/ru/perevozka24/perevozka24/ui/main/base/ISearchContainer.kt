package ru.perevozka24.perevozka24.ui.main.base

interface ISearchContainer {
    fun subscribeSearch(value: String, queryChanged: (String) -> Unit)
    fun clearSearch()
}
