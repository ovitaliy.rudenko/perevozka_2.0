package ru.perevozka24.perevozka24.ui.main.items.detailed.items

import com.xwray.groupie.kotlinandroidextensions.GroupieViewHolder
import com.xwray.groupie.kotlinandroidextensions.Item
import kotlinx.android.synthetic.main.item_detailed_rating.view.*
import ru.perevozka24.perevozka24.R
import ru.perevozka24.perevozka24.ui.main.items.models.ItemModel
import ru.perevozka24.perevozka24.ui.common.ext.setVisibleText

data class ItemDetailedRatingItem(
    private val item: ItemModel
) : Item() {
    override fun getId(): Long = 0
    override fun getLayout(): Int = R.layout.item_detailed_rating

    override fun bind(viewHolder: GroupieViewHolder, position: Int) {
        with(viewHolder.itemView) {
            itemDetailedRating.rating = item.rating
            itemDetailedOnSite.setVisibleText(item.itemTime?.let {
                resources.getString(
                    R.string.item_detailed_on_site,
                    it
                )
            })
        }
    }
}
