package ru.perevozka24.perevozka24.ui.filters.filter.items

import com.xwray.groupie.kotlinandroidextensions.GroupieViewHolder
import kotlinx.android.synthetic.main.item_filter_location.view.*
import ru.perevozka24.perevozka24.R
import ru.perevozka24.perevozka24.ui.filters.filter.FilterItem
import ru.perevozka24.perevozka24.ui.filters.models.RegionCityModel

data class FilterRegionCityItem(
    private val regionCity: RegionCityModel,
    override val defaultTitle: Int,
    override val onClick: (Any?) -> Unit
) : FilterItem(regionCity, defaultTitle, onClick) {
    override fun getId(): Long = (
            regionCity.city?.id ?: regionCity.region.id)?.toLong()
        ?: 0L

    override fun getLayout(): Int = R.layout.item_filter_location

    override fun bind(viewHolder: GroupieViewHolder, position: Int) {
        super.bind(viewHolder, position)
        with(viewHolder.itemView) {
            locationIcon.setImageResource(R.drawable.ic_location)
            isEnabled = regionCity.region.countryId != null
            locationTitle.text =
                regionCity.label() ?: context.getString(R.string.filter_region_default)
        }
    }
}
