package ru.perevozka24.perevozka24.ui.add.photo

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import ru.perevozka24.perevozka24.data.ResourceProvider
import ru.perevozka24.perevozka24.ui.base.BaseViewModel
import ru.perevozka24.perevozka24.ui.common.livedata.postValue

class AddPhotoViewModel(resourceProvider: ResourceProvider) : BaseViewModel(resourceProvider) {

    val items: LiveData<LinkedHashSet<String>> = MutableLiveData(LinkedHashSet())

    fun addFile(file: String) {
        val list = checkNotNull(items.value)
        list.add(file)
        items.postValue(list)
    }

    fun removeFile(file: String) {
        val list = checkNotNull(items.value)
        list.remove(file)
        items.postValue(list)
    }
}
