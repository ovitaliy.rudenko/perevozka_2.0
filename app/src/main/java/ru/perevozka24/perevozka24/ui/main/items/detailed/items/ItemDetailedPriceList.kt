package ru.perevozka24.perevozka24.ui.main.items.detailed.items

import androidx.recyclerview.widget.LinearLayoutManager
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.kotlinandroidextensions.GroupieViewHolder
import com.xwray.groupie.kotlinandroidextensions.Item
import kotlinx.android.synthetic.main.item_detailed_price_list.view.*
import ru.perevozka24.perevozka24.R
import ru.perevozka24.perevozka24.ui.main.items.models.ItemModel

data class ItemDetailedPriceList(
    private val item: ItemModel
) : Item() {
    override fun getId(): Long = 0
    override fun getLayout(): Int = R.layout.item_detailed_price_list

    override fun bind(viewHolder: GroupieViewHolder, position: Int) {
        with(viewHolder.itemView) {
            val adapter = GroupAdapter<GroupieViewHolder>()
            itemDetailedPriceList.adapter = adapter
            itemDetailedPriceList.layoutManager =
                LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
            adapter.addAll(CostMapper.map(item.costs))
        }
    }

    private object CostMapper {
        fun map(costs: List<String>) = costs.map { ItemDetailedPrice(it) }
    }
}
