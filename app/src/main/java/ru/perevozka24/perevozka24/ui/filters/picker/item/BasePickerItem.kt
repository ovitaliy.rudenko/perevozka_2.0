package ru.perevozka24.perevozka24.ui.filters.picker.item

import android.view.View
import com.xwray.groupie.kotlinandroidextensions.GroupieViewHolder
import com.xwray.groupie.kotlinandroidextensions.Item

abstract class BasePickerItem(private val onClickListener: View.OnClickListener) : Item() {

    override fun createViewHolder(itemView: View): GroupieViewHolder {
        itemView.setOnClickListener(onClickListener)
        return super.createViewHolder(itemView)
    }
}
