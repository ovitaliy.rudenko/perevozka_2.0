package ru.perevozka24.perevozka24.ui.main.orders.detailed.dialogs

import ru.perevozka24.perevozka24.data.ResourceProvider
import ru.perevozka24.perevozka24.data.features.offers.OfferRepository
import ru.perevozka24.perevozka24.data.features.offers.OfferTakeParams

class OfferSetPriceViewModel(
    private val offerRepository: OfferRepository,
    resourceProvider: ResourceProvider
) : BaseOfferDialogActionViewModel(resourceProvider) {

    override suspend fun proceed() {
        val params = OfferTakeParams(offer.id, checkNotNull(inputField.value?.value))
        offerRepository.takeOffer(params)
    }
}
