package ru.perevozka24.perevozka24.ui.auth.models

import ru.perevozka24.perevozka24.data.features.user.UserEntity

data class LoginModel(
    val id: String,
    val email: String,
    val name: String
) {
    object Mapper : ru.perevozka24.perevozka24.ui.common.Mapper<UserEntity, LoginModel> {
        override fun map(e: UserEntity): LoginModel {
            return LoginModel(
                id = checkNotNull(e.id),
                email = checkNotNull(e.email),
                name = checkNotNull(e.name ?: e.email)
            )
        }
    }
}
