package ru.perevozka24.perevozka24.ui.main.orders.orders

import android.os.Bundle
import android.view.View
import ru.perevozka24.perevozka24.R
import ru.perevozka24.perevozka24.ui.activityViewModel
import ru.perevozka24.perevozka24.ui.add.AddOrderActivity
import ru.perevozka24.perevozka24.ui.filters.filter.FilterActivity
import ru.perevozka24.perevozka24.ui.filters.filter.FilterType
import ru.perevozka24.perevozka24.ui.filters.models.PickerModel
import ru.perevozka24.perevozka24.ui.main.orders.base.AddOrderActionListener
import ru.perevozka24.perevozka24.ui.main.orders.base.BaseOrderContentFragment
import ru.perevozka24.perevozka24.utils.trackEvent

class OrdersFragment : BaseOrderContentFragment<OrdersViewModel>(), AddOrderActionListener {
    override val viewModel: OrdersViewModel by activityViewModel()

    override fun createFilterIntent() = FilterActivity.create(requireContext(), FilterType.ORDER)
    override fun getAddOrderActionTitle() = R.string.order_create_new_offer
    override fun getAddOrderAction() = View.OnClickListener { v ->
        v.context.run {
            trackEvent(
                "create_order",
                mapOf("type" to "order-list")
            )
            startActivity(AddOrderActivity.create(this))
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        trackEvent("visited_order_list")
    }

    override fun openFilter() {
        super.openFilter()
        trackEvent(
            "filter_pressed",
            mapOf("type" to "order-list")
        )
    }

    override fun openQuickFilter(model: PickerModel) {
        super.openQuickFilter(model)
        trackEvent(
            "quick_filter_pressed",
            mapOf("type" to "order-list")
        )
    }
}
