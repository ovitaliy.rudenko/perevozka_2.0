package ru.perevozka24.perevozka24.ui.main.items.equipment

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import ru.perevozka24.perevozka24.data.ResourceProvider
import ru.perevozka24.perevozka24.ui.common.livedata.postValue
import ru.perevozka24.perevozka24.ui.main.ViewMode
import ru.perevozka24.perevozka24.ui.main.base.BaseContentViewModel
import ru.perevozka24.perevozka24.ui.main.items.models.ItemsList
import ru.perevozka24.perevozka24.utils.trackEvent
import java.util.Locale

class EquipmentViewModel(
    resourceProvider: ResourceProvider
) : BaseContentViewModel<ItemsList>(resourceProvider) {

    override val searchReportName: String = "main_screen_search"
    val mode: LiveData<ViewMode> = MutableLiveData(ViewMode.MAP)

    fun switchMode(newMode: ViewMode) {
        if (mode.value == newMode) return
        trackEvent(
            "view_switched",
            mapOf("type" to newMode.name.toLowerCase(Locale.ROOT))
        )
        this.mode.postValue(newMode)
    }

    override fun refresh() = Unit
}
