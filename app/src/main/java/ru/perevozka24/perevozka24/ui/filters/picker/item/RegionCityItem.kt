package ru.perevozka24.perevozka24.ui.filters.picker.item

import android.view.View
import androidx.core.view.updatePadding
import com.xwray.groupie.kotlinandroidextensions.GroupieViewHolder
import kotlinx.android.synthetic.main.item_filter_loader_region.view.*
import ru.perevozka24.perevozka24.R
import ru.perevozka24.perevozka24.ui.filters.models.RegionCityModel

data class RegionCityItem(val model: RegionCityModel, val onClick: View.OnClickListener) :
    BasePickerItem(onClick) {
    override fun getLayout(): Int = R.layout.item_filter_loader_region
    override fun getId(): Long = model.city?.id?.toLongOrNull()
        ?: model.region.id?.toLongOrNull()
        ?: 0

    override fun bind(viewHolder: GroupieViewHolder, position: Int) {
        with(viewHolder.itemView) {
            tag = model
            filterLoaderRegionText.text =
                model.label(true) ?: context.getString(R.string.filter_region_default)
            val padding =
                if (model.city == null) {
                    0
                } else {
                    resources.getDimensionPixelOffset(R.dimen.picker_row_side_padding)
                }
            filterLoaderRegionText.updatePadding(left = padding)
            setBackgroundResource(
                if (model.city == null) {
                    R.drawable.underlined_transparent
                } else {
                    R.drawable.underlined_fa
                }
            )
        }
    }
}
