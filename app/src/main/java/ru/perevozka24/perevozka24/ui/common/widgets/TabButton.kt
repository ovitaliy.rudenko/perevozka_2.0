package ru.perevozka24.perevozka24.ui.common.widgets

import android.content.Context
import android.util.AttributeSet
import android.widget.FrameLayout
import kotlinx.android.synthetic.main.merge_tab_button.view.*
import ru.perevozka24.perevozka24.R

class TabButton @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : FrameLayout(context, attrs, defStyleAttr) {

    init {
        val set = intArrayOf(
            R.attr.icon,
            android.R.attr.text,
        ).sorted().toIntArray()

        val a = context.obtainStyledAttributes(attrs, set)
        val icon = a.getDrawable(set.indexOf(R.attr.icon))
        val title = a.getString(set.indexOf(android.R.attr.text))
        a.recycle()

        inflate(context, R.layout.merge_tab_button, this)

        tabButtonIcon.setImageDrawable(icon)
        tabButtonText.text = title
    }
}
