package ru.perevozka24.perevozka24.ui.main.items

import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import ru.perevozka24.perevozka24.data.ResourceProvider
import ru.perevozka24.perevozka24.data.features.items.ItemsRepository
import ru.perevozka24.perevozka24.data.features.items.toModel
import ru.perevozka24.perevozka24.data.features.items.updateFavoriteValue
import ru.perevozka24.perevozka24.ui.common.livedata.postValue
import ru.perevozka24.perevozka24.ui.main.base.BaseContentViewModel
import ru.perevozka24.perevozka24.ui.main.items.models.ItemModel
import ru.perevozka24.perevozka24.ui.main.items.models.ItemsList

abstract class AddToFavoriteViewModel(
    private val repository: ItemsRepository,
    resourceProvider: ResourceProvider
) : BaseContentViewModel<ItemsList>(resourceProvider), AddToFavoriteDelegate,
    AddToBlacklistDelegate {
    override fun addToFavorite(item: ItemModel) {
        val itemId = item.id
        val favorite = item.favorite
        val oldList = itemsList.value ?: ItemsList()
        itemsList.postValue(oldList.updateFavoriteValue(itemId, !favorite))
        launchGlobalWork(items) {
            if (favorite) {
                repository.removeFavorite(itemId)
            } else {
                repository.addFavorite(itemId)
            }
        }
    }

    override fun addToBlacklist(item: ItemModel) {
        val userId = item.userId

        GlobalScope.launch {
            safeExecute {
                repository.addToBlackList(userId)
                val oldList = itemsList.value ?: ItemsList()
                itemsList.postValue(oldList.filterNot { userId == it.userId }.toModel())
            }.traceError()
        }
    }
}
