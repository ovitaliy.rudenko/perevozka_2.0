package ru.perevozka24.perevozka24.ui.add

import ru.perevozka24.perevozka24.ui.base.BaseFragment

abstract class BaseCreateOrderStepFragment : BaseFragment(), ICreateOrderStep {
    protected val addOrderViewModel: AddOrderViewModel by lazy {
        (activity as AddOrderActivity).viewModel
    }
}
