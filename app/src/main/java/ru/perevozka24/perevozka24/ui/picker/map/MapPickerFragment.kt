package ru.perevozka24.perevozka24.ui.picker.map

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import com.yandex.mapkit.Animation
import com.yandex.mapkit.MapKitFactory
import com.yandex.mapkit.geometry.Point
import com.yandex.mapkit.map.InputListener
import com.yandex.mapkit.map.Map
import com.yandex.mapkit.map.MapObject
import com.yandex.mapkit.map.VisibleRegionUtils
import com.yandex.runtime.image.ImageProvider
import kotlinx.android.synthetic.main.fragment_picker_map.*
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import org.kodein.di.Kodein
import org.kodein.di.android.x.kodein
import org.kodein.di.generic.instance
import ru.perevozka24.perevozka24.R
import ru.perevozka24.perevozka24.data.features.location.GeoLocationDataSource
import ru.perevozka24.perevozka24.ui.base.BaseFragment
import ru.perevozka24.perevozka24.ui.common.ext.requestLocationPermission
import ru.perevozka24.perevozka24.ui.common.widgets.SimpleTextWatcher
import ru.perevozka24.perevozka24.ui.picker.map.MapPickerActivity.Companion.ADDRESS
import ru.perevozka24.perevozka24.ui.viewModel
import kotlin.math.max

class MapPickerFragment : BaseFragment() {
    override val kodein: Kodein by kodein()
    private val viewModel: MapPickerViewModel by viewModel()
    private val locationDataSource by instance<GeoLocationDataSource>()

    private var pin: MapObject? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_picker_map, container, false)
    }

    private val clickMapListener = object : InputListener {
        override fun onMapTap(p0: Map, p1: Point) =
            viewModel.setLocation(p1, pickerMap.map.cameraPosition.zoom)

        override fun onMapLongTap(p0: Map, p1: Point) =
            viewModel.setLocation(p1, pickerMap.map.cameraPosition.zoom)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        pickerMapMyLocation.setOnClickListener {
            requestLocationPermission { requestLocation() }
        }

        bindPin()
        bindSelectButton()
        bindQuery()

        viewModel.suggestions.observe(viewLifecycleOwner, Observer {
            pickerMapAutoComplete.setAdapter(AddressAdapter(it))
        })

        pickerMap.map.addInputListener(clickMapListener)

        viewModel.updateQuery.observe(viewLifecycleOwner, Observer {
            setQueryText(it)
        })
    }

    private fun bindPin() {
        val mapObjects = pickerMap.map.mapObjects
        viewModel.cameraPosition.observe(viewLifecycleOwner, Observer { position ->
            pin?.let { p ->
                mapObjects.remove(p)
                pin = null
            }
            mapObjects.clear()
            position?.let {
                pickerMap.map.move(it, Animation(Animation.Type.SMOOTH, 1f)) {}
                pin = mapObjects.addPlacemark(
                    it.target,
                    ImageProvider.fromResource(context, R.drawable.ic_pick_address)
                ).apply {
                    addTapListener { _, _ -> false }
                }
            }
        })
    }

    private fun bindSelectButton() {
        viewModel.submitEnabled.observe(viewLifecycleOwner, Observer {
            pickerMapSelect.isEnabled = it
        })
        pickerMapSelect.setOnClickListener {
            val resultIntent = Intent().apply {
                putExtra(ADDRESS, viewModel.address.value)
            }
            activity?.setResult(Activity.RESULT_OK, resultIntent)
            activity?.finish()
        }
    }

    private fun bindQuery() {
        pickerMapAutoCompleteClear.setOnClickListener {
            viewModel.clearQuery()
        }
        pickerMapAutoComplete.addTextChangedListener(queryTextWatcher)
        pickerMapAutoComplete.setOnItemClickListener { parent, _, position, _ ->
            val item = (parent.adapter as AddressAdapter).getItem(position)
            viewModel.addressSelected(item)
        }
    }

    private fun requestLocation() {
        viewLifecycleOwner.lifecycleScope.launch {
            locationDataSource.getLocationFlow().collect {
                viewModel.setLocation(it, pickerMap.map.cameraPosition.zoom)
            }
        }
    }

    private val queryTextWatcher = object : SimpleTextWatcher() {
        override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
            val geometry = VisibleRegionUtils.toPolygon(pickerMap.map.visibleRegion)
            viewModel.queryChanged(geometry, charSequence.toString())
        }
    }

    private fun setQueryText(value: String) {
        pickerMapAutoComplete.removeTextChangedListener(queryTextWatcher)
        pickerMapAutoComplete.setText(value)
        pickerMapAutoComplete.setSelection(max(0, value.length))

        pickerMapAutoComplete.addTextChangedListener(queryTextWatcher)
    }

    override fun onStart() {
        super.onStart()
        pickerMap.onStart()
        MapKitFactory.getInstance().onStart()
    }

    override fun onStop() {
        super.onStop()
        pickerMap.onStop()
        MapKitFactory.getInstance().onStop()
    }
}
