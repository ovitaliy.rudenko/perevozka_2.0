package ru.perevozka24.perevozka24.ui.filters.filter

import com.xwray.groupie.Group
import ru.perevozka24.perevozka24.R
import ru.perevozka24.perevozka24.ui.common.items.DividerItem
import ru.perevozka24.perevozka24.ui.filters.filter.items.FilterCategoryItem
import ru.perevozka24.perevozka24.ui.filters.filter.items.FilterCityItem
import ru.perevozka24.perevozka24.ui.filters.filter.items.FilterCountryItem
import ru.perevozka24.perevozka24.ui.filters.filter.items.FilterGroupTitleItem
import ru.perevozka24.perevozka24.ui.filters.filter.items.FilterRegionItem
import ru.perevozka24.perevozka24.ui.filters.filter.items.FilterSubCategoryItem
import ru.perevozka24.perevozka24.ui.filters.filter.items.NamedValueItem
import ru.perevozka24.perevozka24.ui.filters.models.CargoBodyModel
import ru.perevozka24.perevozka24.ui.filters.models.FilterCargoModel
import ru.perevozka24.perevozka24.ui.filters.models.FilterDistanceCalculatorModel
import ru.perevozka24.perevozka24.ui.filters.models.FilterItemsEquipmentModel
import ru.perevozka24.perevozka24.ui.filters.models.FilterOfferModel
import ru.perevozka24.perevozka24.ui.filters.models.NotificationModel
import ru.perevozka24.perevozka24.ui.filters.models.OfferStatusModel

@SuppressWarnings("LongMethod")
object FilterGroupBuilder {
    fun create(
        filter: Any,
        onActivityPickerClick: (Any?) -> Unit,
        onDialogPickerClick: (type: Class<*>, value: Any?) -> Unit = { _, _ -> }
    ): List<Group> =
        when (filter) {
            is FilterItemsEquipmentModel -> equipmentGroups(filter, onActivityPickerClick)
            is FilterOfferModel -> offerGroups(filter, onActivityPickerClick, onDialogPickerClick)
            is FilterCargoModel -> cargoGroups(filter, onActivityPickerClick, onDialogPickerClick)
            is FilterDistanceCalculatorModel -> distanceGroups(filter, onActivityPickerClick)
            is NotificationModel -> notificationGroups(filter, onActivityPickerClick)
            else -> error("unsupported $filter")
        }.flatMap {
            listOf(it, DividerItem(R.color.grey_de))
        }

    private fun equipmentGroups(
        filter: FilterItemsEquipmentModel,
        onClick: (Any?) -> Unit
    ): List<Group> {
        val list = mutableListOf<Group>()

        list.add(FilterGroupTitleItem(R.string.filter_category))
        list.add(
            FilterCategoryItem(filter.category, R.string.filter_category_main_default, onClick)
        )
        if (filter.subCategory.categoryId != null) {
            list.add(
                FilterSubCategoryItem(filter.subCategory, R.string.filter_sub_main_default, onClick)
            )
        }

        list.add(FilterGroupTitleItem(R.string.filter_city))
        list.add(FilterCountryItem(filter.country, R.string.filter_country_main_default, onClick))

        if (filter.region.countryId != null) {
            list.add(FilterRegionItem(filter.region, R.string.filter_region_main_default, onClick))
        }
        if (filter.city.regionId != null) {
            list.add(FilterCityItem(filter.city, R.string.filter_city_main_default, onClick))
        }
        return list
    }

    private fun cargoGroups(
        filter: FilterCargoModel,
        onActivityPickerClick: (Any?) -> Unit,
        onDialogPickerClick: (type: Class<*>, value: Any?) -> Unit
    ): List<Group> {
        val list = mutableListOf<Group>()

        list.add(FilterGroupTitleItem(R.string.filter_sub_category))
        list.add(
            FilterSubCategoryItem(
                filter.subCategory,
                R.string.filter_category_cargo_default,
                onActivityPickerClick
            )
        )
        list.add(FilterGroupTitleItem(R.string.filter_group_cargo_from))
        list.add(
            FilterCountryItem(
                filter.country,
                R.string.filter_country_default,
                onActivityPickerClick
            )
        )
        if (filter.region.countryId != null) {
            list.add(
                FilterRegionItem(
                    filter.region,
                    R.string.filter_region_default,
                    onActivityPickerClick
                )
            )
        }
        list.add(FilterGroupTitleItem(R.string.filter_group_cargo_to))
        list.add(
            FilterCountryItem(
                filter.countryTo,
                R.string.filter_country_default,
                onActivityPickerClick
            )
        )
        if (filter.regionTo.countryId != null) {
            list.add(
                FilterRegionItem(
                    filter.regionTo,
                    R.string.filter_region_default,
                    onActivityPickerClick
                )
            )
        }
        list.add(FilterGroupTitleItem(R.string.filter_extra))
        list.add(
            NamedValueItem(
                R.string.filter_group_cargo_body_type,
                R.drawable.ic_cargo_following,
                filter.cargoBody,
                CargoBodyModel::class.java,
                onDialogPickerClick
            )
        )
        list.add(
            NamedValueItem(
                R.string.filter_group_status,
                R.drawable.ic_statuses,
                filter.status,
                OfferStatusModel::class.java,
                onDialogPickerClick,
                R.string.filter_status_any
            )
        )

        return list
    }

    private fun offerGroups(
        filter: FilterOfferModel,
        onClick: (Any?) -> Unit,
        onDialogPickerClick: (type: Class<*>, value: Any?) -> Unit
    ): List<Group> {
        val list = mutableListOf<Group>()

        list.add(FilterGroupTitleItem(R.string.filter_category))
        list.add(
            FilterCategoryItem(
                filter.category,
                R.string.filter_category_main_default,
                onClick
            )
        )
        if (filter.subCategory.categoryId != null) {
            list.add(
                FilterSubCategoryItem(filter.subCategory, R.string.filter_sub_main_default, onClick)
            )
        }
        list.add(FilterGroupTitleItem(R.string.filter_city))
        list.add(FilterCountryItem(filter.country, R.string.filter_country_main_default, onClick))
        if (filter.region.countryId != null) {
            list.add(FilterRegionItem(filter.region, R.string.filter_region_main_default, onClick))
        }
        if (filter.city.regionId != null) {
            list.add(FilterCityItem(filter.city, R.string.filter_city_main_default, onClick))
        }

        list.add(FilterGroupTitleItem(R.string.filter_extra))
        list.add(
            NamedValueItem(
                R.string.filter_group_status,
                R.drawable.ic_statuses,
                filter.status,
                OfferStatusModel::class.java,
                onDialogPickerClick,
                R.string.filter_status_any
            )
        )
        return list
    }

    private fun distanceGroups(
        filter: FilterDistanceCalculatorModel,
        onClick: (Any?) -> Unit
    ): List<Group> {
        val list = mutableListOf<Group>()
        list.add(FilterGroupTitleItem(R.string.calculate_distance_from))
        list.add(FilterCountryItem(filter.country, R.string.filter_country_info_default, onClick))
        if (filter.region.countryId != null) {
            list.add(FilterRegionItem(filter.region, R.string.filter_region_info_default, onClick))
        }
        if (filter.city.regionId != null) {
            list.add(FilterCityItem(filter.city, R.string.filter_city_info_default, onClick))
        }
        list.add(FilterGroupTitleItem(R.string.calculate_distance_cargo_to))
        list.add(FilterCountryItem(filter.countryTo, R.string.filter_country_info_default, onClick))
        if (filter.regionTo.countryId != null) {
            list.add(
                FilterRegionItem(
                    filter.regionTo,
                    R.string.filter_region_info_default,
                    onClick
                )
            )
        }
        if (filter.cityTo.regionId != null) {
            list.add(FilterCityItem(filter.cityTo, R.string.filter_city_info_default, onClick))
        }
        return list
    }

    private fun notificationGroups(
        filter: NotificationModel,
        onClick: (Any?) -> Unit
    ): List<Group> {
        val list = mutableListOf<Group>()
        list.add(FilterGroupTitleItem(R.string.filter_category))
        list.add(
            FilterCategoryItem(
                filter.category,
                R.string.filter_category_main_default,
                onClick
            )
        )
        if (filter.subCategory.categoryId != null) {
            list.add(
                FilterSubCategoryItem(
                    filter.subCategory,
                    R.string.filter_sub_main_default,
                    onClick
                )
            )
        }
        list.add(FilterGroupTitleItem(R.string.filter_city))
        list.add(FilterCountryItem(filter.country, R.string.filter_country_main_default, onClick))
        if (filter.region.countryId != null) {
            list.add(FilterRegionItem(filter.region, R.string.filter_region_main_default, onClick))
        }
        if (filter.city.regionId != null) {
            list.add(FilterCityItem(filter.city, R.string.filter_city_main_default, onClick))
        }
        return list
    }
}
