package ru.perevozka24.perevozka24.ui.main.items.detailed.items

import com.xwray.groupie.kotlinandroidextensions.GroupieViewHolder
import com.xwray.groupie.kotlinandroidextensions.Item
import kotlinx.android.synthetic.main.item_detailed_horizontal_info.view.*
import ru.perevozka24.perevozka24.R
import ru.perevozka24.perevozka24.ui.common.ext.setVisibleText

data class ItemDetailedHorizontalInfo(
    private val title: String?,
    private val data: String?
) : Item() {
    override fun getId(): Long = 0
    override fun getLayout(): Int = R.layout.item_detailed_horizontal_info

    override fun bind(viewHolder: GroupieViewHolder, position: Int) {
        with(viewHolder.itemView) {
            itemDetailedInfoTitle.setVisibleText(title)
            itemDetailedInfoData.setVisibleText(data)
        }
    }
}
