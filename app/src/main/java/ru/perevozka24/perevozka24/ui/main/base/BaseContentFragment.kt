package ru.perevozka24.perevozka24.ui.main.base

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import kotlinx.android.synthetic.main.include_filter_equipment.*
import ru.perevozka24.perevozka24.ui.base.BaseFragment
import ru.perevozka24.perevozka24.ui.common.ext.toast
import ru.perevozka24.perevozka24.ui.common.ext.visible
import ru.perevozka24.perevozka24.ui.filters.filter.FilterSelectionFragment.Companion.CHANGED_VALUE
import ru.perevozka24.perevozka24.ui.filters.filter.FilterSelectionFragment.Companion.INITIAL_VALUE
import ru.perevozka24.perevozka24.ui.filters.models.FilterModel

abstract class BaseContentFragment<VM : BaseContentViewModel<out ContentModel>> :
    BaseFragment() {
    protected abstract val viewModel: VM

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        (parentFragment as? ISearchContainer)?.subscribeSearch(viewModel.query) { query ->
            viewModel.search(query)
        }

        contentFilter.setOnClickListener { openFilter() }

        contentMapPin.visible = false

        viewModel.error.observe(viewLifecycleOwner, Observer {
            toast(it)
        })
    }

    open fun openFilter() {
        startActivityForResult(
            createFilterIntent(),
            CODE
        )
    }

    protected abstract fun createFilterIntent(): Intent

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == CODE && resultCode == Activity.RESULT_OK) {
            viewModel.clearSearch()
            (parentFragment as? ISearchContainer)?.clearSearch()
            viewModel.refreshAfterFilterChange(
                data?.getParcelableExtra(INITIAL_VALUE) as? FilterModel,
                data?.getParcelableExtra(CHANGED_VALUE) as? FilterModel
            )
            (parentFragment as? ISearchContainer)?.clearSearch()
        }
    }

    companion object {
        private const val CODE = 1002
        private const val CODE_PICKER = 1003
    }
}
