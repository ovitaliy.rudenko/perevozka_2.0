package ru.perevozka24.perevozka24.ui.support

interface SupportItemAction {
    fun action(resId: Int)
}
