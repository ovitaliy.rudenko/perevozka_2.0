package ru.perevozka24.perevozka24.ui.main.items.equipment

import androidx.lifecycle.liveData
import androidx.lifecycle.switchMap
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import ru.perevozka24.perevozka24.DEFAULT_REQUEST_DELAY
import ru.perevozka24.perevozka24.data.ResourceProvider
import ru.perevozka24.perevozka24.data.features.filter.FilterRepository
import ru.perevozka24.perevozka24.data.features.items.ItemsRepository
import ru.perevozka24.perevozka24.data.features.location.GeoLocationRepository
import ru.perevozka24.perevozka24.data.features.location.LocationParams
import ru.perevozka24.perevozka24.data.features.location.LocationParams.Companion.ZOOM_CITY
import ru.perevozka24.perevozka24.data.features.location.LocationRepository
import ru.perevozka24.perevozka24.ui.LiveEvent
import ru.perevozka24.perevozka24.ui.common.livedata.postValue
import ru.perevozka24.perevozka24.ui.filters.filter.FilterType
import ru.perevozka24.perevozka24.ui.filters.models.CategorySubCategoryModel
import ru.perevozka24.perevozka24.ui.filters.models.FilterItemsEquipmentModel
import ru.perevozka24.perevozka24.ui.main.base.BaseContentViewModel
import ru.perevozka24.perevozka24.ui.main.items.models.ItemModel
import ru.perevozka24.perevozka24.ui.main.items.models.ItemSimpleModel
import ru.perevozka24.perevozka24.ui.main.items.models.ItemSimplesList

class EquipmentMapViewModel(
    private val locationRepository: LocationRepository,
    private val filterRepository: FilterRepository,
    private val repository: ItemsRepository,
    private val geoLocationRepository: GeoLocationRepository,
    resourceProvider: ResourceProvider
) : BaseContentViewModel<ItemSimplesList>(resourceProvider) {

    override val searchReportName: String = "main_screen_search"
    val openItemDialog = LiveEvent<Triple<ItemModel, String?, String?>>()

    private var loadingJob: Job? = null

    init {
        load()
        viewModelScope.launch {
            safe {
                val point = geoLocationRepository.getLocationPoint()
                point?.let {
                    changeLocationEvent.postValue(Pair(it, ZOOM_CITY))
                }
            }
        }
    }

    val changeLocationEvent = filterChangedEvent.switchMap {
        liveData {
            val initial = (it.first as FilterItemsEquipmentModel).run {
                LocationParams(country.name, region.name, city.name)
            }
            val second = (it.second as FilterItemsEquipmentModel).run {
                LocationParams(country.name, region.name, city.name)
            }

            if (initial.buildSearch() != second.buildSearch() && second.buildSearch()
                    .isNotEmpty()
            ) {
                locationRepository.getLocation(second)?.let { point ->
                    emit(Pair(point, second.zoomValue()))
                }
            }
        }
    }

    override fun refresh() = load()

    fun load() {
        loadingJob?.cancel()
        loadingJob = doGlobalWork(items) {
            delay(DEFAULT_REQUEST_DELAY)
            if (query.isEmpty()) {
                repository.getSimpleItem().apply {
                    itemsList.postValue(this)
                }
            } else {
                repository.searchSimple(query).apply {
                    itemsList.postValue(this)
                }
            }
        }
    }

    private suspend fun loadSubCategory(): CategorySubCategoryModel {
        val filter =
            filterRepository.getFilterItems(FilterType.EQUIPMENT) as FilterItemsEquipmentModel
        return CategorySubCategoryModel.create(filter.category, filter.subCategory)
    }

    fun itemClicked(simple: ItemSimpleModel) {
        GlobalScope.launch {
            safeExecute {
                val subcategory = loadSubCategory()
                val item = repository.getItemByUri(simple.uri).first()
                openItemDialog.postValue(
                    Triple(
                        item,
                        subcategory.categoryId,
                        subcategory.subCategoryId
                    )
                )
            }.traceError()
        }
    }
}
