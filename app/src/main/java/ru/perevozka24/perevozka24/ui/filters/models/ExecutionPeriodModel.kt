package ru.perevozka24.perevozka24.ui.filters.models

data class ExecutionPeriodModel(
    val type: ExecutionPeriodType?,
    val date: String?,
    val date2: String?
)
