package ru.perevozka24.perevozka24.ui.main.orders

import ru.perevozka24.perevozka24.ui.main.orders.models.OfferModel

interface AddToFavoriteOrderDelegate {
    fun addToFavorite(model: OfferModel)
}
