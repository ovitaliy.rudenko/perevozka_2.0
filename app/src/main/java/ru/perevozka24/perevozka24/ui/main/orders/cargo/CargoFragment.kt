package ru.perevozka24.perevozka24.ui.main.orders.cargo

import android.os.Bundle
import android.view.View
import ru.perevozka24.perevozka24.R
import ru.perevozka24.perevozka24.data.features.filter.FilterPickerDataSource.Companion.CARGO_TYPE
import ru.perevozka24.perevozka24.ui.activityViewModel
import ru.perevozka24.perevozka24.ui.add.AddOrderActivity
import ru.perevozka24.perevozka24.ui.filters.filter.FilterActivity
import ru.perevozka24.perevozka24.ui.filters.filter.FilterType
import ru.perevozka24.perevozka24.ui.filters.models.PickerModel
import ru.perevozka24.perevozka24.ui.filters.picker.FilterPickerActivity
import ru.perevozka24.perevozka24.ui.main.base.FilterTitleFormatter
import ru.perevozka24.perevozka24.ui.main.orders.base.AddOrderActionListener
import ru.perevozka24.perevozka24.ui.main.orders.base.BaseOrderContentFragment
import ru.perevozka24.perevozka24.utils.trackEvent

class CargoFragment : BaseOrderContentFragment<CargoViewModel>(), AddOrderActionListener {
    override val viewModel: CargoViewModel by activityViewModel()

    override fun createFilterIntent() = FilterActivity.create(requireContext(), FilterType.CARGO)

    override fun createQuickFilterIntent(model: PickerModel) =
        FilterPickerActivity.create(
            requireContext(),
            model,
            R.string.filter_category_cargo_default,
            cargoOnly = true
        )

    override val filterTitleFormatter: FilterTitleFormatter
        get() = CargoFilterTitleFormatter

    override fun getAddOrderActionTitle() = R.string.order_create_new_cargo

    override fun getAddOrderAction() = View.OnClickListener { v ->
        v.context.run {
            trackEvent(
                "create_order",
                mapOf("type" to "cargo")
            )
            startActivity(AddOrderActivity.create(this, category = CARGO_TYPE))
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        trackEvent("visited_cargo")
    }

    override fun openFilter() {
        super.openFilter()
        trackEvent(
            "filter_pressed",
            mapOf("type" to "cargo")
        )
    }

    override fun openQuickFilter(model: PickerModel) {
        super.openQuickFilter(model)
        trackEvent(
            "quick_filter_pressed",
            mapOf("type" to "cargo")
        )
    }
}
