package ru.perevozka24.perevozka24.ui.leftMenu.items

import android.view.View
import androidx.annotation.DrawableRes
import androidx.annotation.StringRes
import com.google.android.material.button.MaterialButton
import com.xwray.groupie.kotlinandroidextensions.GroupieViewHolder
import com.xwray.groupie.kotlinandroidextensions.Item
import ru.perevozka24.perevozka24.R

data class MenuItem(
    @StringRes private val menuTitle: Int,
    @DrawableRes private val menuIcon: Int,
    private val clickListener: View.OnClickListener
) : Item() {
    override fun getId(): Long = menuTitle.toLong()
    override fun getLayout(): Int = R.layout.item_menu

    override fun bind(viewHolder: GroupieViewHolder, position: Int) {
        with(viewHolder.itemView as MaterialButton) {
            setOnClickListener(clickListener)
            text = resources.getString(menuTitle)
            icon = resources.getDrawable(menuIcon, null)
        }
    }
}
