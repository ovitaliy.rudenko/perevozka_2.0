package ru.perevozka24.perevozka24.ui.main.items.detailed.items

import com.xwray.groupie.kotlinandroidextensions.GroupieViewHolder
import com.xwray.groupie.kotlinandroidextensions.Item
import kotlinx.android.synthetic.main.item_detailed_contact.view.*
import ru.perevozka24.perevozka24.R

data class ItemDetailedContactItem(
    private val icon: Int,
    private val contact: String,
    private val callback: (() -> Unit)? = null
) : Item() {
    override fun getId() = 0L
    override fun getLayout(): Int = R.layout.item_detailed_contact

    override fun bind(viewHolder: GroupieViewHolder, position: Int) {
        with(viewHolder.itemView) {
            itemDetailedContactIcon.setImageResource(icon)
            itemDetailedContactTitle.text = contact

            if (callback == null) {
                setOnClickListener(null)
            } else {
                setOnClickListener { callback.invoke() }
            }
        }
    }
}
