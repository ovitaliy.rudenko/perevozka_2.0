package ru.perevozka24.perevozka24.ui.filters.picker.loader

import ru.perevozka24.perevozka24.data.features.filter.FilterPickerRepository
import ru.perevozka24.perevozka24.ui.filters.models.PickerModel
import ru.perevozka24.perevozka24.ui.filters.models.SubCategoryModel

class SubCategoryPickerLoader(
    private val current: SubCategoryModel,
    private val pickerRepository: FilterPickerRepository
) : PickerLoader<SubCategoryModel> {
    override suspend fun load(): List<SubCategoryModel> {
        val id = checkNotNull(current.categoryId)
        return pickerRepository.getSubCategories(current.shipment, id)
    }

    override fun <T : PickerModel> filter(list: List<T>, q: String): List<SubCategoryModel> {
        return list.filter {
            it as SubCategoryModel
            it.name?.contains(q, true) == true
        }.map { it as SubCategoryModel }
    }
}
