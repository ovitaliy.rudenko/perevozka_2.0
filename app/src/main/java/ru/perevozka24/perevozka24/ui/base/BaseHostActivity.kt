package ru.perevozka24.perevozka24.ui.base

import android.os.Bundle
import androidx.navigation.NavController
import androidx.navigation.findNavController
import kotlinx.android.synthetic.main.activity_host.*
import ru.perevozka24.perevozka24.R

abstract class BaseHostActivity : BaseActivity() {
    protected abstract val graph: Int

    val navController: NavController
        get() = findNavController(R.id.nav_host_fragment)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_host)
        navController.setGraph(graph)
        backButton.setOnClickListener { onBackPressed() }
        toolbarTitleView.text = navController.currentDestination?.label
        navController.addOnDestinationChangedListener { _, destination, _ ->
            toolbarTitleView.text = destination.label
        }
    }
}
