package ru.perevozka24.perevozka24.ui.main

enum class ViewMode {
    LIST, MAP
}
