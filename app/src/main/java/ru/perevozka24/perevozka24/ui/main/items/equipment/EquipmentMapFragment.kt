package ru.perevozka24.perevozka24.ui.main.items.equipment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import com.bumptech.glide.Glide
import com.yandex.mapkit.Animation
import com.yandex.mapkit.geometry.Point
import com.yandex.mapkit.map.CameraPosition
import com.yandex.mapkit.map.Cluster
import com.yandex.mapkit.map.ClusterListener
import com.yandex.mapkit.map.ClusterTapListener
import com.yandex.mapkit.map.ClusterizedPlacemarkCollection
import com.yandex.mapkit.map.IconStyle
import com.yandex.mapkit.map.MapObject
import com.yandex.mapkit.map.MapObjectTapListener
import com.yandex.mapkit.map.PlacemarkMapObject
import com.yandex.runtime.image.ImageProvider
import kotlinx.android.synthetic.main.fragment_main_equipment_map.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import org.kodein.di.Kodein
import org.kodein.di.android.x.closestKodein
import ru.perevozka24.perevozka24.App
import ru.perevozka24.perevozka24.R
import ru.perevozka24.perevozka24.ui.add.AddOrderActivity
import ru.perevozka24.perevozka24.ui.common.ext.openLink
import ru.perevozka24.perevozka24.ui.common.ext.safeSuspended
import ru.perevozka24.perevozka24.ui.distance.result.DistanceResultFragment
import ru.perevozka24.perevozka24.ui.filters.models.FilterModel
import ru.perevozka24.perevozka24.ui.main.base.BaseMapFragment
import ru.perevozka24.perevozka24.ui.main.base.ContentModel
import ru.perevozka24.perevozka24.ui.main.base.EquipmentButtonVieModel
import ru.perevozka24.perevozka24.ui.main.base.MapPinTransformation
import ru.perevozka24.perevozka24.ui.main.base.TextImageProvider
import ru.perevozka24.perevozka24.ui.main.items.models.ItemSimpleModel
import ru.perevozka24.perevozka24.ui.main.items.models.ItemSimplesList
import ru.perevozka24.perevozka24.ui.main.orders.detailed.item.OfferMapItem
import ru.perevozka24.perevozka24.ui.viewModel
import ru.perevozka24.perevozka24.utils.trackEvent

class EquipmentMapFragment : BaseMapFragment<EquipmentMapViewModel>(), MapObjectTapListener,
    ClusterListener, ISearchable {
    override val kodein: Kodein by closestKodein()

    override val viewModel: EquipmentMapViewModel
        get() = (parentFragment as EquipmentFragment).mapViewModel
    private val equipmentVieModel: EquipmentButtonVieModel by viewModel()

    private val clusterTapListener = ClusterTapListener {
        zoomPlacemarks(it.placemarks)
        true
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? =
        inflater.inflate(R.layout.fragment_main_equipment_map, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mainMapAddOrder.setOnClickListener { v ->
            v.context.run {
                trackEvent(
                    "create_order",
                    mapOf("type" to "main-screen")
                )
                startActivity(AddOrderActivity.create(this))
            }
        }

        equipmentVieModel.equipmentLink.observe(viewLifecycleOwner) { linkId ->
            mainMapAddEquipment.setOnClickListener {
                trackEvent(
                    "add_ad",
                    mapOf("type" to "main-screen")
                )
                mainMapAddEquipment.context.openLink(getString(linkId))
            }
        }

        viewModel.openItemDialog.observe(viewLifecycleOwner, Observer {
            EquipmentDialog.create(it.first, it.second, it.third)
                .show(parentFragmentManager, "equipment")
        })

        viewModel.changeLocationEvent.observe(viewLifecycleOwner) { centerMap(it.first, it.second) }
    }

    override fun addMapItems(
        content: ContentModel
    ): Collection<MapObject> {
        val clusterizedCollection = mapObjects.addClusterizedPlacemarkCollection(this)
        loadItems(clusterizedCollection, content)
        return listOf<MapObject>(clusterizedCollection)
    }

    override fun onMapObjectTap(mapObject: MapObject, point: Point): Boolean {
        viewModel.itemClicked(mapObject.userData as ItemSimpleModel)
        return true
    }

    override fun onClusterAdded(cluster: Cluster) {
        val context = App.instance
        val mapPinSize = context.resources.getDimensionPixelSize(R.dimen.map_pin_size)
        cluster.appearance.setIcon(
            TextImageProvider(
                context,
                cluster.size.toString(),
                mapPinSize
            )
        )
        cluster.addClusterTapListener(clusterTapListener)
    }

    private fun zoomPlacemarks(placemarks: List<PlacemarkMapObject>) {
        val points = placemarks.map { it.geometry }.toTypedArray()
        val (northEast, southWest) = OfferMapItem.bounds(points)
        val center = OfferMapItem.center(northEast, southWest)

        with(mapview.map) {
            val zoom = DistanceResultFragment.getBoundsZoomLevel(
                maxZoom,
                northEast,
                southWest,
                mapview.width(),
                mapview.height()
            )
                .minus(2f)
                .coerceAtLeast(OfferMapItem.MIN_ZOOM)
                .coerceAtMost(OfferMapItem.MAX_ZOOM)
            move(
                CameraPosition(center, zoom, 0f, 0f),
                Animation(Animation.Type.SMOOTH, 1f)
            ) {}
        }
    }

    private fun loadItems(collection: ClusterizedPlacemarkCollection, content: ContentModel) {
        val tapListener = this
        val context = context ?: return
        val mapPinSize = resources.getDimensionPixelSize(R.dimen.map_pin_size)
        GlobalScope.launch {
            safeSuspended {
                val pairs = (content as ItemSimplesList)
                    .filter { item -> item.coordX != null && item.coordY != null }
                    .mapNotNull { item ->
                        safeSuspended {
                            val bitmap = withContext(Dispatchers.IO) {
                                Glide.with(context).asBitmap()
                                    .skipMemoryCache(true)
                                    .load(item.categoryIcon)
                                    .error(R.drawable.ic_error_outline_black_24dp)
                                    .transform(MapPinTransformation())
                                    .submit(mapPinSize, mapPinSize).get()
                            }
                            Pair(item, bitmap)
                        }
                    }
                withContext(Dispatchers.Main) {
                    if (isVisible) {
                        pairs.forEach {
                            val item = it.first
                            val bitmap = it.second

                            val coordX = checkNotNull(item.coordX)
                            val coordY = checkNotNull(item.coordY)
                            val placemark = collection.addPlacemark(
                                Point(coordX, coordY),
                                ImageProvider.fromBitmap(bitmap),
                                IconStyle()
                            )

                            placemark.userData = item
                            placemark.addTapListener(tapListener)
                        }
                        collection.clusterPlacemarks(V1, V2)
                    }
                }
            }
        }
    }

    override fun search(q: String) {
        viewModel.search(q)
    }

    override fun refreshAfterFilterChange(old: FilterModel?, new: FilterModel?) {
        viewModel.refreshAfterFilterChange(old, new)
    }

    private companion object {
        const val V1 = 30.0
        const val V2 = 15
    }
}
