package ru.perevozka24.perevozka24.ui.filters.models

import kotlinx.android.parcel.Parcelize
import ru.perevozka24.perevozka24.data.features.location.LocationParams

@Parcelize
data class FilterDistanceCalculatorModel(
    val country: CountryModel,
    val region: RegionModel,
    val city: CityModel,
    val countryTo: CountryModel,
    val regionTo: RegionModel,
    val cityTo: CityModel
) : FilterModel {

    override fun updated(value: PickerModel): FilterModel {
        return when (value) {
            is CountryModel -> if (value.direction == Direction.FROM) {
                copy(
                    country = value,
                    region = RegionModel.default(countryId = value.id, direction = Direction.FROM),
                    city = CityModel.default(direction = Direction.FROM)
                )
            } else {
                copy(
                    countryTo = value,
                    regionTo = RegionModel.default(countryId = value.id, direction = Direction.TO),
                    cityTo = CityModel.default(direction = Direction.TO)
                )
            }
            is RegionModel -> if (value.direction == Direction.FROM) {
                copy(
                    region = value,
                    city = CityModel.default(
                        countryId = value.countryId,
                        regionId = value.id,
                        direction = Direction.FROM
                    )
                )
            } else {
                copy(
                    regionTo = value,
                    cityTo = CityModel.default(
                        countryId = value.countryId,
                        regionId = value.id,
                        direction = Direction.TO
                    )
                )
            }
            is CityModel -> if (value.direction == Direction.FROM) {
                copy(city = value.copy(direction = Direction.FROM))
            } else {
                copy(cityTo = value.copy(direction = Direction.TO))
            }
            else -> error("unknown type")
        }
    }

    fun getFromParams() = LocationParams(country.name, region.name, city.name)
    fun getToParams() = LocationParams(countryTo.name, regionTo.name, cityTo.name)

    companion object {
        fun default() = FilterDistanceCalculatorModel(
            country = CountryModel.default(Direction.FROM),
            region = RegionModel.default(null, Direction.FROM),
            city = CityModel.default(direction = Direction.FROM),
            countryTo = CountryModel.default(Direction.TO),
            regionTo = RegionModel.default(null, Direction.TO),
            cityTo = CityModel.default(direction = Direction.TO),
        )
    }
}
