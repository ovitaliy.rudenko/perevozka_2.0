package ru.perevozka24.perevozka24.ui.main.items.base

import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.GroupieViewHolder
import kotlinx.android.synthetic.main.content_list.*
import org.kodein.di.Kodein
import org.kodein.di.android.x.kodein
import ru.perevozka24.perevozka24.R
import ru.perevozka24.perevozka24.ui.base.BaseContentLoaderFragment
import ru.perevozka24.perevozka24.ui.common.ext.toast
import ru.perevozka24.perevozka24.ui.common.ext.updateOrReplace
import ru.perevozka24.perevozka24.ui.main.items.AddToBlacklistDelegate
import ru.perevozka24.perevozka24.ui.main.items.AddToFavoriteDelegate
import ru.perevozka24.perevozka24.ui.main.items.AddToFavoriteViewModel
import ru.perevozka24.perevozka24.ui.main.items.ItemAction
import ru.perevozka24.perevozka24.ui.main.items.ItemActionImpl
import ru.perevozka24.perevozka24.ui.main.items.items.EquipmentMapper
import ru.perevozka24.perevozka24.ui.main.items.models.ItemModel

abstract class BaseItemsFragment<T : AddToFavoriteViewModel> : BaseContentLoaderFragment(),
    AddToFavoriteDelegate,
    AddToBlacklistDelegate {
    override val contentLayoutId: Int = R.layout.content_list

    override val kodein: Kodein by kodein()
    abstract val viewModel: T

    private val adapter = GroupAdapter<GroupieViewHolder>()
    protected open val actions: ItemAction = ItemActionImpl(this, this, this)
    protected open val hasExtraActions = true

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        subscribe(viewModel.items)
        viewModel.itemsList.observe(viewLifecycleOwner, Observer { items ->
            adapter.updateOrReplace(EquipmentMapper.map(items, actions, hasExtraActions))
        })
        contentList.adapter = adapter
        viewModel.error.observe(viewLifecycleOwner, Observer {
            toast(it)
        })
    }

    override fun addToFavorite(item: ItemModel) = viewModel.addToFavorite(item)

    override fun addToBlacklist(item: ItemModel) = viewModel.addToBlacklist(item)
}
