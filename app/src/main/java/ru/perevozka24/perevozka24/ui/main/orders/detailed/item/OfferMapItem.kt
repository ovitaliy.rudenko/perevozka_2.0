package ru.perevozka24.perevozka24.ui.main.orders.detailed.item

import android.os.Handler
import com.xwray.groupie.kotlinandroidextensions.GroupieViewHolder
import com.xwray.groupie.kotlinandroidextensions.Item
import com.yandex.mapkit.Animation
import com.yandex.mapkit.geometry.Point
import com.yandex.mapkit.geometry.Polyline
import com.yandex.mapkit.map.CameraPosition
import com.yandex.runtime.image.ImageProvider
import kotlinx.android.synthetic.main.item_offer_detailed_map.view.*
import ru.perevozka24.perevozka24.R
import ru.perevozka24.perevozka24.ui.distance.result.DistanceResultFragment
import ru.perevozka24.perevozka24.ui.main.orders.detailed.RouteData

data class OfferMapItem(
    private val route: RouteData
) : Item() {
    override fun getId() = 0L

    override fun getLayout(): Int = R.layout.item_offer_detailed_map

    private val itemHandler = Handler()

    override fun bind(viewHolder: GroupieViewHolder, position: Int) {
        val start: Point = route.start
        val end: Point = route.end
        val route: Polyline = route.polyline
        with(viewHolder.itemView) {
            itemHandler.postDelayed({
                if (parent == null) return@postDelayed
                itemOfferMap.onStart()
                with(itemOfferMap.map) {
                    mapObjects.addPlacemark(
                        start,
                        ImageProvider.fromResource(context, R.drawable.ic_detailed_map_pin)
                    )
                    mapObjects.addPlacemark(
                        end,
                        ImageProvider.fromResource(context, R.drawable.ic_distance_start)
                    )
                    mapObjects.addPolyline(route)

                    val (northEast, southWest) = bounds(arrayOf(start, end))

                    val center = center(northEast, southWest)

                    itemHandler.postDelayed({
                        parent?.let {
                            val zoom = DistanceResultFragment.getBoundsZoomLevel(
                                maxZoom,
                                northEast,
                                southWest,
                                width,
                                height
                            ).minus(2f).coerceAtLeast(MIN_ZOOM).coerceAtMost(MAX_ZOOM)
                            move(
                                CameraPosition(center, zoom, 0f, 0f),
                                Animation(Animation.Type.LINEAR, 0f)
                            ) {}
                        }
                    }, DELAY)

                    isRotateGesturesEnabled = false
                    isZoomGesturesEnabled = false
                    isScrollGesturesEnabled = false
                    isTiltGesturesEnabled = false
                }
            }, INIT_MAP_DELAY)
        }
    }

    override fun unbind(viewHolder: GroupieViewHolder) {
        super.unbind(viewHolder)
        itemHandler.removeCallbacksAndMessages(null)
        with(viewHolder.itemView) {
            itemOfferMap.onStop()
        }
    }

    companion object {
        const val MAX_ZOOM = 16f
        const val MIN_ZOOM = 2f

        private const val DELAY = 100L
        private const val INIT_MAP_DELAY = 100L

        fun bounds(points: Array<Point>): Pair<Point, Point> {
            val north = points.map { it.latitude }.maxOrNull() ?: error("no north max")
            val east = points.map { it.longitude }.maxOrNull() ?: error("no east max")
            val south = points.map { it.latitude }.minOrNull() ?: error("no north max")
            val west = points.map { it.longitude }.minOrNull() ?: error("no north max")
            val northEast = Point(north, east)
            val southWest = Point(south, west)
            return Pair(northEast, southWest)
        }

        fun center(northEast: Point, southWest: Point) = Point(
            (southWest.latitude + northEast.latitude) / 2,
            (southWest.longitude + northEast.longitude) / 2
        )
    }
}
