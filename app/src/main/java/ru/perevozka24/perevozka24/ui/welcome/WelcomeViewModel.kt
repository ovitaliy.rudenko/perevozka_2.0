package ru.perevozka24.perevozka24.ui.welcome

import androidx.lifecycle.ViewModel
import ru.perevozka24.perevozka24.data.features.auth.AuthRepository

class WelcomeViewModel(private val authRepository: AuthRepository) : ViewModel() {
    fun onBoardingShown() {
        authRepository.setShowOnboarding()
    }
}
