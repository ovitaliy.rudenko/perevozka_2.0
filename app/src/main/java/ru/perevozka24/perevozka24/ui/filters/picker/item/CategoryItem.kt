package ru.perevozka24.perevozka24.ui.filters.picker.item

import android.view.View
import com.xwray.groupie.kotlinandroidextensions.GroupieViewHolder
import kotlinx.android.synthetic.main.item_filter_loader_category.view.*
import ru.perevozka24.perevozka24.R
import ru.perevozka24.perevozka24.ui.filters.models.CategoryModel

data class CategoryItem(val model: CategoryModel, val onClick: View.OnClickListener) :
    BasePickerItem(onClick) {
    override fun getLayout(): Int = R.layout.item_filter_loader_category
    override fun getId(): Long = model.hashCode().toLong()

    override fun bind(viewHolder: GroupieViewHolder, position: Int) {
        with(viewHolder.itemView) {
            tag = model
            filterLoaderCategoryText.text = model.name
                ?: resources.getString(R.string.filter_category_default)
        }
    }
}
