package ru.perevozka24.perevozka24.ui.filters.models

import androidx.core.text.HtmlCompat
import kotlinx.android.parcel.Parcelize

@Parcelize
data class RegionCityModel(
    val direction: Direction,
    val region: RegionModel,
    val city: CityModel?
) : PickerModel {

    fun label(userBold: Boolean = false): CharSequence? {
        val region = region.name
        val city = city?.name
        return when {
            city != null && region != null -> HtmlCompat.fromHtml(
                when (userBold) {
                    true -> "<b>$region</b> - $city"
                    false -> "$region - $city"
                },
                HtmlCompat.FROM_HTML_MODE_LEGACY
            )
            region != null -> region
            else -> null
        }
    }

    companion object {
        fun default(countryId: String?, direction: Direction = Direction.NONE) =
            RegionCityModel(
                direction = direction,
                region = RegionModel.default(countryId, direction),
                city = null
            )
    }
}
