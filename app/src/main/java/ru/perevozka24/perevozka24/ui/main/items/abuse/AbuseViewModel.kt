package ru.perevozka24.perevozka24.ui.main.items.abuse

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import ru.perevozka24.perevozka24.data.ResourceProvider
import ru.perevozka24.perevozka24.data.Result
import ru.perevozka24.perevozka24.data.features.items.ItemAbuseParams
import ru.perevozka24.perevozka24.data.features.items.ItemsRepository
import ru.perevozka24.perevozka24.ui.base.BaseViewModel
import ru.perevozka24.perevozka24.ui.common.livedata.postValue
import ru.perevozka24.perevozka24.ui.common.widgets.form.EmptyFieldValidator
import ru.perevozka24.perevozka24.ui.common.widgets.form.InputField
import ru.perevozka24.perevozka24.utils.trackEvent

class AbuseViewModel(
    private val repository: ItemsRepository,
    resourceProvider: ResourceProvider
) : BaseViewModel(resourceProvider) {

    lateinit var itemId: String

    val messageFieldError: LiveData<String> = MutableLiveData()
    private val messageField =
        MutableLiveData(
            InputField(
                "",
                EmptyFieldValidator(),
                { messageFieldError.postValue(it) }
            )
        )

    val phoneFieldError: LiveData<String> = MutableLiveData()
    private val phoneField =
        MutableLiveData(
            InputField(
                "",
                EmptyFieldValidator(),
                { phoneFieldError.postValue(it) }
            )
        )

    val executing: LiveData<Result<Unit>> = MutableLiveData()

    fun messageChange(message: String) {
        messageField.value?.value = message
    }

    fun phoneChange(password: String) {
        phoneField.value?.value = password
    }

    fun abuse() {
        val fields = arrayOf(
            messageField,
            phoneField
        )

        val isValid = fields.map {
            it.value?.validate()
            (it.value?.hasError ?: true).not()
        }
            .all { it }

        if (isValid) {
            doWork(executing) {
                val params = ItemAbuseParams(
                    itemId = itemId,
                    contact = checkNotNull(phoneField.value?.value),
                    text = checkNotNull(messageField.value?.value)
                )
                repository.abuse(params)
                trackEvent("complaint_sended")
            }
        }
    }
}
