package ru.perevozka24.perevozka24.ui.common.items

import com.xwray.groupie.kotlinandroidextensions.GroupieViewHolder
import com.xwray.groupie.kotlinandroidextensions.Item
import kotlinx.android.synthetic.main.item_error.view.*
import ru.perevozka24.perevozka24.R

data class ErrorItem(private val message: String) : Item() {
    override fun getId(): Long = 0
    override fun bind(viewHolder: GroupieViewHolder, position: Int) {
        with(viewHolder.itemView) {
            errorMessageText.text = message
        }
    }

    override fun getLayout(): Int = R.layout.item_error
}
