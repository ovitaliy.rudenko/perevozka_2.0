package ru.perevozka24.perevozka24.ui.main.items.my

import android.os.Bundle
import android.view.View
import androidx.navigation.fragment.findNavController
import org.kodein.di.Kodein
import org.kodein.di.android.x.kodein
import ru.perevozka24.perevozka24.ui.main.items.ItemAction
import ru.perevozka24.perevozka24.ui.main.items.base.BaseItemsFragment
import ru.perevozka24.perevozka24.ui.main.items.detailed.ItemDetailedActivityDirections
import ru.perevozka24.perevozka24.ui.main.items.models.ItemModel
import ru.perevozka24.perevozka24.ui.viewModel
import ru.perevozka24.perevozka24.utils.trackEvent

class MyItemsFragment : BaseItemsFragment<MyItemsViewModel>() {
    override val kodein: Kodein by kodein()
    override val viewModel: MyItemsViewModel by viewModel()

    private val defaultAction = super.actions
    override val actions: ItemAction = object : ItemAction by defaultAction {
        override fun openDetailed(itemModel: ItemModel, view: View) {
            findNavController().navigate(
                ItemDetailedActivityDirections.actionGlobalItemDetailedActivity(
                    itemModel, isMine = true
                )
            )
        }
    }
    override val hasExtraActions = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        trackEvent("visited_my_ads")
    }
}
