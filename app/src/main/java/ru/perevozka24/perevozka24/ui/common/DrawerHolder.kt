package ru.perevozka24.perevozka24.ui.common

interface DrawerHolder {
    fun openLeftDrawer()
    fun closeLeftDrawer()
}
