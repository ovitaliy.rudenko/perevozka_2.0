package ru.perevozka24.perevozka24.ui.leftMenu

import android.view.View
import ru.perevozka24.perevozka24.ui.main.MainScreenMode

interface LeftMenuAction {
    val loginClick: View.OnClickListener
    val logoutClick: View.OnClickListener
    val profileClick: View.OnClickListener
    val rulesClickLister: View.OnClickListener
    val redirectClick: (actionId: Int) -> View.OnClickListener
    val switchTabClick: (mode: MainScreenMode) -> View.OnClickListener
    val unimplementedClick: View.OnClickListener
}
