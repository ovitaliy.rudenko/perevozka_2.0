package ru.perevozka24.perevozka24.ui.main.orders.models

import ru.perevozka24.perevozka24.R
import ru.perevozka24.perevozka24.data.features.offers.entity.OfferEntity
import ru.perevozka24.perevozka24.data.features.offers.entity.OfferProgressStatusEntity
import ru.perevozka24.perevozka24.data.features.offers.entity.OfferStatusEntity

enum class OfferStatus {
    NONE {
        override val contactsVisible: Boolean
            get() = false
        override val contactsBlur: Boolean
            get() = false
        override val hint: Int? = null
    },
    CAN_TAKE {
        override val contactsVisible: Boolean
            get() = true
        override val contactsBlur: Boolean
            get() = true
        override val hint: Int? = R.string.offer_contacts_hint
    },
    ADD_EQUIPMENT {
        override val contactsVisible: Boolean
            get() = true
        override val contactsBlur: Boolean
            get() = true
        override val hint: Int? = R.string.offer_incorrect_equipment
    },
    CONFIRM {
        override val contactsVisible: Boolean
            get() = true
        override val contactsBlur: Boolean
            get() = false
        override val hint: Int? = null
        override val callVisible = true
    };

    abstract val contactsVisible: Boolean
    abstract val contactsBlur: Boolean
    abstract val hint: Int?
    open val callVisible = false

    companion object {
        const val REMAINING_TIME = "{remaining_time}"
        fun build(offer: OfferEntity, isMy: Boolean): OfferStatus =
            when {
                offer.offerStatus == OfferStatusEntity.NOAUTH
                        || offer.offerStatus == OfferStatusEntity.NOPAID -> ADD_EQUIPMENT
                offer.message?.contains(REMAINING_TIME) == true && isMy -> CONFIRM
                isMy && offer.status == OfferProgressStatusEntity.OPENED -> CONFIRM
                !isMy && offer.status == OfferProgressStatusEntity.OPENED -> CAN_TAKE
                else -> NONE
            }
    }
}
