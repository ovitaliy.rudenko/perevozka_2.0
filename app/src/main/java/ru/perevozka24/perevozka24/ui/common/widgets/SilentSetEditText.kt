package ru.perevozka24.perevozka24.ui.common.widgets

import android.content.Context
import android.text.TextWatcher
import android.util.AttributeSet
import androidx.appcompat.widget.AppCompatEditText

class SilentSetEditText @JvmOverloads constructor(context: Context, attrs: AttributeSet? = null) :
    AppCompatEditText(context, attrs) {

    private val textWatchers = mutableListOf<TextWatcher>()

    override fun addTextChangedListener(watcher: TextWatcher) {
        super.addTextChangedListener(watcher)
        textWatchers.add(watcher)
    }

    override fun removeTextChangedListener(watcher: TextWatcher) {
        super.removeTextChangedListener(watcher)
        textWatchers.remove(watcher)
    }

    fun setSilentText(text: String?) {
        if (hasFocus()) return
        textWatchers.forEach {
            super.removeTextChangedListener(it)
        }
        setText(text)
        textWatchers.forEach {
            super.addTextChangedListener(it)
        }
    }
}
