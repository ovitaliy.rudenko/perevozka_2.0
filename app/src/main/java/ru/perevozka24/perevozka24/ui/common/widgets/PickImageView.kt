package ru.perevozka24.perevozka24.ui.common.widgets

import android.content.Context
import android.util.AttributeSet
import android.view.Gravity
import android.view.View
import android.widget.LinearLayout
import androidx.annotation.StyleableRes
import kotlinx.android.synthetic.main.merge_pick_media.view.*
import ru.perevozka24.perevozka24.R

class PickImageView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : LinearLayout(context, attrs, defStyleAttr) {

    init {
        orientation = VERTICAL
        gravity = Gravity.CENTER

        View.inflate(context, R.layout.merge_pick_media, this)
        val set = intArrayOf(
            android.R.attr.src,
            android.R.attr.text
        )

        val a = context.obtainStyledAttributes(attrs, set)
        val icon = a.getDrawable(ICON_INDEX)
        val title = a.getString(TITLE_INDEX)
        a.recycle()
        pickMediaIcon.setImageDrawable(icon)
        pickMediaTitle.text = title
    }

    private companion object {
        @StyleableRes
        const val ICON_INDEX = 0

        @StyleableRes
        const val TITLE_INDEX = 1
    }
}
