package ru.perevozka24.perevozka24.ui.filters.models

import kotlinx.android.parcel.Parcelize
import ru.perevozka24.perevozka24.App
import ru.perevozka24.perevozka24.data.features.offers.entity.OfferProgressStatusEntity
import ru.perevozka24.perevozka24.ui.base.INameModel
import ru.perevozka24.perevozka24.ui.main.orders.models.OfferProgressStatus

@Parcelize
data class OfferStatusModel(
    val status: OfferProgressStatus
) : INameModel {

    override val name: String
        get() {
            return App.instance.resources.getString(status.title)
        }

    object Mapper :
        ru.perevozka24.perevozka24.ui.common.Mapper<OfferProgressStatusEntity, OfferStatusModel> {
        override fun map(e: OfferProgressStatusEntity): OfferStatusModel {
            val offerProgressStatus = OfferProgressStatus.Mapper.map(e)
            return OfferStatusModel(
                status = offerProgressStatus
            )
        }
    }
}
