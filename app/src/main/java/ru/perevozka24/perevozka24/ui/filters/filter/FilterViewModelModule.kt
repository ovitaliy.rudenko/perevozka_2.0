package ru.perevozka24.perevozka24.ui.filters.filter

import org.kodein.di.Kodein
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.provider

val filterViewModelModule = Kodein.Module(name = "filterViewModelModule") {
    bind<FilterSelectionViewModel>() with provider {
        FilterSelectionViewModel(
            instance(),
            instance()
        )
    }
}
