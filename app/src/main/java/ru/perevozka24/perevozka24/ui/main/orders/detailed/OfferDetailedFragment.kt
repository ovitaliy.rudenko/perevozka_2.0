package ru.perevozka24.perevozka24.ui.main.orders.detailed

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.lifecycle.Observer
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.GroupieViewHolder
import com.yandex.mapkit.MapKitFactory
import com.yandex.mapkit.directions.DirectionsFactory
import kotlinx.android.synthetic.main.fragment_offer_detailed.*
import ru.perevozka24.perevozka24.R
import ru.perevozka24.perevozka24.ui.auth.AuthActivity
import ru.perevozka24.perevozka24.ui.base.BaseFragment
import ru.perevozka24.perevozka24.ui.common.ext.call
import ru.perevozka24.perevozka24.ui.common.ext.openLink
import ru.perevozka24.perevozka24.ui.common.ext.toast
import ru.perevozka24.perevozka24.ui.common.ext.updateOrReplace
import ru.perevozka24.perevozka24.ui.common.ext.visible
import ru.perevozka24.perevozka24.ui.main.orders.base.BaseOrderContentFragment
import ru.perevozka24.perevozka24.ui.main.orders.detailed.dialogs.OfferConfirmDialog
import ru.perevozka24.perevozka24.ui.main.orders.detailed.dialogs.OfferSetPriceDialog
import ru.perevozka24.perevozka24.ui.main.orders.detailed.item.Mapper
import ru.perevozka24.perevozka24.ui.main.orders.models.OfferModel
import ru.perevozka24.perevozka24.ui.viewModel
import ru.perevozka24.perevozka24.utils.trackEvent

class OfferDetailedFragment : BaseFragment(), Reloadable {

    val viewModel: OfferDetailedViewModel by viewModel()

    private val adapter = GroupAdapter<GroupieViewHolder>()

    private val offer by lazy(LazyThreadSafetyMode.NONE) {
        checkNotNull(arguments?.getParcelable(OFFER) as? OfferModel)
    }

    private val action = object : OfferDetailedAction {
        override fun addToFavorite(offer: OfferModel) {
            viewModel.addFavorite(offer)
        }

        override fun pay(offer: OfferModel) {
            context?.openLink(getString(R.string.link_pay, offer.id))
        }

        override fun call(offer: OfferModel) {
            offer.contacts?.let {
                trackEvent("call_from_single_order")
                context?.call(it)
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel.init(offer, requireArguments().getBoolean(IS_MINE))
        trackEvent("single_order_visited")
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_offer_detailed, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        DirectionsFactory.initialize(context)
        viewModel.data.observe(viewLifecycleOwner, Observer {
            adapter.updateOrReplace(Mapper.map(requireContext(), it, action))
        })
        viewModel.error.observe(viewLifecycleOwner, Observer {
            toast(it)
        })
        viewModel.showRequireConfirmationMassage.observe(viewLifecycleOwner, Observer {
            OfferConfirmDialog.create(it).show(parentFragmentManager, "confirm")
        })
        offerDetailedList.adapter = adapter
        offerDetailedList.itemAnimator = null
        offerDetailedList.setHasFixedSize(true)
        bindButtons()

        viewModel.onAbused.observe(viewLifecycleOwner, Observer {
            requireActivity().apply {
                setResult(
                    Activity.RESULT_OK,
                    Intent().apply { putExtra(BaseOrderContentFragment.OFFER_TO_REMOVE, it) })
                finish()
            }
        })
    }

    @SuppressWarnings("LongMethod")
    private fun bindButtons() {
        viewModel.showButtonsContainer.observe(viewLifecycleOwner, Observer {
            offerDetailedButtonsContainer.visible = it
            val bottomPadding = when (it) {
                true -> resources.getDimensionPixelOffset(R.dimen.detailed_button_container_height)
                else -> 0
            }
            offerDetailedList.setPadding(0, 0, 0, bottomPadding)
        })
        viewModel.showPayButton.observe(viewLifecycleOwner, Observer {
            offerDetailedPay.visible = it
        })
        viewModel.showTakeButton.observe(viewLifecycleOwner, Observer {
            offerDetailedTake.visible = it
        })
        viewModel.showAddEquipmentButton.observe(viewLifecycleOwner, Observer {
            offerDetailedAddEquipment.visible = it
        })
        viewModel.showLoginButton.observe(viewLifecycleOwner, Observer {
            offerDetailedLogin.visible = it
        })
        viewModel.showConfirmButton.observe(viewLifecycleOwner, Observer {
            offerDetailedConfirm.visible = it
        })
        viewModel.confirmationTime.observe(viewLifecycleOwner, Observer {
            if (it.isEmpty()) {
                offerDetailedConfirm.text = getString(R.string.offer_set_price_confirm)
            } else {
                offerDetailedConfirm.text = getString(R.string.offer_confirm, it)
            }
        })
        viewModel.showDeclineButton.observe(viewLifecycleOwner, Observer {
            offerDetailedDecline.visible = it
        })

        //
        viewModel.isAuthorized.observe(viewLifecycleOwner, Observer { isAuthorized ->
            offerDetailedAddEquipment.setOnClickListener {
                trackEvent(
                    "add_ad",
                    mapOf("type" to "add")
                )
                val linkId = if (isAuthorized) {
                    R.string.link_add_equipment_logged_in
                } else {
                    R.string.link_add_equipment_not_logged_in
                }
                context?.openLink(getString(linkId))
            }
        })

        offerDetailedLogin.setOnClickListener {
            startActivity(Intent(requireContext(), AuthActivity::class.java))
            activity?.finish()
        }
        offerDetailedPay.setOnClickListener {
            context?.openLink(getString(R.string.link_pay, offer.id))
        }
        offerDetailedTake.setOnClickListener {
            trackEvent("order_get")
            viewModel.takeOffer()
        }
        offerDetailedConfirm.setOnClickListener {
            OfferSetPriceDialog.create(offer).show(childFragmentManager, "setPrice")
        }
        offerDetailedDecline.setOnClickListener {
            trackEvent("order_refuse")
            viewModel.decline()
        }
    }

    override fun reload() {
        viewModel.reload()
    }

    override fun onStart() {
        super.onStart()
        MapKitFactory.getInstance().onStart()
    }

    override fun onStop() {
        super.onStop()
        MapKitFactory.getInstance().onStop()
    }

    companion object {
        private const val OFFER = "offer"
        private const val IS_MINE = "isMine"
        fun create(offer: OfferModel, isMine: Boolean): OfferDetailedFragment {
            return OfferDetailedFragment().apply {
                arguments = bundleOf(OFFER to offer, IS_MINE to isMine)
            }
        }
    }
}
