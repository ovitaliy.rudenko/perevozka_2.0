package ru.perevozka24.perevozka24.ui.main.orders.models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize
import org.threeten.bp.LocalDateTime

sealed class PerformTime : Parcelable {
    @Parcelize
    object AnyTime : PerformTime()

    @Parcelize
    class Period(val start: LocalDateTime, val end: LocalDateTime) : PerformTime()

    @Parcelize
    class Fixed(val date: LocalDateTime) : PerformTime()

    @Parcelize
    class Before(val date: LocalDateTime) : PerformTime()
}
