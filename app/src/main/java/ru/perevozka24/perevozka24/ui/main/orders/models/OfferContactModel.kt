package ru.perevozka24.perevozka24.ui.main.orders.models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
class OfferContactModel(
    val contacts: String? = null,
    val viewsCount: Int = 0
) : Parcelable
