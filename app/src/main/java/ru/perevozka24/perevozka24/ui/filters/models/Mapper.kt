package ru.perevozka24.perevozka24.ui.filters.models

import ru.perevozka24.perevozka24.data.features.filter.entities.CategoryEntity
import ru.perevozka24.perevozka24.data.features.filter.entities.CityEntity
import ru.perevozka24.perevozka24.data.features.filter.entities.CountryEntity
import ru.perevozka24.perevozka24.data.features.filter.entities.RegionEntity
import ru.perevozka24.perevozka24.data.features.filter.entities.RegionsEntity
import ru.perevozka24.perevozka24.data.features.filter.entities.SubCategoryEntity
import ru.perevozka24.perevozka24.ui.common.Combine2Mapper
import ru.perevozka24.perevozka24.ui.common.Mapper

object CategoryMapper : Mapper<CategoryEntity, CategoryModel> {
    override fun map(e: CategoryEntity) =
        CategoryModel(
            id = e.id,
            name = e.name,
            sale = e.sale,
            shipment = e.shipment,
            icon = e.icon
        )
}

object SubCategoryMapper : Mapper<SubCategoryEntity, SubCategoryModel> {
    override fun map(e: SubCategoryEntity) =
        SubCategoryModel(
            id = e.id,
            icon = e.icon,
            name = e.name,
            categoryId = e.categoryId,
            shipment = checkNotNull(e.shipment)
        )
}

object CategorySubCategoryMapper :
    Combine2Mapper<CategoryEntity, SubCategoryEntity, CategorySubCategoryModel> {
    override fun map(e1: CategoryEntity, e2: SubCategoryEntity): CategorySubCategoryModel {
        return CategorySubCategoryModel(
            categoryId = e1.id,
            categoryName = e1.name,
            categoryIcon = e1.icon,
            shipment = checkNotNull(e1.shipment),
            subCategoryId = e2.id,
            icon = e2.icon,
            subCategoryName = e2.name,
            sale = e1.sale
        )
    }
}

object CityMapper : Mapper<CityEntity, CityModel> {
    override fun map(e: CityEntity) =
        CityModel(
            id = e.id,
            name = e.name,
            regionId = e.regionId,
            countryId = e.countryId,
            direction = e.direction ?: Direction.NONE
        )
}

object RegionMapper : Mapper<RegionEntity, RegionModel> {
    override fun map(e: RegionEntity) =
        RegionModel(
            id = e.id,
            name = e.name,
            countryId = e.countryId,
            direction = e.direction ?: Direction.NONE
        )
}

object RegionsMapper : Mapper<RegionsEntity, RegionModel> {
    override fun map(e: RegionsEntity) =
        RegionModel(
            id = e.id,
            name = e.name,
            countryId = e.countryId,
            direction = e.direction ?: Direction.NONE
        )
}

object RegionCityMapper : Mapper<RegionsEntity, RegionCityModel> {
    override fun map(e: RegionsEntity) = RegionCityModel(
        Direction.NONE,
        RegionModel(
            id = e.id,
            name = e.name,
            countryId = e.countryId,
            direction = Direction.NONE
        ),
        null
    )

    override fun map(list: List<RegionsEntity>): List<RegionCityModel> {
        return list.flatMap { e ->
            mutableListOf<RegionCityModel>().apply {
                val r = map(e)
                add(r)
                addAll(e.cities.map { r.copy(city = CityMapper.map(it)) })
            }
        }
    }
}

object CountryMapper : Mapper<CountryEntity, CountryModel> {
    override fun map(e: CountryEntity) = CountryModel(
        id = e.code,
        name = e.name,
        direction = e.direction ?: Direction.NONE
    )
}
