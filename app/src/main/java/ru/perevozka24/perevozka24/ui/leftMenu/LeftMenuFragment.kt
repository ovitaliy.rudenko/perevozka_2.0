package ru.perevozka24.perevozka24.ui.leftMenu

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.navigation.Navigation
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.GroupieViewHolder
import kotlinx.android.synthetic.main.fragment_left_menu.*
import ru.perevozka24.perevozka24.R
import ru.perevozka24.perevozka24.ui.base.BaseFragment
import ru.perevozka24.perevozka24.ui.common.DrawerHolder
import ru.perevozka24.perevozka24.ui.common.ext.openLink
import ru.perevozka24.perevozka24.ui.common.ext.restartApp
import ru.perevozka24.perevozka24.ui.common.ext.toast
import ru.perevozka24.perevozka24.ui.main.MainScreenMode
import ru.perevozka24.perevozka24.ui.main.MainViewModel
import ru.perevozka24.perevozka24.ui.viewModel

class LeftMenuFragment : BaseFragment(), LeftMenuAction {

    private val viewModel: MainViewModel by viewModel()
    private val leftMenuViewModel: LeftMenuViewModel by viewModel()

    private val adapter = GroupAdapter<GroupieViewHolder>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(R.layout.fragment_left_menu, container, false)

    override fun onResume() {
        super.onResume()
        leftMenuViewModel.updateMyOfferItems()
    }

    override val redirectClick: (actionId: Int) -> View.OnClickListener =
        fun(actionId: Int): View.OnClickListener {
            return View.OnClickListener {
                val navigationView = activity?.findViewById<View>(R.id.nav_host_fragment)

                (activity as? DrawerHolder)?.closeLeftDrawer()
                if (navigationView != null && actionId != 0) {
                    Navigation.findNavController(navigationView).navigate(actionId)
                }
            }
        }

    override val loginClick = redirectClick(
        R.id.action_global_loginActivity
    )
    override val logoutClick: View.OnClickListener = View.OnClickListener {
        leftMenuViewModel.logout()
    }
    override val profileClick: View.OnClickListener = View.OnClickListener {
        viewModel.openProfile()
    }

    override val rulesClickLister = View.OnClickListener {
        val context = it.context
        context.openLink(context.getString(R.string.link_rules))
    }

    override val switchTabClick = fun(mode: MainScreenMode): View.OnClickListener {
        return View.OnClickListener {
            (activity as? DrawerHolder)?.closeLeftDrawer()
            viewModel.changeMode(mode)
        }
    }

    override val unimplementedClick = View.OnClickListener {
        toast("В разработке")
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        leftMenuList.adapter = adapter

        leftMenuViewModel.params.observe(viewLifecycleOwner, Observer {
            val params = it ?: return@Observer
            adapter.updateAsync(LeftMenuItemsBuilder.buildItems(params, this))
        })

        leftMenuViewModel.restart.observe(viewLifecycleOwner, Observer {
            activity?.restartApp()
        })
    }
}
