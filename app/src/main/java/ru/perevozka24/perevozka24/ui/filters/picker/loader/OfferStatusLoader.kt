package ru.perevozka24.perevozka24.ui.filters.picker.loader

import ru.perevozka24.perevozka24.data.features.filter.FilterPickerRepository
import ru.perevozka24.perevozka24.ui.filters.models.OfferStatusModel
import ru.perevozka24.perevozka24.ui.filters.models.PickerModel

class OfferStatusLoader(
    private val pickerRepository: FilterPickerRepository
) : PickerLoader<OfferStatusModel> {
    override suspend fun load(): List<OfferStatusModel> {
        return pickerRepository.getStatuses()
    }

    override fun <T : PickerModel> filter(list: List<T>, q: String): List<OfferStatusModel> =
        list.map { it as OfferStatusModel }
}
