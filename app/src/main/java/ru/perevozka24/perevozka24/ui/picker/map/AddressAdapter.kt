package ru.perevozka24.perevozka24.ui.picker.map

import android.location.Address
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.Filter
import android.widget.Filterable
import kotlinx.android.synthetic.main.item_address.view.*
import ru.perevozka24.perevozka24.R
import ru.perevozka24.perevozka24.data.features.location.entity.SimpleAddress

class AddressAdapter(private val addressList: List<SimpleAddress>) : BaseAdapter(), Filterable {

    override fun getCount(): Int = addressList.size

    override fun getItem(position: Int) = addressList[position]

    override fun getItemId(position: Int): Long = position.toLong()

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val view = convertView ?: LayoutInflater.from(parent.context)
            .inflate(R.layout.item_address, parent, false)
        with(view) {
            itemAddressTitle.text = getItem(position).toText()
        }
        return view
    }

    override fun getDropDownView(position: Int, convertView: View?, parent: ViewGroup): View {
        return getView(position, convertView, parent)
    }

    override fun getFilter(): Filter {
        return object : Filter() {
            override fun performFiltering(constraint: CharSequence?): FilterResults {
                return FilterResults().apply {
                    count = addressList.size
                    values = addressList
                }
            }

            override fun publishResults(constraint: CharSequence?, results: FilterResults?) {
                notifyDataSetChanged()
            }

            override fun convertResultToString(resultValue: Any?): CharSequence {
                return (resultValue as? Address)?.toText() ?: ""
            }
        }
    }
}
