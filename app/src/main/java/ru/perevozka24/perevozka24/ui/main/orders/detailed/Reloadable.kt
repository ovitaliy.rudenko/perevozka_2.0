package ru.perevozka24.perevozka24.ui.main.orders.detailed

interface Reloadable {
    fun reload()
}
