package ru.perevozka24.perevozka24.ui.profile.models

import kotlinx.android.parcel.Parcelize
import ru.perevozka24.perevozka24.App
import ru.perevozka24.perevozka24.R
import ru.perevozka24.perevozka24.ui.base.INameModel

sealed class ProfileCompanyType(val value: Int) : INameModel {
    @Parcelize
    object Company : ProfileCompanyType(COMPANY_CODE) {
        override val name: String = App.instance.getString(R.string.profile_type_company)
    }

    @Parcelize
    object Entrepreneur : ProfileCompanyType(ENTREPRENEUR_CODE) {
        override val name: String =
            App.instance.getString(R.string.profile_type_individual_entrepreneur)
    }

    @Parcelize
    object Individual : ProfileCompanyType(INDIVIDUAL_CODE) {
        override val name: String =
            App.instance.getString(R.string.profile_type_private_individual)
    }

    companion object {
        const val COMPANY_CODE = 1
        const val ENTREPRENEUR_CODE = 2
        const val INDIVIDUAL_CODE = 3
    }
}
