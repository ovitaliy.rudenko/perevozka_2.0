package ru.perevozka24.perevozka24.ui.main.orders.models

import ru.perevozka24.perevozka24.ui.main.base.ContentModel

data class OffersModel(
    val data: List<OfferModel>,
    val status: OfferStatus,
    val viewsCount: Int
) : ContentModel {
    override fun isEmpty() = data.isEmpty()

    companion object {
        fun default() = OffersModel(emptyList(), OfferStatus.NONE, 0)
    }
}
