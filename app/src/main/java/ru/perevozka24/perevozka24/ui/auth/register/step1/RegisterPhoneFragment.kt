package ru.perevozka24.perevozka24.ui.auth.register.step1

import android.os.Bundle
import android.text.InputType
import android.view.View
import androidx.navigation.fragment.findNavController
import com.redmadrobot.inputmask.MaskedTextChangedListener
import com.redmadrobot.inputmask.helper.AffinityCalculationStrategy
import kotlinx.android.synthetic.main.fragment_auth_register_contact.*
import ru.perevozka24.perevozka24.R
import ru.perevozka24.perevozka24.ui.viewModel

class RegisterPhoneFragment : BaseRegisterContactFragment<RegisterPhoneViewModel>() {
    override val viewModel: RegisterPhoneViewModel by viewModel()

    override fun getContactViewHint(): Int = R.string.auth_hint_phone

    override fun getContactViewTitle(): Int = R.string.auth_input_title_phone

    override fun getOtherWayButtonTitle(): Int = R.string.auth_using_email

    override fun getOtherWayClickAction() {
        findNavController().navigate(R.id.action_registerPhoneFragment_to_registerEmailFragment)
    }

    override fun getProceedClickAction(contact: String) {
        findNavController().navigate(
            RegisterPhoneFragmentDirections.actionRegisterPhoneFragmentToPhoneRegisterFragment(
                contact.replace("[^0-9]", "")
            )
        )
    }

    override fun getInputType(): Int = InputType.TYPE_CLASS_PHONE

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val affineFormats: MutableList<String> = ArrayList()
        MaskedTextChangedListener.installOn(
            contactView.editText,
            "+7 ([000]) [000]-[00]-[00]",
            affineFormats, AffinityCalculationStrategy.WHOLE_STRING,
            null
        )
    }
}
