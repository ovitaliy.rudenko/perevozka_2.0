package ru.perevozka24.perevozka24.ui.distance

import ru.perevozka24.perevozka24.R
import ru.perevozka24.perevozka24.ui.base.BaseHostActivity

class DistanceActivity : BaseHostActivity() {

    override val graph: Int = R.navigation.nav_calculate_distance
}
