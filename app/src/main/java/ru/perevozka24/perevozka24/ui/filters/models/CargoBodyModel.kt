package ru.perevozka24.perevozka24.ui.filters.models

import kotlinx.android.parcel.Parcelize
import ru.perevozka24.perevozka24.App
import ru.perevozka24.perevozka24.R
import ru.perevozka24.perevozka24.data.features.filter.entities.CargoBodyEntity
import ru.perevozka24.perevozka24.ui.base.INameModel

@Parcelize
data class CargoBodyModel(
    val id: String?,
    override val name: String
) : INameModel {
    companion object {
        fun default() = CargoBodyModel(
            id = null,
            name = App.instance.getString(R.string.filter_cargo_body_type_any)
        )
    }

    object Mapper : ru.perevozka24.perevozka24.ui.common.Mapper<CargoBodyEntity, CargoBodyModel> {
        override fun map(e: CargoBodyEntity): CargoBodyModel {
            return CargoBodyModel(
                id = e.id,
                name = e.name
            )
        }
    }
}
