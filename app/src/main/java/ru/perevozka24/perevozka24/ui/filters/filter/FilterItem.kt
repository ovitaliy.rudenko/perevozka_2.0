package ru.perevozka24.perevozka24.ui.filters.filter

import androidx.annotation.CallSuper
import com.xwray.groupie.kotlinandroidextensions.GroupieViewHolder
import com.xwray.groupie.kotlinandroidextensions.Item
import ru.perevozka24.perevozka24.ui.filters.models.PickerModel

abstract class FilterItem(
    val item: PickerModel,
    open val defaultTitle: Int,
    open val onClick: (Any?) -> Unit
) : Item() {
    @CallSuper
    override fun bind(viewHolder: GroupieViewHolder, position: Int) {
        viewHolder.itemView.setOnClickListener { onClick(this) }
    }
}
