package ru.perevozka24.perevozka24.ui.main.orders.models

import android.os.Parcelable
import kotlinx.android.parcel.IgnoredOnParcel
import kotlinx.android.parcel.Parcelize
import ru.perevozka24.perevozka24.data.features.offers.entity.ImageEntity

@Parcelize
data class ImageModel(
    val filename: String,
    val cover: Boolean
) : Parcelable {
    @IgnoredOnParcel
    val link = "https://perevozka24.ru/i/u_offer/$filename"

    object Mapper : ru.perevozka24.perevozka24.ui.common.Mapper<ImageEntity, ImageModel> {
        override fun map(e: ImageEntity): ImageModel {
            return ImageModel(
                checkNotNull(e.filename),
                checkNotNull(e.cover)
            )
        }
    }
}
