package ru.perevozka24.perevozka24.ui.support.items

import com.xwray.groupie.Group
import ru.perevozka24.perevozka24.R
import ru.perevozka24.perevozka24.ui.support.SupportItemAction

object SupportItemBuilder {

    fun build(
        call: SupportItemAction,
        email: SupportItemAction,
        site: SupportItemAction
    ): List<Group> {
        return mutableListOf<Group>().apply {
            add(SupportTitleItem)
            addAll(addTopBlock(call, email, site))
            addAll(addOffice(call, email))
            addAll(addFinance(call, email))
            addAll(addCooperation(call, email))
        }
    }

    private fun addOffice(
        call: SupportItemAction,
        email: SupportItemAction
    ): Collection<Group> {
        return mutableListOf<Group>().apply {
            add(SupportHeaderItem(R.string.support_office))
            add(
                SupportContentItem(
                    R.string.support_office_user_1,
                    R.string.support_office_user_1_phone,
                    R.string.support_office_user_1_email,
                    false,
                    call, email
                )
            )
            add(
                SupportContentItem(
                    R.string.support_office_user_2,
                    R.string.support_office_user_2_phone,
                    R.string.support_office_user_2_email,
                    false,
                    call, email
                )
            )
            add(SupportFooterItem)
        }
    }

    private fun addFinance(
        call: SupportItemAction,
        email: SupportItemAction
    ): Collection<Group> {
        return mutableListOf<Group>().apply {
            add(SupportHeaderItem(R.string.support_finance))
            add(
                SupportContentItem(
                    R.string.support_finance_user_1,
                    R.string.support_finance_user_1_phone,
                    R.string.support_finance_user_1_email,
                    false,
                    call, email
                )
            )
            add(SupportFooterItem)
        }
    }

    private fun addCooperation(
        call: SupportItemAction,
        email: SupportItemAction
    ): Collection<Group> {
        return mutableListOf<Group>().apply {
            add(SupportHeaderItem(R.string.support_cooperation))
            add(
                SupportContentItem(
                    R.string.support_cooperation_user_1,
                    R.string.support_cooperation_user_1_phone,
                    R.string.support_cooperation_user_1_email,
                    true,
                    call, email
                )
            )
            add(
                SupportContentItem(
                    R.string.support_cooperation_user_2,
                    R.string.support_cooperation_user_2_phone,
                    R.string.support_cooperation_user_2_email,
                    true,
                    call, email
                )
            )
            add(SupportFooterItem)
        }
    }

    private fun addTopBlock(
        call: SupportItemAction,
        email: SupportItemAction,
        site: SupportItemAction
    ): Collection<Group> {
        return mutableListOf<Group>().apply {
            add(
                SupportRowItem(
                    R.drawable.ic_support_calendar,
                    R.string.support_working_hours,
                    null,
                    call
                )
            )
            add(
                SupportRowItem(
                    R.drawable.ic_support_phone,
                    R.string.support_block_phone_1,
                    R.string.support_call,
                    call
                )
            )
            add(
                SupportRowItem(
                    R.drawable.ic_support_phone,
                    R.string.support_block_phone_2,
                    R.string.support_call,
                    call
                )
            )
            add(
                SupportRowItem(
                    R.drawable.ic_support_email,
                    R.string.support_block_email,
                    R.string.support_write,
                    email
                )
            )
            add(
                SupportRowItem(
                    R.drawable.ic_support_globe,
                    R.string.support_block_site,
                    R.string.support_navigate,
                    site
                )
            )
        }
    }
}
