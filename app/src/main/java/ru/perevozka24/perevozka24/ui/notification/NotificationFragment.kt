package ru.perevozka24.perevozka24.ui.notification

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import com.xwray.groupie.Group
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.GroupieViewHolder
import kotlinx.android.synthetic.main.content_list.*
import ru.perevozka24.perevozka24.R
import ru.perevozka24.perevozka24.data.Result
import ru.perevozka24.perevozka24.ui.base.BaseContentLoaderFragment
import ru.perevozka24.perevozka24.ui.common.ext.showSnackBar
import ru.perevozka24.perevozka24.ui.common.ext.updateOrReplace
import ru.perevozka24.perevozka24.ui.common.items.DividerItem
import ru.perevozka24.perevozka24.ui.filters.filter.FilterGroupBuilder
import ru.perevozka24.perevozka24.ui.filters.filter.FilterItem
import ru.perevozka24.perevozka24.ui.filters.models.PickerModel
import ru.perevozka24.perevozka24.ui.filters.picker.FilterPickerActivity
import ru.perevozka24.perevozka24.ui.filters.picker.loader.PickerClearMapper
import ru.perevozka24.perevozka24.ui.notification.item.NotificationCheckboxItem
import ru.perevozka24.perevozka24.ui.notification.item.NotificationEnableBackgroundModeItem
import ru.perevozka24.perevozka24.ui.viewModel
import ru.perevozka24.perevozka24.utils.trackEvent

class NotificationFragment : BaseContentLoaderFragment() {
    override val contentLayoutId: Int = R.layout.content_list

    private val viewModel: NotificationViewModel by viewModel()
    private val adapter = GroupAdapter<GroupieViewHolder>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        trackEvent("visited_notification")
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        subscribe(viewModel.result)
        viewModel.error.observe(viewLifecycleOwner, Observer {
            showSnackBar(it)
        })

        viewModel.result.observe(viewLifecycleOwner, Observer {
            val notification = (it as? Result.Success)?.data ?: return@Observer
            val items = mutableListOf<Group>().apply {
                add(
                    NotificationCheckboxItem(
                        R.string.notification_enabled_title,
                        notification.enabled
                    ) { newValue ->
                        viewModel.enabledChange(newValue)
                    }
                )
                add(
                    NotificationCheckboxItem(
                        R.string.notification_sound_enabled_title,
                        notification.sounds
                    ) { newValue ->
                        viewModel.enabledSoundChange(newValue)
                    }
                )
                add(DividerItem(R.color.grey_de))
                addAll(
                    FilterGroupBuilder.create(notification, onClick)
                )
                if (NotificationEnableBackgroundModeItem.needWarning()) {
                    add(NotificationEnableBackgroundModeItem)
                }
            }

            adapter.updateOrReplace(items)
        })
        contentList.adapter = adapter
    }

    private val onClick = fun(v: Any?) {
        val item = v as? FilterItem ?: return
        val picker = item.item
        val title = PickerClearMapper.mainTitle(picker)
        startActivityForResult(
            FilterPickerActivity.create(
                requireContext(),
                picker,
                title
            ), REQUEST_CODE
        )
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            val value = data?.getParcelableExtra<PickerModel>(FilterPickerActivity.MODEL)
            viewModel.pickerValueChanged(checkNotNull(value))
        }
    }

    private companion object {
        const val REQUEST_CODE = 1000
    }
}
