package ru.perevozka24.perevozka24.ui.filters.picker.loader

import ru.perevozka24.perevozka24.data.features.filter.FilterPickerRepository
import ru.perevozka24.perevozka24.ui.filters.models.CategoryModel
import ru.perevozka24.perevozka24.ui.filters.models.PickerModel

class CategoryPickerLoader(
    private val pickerRepository: FilterPickerRepository
) : PickerLoader<CategoryModel> {
    override suspend fun load(): List<CategoryModel> {
        return pickerRepository.getCategories()
    }

    override fun <T : PickerModel> filter(list: List<T>, q: String): List<CategoryModel> {
        return list.filter { (it as CategoryModel).name?.contains(q, true) == true }
            .map { it as CategoryModel }
    }
}
