package ru.perevozka24.perevozka24.ui.common.widgets.form

class SameValueValidator(
    private val originalField: InputField<String>,
    private val error: String = "Значения не совпадают"
) :
    InputFieldValidator<String> {
    override fun validate(input: String): String? {
        if (originalField.value != input) {
            return error
        }
        return null
    }
}
