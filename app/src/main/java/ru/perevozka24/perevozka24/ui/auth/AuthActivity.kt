package ru.perevozka24.perevozka24.ui.auth

import ru.perevozka24.perevozka24.R
import ru.perevozka24.perevozka24.ui.base.BaseHostActivity
import ru.perevozka24.perevozka24.ui.common.ext.hideKeyboard

class AuthActivity : BaseHostActivity() {

    override val graph: Int = R.navigation.nav_auth

    override fun onPause() {
        super.onPause()
        hideKeyboard(this)
    }
}
