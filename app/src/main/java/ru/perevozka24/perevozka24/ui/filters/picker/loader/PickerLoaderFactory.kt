package ru.perevozka24.perevozka24.ui.filters.picker.loader

import ru.perevozka24.perevozka24.data.features.filter.FilterLocationPickerRepository
import ru.perevozka24.perevozka24.data.features.filter.FilterPickerRepository
import ru.perevozka24.perevozka24.data.features.geolocation.LocationTrackerRepository
import ru.perevozka24.perevozka24.ui.filters.models.CargoBodyModel
import ru.perevozka24.perevozka24.ui.filters.models.CategoryModel
import ru.perevozka24.perevozka24.ui.filters.models.CategorySubCategoryModel
import ru.perevozka24.perevozka24.ui.filters.models.CityModel
import ru.perevozka24.perevozka24.ui.filters.models.CountryModel
import ru.perevozka24.perevozka24.ui.filters.models.OfferStatusModel
import ru.perevozka24.perevozka24.ui.filters.models.PickerModel
import ru.perevozka24.perevozka24.ui.filters.models.RegionCityModel
import ru.perevozka24.perevozka24.ui.filters.models.RegionModel
import ru.perevozka24.perevozka24.ui.filters.models.SubCategoryModel
import ru.perevozka24.perevozka24.ui.geolocation.GeolocationModel

object PickerLoaderFactory {

    fun create(
        model: PickerModel,
        pickerRepo: FilterPickerRepository,
        locationRepo: FilterLocationPickerRepository,
        extra: Array<out Any>
    ): PickerLoader<PickerModel> =
        when (model) {
            is CategoryModel -> CategoryPickerLoader(pickerRepo)
            is SubCategoryModel -> SubCategoryPickerLoader(model, pickerRepo)
            is CategorySubCategoryModel -> CategorySubCategoryPickerLoader(
                model,
                pickerRepo,
                extra
            )
            is CountryModel -> CountryPickerLoader(model, locationRepo)
            is RegionCityModel -> RegionCityPickerLoader(model, locationRepo)
            is RegionModel -> RegionPickerLoader(model, locationRepo)
            is CityModel -> CityPickerLoader(model, locationRepo)
            else -> error("unknown class")
        }

    fun create(
        type: Class<in PickerModel>,
        pickerRepo: FilterPickerRepository,
        geoLocationRepository: LocationTrackerRepository
    ): PickerLoader<PickerModel> =
        when (type) {
            GeolocationModel::class.java -> GeolocationPickerLoader(geoLocationRepository)
            CargoBodyModel::class.java -> CargoBodyTypeLoader(pickerRepo)
            OfferStatusModel::class.java -> OfferStatusLoader(pickerRepo)
            else -> error("unknown class $type")
        }
}
