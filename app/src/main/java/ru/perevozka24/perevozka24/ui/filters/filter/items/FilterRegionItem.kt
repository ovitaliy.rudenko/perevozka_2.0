package ru.perevozka24.perevozka24.ui.filters.filter.items

import com.xwray.groupie.kotlinandroidextensions.GroupieViewHolder
import kotlinx.android.synthetic.main.item_filter_location.view.*
import ru.perevozka24.perevozka24.R
import ru.perevozka24.perevozka24.ui.filters.filter.FilterItem
import ru.perevozka24.perevozka24.ui.filters.models.RegionModel

data class FilterRegionItem(
    private val region: RegionModel,
    override val defaultTitle: Int,
    override val onClick: (Any?) -> Unit
) : FilterItem(region, defaultTitle, onClick) {
    override fun getId(): Long = region.id?.toLong() ?: 0
    override fun getLayout(): Int = R.layout.item_filter_location

    override fun bind(viewHolder: GroupieViewHolder, position: Int) {
        super.bind(viewHolder, position)
        with(viewHolder.itemView) {
            locationIcon.setImageResource(R.drawable.ic_location)
            isEnabled = region.countryId != null
            locationTitle.text =
                when {
                    region.name != null -> region.name
                    else -> context.getString(defaultTitle)
                }
        }
    }
}
