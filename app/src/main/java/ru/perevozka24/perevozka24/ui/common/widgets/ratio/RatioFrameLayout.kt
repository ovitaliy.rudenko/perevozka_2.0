package ru.perevozka24.perevozka24.ui.common.widgets.ratio

import android.content.Context
import android.util.AttributeSet
import android.view.View.MeasureSpec.makeMeasureSpec
import android.widget.FrameLayout
import ru.perevozka24.perevozka24.R

open class RatioFrameLayout @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : FrameLayout(context, attrs, defStyleAttr) {
    private var axis: Axis = Axis.HORIZONTAL
    private var ratio: Float = 1f

    init {
        if (attrs != null) {
            val array = context.obtainStyledAttributes(attrs, R.styleable.RatioFrameLayout)
            ratio = array.getFloat(R.styleable.RatioFrameLayout_ratio, 1f)
            axis = findAxis(
                array.getInt(
                    R.styleable.RatioFrameLayout_axis,
                    0
                )
            )
            array.recycle()
        }
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        val originalWidth = MeasureSpec.getSize(widthMeasureSpec)
        val originalHeight = MeasureSpec.getSize(heightMeasureSpec)
        if (axis == Axis.HORIZONTAL) {
            val height = originalWidth * ratio
            super.onMeasure(widthMeasureSpec, makeMeasureSpec(height.toInt(), MeasureSpec.EXACTLY))
        } else {
            val width = originalHeight * ratio
            super.onMeasure(makeMeasureSpec(width.toInt(), MeasureSpec.EXACTLY), heightMeasureSpec)
        }
    }
}
