package ru.perevozka24.perevozka24.ui.welcome

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import ru.perevozka24.perevozka24.ui.base.BaseFragment

class WelcomePageFragment : BaseFragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(checkNotNull(arguments?.getInt(LAYOUT_ID)), container, false)

    companion object {
        private const val LAYOUT_ID = "layout_id"
        fun create(layoutId: Int) = WelcomePageFragment().apply {
            arguments = bundleOf(LAYOUT_ID to layoutId)
        }
    }
}
