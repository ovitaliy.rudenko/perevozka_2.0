package ru.perevozka24.perevozka24.ui.add.address

import ru.perevozka24.perevozka24.R

class AddressCargoToStepFragment : BaseAddressStepFragment() {
    override val title: Int = R.string.create_order_step_cargo_address_to_title
    override val needDetectLocation: Boolean = false

    override fun submit() {
        addOrderViewModel.locationToSelected(
            checkNotNull(viewModel.country.value),
            checkNotNull(viewModel.region.value),
            checkNotNull(viewModel.city.value),
            checkNotNull(viewModel.address.value)
        )
    }
}
