package ru.perevozka24.perevozka24.ui.main.items.abuse

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import kotlinx.android.synthetic.main.dialog_abuse.*
import org.kodein.di.Kodein
import org.kodein.di.android.x.closestKodein
import ru.perevozka24.perevozka24.R
import ru.perevozka24.perevozka24.data.Result
import ru.perevozka24.perevozka24.ui.base.BaseFragment
import ru.perevozka24.perevozka24.ui.common.ext.showSnackBar
import ru.perevozka24.perevozka24.ui.common.ext.toast
import ru.perevozka24.perevozka24.ui.common.ext.visible
import ru.perevozka24.perevozka24.ui.common.widgets.form.addTextChangedListener
import ru.perevozka24.perevozka24.ui.viewModel

class AbuseFragment : BaseFragment() {

    override val kodein: Kodein by closestKodein()
    private val viewModel: AbuseViewModel by viewModel()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.dialog_abuse, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.itemId = checkNotNull(arguments?.getString(ITEM_ID))
        // message
        abuseMessage.addTextChangedListener { viewModel.messageChange(it.toString()) }
        viewModel.messageFieldError.observe(viewLifecycleOwner, Observer {
            abuseMessage.error(it)
        })

        // phone
        abusePhone.addTextChangedListener { viewModel.phoneChange(it.toString()) }
        viewModel.phoneFieldError.observe(viewLifecycleOwner, Observer {
            abusePhone.error(it)
        })
        //
        abuseSave.setOnClickListener { viewModel.abuse() }
        viewModel.executing.observe(viewLifecycleOwner, Observer {
            when (it) {
                is Result.Loading -> abuseProgressBar.visible = true
                is Result.Error -> {
                    abuseProgressBar.visible = false
                    showSnackBar(it.message)
                }
                is Result.Success -> {
                    abuseProgressBar.visible = false
                    toast(getString(R.string.offer_decline_sent))
                    activity?.finish()
                }
            }
        })
    }

    companion object {
        private const val ITEM_ID = "item_id"

        @JvmStatic
        fun newInstance(itemId: String) = AbuseFragment().apply {
            arguments = Bundle()
            arguments?.putString(ITEM_ID, itemId)
        }
    }
}
