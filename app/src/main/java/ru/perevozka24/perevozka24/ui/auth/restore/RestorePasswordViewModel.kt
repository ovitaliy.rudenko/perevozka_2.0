package ru.perevozka24.perevozka24.ui.auth.restore

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import ru.perevozka24.perevozka24.data.ResourceProvider
import ru.perevozka24.perevozka24.data.Result
import ru.perevozka24.perevozka24.data.features.auth.AuthRepository
import ru.perevozka24.perevozka24.data.features.auth.RestoreParams
import ru.perevozka24.perevozka24.ui.base.BaseViewModel
import ru.perevozka24.perevozka24.ui.common.livedata.postValue
import ru.perevozka24.perevozka24.ui.common.widgets.form.EmptyFieldValidator
import ru.perevozka24.perevozka24.ui.common.widgets.form.InputField

class RestorePasswordViewModel(
    private val authRepository: AuthRepository,
    resourceProvider: ResourceProvider
) : BaseViewModel(resourceProvider) {

    val emailFieldError: LiveData<String> = MutableLiveData()
    private val emailField =
        MutableLiveData(
            InputField(
                "",
                EmptyFieldValidator(),
                { emailFieldError.postValue(it) }
            )
        )

    val restoring: LiveData<Result<String>> = MutableLiveData()

    fun emailChange(email: String) {
        emailField.value?.value = email
    }

    fun restore() {
        val fields = arrayOf(
            emailField
        )

        val isValid = fields.map {
            it.value?.validate()
            (it.value?.hasError ?: true).not()
        }
            .all { it }

        if (isValid) {
            doWork(restoring) {
                val params = RestoreParams(
                    email = checkNotNull(emailField.value?.value)
                )
                authRepository.restore(params)
            }
        }
    }
}
