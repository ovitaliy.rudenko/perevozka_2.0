package ru.perevozka24.perevozka24.ui.main.base

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import ru.perevozka24.perevozka24.data.ResourceProvider
import ru.perevozka24.perevozka24.data.Result
import ru.perevozka24.perevozka24.ui.LiveEvent
import ru.perevozka24.perevozka24.ui.base.BaseViewModel
import ru.perevozka24.perevozka24.ui.filters.models.FilterModel
import ru.perevozka24.perevozka24.utils.trackEvent

abstract class BaseContentViewModel<T : ContentModel>(
    resourceProvider: ResourceProvider
) : BaseViewModel(resourceProvider) {

    abstract val searchReportName: String
    val items: LiveData<Result<T>> = MutableLiveData()
    val itemsList: LiveData<T> = MutableLiveData()
    open val filterChangedEvent = LiveEvent<Pair<FilterModel, FilterModel>>()

    var query = ""
        private set

    abstract fun refresh()

    fun search(query: String) {
        trackEvent(searchReportName, mapOf("search_text" to query))
        this.query = query
        refresh()
    }

    fun clearSearch() {
        this.query = ""
        refresh()
    }

    open fun refreshAfterFilterChange(initial: FilterModel?, changed: FilterModel?) {
        refresh()

        initial?.let { i ->
            changed?.let { c ->
                filterChangedEvent.postValue(Pair(i, c))
            }
        }
    }
}
