package ru.perevozka24.perevozka24.ui.filters.models

import kotlinx.android.parcel.Parcelize
import ru.perevozka24.perevozka24.ICON_DEFAULT

@Parcelize
data class SubCategoryModel(
    val id: String?,
    val icon: String?,
    val name: String?,
    val categoryId: String?,
    val shipment: Int
) : PickerModel {
    companion object {
        fun default(categoryId: String? = null) = SubCategoryModel(
            id = null,
            icon = ICON_DEFAULT,
            name = null,
            categoryId = categoryId,
            shipment = 0
        )
    }
}
