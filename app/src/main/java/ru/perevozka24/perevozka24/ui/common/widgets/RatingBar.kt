package ru.perevozka24.perevozka24.ui.common.widgets

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import androidx.annotation.StyleableRes
import androidx.core.view.forEachIndexed
import ru.perevozka24.perevozka24.R
import kotlin.math.ceil

class RatingBar @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : LinearLayout(context, attrs, defStyleAttr) {

    private var ratingValue: Float = 0f
    var rating: Float
        get() = ratingValue
        set(value) {
            ratingValue = value

            val r = ceil(value * 2).toInt()

            forEachIndexed { index, v ->
                val image = when {
                    index * 2 + 1 == r -> R.drawable.ic_rating_star_half
                    index * 2 < r -> R.drawable.ic_rating_star_yellow
                    else -> R.drawable.ic_rating_star_gray
                }

                (v as ImageView).setImageResource(image)
            }
        }

    init {
        View.inflate(context, R.layout.merge_rating_bar, this)
        val set = intArrayOf(
            android.R.attr.rating // idx 0
        )
        @StyleableRes val ratingIndex = 0
        val a = context.obtainStyledAttributes(attrs, set)
        val rating = a.getFloat(ratingIndex, 0f)
        a.recycle()

        this.rating = rating
    }
}
