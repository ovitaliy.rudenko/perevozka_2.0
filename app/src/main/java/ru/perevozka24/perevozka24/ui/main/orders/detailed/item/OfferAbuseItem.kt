package ru.perevozka24.perevozka24.ui.main.orders.detailed.item

import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.kotlinandroidextensions.GroupieViewHolder
import com.xwray.groupie.kotlinandroidextensions.Item
import kotlinx.android.synthetic.main.item_offer_detailed_abuse.view.*
import ru.perevozka24.perevozka24.R
import ru.perevozka24.perevozka24.ui.main.orders.models.OfferAbuseModel

data class OfferAbuseItem(private val abuses: List<OfferAbuseModel>) : Item() {

    override fun getId() = 0L
    override fun getLayout(): Int = R.layout.item_offer_detailed_abuse

    override fun bind(viewHolder: GroupieViewHolder, position: Int) {
        with(viewHolder.itemView) {
            val a = GroupAdapter<GroupieViewHolder>()
            a.update(Mapper.mapAbuses(abuses))
            itemOfferAbuseList.layoutParams.height =
                resources.getDimensionPixelOffset(R.dimen.filter_item_height) * abuses.size
            itemOfferAbuseList.adapter = a
        }
    }
}
