package ru.perevozka24.perevozka24.ui.main.orders.detailed.item

import android.text.method.LinkMovementMethod
import androidx.core.content.res.ResourcesCompat
import androidx.core.text.HtmlCompat
import com.xwray.groupie.kotlinandroidextensions.GroupieViewHolder
import com.xwray.groupie.kotlinandroidextensions.Item
import kotlinx.android.synthetic.main.item_offer_abuse_item.view.*
import org.threeten.bp.format.DateTimeFormatter
import ru.perevozka24.perevozka24.R
import ru.perevozka24.perevozka24.ui.main.orders.models.OfferAbuseModel

data class OfferAbuseSubItem(
    private val abuse: OfferAbuseModel,
    private val odd: Boolean
) : Item() {

    override fun getId() = 0L
    override fun getLayout(): Int = R.layout.item_offer_abuse_item

    override fun bind(viewHolder: GroupieViewHolder, position: Int) {
        with(viewHolder.itemView) {
            itemOfferAbuseListPerformer.text =
                HtmlCompat.fromHtml(abuse.link, HtmlCompat.FROM_HTML_MODE_LEGACY)
            itemOfferAbuseListPerformer.movementMethod = LinkMovementMethod.getInstance()
            itemOfferAbuseListPerformerDate.text =
                abuse.date.format(DateTimeFormatter.ofPattern(DATE_FORMAT))

            background = when (odd) {
                true -> ResourcesCompat.getDrawable(resources, R.drawable.rect_round_gray, null)
                false -> null
            }
        }
    }

    companion object {
        const val DATE_FORMAT = "yyyy-MM-dd в HH:mm"
    }
}
