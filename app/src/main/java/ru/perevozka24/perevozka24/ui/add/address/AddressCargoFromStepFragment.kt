package ru.perevozka24.perevozka24.ui.add.address

import ru.perevozka24.perevozka24.R

class AddressCargoFromStepFragment : BaseAddressStepFragment() {
    override val title: Int = R.string.create_order_step_cargo_address_from_title
}
