package ru.perevozka24.perevozka24.ui.common

interface /**/ItemMapper<E, C, M> {
    fun map(e: E, callback: C, params: Any): M
    fun map(list: List<E>, callback: C, params: Any): List<M> =
        list.map { map(it, callback, params) }
}
