package ru.perevozka24.perevozka24.ui.filters.picker.item

import android.view.View
import com.xwray.groupie.kotlinandroidextensions.GroupieViewHolder
import kotlinx.android.synthetic.main.item_filter_loader_category.view.*
import ru.perevozka24.perevozka24.R
import ru.perevozka24.perevozka24.ui.filters.models.PickerModel

data class ClearItem(val model: PickerModel, val title: Int, val onClick: View.OnClickListener) :
    BasePickerItem(onClick) {
    override fun getLayout(): Int = R.layout.item_filter_loader_category
    override fun getId(): Long = model.hashCode().toLong()

    override fun bind(viewHolder: GroupieViewHolder, position: Int) {
        with(viewHolder.itemView) {
            tag = model
            filterLoaderCategoryText.text = resources.getString(title)
        }
    }
}
