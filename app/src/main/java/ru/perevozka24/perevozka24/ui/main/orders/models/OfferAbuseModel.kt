package ru.perevozka24.perevozka24.ui.main.orders.models

import org.threeten.bp.LocalDateTime
import ru.perevozka24.perevozka24.data.features.offers.entity.OfferAbuseEntity

data class OfferAbuseModel(
    val id: String,
    val offerId: String,
    val userId: String,
    val text: String,
    val link: String,
    val date: LocalDateTime
) {
    object Mapper : ru.perevozka24.perevozka24.ui.common.Mapper<OfferAbuseEntity, OfferAbuseModel> {
        override fun map(e: OfferAbuseEntity): OfferAbuseModel {
            return OfferAbuseModel(
                id = e.id,
                offerId = e.offerId,
                userId = e.userId,
                text = e.text,
                link = e.link,
                date = e.date
            )
        }
    }
}
