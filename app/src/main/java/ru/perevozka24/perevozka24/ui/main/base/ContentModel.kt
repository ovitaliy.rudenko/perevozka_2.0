package ru.perevozka24.perevozka24.ui.main.base

interface ContentModel {
    fun isEmpty(): Boolean
}
