package ru.perevozka24.perevozka24.ui.filters.filter.items

import com.xwray.groupie.kotlinandroidextensions.GroupieViewHolder
import kotlinx.android.synthetic.main.item_filter_sub_category.view.*
import ru.perevozka24.perevozka24.R
import ru.perevozka24.perevozka24.ui.common.ext.load
import ru.perevozka24.perevozka24.ui.common.ext.visible
import ru.perevozka24.perevozka24.ui.filters.filter.FilterItem
import ru.perevozka24.perevozka24.ui.filters.models.SubCategoryModel

data class FilterSubCategoryItem(
    private val subCategory: SubCategoryModel,
    override val defaultTitle: Int,
    override val onClick: (Any?) -> Unit
) : FilterItem(subCategory, defaultTitle, onClick) {
    override fun getId(): Long = 0
    override fun getLayout(): Int = R.layout.item_filter_sub_category

    override fun bind(viewHolder: GroupieViewHolder, position: Int) {
        super.bind(viewHolder, position)
        with(viewHolder.itemView) {
            isEnabled = subCategory.categoryId != null
            subCategoryTitle.text =
                subCategory.name ?: context.getString(defaultTitle)
            subCategoryIcon.load(subCategory.icon)
            subCategoryIcon.visible = subCategory.icon != null
        }
    }
}
