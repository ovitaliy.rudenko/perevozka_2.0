package ru.perevozka24.perevozka24.ui.main.orders.my

import android.os.Bundle
import android.view.View
import androidx.navigation.fragment.findNavController
import org.kodein.di.Kodein
import org.kodein.di.android.x.kodein
import ru.perevozka24.perevozka24.ui.main.items.detailed.ItemDetailedActivityDirections
import ru.perevozka24.perevozka24.ui.main.orders.OrderAction
import ru.perevozka24.perevozka24.ui.main.orders.base.BaseOrdersFragment
import ru.perevozka24.perevozka24.ui.main.orders.models.OfferModel
import ru.perevozka24.perevozka24.ui.viewModel
import ru.perevozka24.perevozka24.utils.trackEvent

class MyOrdersFragment : BaseOrdersFragment<MyOrderViewModel>() {

    override val kodein: Kodein by kodein()
    override val viewModel: MyOrderViewModel by viewModel()

    private val defaultAction = super.actions
    override val actions: OrderAction = object : OrderAction by defaultAction {
        override fun openDetailed(model: OfferModel, view: View) {
            findNavController().navigate(
                ItemDetailedActivityDirections.actionGlobalOfferDetailedActivity(
                    model,
                    isMine = true
                )
            )
        }
    }
    override val hasExtraActions = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        trackEvent("visited_my_orders")
    }
}
