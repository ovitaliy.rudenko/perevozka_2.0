package ru.perevozka24.perevozka24.ui.distance.result

import com.yandex.mapkit.directions.driving.DrivingRoute
import com.yandex.mapkit.geometry.Point
import ru.perevozka24.perevozka24.ui.filters.models.CountryModel
import ru.perevozka24.perevozka24.ui.filters.models.RegionCityModel
import java.io.Serializable

data class DistanceCalculationResult(
    val consumption: Double?,
    val price: Double?,
    val route: List<Point>,
    val distanceValue: Double,
    val distanceText: String,
    val duration: String,
    val durationWithTrafficJam: String,
    val startCountry: CountryModel,
    val startRegionCity: RegionCityModel,
    val endCountry: CountryModel,
    val endRegionCity: RegionCityModel
) : Serializable {

    companion object {

        @SuppressWarnings("LongParameterList")
        fun map(
            startCountry: CountryModel,
            startRegionCity: RegionCityModel,
            endCountry: CountryModel,
            endRegionCity: RegionCityModel,
            consumption: Double?,
            price: Double?,
            route: DrivingRoute
        ): DistanceCalculationResult {
            return DistanceCalculationResult(
                startCountry = startCountry,
                startRegionCity = startRegionCity,
                endCountry = endCountry,
                endRegionCity = endRegionCity,
                consumption = consumption,
                price = price,
                route = route.geometry.points,
                distanceValue = route.metadata.weight.distance.value,
                distanceText = route.metadata.weight.distance.text,
                duration = arrayOf(
                    route.metadata.weight.time,
                    route.metadata.weight.timeWithTraffic
                ).minBy { it.value }?.text ?: "",
                durationWithTrafficJam = arrayOf(
                    route.metadata.weight.time,
                    route.metadata.weight.timeWithTraffic
                ).maxBy { it.value }?.text ?: ""
            )
        }
    }
}
