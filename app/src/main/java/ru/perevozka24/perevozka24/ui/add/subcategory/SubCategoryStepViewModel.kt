package ru.perevozka24.perevozka24.ui.add.subcategory

import ru.perevozka24.perevozka24.data.ResourceProvider
import ru.perevozka24.perevozka24.data.features.filter.FilterLocationPickerRepository
import ru.perevozka24.perevozka24.data.features.filter.FilterPickerRepository
import ru.perevozka24.perevozka24.ui.filters.models.SubCategoryModel
import ru.perevozka24.perevozka24.ui.filters.picker.FilterPickerViewModel

class SubCategoryStepViewModel(
    pickerRepository: FilterPickerRepository,
    pickerLocationRepository: FilterLocationPickerRepository,
    resourceProvider: ResourceProvider
) : FilterPickerViewModel(pickerRepository, pickerLocationRepository, resourceProvider) {

    fun init(categoryId: String) = init(SubCategoryModel.default(categoryId))
}
