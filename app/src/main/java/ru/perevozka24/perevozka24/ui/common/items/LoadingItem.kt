package ru.perevozka24.perevozka24.ui.common.items

import android.view.View
import android.widget.FrameLayout.LayoutParams.MATCH_PARENT
import android.widget.FrameLayout.LayoutParams.WRAP_CONTENT
import com.xwray.groupie.kotlinandroidextensions.GroupieViewHolder
import com.xwray.groupie.kotlinandroidextensions.Item
import ru.perevozka24.perevozka24.R

data class LoadingItem(
    private val itemWidth: Int,
    private val itemHeight: Int
) : Item() {
    override fun getId(): Long = 0

    override fun createViewHolder(itemView: View): GroupieViewHolder {
        with(itemView.layoutParams) {
            width = itemWidth
            height = itemHeight
        }
        itemView.postInvalidate()
        return super.createViewHolder(itemView)
    }

    override fun bind(viewHolder: GroupieViewHolder, position: Int) = Unit

    override fun getLayout(): Int = R.layout.item_loading

    companion object {
        fun matchParent() = LoadingItem(MATCH_PARENT, MATCH_PARENT)
        fun horizontal() = LoadingItem(MATCH_PARENT, WRAP_CONTENT)
    }
}
