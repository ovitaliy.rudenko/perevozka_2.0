package ru.perevozka24.perevozka24.ui.main.items.detailed

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import ru.perevozka24.perevozka24.data.ResourceProvider
import ru.perevozka24.perevozka24.data.Result
import ru.perevozka24.perevozka24.data.features.items.ItemsRepository
import ru.perevozka24.perevozka24.ui.base.BaseViewModel
import ru.perevozka24.perevozka24.ui.common.livedata.postValue
import ru.perevozka24.perevozka24.ui.main.items.models.ItemModel

class ItemDetailedViewModel(
    private val repository: ItemsRepository,
    resourceProvider: ResourceProvider
) : BaseViewModel(resourceProvider) {

    val item: LiveData<Result<ItemModel>> = MutableLiveData()
    val blocked: LiveData<Boolean> = MutableLiveData()

    val isMine: LiveData<Boolean> = MutableLiveData()

    fun init(item: ItemModel, isMine: Boolean) {
        this.isMine.postValue(isMine)
        this.item.postValue(Result.Success(item))
    }

    fun addFavorite(item: ItemModel) {
        GlobalScope.launch {
            init(item.copy(favorite = !item.favorite), isMine.value ?: false)
            safeExecute {
                if (item.favorite) {
                    repository.removeFavorite(item.id)
                } else {
                    repository.addFavorite(item.id)
                }
            }.traceError()
        }
    }

    fun block(item: ItemModel) {
        GlobalScope.launch {
            safeExecute { repository.addToBlackList(item.userId) }.traceError()
            blocked.postValue(true)
        }
    }
}
