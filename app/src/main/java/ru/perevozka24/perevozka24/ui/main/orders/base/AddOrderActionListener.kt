package ru.perevozka24.perevozka24.ui.main.orders.base

import android.view.View

interface AddOrderActionListener {
    fun getAddOrderActionTitle(): Int
    fun getAddOrderAction(): View.OnClickListener
}
