package ru.perevozka24.perevozka24.ui.filters.models

import ru.perevozka24.perevozka24.R

enum class ExecutionPeriodType {
    ANY_TIME {
        override val title: Int
            get() = R.string.create_order_step_extra_info_any_time

        override val id: String = "1"
    },
    PERIOD {
        override val title: Int
            get() = R.string.create_order_step_extra_info_period
        override val id: String = "2"
    },
    FIXED_DATE {
        override val title: Int
            get() = R.string.create_order_step_extra_info_fixed_date
        override val id: String = "3"
    },
    BEFORE {
        override val title: Int
            get() = R.string.create_order_step_extra_info_before
        override val id: String = "4"
    };

    abstract val id: String
    abstract val title: Int
}
