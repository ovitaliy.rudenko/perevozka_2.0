package ru.perevozka24.perevozka24.ui.auth

import ru.perevozka24.perevozka24.R
import ru.perevozka24.perevozka24.ui.base.BaseHostActivity
import ru.perevozka24.perevozka24.ui.common.ext.hideKeyboard

class RegistrationActivity : BaseHostActivity() {

    override val graph: Int = R.navigation.nav_register

    override fun onPause() {
        super.onPause()
        hideKeyboard(this)
    }
}
