package ru.perevozka24.perevozka24.ui.common.widgets

import android.content.Context
import android.graphics.drawable.Drawable
import android.util.AttributeSet
import android.view.Gravity
import android.view.View
import android.widget.LinearLayout
import androidx.core.content.res.ResourcesCompat
import androidx.core.view.children
import ru.perevozka24.perevozka24.R
import kotlin.math.max

class StepView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : LinearLayout(context, attrs, defStyleAttr) {
    private var _steps: Int = 0
    private var _step: Int = 0
    private val itemMargin: Int
    private val indicator: Drawable
    private val indicatorSize: Int
    private val selector: StepSelector
    var steps: Int
        get() = _steps
        set(value) {
            setValues(value, _step)
        }

    var step: Int
        get() = _step
        set(value) {
            setValues(_steps, max(1, value))
        }

    init {
        orientation = HORIZONTAL
        gravity = Gravity.CENTER

        val a = context.theme.obtainStyledAttributes(attrs, R.styleable.StepView, 0, 0)
        val steps = a.getInteger(R.styleable.StepView_steps, DEFAULT_STEPS)
        val step = a.getInteger(R.styleable.StepView_step, 1)
        itemMargin = a.getDimension(
            R.styleable.StepView_divider_width,
            resources.getDimension(R.dimen.steps_margin)
        ).toInt()
        indicator = a.getDrawable(R.styleable.StepView_indicator)
            ?: ResourcesCompat.getDrawable(resources, R.drawable.dr_step_bg, null)
                    ?: error("can't find indicator")
        indicatorSize = a.getDimension(R.styleable.StepView_indicator_size, 0f).toInt()
        selector = a.getInt(R.styleable.StepView_selector, 0).run {
            when (this) {
                0 -> ProgressSelector
                else -> PositionSelector
            }
        }
        a.recycle()
        setValues(steps, max(1, step))
    }

    private fun setValues(steps: Int, step: Int) {
        if (steps == _steps && step == _step) return

        while (steps > childCount) {
            addView(createStepView(childCount))
        }
        while (steps < childCount) {
            removeViewAt(childCount - 1)
        }
        children.forEachIndexed { index, view ->
            selector.select(index, view, step)
        }

        _steps = steps
        _step = step
    }

    private fun createStepView(position: Int): View {
        return View(context).apply {
            layoutParams = createLayoutParams().applyMargin(position)
            background = indicator.constantState?.newDrawable()
        }
    }

    private fun createLayoutParams(): LayoutParams {
        return if (indicatorSize > 0) {
            LayoutParams(indicatorSize, indicatorSize)
        } else {
            LayoutParams(0, LayoutParams.MATCH_PARENT, 1f)
        }
    }

    private fun LayoutParams.applyMargin(position: Int): LayoutParams {
        return apply {
            marginStart = if (position == 0) {
                0
            } else {
                itemMargin
            }
        }
    }

    private companion object {
        const val DEFAULT_STEPS = 6
    }

    private interface StepSelector {
        fun select(viewIndex: Int, v: View, position: Int)
    }

    private object ProgressSelector : StepSelector {
        override fun select(viewIndex: Int, v: View, position: Int) {
            v.isSelected = viewIndex + 1 <= position
        }
    }

    private object PositionSelector : StepSelector {
        override fun select(viewIndex: Int, v: View, position: Int) {
            v.isSelected = viewIndex + 1 == position
        }
    }
}
