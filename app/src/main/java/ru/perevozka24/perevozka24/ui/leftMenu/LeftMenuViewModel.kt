package ru.perevozka24.perevozka24.ui.leftMenu

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import ru.perevozka24.perevozka24.data.ResourceProvider
import ru.perevozka24.perevozka24.data.features.auth.AuthRepository
import ru.perevozka24.perevozka24.data.features.geolocation.LocationTrackerRepository
import ru.perevozka24.perevozka24.data.features.items.ItemsRepository
import ru.perevozka24.perevozka24.data.features.offers.OffersRepository
import ru.perevozka24.perevozka24.ui.LiveEvent
import ru.perevozka24.perevozka24.ui.auth.models.LoginModel
import ru.perevozka24.perevozka24.ui.base.BaseViewModel
import ru.perevozka24.perevozka24.ui.common.livedata.Combined4LiveData
import ru.perevozka24.perevozka24.ui.common.livedata.distinctUntilChanged
import ru.perevozka24.perevozka24.ui.common.livedata.postValue
import ru.perevozka24.perevozka24.ui.geolocation.GeolocationModel
import java.util.concurrent.atomic.AtomicReference

class LeftMenuViewModel(
    private val userRepository: AuthRepository,
    private val itemsRepository: ItemsRepository,
    private val offersRepository: OffersRepository,
    private val locationTrackerRepository: LocationTrackerRepository,
    resourceProvider: ResourceProvider
) : BaseViewModel(resourceProvider) {

    private val loginModel: LiveData<LoginModel> = MutableLiveData()
    private val geolocationItems: LiveData<List<GeolocationModel>> = MutableLiveData()
    private val hasItems: LiveData<Boolean> = MutableLiveData()
    private val hasOrders: LiveData<Boolean> = MutableLiveData()

    val params: LiveData<LeftMenuParams> = Combined4LiveData(
        loginModel,
        geolocationItems,
        hasItems,
        hasOrders
    ) { l, g, hasItems, hasOrders ->
        LeftMenuParams(l, g, hasItems ?: false, hasOrders ?: false)
    }.distinctUntilChanged()

    init {
        GlobalScope.launch {
            safeExecute {
                AtomicReference(userRepository.getUser())
            }
                .traceError()
                .doOnSuccess {
                    it.get()?.let { u -> loginModel.postValue(u) }
                    updateMyOfferItems()
                }
        }

        GlobalScope.launch {
            safeExecute {
                checkNotNull(locationTrackerRepository.getItems())
            }
                .traceError()
                .doOnSuccess {
                    geolocationItems.postValue(it)
                }
        }
    }

    fun updateMyOfferItems() {
        if (!userRepository.isAuthorized()) return
        GlobalScope.launch {
            safe {
                hasItems.postValue(itemsRepository.hasMyItems())
            }
        }
        GlobalScope.launch {
            safe {
                hasOrders.postValue(offersRepository.hasMyOffers())
            }
        }
    }

    val restart = LiveEvent<Unit>()

    fun logout() {
        userRepository.logout()
        restart.postValue(Unit)
    }
}
