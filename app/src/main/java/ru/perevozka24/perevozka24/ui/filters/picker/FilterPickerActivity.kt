package ru.perevozka24.perevozka24.ui.filters.picker

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.commit
import kotlinx.android.synthetic.main.activity_fragment_container.*
import org.kodein.di.android.kodein
import ru.perevozka24.perevozka24.R
import ru.perevozka24.perevozka24.ui.base.BaseFragmentActivity
import ru.perevozka24.perevozka24.ui.common.ext.hideKeyboard
import ru.perevozka24.perevozka24.ui.filters.OnPickerValueSelected
import ru.perevozka24.perevozka24.ui.filters.models.PickerModel
import ru.perevozka24.perevozka24.ui.filters.picker.loader.PickerMapper

class FilterPickerActivity : BaseFragmentActivity(), OnPickerValueSelected {

    override val kodein by kodein()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val type: PickerModel = checkNotNull(intent.getParcelableExtra(MODEL))
        toolbarTitleView.setText(PickerMapper.mapToTitle(type))
        if (savedInstanceState == null) {
            val clearTitle = intent.getIntExtra(CLEAR_TITLE, 0)
            val cargoOnly = intent.getBooleanExtra(CARGO_ONLY, false)
            supportFragmentManager.commit {
                replace(R.id.container, FilterPickerFragment.create(type, clearTitle, cargoOnly))
            }
        }
    }

    override fun onPause() {
        super.onPause()
        hideKeyboard(this)
    }
    override fun onSelectPickerValue(value: PickerModel) {
        val resultIntent = Intent().apply {
            putExtra(MODEL, value)
        }
        setResult(Activity.RESULT_OK, resultIntent)
        finish()
    }

    companion object {
        const val MODEL = "model"
        private const val CLEAR_TITLE = "clear_title"
        private const val CARGO_ONLY = "cargo_only"

        fun create(
            context: Context,
            model: PickerModel,
            clearTitle: Int,
            cargoOnly: Boolean = false
        ): Intent {
            return Intent(context, FilterPickerActivity::class.java).apply {
                putExtra(MODEL, model)
                putExtra(CLEAR_TITLE, clearTitle)
                putExtra(CARGO_ONLY, cargoOnly)
            }
        }
    }
}
