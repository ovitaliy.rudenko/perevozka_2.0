package ru.perevozka24.perevozka24.ui.distance.result

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.navigation.fragment.navArgs
import com.yandex.mapkit.geometry.Point
import com.yandex.mapkit.map.CameraPosition
import com.yandex.runtime.image.ImageProvider
import kotlinx.android.synthetic.main.fragment_calculate_distance_result.*
import ru.perevozka24.perevozka24.MainActivity
import ru.perevozka24.perevozka24.R
import ru.perevozka24.perevozka24.ui.base.BaseFragment
import ru.perevozka24.perevozka24.ui.common.ext.restart
import ru.perevozka24.perevozka24.ui.common.ext.setVisibleText
import ru.perevozka24.perevozka24.ui.viewModel
import kotlin.math.ln
import kotlin.math.max
import kotlin.math.min
import kotlin.math.sin

class DistanceResultFragment : BaseFragment() {

    private val args: DistanceResultFragmentArgs by navArgs()
    private val viewModel: DistanceResultViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel.init(args.result)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(R.layout.fragment_calculate_distance_result, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel.cities.observe(viewLifecycleOwner, Observer {
            it ?: return@Observer
            distanceResultCities.text = resources.getString(
                R.string.calculate_distance_result_cities,
                it.first, it.second
            )
        })
        bindValues()
        bindMap()

        viewModel.showCargoScreen.observe(viewLifecycleOwner, Observer {
            val intent = MainActivity.create(requireContext(), MainActivity.VIEW_CARGO)
                .restart()
            startActivity(intent)
        })
        distanceResultSearch.setOnClickListener { viewModel.saveSearch() }
    }

    private fun bindValues() {
        viewModel.distanceString.observe(viewLifecycleOwner, Observer {
            arrayOf(distanceResultValue, distanceResultTitle)
                .setVisibleText(it, true)
        })
        viewModel.consumptionString.observe(viewLifecycleOwner, Observer {
            arrayOf(distanceResultConsumptionValue, distanceResultConsumptionTitle)
                .setVisibleText(it, true)
        })

        viewModel.priceString.observe(viewLifecycleOwner, Observer {
            arrayOf(distanceResultPriceValue, distanceResultPriceTitle)
                .setVisibleText(it, true)
        })

        viewModel.durationString.observe(viewLifecycleOwner, Observer {
            arrayOf(distanceResultTimeValue, distanceResultTimeTitle)
                .setVisibleText(it, true)
        })
        viewModel.durationWithTrafficJamString.observe(viewLifecycleOwner, Observer {
            arrayOf(distanceResultTimeJamValue, distanceResultTimeJamTitle)
                .setVisibleText(it, true)
        })
    }

    private fun bindMap() {
        val mapObjects = distanceResultMap.map.mapObjects
        viewModel.start.observe(viewLifecycleOwner, Observer {
            it ?: return@Observer
            mapObjects.addPlacemark(
                it,
                ImageProvider.fromResource(context, R.drawable.ic_distance_start)
            )
        })
        viewModel.end.observe(viewLifecycleOwner, Observer {
            it ?: return@Observer
            mapObjects.addPlacemark(
                it,
                ImageProvider.fromResource(context, R.drawable.ic_detailed_map_pin)
            )
        })
        viewModel.route.observe(viewLifecycleOwner, Observer {
            it ?: return@Observer
            mapObjects.addPolyline(it)
        })
        viewModel.bounds.observe(viewLifecycleOwner, Observer {
            it ?: return@Observer

            val center = Point(
                (it.southWest.latitude + it.northEast.latitude) / 2 +
                        (it.southWest.latitude - it.northEast.latitude) / 2,
                (it.southWest.longitude + it.northEast.longitude) / 2
            )
            val zoom = getBoundsZoomLevel(
                distanceResultMap.map.maxZoom,
                it.northEast,
                it.southWest,
                distanceResultMap.width(),
                distanceResultMap.width()
            )

            distanceResultMap.map.move(
                CameraPosition(center, zoom - 2, 0f, 0f)
            )
        })
    }

    override fun onResume() {
        super.onResume()
        distanceResultMap.onStart()
    }

    override fun onStop() {
        super.onStop()
        distanceResultMap.onStop()
    }

    companion object {
        private const val FULL_CIRCLE = 360f
        private const val HALF_CIRCLE = 180f
        private const val WORLD = 256f
        private const val LN2 = .693147180559945309417f

        fun getBoundsZoomLevel(
            maxZoom: Float,
            northeast: Point,
            southwest: Point,
            width: Int,
            height: Int
        ): Float {
            val latFraction =
                (latRad(northeast.latitude) - latRad(southwest.latitude)) / Math.PI.toFloat()
            val lngDiff: Float = northeast.longitude.toFloat() - southwest.longitude.toFloat()
            val lngFraction = (if (lngDiff < 0) lngDiff + FULL_CIRCLE else lngDiff) / FULL_CIRCLE
            val latZoom = zoom(height.toFloat(), latFraction)
            val lngZoom = zoom(width.toFloat(), lngFraction)
            val zoom = min(min(latZoom, lngZoom), maxZoom)
            return zoom.toFloat()
        }

        private fun latRad(lat: Double): Float {
            val sin = sin(lat * Math.PI / HALF_CIRCLE)
            val radX2 = ln((1 + sin) / (1 - sin)) / 2
            return (max(min(radX2, Math.PI), -Math.PI) / 2).toFloat()
        }

        private fun zoom(mapPx: Float, fraction: Float): Float {
            return ln((mapPx / WORLD / fraction)) / LN2
        }
    }
}
