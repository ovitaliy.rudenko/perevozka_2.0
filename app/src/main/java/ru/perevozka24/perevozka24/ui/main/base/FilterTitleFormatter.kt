package ru.perevozka24.perevozka24.ui.main.base

import android.content.Context
import ru.perevozka24.perevozka24.R
import ru.perevozka24.perevozka24.ui.filters.models.CategorySubCategoryModel

interface FilterTitleFormatter {
    fun format(context: Context, m: CategorySubCategoryModel): String?
}

object DefaultFilterTitleFormatter : FilterTitleFormatter {
    override fun format(context: Context, m: CategorySubCategoryModel): String? {
        return when {
            m.subCategoryName == null -> m.categoryName
            m.sale == 1 -> "${m.subCategoryName} ${context.getString(R.string.filter_sale)}"
            else -> m.subCategoryName
        }
    }
}
