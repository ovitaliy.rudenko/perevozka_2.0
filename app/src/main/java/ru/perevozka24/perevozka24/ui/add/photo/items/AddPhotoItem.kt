package ru.perevozka24.perevozka24.ui.add.photo.items

import com.xwray.groupie.kotlinandroidextensions.GroupieViewHolder
import com.xwray.groupie.kotlinandroidextensions.Item
import kotlinx.android.synthetic.main.item_add_photo.view.*
import ru.perevozka24.perevozka24.R
import ru.perevozka24.perevozka24.ui.common.ext.load
import ru.perevozka24.perevozka24.ui.common.ext.visible

data class AddPhotoItem(val path: String?, val delete: (String) -> Unit) : Item() {
    override fun getId(): Long = path?.hashCode()?.toLong() ?: 0
    override fun getLayout(): Int = R.layout.item_add_photo

    override fun bind(viewHolder: GroupieViewHolder, position: Int) {
        with(viewHolder.itemView) {
            addPhotoImage.load(path)
            addPhotoDelete.visible = path != null
            path?.let { p ->
                addPhotoDelete.setOnClickListener { delete(p) }
            }
        }
    }
}
