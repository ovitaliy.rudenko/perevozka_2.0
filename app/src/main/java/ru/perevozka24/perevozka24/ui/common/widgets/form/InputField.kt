package ru.perevozka24.perevozka24.ui.common.widgets.form

class InputField<T>(
    initialValue: T,
    private val validators: List<InputFieldValidator<T>>,
    private val validationCallback: ((String) -> Unit)?
) {
    constructor(
        initialValue: T,
        validator: InputFieldValidator<T>,
        validationCallback: ((String) -> Unit)? = null
    ) : this(initialValue, listOf(validator), validationCallback)

    var value: T = initialValue

    private var _errors = emptyList<String>()

    val error: String
        get() = _errors.joinToString("\n")

    val hasError: Boolean
        get() = _errors.isNotEmpty()

    fun validate() {
        _errors = validators.mapNotNull { it.validate(value) }
        validationCallback?.invoke(error)
    }

    fun copy(
        newValue: T
    ) = InputField(
        newValue,
        validators,
        validationCallback
    )
}
