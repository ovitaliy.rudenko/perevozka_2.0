package ru.perevozka24.perevozka24.ui.splash

import androidx.lifecycle.ViewModel
import ru.perevozka24.perevozka24.data.features.auth.AuthRepository
import ru.perevozka24.perevozka24.ui.LiveEvent

class SplashViewModel(authRepository: AuthRepository) : ViewModel() {

    val showWelcome = LiveEvent<Unit>()
    val showMain = LiveEvent<Unit>()

    init {
        if (authRepository.isShowOnbording()) {
            showWelcome.postValue(Unit)
        } else {
            showMain.postValue(Unit)
        }
    }
}
