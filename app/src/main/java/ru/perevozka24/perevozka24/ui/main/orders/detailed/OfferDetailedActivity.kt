package ru.perevozka24.perevozka24.ui.main.orders.detailed

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.commit
import androidx.lifecycle.Observer
import androidx.navigation.navArgs
import kotlinx.android.synthetic.main.activity_fragment_loading_container.*
import org.kodein.di.Kodein
import org.kodein.di.android.kodein
import ru.perevozka24.perevozka24.R
import ru.perevozka24.perevozka24.ui.base.BaseContentLoaderFragmentActivity
import ru.perevozka24.perevozka24.ui.main.orders.models.OfferModel
import ru.perevozka24.perevozka24.ui.viewModel

class OfferDetailedActivity : BaseContentLoaderFragmentActivity<OfferModel>() {
    override val kodein: Kodein by kodein()

    private val args: OfferDetailedActivityArgs by navArgs()

    override val viewModel: OfferDetailedLoadViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        viewModel.title.observe(this, Observer {
            toolbarTitleView.text = it
        })

        if (isFromPush) {
            openFromPush(intent)
        } else {
            viewModel.init(args.offer, args.offerId)
        }
    }

    override fun showLoaded(value: OfferModel) {
        supportFragmentManager.commit {
            replace(R.id.container, OfferDetailedFragment.create(value, args.isMine))
        }
    }

    override fun onNewIntent(intent: Intent) {
        super.onNewIntent(intent)
        openFromPush(intent)
    }

    private fun openFromPush(intent: Intent) {
        val pushOfferId = intent.getStringExtra("offerId") ?: intent.getStringExtra("offer_id")

        if (pushOfferId != null) {
            viewModel.init(null, pushOfferId)
        }
    }

    private val isFromPush: Boolean
        get() = intent.action != null && intent.action == "view_offers" || intent.hasExtra("fromPush")

    companion object {
        fun create(context: Context, model: OfferModel): Intent {
            return Intent(context, OfferDetailedActivity::class.java).apply {
                putExtra("offer", model)
            }
        }
    }
}
