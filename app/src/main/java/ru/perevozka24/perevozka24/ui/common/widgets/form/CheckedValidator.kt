package ru.perevozka24.perevozka24.ui.common.widgets.form

class CheckedValidator(private val error: String) : InputFieldValidator<Boolean> {
    override fun validate(input: Boolean): String? {
        if (!input) {
            return error
        }
        return null
    }
}
