package ru.perevozka24.perevozka24.ui.main.orders.detailed.item

import com.xwray.groupie.kotlinandroidextensions.GroupieViewHolder
import com.xwray.groupie.kotlinandroidextensions.Item
import kotlinx.android.synthetic.main.item_offer_detailed_title.view.*
import ru.perevozka24.perevozka24.R
import ru.perevozka24.perevozka24.ui.main.orders.models.OfferModel

data class OfferTitleItem(private val offer: OfferModel) : Item() {

    override fun getId() = 0L
    override fun getLayout(): Int = R.layout.item_offer_detailed_title

    override fun bind(viewHolder: GroupieViewHolder, position: Int) {
        with(viewHolder.itemView) {
            itemOfferTitle.text = offer.name
        }
    }
}
