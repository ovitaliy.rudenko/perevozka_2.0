package ru.perevozka24.perevozka24.ui.auth.register.step1

import ru.perevozka24.perevozka24.data.ResourceProvider
import ru.perevozka24.perevozka24.ui.common.widgets.form.EmailValidator
import ru.perevozka24.perevozka24.ui.common.widgets.form.InputFieldValidator

class RegisterEmailViewModel(
    resourceProvider: ResourceProvider
) : BaseRegisterContactViewModel(resourceProvider) {

    override val validator: InputFieldValidator<String>
        get() = EmailValidator()
}
