package ru.perevozka24.perevozka24.ui.add.subcategory

import android.os.Bundle
import android.view.View
import androidx.core.widget.addTextChangedListener
import androidx.lifecycle.Observer
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.GroupieViewHolder
import kotlinx.android.synthetic.main.content_filter_picker.*
import ru.perevozka24.perevozka24.R
import ru.perevozka24.perevozka24.ui.add.AddOrderActivity
import ru.perevozka24.perevozka24.ui.add.AddOrderViewModel
import ru.perevozka24.perevozka24.ui.add.ICreateOrderStep
import ru.perevozka24.perevozka24.ui.base.BaseContentLoaderFragment
import ru.perevozka24.perevozka24.ui.common.ext.updateOrReplace
import ru.perevozka24.perevozka24.ui.filters.models.SubCategoryModel
import ru.perevozka24.perevozka24.ui.filters.picker.loader.PickerMapper
import ru.perevozka24.perevozka24.ui.viewModel

class SubCategoryStepFragment : BaseContentLoaderFragment(), ICreateOrderStep {
    override val title: Int = R.string.create_order_step_category_title

    private val addOrderViewModel: AddOrderViewModel by lazy {
        (activity as AddOrderActivity).viewModel
    }
    private val stepViewModel: SubCategoryStepViewModel by viewModel()

    override val contentLayoutId: Int = R.layout.fragment_add_order_step_subcategory
    private val adapter = GroupAdapter<GroupieViewHolder>()

    private val onClickListener = View.OnClickListener { v ->
        addOrderViewModel.subCategorySelected(v.tag as SubCategoryModel)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        subscribe(stepViewModel.result)
        filterPickerList.adapter = adapter
        stepViewModel.init(checkNotNull(addOrderViewModel.categoryModel?.id))
        stepViewModel.filtered.observe(viewLifecycleOwner, Observer { list ->
            list ?: return@Observer
            val mapped = PickerMapper.map(list, null, onClickListener)
            adapter.updateOrReplace(mapped)
        })
        filterQuery.addTextChangedListener { stepViewModel.filterChanged(it.toString()) }
    }
}
