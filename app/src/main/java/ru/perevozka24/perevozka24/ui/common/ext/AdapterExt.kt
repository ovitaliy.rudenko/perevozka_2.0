package ru.perevozka24.perevozka24.ui.common.ext

import com.xwray.groupie.Group
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.GroupieViewHolder

private const val UPDATE_THRESHOLD = 100
fun <VH : GroupieViewHolder> GroupAdapter<VH>.updateOrReplace(
    items: List<Group>,
    onAsyncUpdateListener: (() -> Unit)? = null
) {
    if (items.size > UPDATE_THRESHOLD || itemCount > UPDATE_THRESHOLD) {
        clear()
        addAll(items)
        onAsyncUpdateListener?.invoke()
    } else {
        updateAsync(items, onAsyncUpdateListener)
    }
}
