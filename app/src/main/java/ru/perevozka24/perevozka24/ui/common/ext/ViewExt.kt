package ru.perevozka24.perevozka24.ui.common.ext

import android.view.View
import android.widget.AdapterView
import android.widget.Spinner
import ru.perevozka24.perevozka24.ui.common.widgets.SimpleOnItemSelectedListener

inline fun <T> Spinner.onItemSelected(
    crossinline onSelectionChanged: (selectedValue: T) -> Unit
) {
    onItemSelectedListener = object : SimpleOnItemSelectedListener() {
        @Suppress("UNCHECKED_CAST")
        override fun onItemSelected(parent: AdapterView<*>, view: View?, position: Int, id: Long) {
            onSelectionChanged(parent.adapter.getItem(position) as T)
        }
    }
}

inline fun <T : View> Array<T>.setOnClickListener(crossinline clicked: (View) -> Unit) {
    this.forEach { view ->
        view.setOnClickListener { clicked(it) }
    }
}

var View.visible: Boolean
    get() = visibility == View.VISIBLE
    set(value) = visible(value)

fun View.visible(value: Boolean, invisibility: Int = View.GONE) {
    visibility = if (value) View.VISIBLE else invisibility
}

fun showOnly(toShow: View, vararg toHide: View) {
    toShow.visible = true
    toHide.forEach {
        it.visible = false
    }
}

fun showOnly(root: View, show: Int, vararg toHide: Int) {
    root.findViewById<View>(show).visible = true
    toHide.forEach {
        root.findViewById<View>(it).visible = false
    }
}
