package ru.perevozka24.perevozka24.ui.filters.picker.item

import android.view.View
import com.xwray.groupie.kotlinandroidextensions.GroupieViewHolder
import kotlinx.android.synthetic.main.item_filter_loader_category.view.filterLoaderCategoryText
import kotlinx.android.synthetic.main.item_filter_loader_category_add_order.view.*
import ru.perevozka24.perevozka24.R
import ru.perevozka24.perevozka24.ui.filters.models.CategoryModel

data class CategoryAddOrderItem(val model: CategoryModel, val onClick: View.OnClickListener) :
    BasePickerItem(onClick) {
    override fun getLayout(): Int = R.layout.item_filter_loader_category_add_order
    override fun getId(): Long = model.hashCode().toLong()

    override fun bind(viewHolder: GroupieViewHolder, position: Int) {
        with(viewHolder.itemView) {
            tag = model
            filterLoaderCategoryIcon.setImageResource(model.icon)
            filterLoaderCategoryText.text = model.name
                ?: resources.getString(R.string.filter_category_default)
        }
    }
}
