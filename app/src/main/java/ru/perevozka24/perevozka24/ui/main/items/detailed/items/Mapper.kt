package ru.perevozka24.perevozka24.ui.main.items.detailed.items

import android.content.Context
import com.xwray.groupie.kotlinandroidextensions.Item
import ru.perevozka24.perevozka24.R
import ru.perevozka24.perevozka24.ui.common.items.DividerItem
import ru.perevozka24.perevozka24.ui.main.items.detailed.ItemDetailedAction
import ru.perevozka24.perevozka24.ui.main.items.models.ItemModel

object Mapper {

    fun map(
        context: Context,
        itemModel: ItemModel,
        isMine: Boolean,
        actions: ItemDetailedAction
    ): List<Item> {
        val divider = DividerItem(
            marginTop = R.dimen.item_detailed_vertical_spacing,
            marginBottom = R.dimen.item_detailed_vertical_spacing
        )
        return mutableListOf<Item>().apply {
            add(ItemDetailedHeaderItem(itemModel, !isMine, actions))
            add(ItemDetailedTitleItem(itemModel))
            add(ItemDetailedRatingItem(itemModel))
            add(divider)

            itemModel.userName?.let {
                add(ItemDetailedContactItem(R.drawable.ic_menu_profile, it))
                add(divider)
            }
            add(ItemDetailedContactItem(R.drawable.ic_phone, itemModel.phone) {
                actions.call(itemModel)
            })

            itemModel.costs.takeIf { it.isNotEmpty() }?.let {
                add(divider)
                add(ItemDetailedPriceList(itemModel))
            }

            itemModel.address?.let {
                add(divider)
                add(ItemDetailedVerticalInfo(context.getString(R.string.address), it))
            }

            itemModel.opts.forEach {
                add(divider)
                add(ItemDetailedHorizontalInfo(it.label, it.value))
            }

            itemModel.opts.forEach {
                add(divider)
                add(ItemDetailedHorizontalInfo(it.label, it.value))
            }

            itemModel.text?.let {
                add(divider)
                add(ItemDetailedVerticalInfo(it, null))
            }

            if (itemModel.coordX != null && itemModel.coordY != null) {
                add(ItemDetailedMap(itemModel.coordX, itemModel.coordY))
            }
        }
    }
}
