package ru.perevozka24.perevozka24.ui.main.orders.detailed.dialogs

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.lifecycle.Observer
import kotlinx.android.synthetic.main.dialog_offer_set_price.*
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.kodein
import ru.perevozka24.perevozka24.R
import ru.perevozka24.perevozka24.ui.base.FullScreenDialog
import ru.perevozka24.perevozka24.ui.common.ext.showSnackBar
import ru.perevozka24.perevozka24.ui.common.ext.toast
import ru.perevozka24.perevozka24.ui.common.widgets.form.addTextChangedListener
import ru.perevozka24.perevozka24.ui.main.orders.detailed.Reloadable
import ru.perevozka24.perevozka24.ui.main.orders.models.OfferModel
import ru.perevozka24.perevozka24.ui.viewModel
import ru.perevozka24.perevozka24.utils.trackEvent

class OfferSetPriceDialog : FullScreenDialog(), KodeinAware {

    override val kodein: Kodein by kodein()
    val viewModel: OfferSetPriceViewModel by viewModel()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.dialog_offer_set_price, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val offer: OfferModel = checkNotNull(arguments?.getParcelable(OFFER))
        viewModel.init(offer)
        // field
        dialogOfferSetPriceValue.addTextChangedListener {
            viewModel.setValue(it?.toString() ?: "")
        }
        viewModel.inputFieldError.observe(viewLifecycleOwner, Observer {
            dialogOfferSetPriceValue.error(it)
        })

        // button
        dialogOfferSetPriceConfirm.setOnClickListener {
            trackEvent("order_confirm")
            viewModel.submit()
        }
        dialogOfferSetPriceCancel.setOnClickListener { dismiss() }

        // events
        viewModel.submited.observe(viewLifecycleOwner, Observer {
            toast(getString(R.string.offer_set_price_confirmed))
            dismiss()
        })
        viewModel.error.observe(viewLifecycleOwner, Observer {
            showSnackBar(it)
        })
    }

    override fun onPause() {
        (parentFragment as? Reloadable)?.reload()
        super.onPause()
    }

    companion object {
        private const val OFFER = "offer"
        fun create(offer: OfferModel) = OfferSetPriceDialog().apply {
            arguments = bundleOf(OFFER to offer)
        }
    }
}
