package ru.perevozka24.perevozka24.ui.auth.register.step1

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import ru.perevozka24.perevozka24.data.ResourceProvider
import ru.perevozka24.perevozka24.ui.LiveEvent
import ru.perevozka24.perevozka24.ui.base.BaseViewModel
import ru.perevozka24.perevozka24.ui.common.livedata.postValue
import ru.perevozka24.perevozka24.ui.common.widgets.form.InputField
import ru.perevozka24.perevozka24.ui.common.widgets.form.InputFieldValidator

abstract class BaseRegisterContactViewModel(
    resourceProvider: ResourceProvider
) : BaseViewModel(resourceProvider) {

    abstract val validator: InputFieldValidator<String>

    private val contactField =
        MutableLiveData(
            InputField(
                "",
                validator,
                { contactFieldError.postValue(it) }
            )
        )
    val contactFieldError: LiveData<String> = MutableLiveData()

    val proceedEvent = LiveEvent<String>()

    fun contactChange(value: String) {
        contactField.value?.value = value
    }

    fun proceed() {
        val fields = arrayOf(
            contactField,
        )

        val isValid = fields.map {
            it.value?.validate()
            (it.value?.hasError ?: true).not()
        }
            .all { it }

        if (isValid) {
            val contact = checkNotNull(contactField.value).value
            proceedEvent.postValue(contact)
        }
    }
}
