package ru.perevozka24.perevozka24.ui.common.ext

import android.content.Context
import org.threeten.bp.LocalDate
import org.threeten.bp.LocalDateTime
import org.threeten.bp.format.DateTimeFormatter
import ru.perevozka24.perevozka24.DEFAULT_DATE_FORMAT_2
import ru.perevozka24.perevozka24.R

private val TIME_FORMAT = DateTimeFormatter.ofPattern("HH:mm")
private val DATE_TIME_FORMAT = DateTimeFormatter.ofPattern(DEFAULT_DATE_FORMAT_2)

fun LocalDateTime.formatToDefault(context: Context): String {
    return when {
        isToday() -> context.getString(R.string.today_at, format(TIME_FORMAT))
        isYesterday() -> context.getString(R.string.yesterday_at, format(TIME_FORMAT))
        else -> format(DATE_TIME_FORMAT)
    }
}

fun LocalDateTime.isToday(): Boolean {
    val today = LocalDate.now()
    return today.isEqual(toLocalDate())
}

fun LocalDateTime.isYesterday(): Boolean {
    val yesterday = LocalDate.now().minusDays(1)
    return yesterday.isEqual(toLocalDate())
}
