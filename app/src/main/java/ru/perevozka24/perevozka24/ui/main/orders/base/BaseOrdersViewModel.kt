package ru.perevozka24.perevozka24.ui.main.orders.base

import ru.perevozka24.perevozka24.data.ResourceProvider
import ru.perevozka24.perevozka24.data.features.offers.OffersRepository
import ru.perevozka24.perevozka24.data.features.offers.updateFavoriteValue
import ru.perevozka24.perevozka24.ui.common.livedata.postValue
import ru.perevozka24.perevozka24.ui.main.base.BaseContentViewModel
import ru.perevozka24.perevozka24.ui.main.orders.AddToFavoriteOrderDelegate
import ru.perevozka24.perevozka24.ui.main.orders.models.OfferModel
import ru.perevozka24.perevozka24.ui.main.orders.models.OffersModel

abstract class BaseOrdersViewModel(
    private val repository: OffersRepository,
    resourceProvider: ResourceProvider
) : BaseContentViewModel<OffersModel>(resourceProvider), AddToFavoriteOrderDelegate {

    override fun addToFavorite(model: OfferModel) {
        val itemId = model.id
        val favorite = model.favorite
        val offers = checkNotNull(itemsList.value)
        itemsList.postValue(offers.updateFavoriteValue(itemId, !favorite))
        launchGlobalWork(items) {
            if (favorite) {
                repository.removeFavorite(itemId)
            } else {
                repository.addFavorite(itemId)
            }
        }
    }
}
