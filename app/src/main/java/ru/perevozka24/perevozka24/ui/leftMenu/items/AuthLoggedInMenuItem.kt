package ru.perevozka24.perevozka24.ui.leftMenu.items

import android.view.View
import com.xwray.groupie.kotlinandroidextensions.GroupieViewHolder
import com.xwray.groupie.kotlinandroidextensions.Item
import kotlinx.android.synthetic.main.item_menu_logged_in.view.*
import ru.perevozka24.perevozka24.R
import ru.perevozka24.perevozka24.ui.auth.models.LoginModel

class AuthLoggedInMenuItem(
    private val logout: View.OnClickListener,
    private val login: LoginModel
) : Item() {
    override fun getId(): Long = 0
    override fun getLayout(): Int = R.layout.item_menu_logged_in

    override fun bind(viewHolder: GroupieViewHolder, position: Int) {
        with(viewHolder.itemView) {
            itemMenuLoggedInLogout.setOnClickListener(logout)
            itemMenuLoggedInEmail.text = login.email
            itemMenuLoggedInName.text = login.name
        }
    }
}
