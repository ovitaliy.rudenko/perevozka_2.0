package ru.perevozka24.perevozka24.ui.main.orders.detailed.item

import com.xwray.groupie.kotlinandroidextensions.GroupieViewHolder
import com.xwray.groupie.kotlinandroidextensions.Item
import kotlinx.android.synthetic.main.item_offer_detailed_row.view.*
import ru.perevozka24.perevozka24.R

data class OfferColumnItem(private val title: String, private val value: String) : Item() {

    override fun getId() = 0L
    override fun getLayout(): Int = R.layout.item_offer_detailed_column

    override fun bind(viewHolder: GroupieViewHolder, position: Int) {
        with(viewHolder.itemView) {
            itemOfferRowTitle.text = title
            itemOfferRowValue.text = value
        }
    }
}
