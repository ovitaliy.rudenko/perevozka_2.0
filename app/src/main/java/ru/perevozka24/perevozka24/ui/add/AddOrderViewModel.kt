package ru.perevozka24.perevozka24.ui.add

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import ru.perevozka24.perevozka24.data.ResourceProvider
import ru.perevozka24.perevozka24.data.Result
import ru.perevozka24.perevozka24.data.features.auth.AuthRepository
import ru.perevozka24.perevozka24.data.features.filter.FilterPickerRepository
import ru.perevozka24.perevozka24.data.features.offers.OfferCreateParams
import ru.perevozka24.perevozka24.data.features.offers.OfferRepository
import ru.perevozka24.perevozka24.data.features.upload.UploadRepository
import ru.perevozka24.perevozka24.ui.LiveEvent
import ru.perevozka24.perevozka24.ui.base.BaseViewModel
import ru.perevozka24.perevozka24.ui.common.livedata.postValue
import ru.perevozka24.perevozka24.ui.filters.models.CategoryModel
import ru.perevozka24.perevozka24.ui.filters.models.CityModel
import ru.perevozka24.perevozka24.ui.filters.models.CountryModel
import ru.perevozka24.perevozka24.ui.filters.models.ExecutionPeriodModel
import ru.perevozka24.perevozka24.ui.filters.models.RateTypeModel
import ru.perevozka24.perevozka24.ui.filters.models.RegionModel
import ru.perevozka24.perevozka24.ui.filters.models.SubCategoryModel
import ru.perevozka24.perevozka24.utils.trackEvent

@SuppressWarnings("TooManyFunctions")
class AddOrderViewModel(
    private val offerRepository: OfferRepository,
    private val uploadRepository: UploadRepository,
    private val userRepository: AuthRepository,
    private val pickerRepository: FilterPickerRepository,
    resourceProvider: ResourceProvider
) : BaseViewModel(resourceProvider) {

    private var steps: Int = 0
    private var currentStep: Int = 0
    val stepNext = LiveEvent<Int>()

    val loading: LiveData<Boolean> = MutableLiveData(false)
    val saved: LiveData<Boolean> = MutableLiveData()

    var categoryModel: CategoryModel? = null
    var subCategoryModel: SubCategoryModel? = null
    var countryFrom: CountryModel? = null
    var regionFrom: RegionModel? = null
    var cityFrom: CityModel? = null
    var addressFrom: String? = null
    var countryTo: CountryModel? = null
    var regionTo: RegionModel? = null
    var cityTo: CityModel? = null
    var addressTo: String? = null
    var task: String? = null
    var photos: Array<String>? = null
    var rateAmount: String? = null
    var rateType: RateTypeModel? = null
    var username: String? = null
    var phoneNumber: String? = null
    var userId: String? = null
    var periodModel: ExecutionPeriodModel? = null

    fun init(category: String?, subcategory: String?) {
        stepNext.postValue(0)
        if (category != null) {
            GlobalScope.launch {
                categoryModel = pickerRepository.getCategories().first { it.id == category }
                delay(STEP_UPDATE_DELAY)
                stepNext.postValue(1)
                if (subcategory != null) {
                    GlobalScope.launch {
                        val value = safe {
                            pickerRepository.getSubCategories(0, category)
                                .first { it.id == subcategory }
                        }
                        value?.let {
                            subCategoryModel = it
                            delay(STEP_UPDATE_DELAY)
                            stepNext.postValue(2)
                        }
                    }
                }
            }
        }
    }

    fun setStepsCount(count: Int) {
        steps = count
    }

    fun stepChanged(step: Int) {
        val prevStep = currentStep
        currentStep = step

        if (prevStep < step) {
            stepNext.postValue(step)
        }
    }

    fun next() {
        if (currentStep < steps - 1) {
            stepNext.postValue(++currentStep)
        } else {
            submit()
        }
    }

    fun categorySelected(category: CategoryModel) {
        categoryModel = category
        trackEvent("order_main_category")
        next()
    }

    fun subCategorySelected(subCategory: SubCategoryModel) {
        subCategoryModel = subCategory
        trackEvent("order_category")
        next()
    }

    fun locationSelected(
        country: CountryModel,
        region: RegionModel,
        city: CityModel,
        address: String
    ) {
        countryFrom = country
        regionFrom = region
        cityFrom = city
        addressFrom = address
        trackEvent("order_address")
        next()
    }

    fun locationToSelected(
        country: CountryModel,
        region: RegionModel,
        city: CityModel,
        address: String
    ) {
        countryTo = country
        regionTo = region
        cityTo = city
        addressTo = address
        trackEvent("order_address_to")
        next()
    }

    fun photoSelected(photos: Array<String>?) {
        this.photos = photos
        trackEvent("order_photo")
        next()
    }

    fun taskSelected(task: String) {
        this.task = task
        trackEvent("order_desc")
        next()
    }

    fun extraInfoSelected(period: ExecutionPeriodModel, price: String?, rate: RateTypeModel?) {
        periodModel = period
        this.rateAmount = price
        this.rateType = rate
        trackEvent("order_add_info")
        next()
    }

    fun contactsSelected(username: String, phoneNumber: String) {
        this.username = username
        this.phoneNumber = phoneNumber
        trackEvent("order_contacts")
        next()
    }

    private fun submit() {
        loading.postValue(true)
        GlobalScope.launch {
            trackEvent("order_placed")
            val result = safeExecute {

                val images = photos?.let {
                    it.map { image -> uploadRepository.uploadOfferImage(image) }
                        .map { path -> path.substringAfterLast("/") }
                } ?: emptyList()

                val params = OfferCreateParams(
                    toUserId = userId,
                    categoryId = subCategoryModel?.id,

                    country = countryFrom?.id,
                    regionId = regionFrom?.id,
                    cityId = cityFrom?.id,
                    address = addressFrom,
                    countryTo = countryTo?.id,
                    regionIdTo = regionTo?.id,
                    cityIdTo = cityTo?.id,
                    addressTo = addressTo,

                    offer = task,

                    defaultImage = images.takeIf { it.isNotEmpty() }?.firstOrNull(),
                    images = images,

                    paymentRate = rateAmount,
                    paymentType = rateType?.id,

                    name = username,
                    phone = phoneNumber,

                    periodType = periodModel?.type?.id,
                    periodDate1 = periodModel?.date,
                    periodDate2 = periodModel?.date2
                )

                offerRepository.createOffer(params)
            }

            if (result is Result.Success) {
                saved.postValue(userRepository.isAuthorized())
            } else if (result is Result.Error) {
                error.postValue(result.message)
            }

            loading.postValue(false)
        }
    }

    private companion object {
        const val STEP_UPDATE_DELAY = 500L
    }
}
