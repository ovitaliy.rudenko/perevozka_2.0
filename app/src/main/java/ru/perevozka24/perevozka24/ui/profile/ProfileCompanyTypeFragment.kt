package ru.perevozka24.perevozka24.ui.profile

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.GroupieViewHolder
import kotlinx.android.synthetic.main.fragment_profile_company_type.*
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.kodein
import ru.perevozka24.perevozka24.R
import ru.perevozka24.perevozka24.ui.filters.picker.dialog.CheckedItem
import ru.perevozka24.perevozka24.ui.profile.models.ProfileCompanyType

class ProfileCompanyTypeFragment : Fragment(R.layout.fragment_profile_company_type), KodeinAware {

    override val kodein: Kodein by kodein()
    val viewModel: ProfileViewModel by lazy {
        (activity as ProfileActivity).viewModel
    }

    private val adapter = GroupAdapter<GroupieViewHolder>()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        profileCompanyType.adapter = adapter
        viewModel.profileData.observe(viewLifecycleOwner, Observer { pr ->
            val items = mutableListOf(
                ProfileCompanyType.Company,
                ProfileCompanyType.Entrepreneur,
                ProfileCompanyType.Individual
            ).map {
                CheckedItem(it, pr.mode == it.value) { v, _ ->
                    viewModel.modeChanged(v.value)
                }
            }

            adapter.update(items)
            profileSaveCompanyType.isEnabled = pr.mode != null
        })

        profileSaveCompanyType.setOnClickListener {
            viewModel.next()
        }
    }
}
