package ru.perevozka24.perevozka24.ui.distance.result

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import com.yandex.mapkit.geometry.BoundingBox
import com.yandex.mapkit.geometry.Point
import com.yandex.mapkit.geometry.Polyline
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import ru.perevozka24.perevozka24.data.features.filter.FilterRepository
import ru.perevozka24.perevozka24.ui.LiveEvent
import ru.perevozka24.perevozka24.ui.common.livedata.postValue
import java.text.DecimalFormat
import java.util.Locale
import kotlin.math.ceil

class DistanceResultViewModel(
    private val filterRepository: FilterRepository
) : ViewModel() {

    val cities: LiveData<Pair<String, String>> = MutableLiveData()
    val durationString = MutableLiveData<String>()
    val durationWithTrafficJamString = MutableLiveData<String>()
    val distanceString = MutableLiveData<String>()
    val route = MutableLiveData<Polyline>()
    val bounds = Transformations.map(route) { r ->
        val list = r.points
        val north = list.map { it.latitude }.maxOrNull() ?: 0.0
        val east = list.map { it.longitude }.maxOrNull() ?: 0.0
        val south = list.map { it.latitude }.minOrNull() ?: 0.0
        val west = list.map { it.longitude }.minOrNull() ?: 0.0

        BoundingBox(Point(south, west), Point(north, east))
    }
    lateinit var result: DistanceCalculationResult

    val start: LiveData<Point> = Transformations.map(route) {
        it.points.first()
    }
    val end: LiveData<Point> = Transformations.map(route) {
        it.points.last()
    }

    val consumptionString: LiveData<String?> = MutableLiveData<String?>()
    val priceString: LiveData<String?> = MutableLiveData<String?>()
    val showCargoScreen = LiveEvent<Unit>()

    fun init(result: DistanceCalculationResult) {
        this.result = result
        cities.postValue(
            Pair(
                checkNotNull(result.startRegionCity.city?.name),
                checkNotNull(result.endRegionCity.city?.name)
            )
        )
        durationString.postValue(result.duration)
        durationWithTrafficJamString.postValue(result.durationWithTrafficJam)
        distanceString.postValue(result.distanceText)

        val decimalFormat = DecimalFormat("#.#")

        val calculatedConsumption = result.consumption?.let {
            calculateConsumption(it, result.distanceValue)
        }

        consumptionString.postValue(
            calculatedConsumption?.let {
                String.format(
                    Locale.getDefault(), "%d л.",
                    calculatedConsumption.toInt()
                )
            }

        )

        priceString.postValue(
            calculatedConsumption?.let { con ->
                result.price?.let { p ->
                    String.format(
                        Locale.getDefault(), "%s руб.",
                        decimalFormat.format(con * p)
                    )
                }
            }
        )

        route.postValue(Polyline(result.route))
    }

    fun saveSearch() {
        GlobalScope.launch {
            filterRepository.updateCargoRoutes(
                result.startCountry,
                result.startRegionCity.region,
                result.endCountry,
                result.endRegionCity.region
            )

            showCargoScreen.postValue(Unit)
        }
    }

    private companion object {
        const val KM = 1000.0
        const val PER_LITTER = 100.0
        fun calculateConsumption(consumption: Double, distance: Double): Double {
            val result = consumption / PER_LITTER * distance / KM
            return ceil(result)
        }
    }
}
