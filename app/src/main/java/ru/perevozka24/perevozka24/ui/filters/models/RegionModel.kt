package ru.perevozka24.perevozka24.ui.filters.models

import kotlinx.android.parcel.Parcelize

@Parcelize
data class RegionModel(
    val id: String?,
    val name: String?,
    val countryId: String?,
    val direction: Direction
) : PickerModel {
    companion object {
        fun default(countryId: String?, direction: Direction = Direction.NONE) = RegionModel(
            id = null,
            name = null,
            countryId = countryId,
            direction = direction
        )
    }
}
