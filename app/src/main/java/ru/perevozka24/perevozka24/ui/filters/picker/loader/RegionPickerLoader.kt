package ru.perevozka24.perevozka24.ui.filters.picker.loader

import ru.perevozka24.perevozka24.data.features.filter.FilterLocationPickerRepository
import ru.perevozka24.perevozka24.ui.filters.models.PickerModel
import ru.perevozka24.perevozka24.ui.filters.models.RegionModel

class RegionPickerLoader(
    private val current: RegionModel,
    private val pickerRepository: FilterLocationPickerRepository
) : PickerLoader<RegionModel> {
    override suspend fun load(): List<RegionModel> {
        val id = checkNotNull(current.countryId)
        return pickerRepository.getRegions(id).map { it.copy(direction = current.direction) }
    }

    override fun <T : PickerModel> filter(list: List<T>, q: String): List<RegionModel> {
        return list.filter {
            it as RegionModel
            it.name?.contains(q, true) == true
        }.map { it as RegionModel }
    }
}
