package ru.perevozka24.perevozka24.ui.main.orders.detailed.item

import com.xwray.groupie.kotlinandroidextensions.GroupieViewHolder
import com.xwray.groupie.kotlinandroidextensions.Item
import kotlinx.android.synthetic.main.item_offer_detailed_buttons.view.*
import ru.perevozka24.perevozka24.R
import ru.perevozka24.perevozka24.ui.main.orders.detailed.OfferDetailedAction
import ru.perevozka24.perevozka24.ui.main.orders.models.OfferModel

data class OfferButtonsItem(
    private val offer: OfferModel,
    private val actions: OfferDetailedAction
) : Item() {

    override fun getId() = 0L
    override fun getLayout(): Int = R.layout.item_offer_detailed_buttons

    override fun bind(viewHolder: GroupieViewHolder, position: Int) {
        with(viewHolder.itemView) {
            itemOfferPay.setOnClickListener { actions.pay(offer) }
        }
    }
}
