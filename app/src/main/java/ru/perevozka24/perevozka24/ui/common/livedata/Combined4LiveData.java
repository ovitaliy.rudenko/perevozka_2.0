package ru.perevozka24.perevozka24.ui.common.livedata;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MediatorLiveData;

import kotlin.jvm.functions.Function4;
import timber.log.Timber;

@SuppressWarnings("GenericsNaming")
public class Combined4LiveData<A, B, C, D, R> extends MediatorLiveData<R> {

    private final Function4<A, B, C, D, R> combine;
    private A t1 = null;
    private B t2 = null;
    private C t3 = null;
    private D t4 = null;

    public Combined4LiveData(
            LiveData<A> t1LiveData,
            LiveData<B> t2LiveData,
            LiveData<C> t3LiveData,
            LiveData<D> t4LiveData,
            Function4<A, B, C, D, R> combine
    ) {
        this.combine = combine;

        addSource(t1LiveData, t -> {
            t1 = t;
            notifyOnChange();
        });

        addSource(t2LiveData, t -> {
            t2 = t;
            notifyOnChange();
        });

        addSource(t3LiveData, t -> {
            t3 = t;
            notifyOnChange();
        });
        addSource(t4LiveData, t -> {
            t4 = t;
            notifyOnChange();
        });
    }

    private void notifyOnChange() {
        try {
            postValue(combine.invoke(t1, t2, t3, t4));
        } catch (IllegalArgumentException e) {
            Timber.e(e);
        }
    }
}
