package ru.perevozka24.perevozka24.ui.add.extra

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.liveData
import androidx.lifecycle.map
import ru.perevozka24.perevozka24.R
import ru.perevozka24.perevozka24.data.ResourceProvider
import ru.perevozka24.perevozka24.data.features.filter.FilterPickerRepository
import ru.perevozka24.perevozka24.ui.LiveEvent
import ru.perevozka24.perevozka24.ui.base.BaseViewModel
import ru.perevozka24.perevozka24.ui.common.livedata.distinctUntilChanged
import ru.perevozka24.perevozka24.ui.common.livedata.postValue
import ru.perevozka24.perevozka24.ui.common.widgets.form.EmptyFieldValidator
import ru.perevozka24.perevozka24.ui.common.widgets.form.InputField
import ru.perevozka24.perevozka24.ui.filters.models.ExecutionPeriodType
import ru.perevozka24.perevozka24.ui.filters.models.RateTypeModel

class ExtraInfoStepViewModel(
    pickerRepository: FilterPickerRepository,
    resourceProvider: ResourceProvider
) : BaseViewModel(resourceProvider) {

    val rateTypes: LiveData<List<RateTypeModel>> = liveData {
        val rates = pickerRepository.getRate()
        selectedType.postValue(rates[0])
        emit(rates)
    }

    val periodType: LiveData<ExecutionPeriodType> = MutableLiveData(ExecutionPeriodType.ANY_TIME)
        .distinctUntilChanged()

    val date1FieldError: LiveData<String?> = MutableLiveData()
    val date1Field = MutableLiveData(
        InputField(
            "",
            EmptyFieldValidator(),
            { e -> date1FieldError.postValue(e) }
        )
    )

    val date2FieldError: LiveData<String?> = MutableLiveData()
    val date2Field = MutableLiveData(
        InputField(
            "",
            EmptyFieldValidator(),
            { e -> date2FieldError.postValue(e) }
        )
    )

    val dateFieldVisible = periodType.map {
        it != ExecutionPeriodType.ANY_TIME
    }
    val dateTitle = periodType.map {
        when (it) {
            ExecutionPeriodType.PERIOD -> R.string.create_order_step_extra_info_period_from
            else -> R.string.create_order_step_extra_info_date
        }
    }
    val dateField2Visible = periodType.map {
        it == ExecutionPeriodType.PERIOD
    }

    val selectedPrice: LiveData<String> = MutableLiveData()
    val selectedType: LiveData<RateTypeModel> = MutableLiveData()

    val moveNext = LiveEvent<Unit>()

    fun typeChanged(type: RateTypeModel) {
        selectedType.postValue(type)
    }

    fun priceChanged(price: String) {
        selectedPrice.postValue(price)
    }

    fun changeExecutionType(type: ExecutionPeriodType) {
        if (periodType.value == type) return
        periodType.postValue(type)
        if (type == ExecutionPeriodType.ANY_TIME) {
            date1Changed(null)
        }
        date2Changed(null)
    }

    fun date1Changed(date: String?) {
        date1FieldError.postValue(null)
        date1Field.value = date1Field.value?.copy(date ?: "")
    }

    fun date2Changed(date: String?) {
        date2FieldError.postValue(null)
        date2Field.value = date2Field.value?.copy(date ?: "")
    }

    fun onNextClicked() {
        val fieldsToValidate = mutableListOf<MutableLiveData<InputField<String>>>()

        if (periodType.value != ExecutionPeriodType.ANY_TIME) {
            fieldsToValidate.add(date1Field)
        }
        if (periodType.value == ExecutionPeriodType.PERIOD) {
            fieldsToValidate.add(date2Field)
        }

        val isValid = fieldsToValidate.map {
            it.value?.validate()
            (it.value?.hasError ?: true).not()
        }
            .all { it }

        if (isValid) {
            moveNext.postValue(Unit)
        }
    }
}
