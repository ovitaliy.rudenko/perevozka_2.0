package ru.perevozka24.perevozka24.ui.common.widgets.form

import android.content.Context
import android.text.Editable
import android.text.TextWatcher
import android.util.AttributeSet
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.inputmethod.EditorInfo
import android.widget.TextView
import androidx.annotation.StyleableRes
import androidx.constraintlayout.widget.ConstraintLayout
import ru.perevozka24.perevozka24.R
import ru.perevozka24.perevozka24.ui.common.ext.setVisibleText
import ru.perevozka24.perevozka24.ui.common.ext.visible
import ru.perevozka24.perevozka24.ui.common.widgets.HiddenHintEditText

class InputFieldView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : ConstraintLayout(context, attrs, defStyleAttr) {
    val xmlns = "http://schemas.android.com/apk/res/android"

    var text: CharSequence?
        get() = findViewById<HiddenHintEditText>(R.id.editView).text ?: ""
        set(value) {
            findViewById<HiddenHintEditText>(R.id.editView).setText(value)
        }

    var hint: CharSequence?
        get() = findViewById<HiddenHintEditText>(R.id.editView).hint ?: ""
        set(value) {
            findViewById<HiddenHintEditText>(R.id.editView).setCustomHint(value)
        }

    var title: CharSequence?
        get() = findViewById<TextView>(R.id.titleView).text
        set(value) {
            findViewById<TextView>(R.id.titleView).text = value
        }

    val editText: HiddenHintEditText
        get() = findViewById(R.id.editView)

    var inputType: Int
        get() = editText.inputType
        set(value) {
            editText.inputType = value
        }

    init {
        LayoutInflater.from(context).inflate(R.layout.merge_input_field, this, true)

        val set = intArrayOf(
            android.R.attr.title,
            android.R.attr.inputType,
            android.R.attr.hint,
            R.attr.unit,
            android.R.attr.lines
        )

        val a = context.obtainStyledAttributes(attrs, set)
        val title = a.getText(TITLE_INDEX)
        val hint = a.getString(HINT_INDEX) ?: attrs?.getAttributeValue(xmlns, "hint")
            ?.let {
                if (it.startsWith("@")) {
                    resources.getString(it.replace("@", "").toInt())
                } else {
                    it
                }
            }
        val input = a.getInt(INPUT_TYPE_INDEX, EditorInfo.TYPE_NULL)
        val unit = a.getString(UNIT_INDEX)
        val lines = a.getInt(LINES_INDEX, 1)
        a.recycle()

        this.title = title
        this.inputType = input
        with(editText) {
            setCustomHint(hint)
            setLines(lines)
            if (lines > 1) {
                gravity = Gravity.TOP
            }
        }
        arrayOf<View>(
            findViewById(R.id.unitDividerView),
            findViewById(R.id.unitView)
        ).setVisibleText(unit)
        findViewById<View>(R.id.errorView).visible = false
    }

    fun error(msg: CharSequence?) {
        findViewById<HiddenHintEditText>(R.id.errorView).setVisibleText(msg)
    }

    fun addTextChangedListener(watcher: TextWatcher) {
        findViewById<HiddenHintEditText>(R.id.editView).addTextChangedListener(watcher)
    }

    fun removeTextChangedListener(watcher: TextWatcher) {
        findViewById<HiddenHintEditText>(R.id.editView).removeTextChangedListener(watcher)
    }

    private companion object {
        @StyleableRes
        const val TITLE_INDEX = 0

        @StyleableRes
        const val INPUT_TYPE_INDEX = 1

        @StyleableRes
        const val HINT_INDEX = 2

        @StyleableRes
        const val UNIT_INDEX = 3

        @StyleableRes
        const val LINES_INDEX = 4
    }
}

inline fun InputFieldView.addTextChangedListener(
    crossinline beforeTextChanged: (
        text: CharSequence?,
        start: Int,
        count: Int,
        after: Int
    ) -> Unit = { _, _, _, _ -> },
    crossinline onTextChanged: (
        text: CharSequence?,
        start: Int,
        count: Int,
        after: Int
    ) -> Unit = { _, _, _, _ -> },
    crossinline afterTextChanged: (text: Editable?) -> Unit = {}
): TextWatcher {
    val textWatcher = object : TextWatcher {
        override fun afterTextChanged(s: Editable?) {
            afterTextChanged.invoke(s)
        }

        override fun beforeTextChanged(text: CharSequence?, start: Int, count: Int, after: Int) {
            beforeTextChanged.invoke(text, start, count, after)
        }

        override fun onTextChanged(text: CharSequence?, start: Int, before: Int, count: Int) {
            onTextChanged.invoke(text, start, before, count)
        }
    }
    addTextChangedListener(textWatcher)

    return textWatcher
}
