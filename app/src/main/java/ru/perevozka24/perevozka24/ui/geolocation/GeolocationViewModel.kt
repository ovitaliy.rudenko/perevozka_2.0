package ru.perevozka24.perevozka24.ui.geolocation

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import ru.perevozka24.perevozka24.data.ResourceProvider
import ru.perevozka24.perevozka24.data.Result
import ru.perevozka24.perevozka24.data.features.geolocation.LocationTrackerRepository
import ru.perevozka24.perevozka24.ui.base.BaseViewModel
import ru.perevozka24.perevozka24.ui.common.livedata.Combined2LiveData
import ru.perevozka24.perevozka24.ui.common.livedata.distinctUntilChanged
import ru.perevozka24.perevozka24.ui.common.livedata.postValue
import java.util.concurrent.atomic.AtomicReference

class GeolocationViewModel(
    private val repository: LocationTrackerRepository,
    resourceProvider: ResourceProvider
) : BaseViewModel(resourceProvider) {

    val enabled: LiveData<Boolean> = MutableLiveData()

    val result: LiveData<Result<AtomicReference<GeolocationModel>>> = MutableLiveData()

    val enableEvent: LiveData<Boolean> = Combined2LiveData(enabled, result) { e, r ->
        val selected = (r as? Result.Success)?.data?.get()
        e == true && selected != null
    }.distinctUntilChanged()

    init {
        load()
    }

    fun load(progress: Boolean = true) {
        doWork(result, progress) {
            enabled.postValue(repository.isEnabled())
            AtomicReference<GeolocationModel>(repository.getCurrent())
        }
    }

    fun setEnabledChanged(enabled: Boolean) {
        GlobalScope.launch {
            repository.setEnabled(enabled)
            load(false)
        }
    }

    fun setCurrentItem(item: GeolocationModel?) {
        GlobalScope.launch {
            repository.selectItem(item?.id)
            load(false)
        }
    }
}
