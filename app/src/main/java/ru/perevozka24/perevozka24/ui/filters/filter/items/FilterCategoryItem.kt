package ru.perevozka24.perevozka24.ui.filters.filter.items

import com.xwray.groupie.kotlinandroidextensions.GroupieViewHolder
import kotlinx.android.synthetic.main.item_filter_category.view.*
import ru.perevozka24.perevozka24.ICON_DEFAULT
import ru.perevozka24.perevozka24.R
import ru.perevozka24.perevozka24.ui.common.ext.load
import ru.perevozka24.perevozka24.ui.filters.filter.FilterItem
import ru.perevozka24.perevozka24.ui.filters.models.CategoryModel

data class FilterCategoryItem(
    val category: CategoryModel,
    override val defaultTitle: Int,
    override val onClick: (Any?) -> Unit
) : FilterItem(category, defaultTitle, onClick) {
    override fun getId(): Long = 0
    override fun getLayout(): Int = R.layout.item_filter_category

    override fun bind(viewHolder: GroupieViewHolder, position: Int) {
        super.bind(viewHolder, position)
        with(viewHolder.itemView) {
            categoryIcon.load(ICON_DEFAULT)
            categoryTitle.text =
                category.name ?: context.getString(defaultTitle)
        }
    }
}
