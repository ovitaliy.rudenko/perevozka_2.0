package ru.perevozka24.perevozka24.ui.filters.picker.loader

import ru.perevozka24.perevozka24.data.features.filter.FilterPickerRepository
import ru.perevozka24.perevozka24.ui.filters.models.CargoBodyModel
import ru.perevozka24.perevozka24.ui.filters.models.PickerModel

class CargoBodyTypeLoader(
    private val pickerRepository: FilterPickerRepository
) : PickerLoader<CargoBodyModel> {
    override suspend fun load(): List<CargoBodyModel> {
        return pickerRepository.getCargoBodyType()
    }

    override fun <T : PickerModel> filter(list: List<T>, q: String): List<CargoBodyModel> =
        list.map { it as CargoBodyModel }
}
