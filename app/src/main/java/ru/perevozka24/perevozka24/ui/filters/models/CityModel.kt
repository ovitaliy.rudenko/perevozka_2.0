package ru.perevozka24.perevozka24.ui.filters.models

import kotlinx.android.parcel.Parcelize

@Parcelize
data class CityModel(
    val id: String?,
    val name: String?,
    val countryId: String?,
    val regionId: String?,
    val direction: Direction
) : PickerModel {
    companion object {
        fun default(
            countryId: String? = null,
            regionId: String? = null,
            direction: Direction = Direction.NONE
        ) =
            CityModel(null, null, countryId, regionId, direction)
    }
}
