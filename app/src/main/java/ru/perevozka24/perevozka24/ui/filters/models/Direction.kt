package ru.perevozka24.perevozka24.ui.filters.models

enum class Direction {
    FROM, TO, NONE
}
