package ru.perevozka24.perevozka24.ui.common

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

private const val DEFAULT_RETRIES = 3
private const val DEFAULT_DELAY = 1000L

class PostRetry(
    private val initialDelay: Long,
    private val delay: Long,
    private val retries: Int,
    private val block: () -> Unit
) {
    @SuppressWarnings("EmptyCatchBlock")
    fun run() {
        GlobalScope.launch {
            for (i in 0..retries) {
                val d = if (i == 0) initialDelay else delay
                withContext(Dispatchers.Default) { delay(d) }
                try {
                    withContext(Dispatchers.Main) { block() }
                    break
                } catch (ignore: Exception) {
                }
            }
        }
    }
}

fun postRetry(
    initialDelay: Long = 0,
    delay: Long = DEFAULT_DELAY,
    retries: Int = DEFAULT_RETRIES,
    block: () -> Unit
) {
    PostRetry(initialDelay, delay, retries, block).run()
}
