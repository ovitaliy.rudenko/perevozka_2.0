package ru.perevozka24.perevozka24.ui.main.base

import android.graphics.Bitmap
import android.graphics.BitmapShader
import android.graphics.Canvas
import android.graphics.Matrix
import android.graphics.Paint
import android.graphics.Shader
import android.graphics.drawable.BitmapDrawable
import androidx.core.content.res.ResourcesCompat
import com.bumptech.glide.load.Key
import com.bumptech.glide.load.engine.bitmap_recycle.BitmapPool
import com.bumptech.glide.load.resource.bitmap.BitmapTransformation
import ru.perevozka24.perevozka24.App
import ru.perevozka24.perevozka24.R
import ru.perevozka24.perevozka24.ui.common.weak
import java.security.MessageDigest
import kotlin.math.max

class MapPinTransformation : BitmapTransformation() {

    override fun updateDiskCacheKey(messageDigest: MessageDigest) {
        messageDigest.update(ID_BYTES)
    }

    override fun transform(
        pool: BitmapPool,
        toTransform: Bitmap,
        outWidth: Int,
        outHeight: Int
    ): Bitmap {
        val pin = Bitmap.createBitmap(pinBg.width, pinBg.height, Bitmap.Config.ARGB_8888)
        val circle = crop(toTransform)
        val canvas = Canvas(pin)

        val scale = pinBg.width.toFloat() * ICON_RATIO / circle.width
        val translate = pinBg.width * (1 - ICON_RATIO) / 2
        val matrix = Matrix().apply {
            setScale(scale, scale)
            postTranslate(translate, translate)
        }

        canvas.drawBitmap(pinBg, Matrix(), null)
        canvas.drawBitmap(circle, matrix, null)

        return Bitmap.createScaledBitmap(pin, outWidth, outHeight, true)
    }

    override fun equals(other: Any?): Boolean {
        return other is MapPinTransformation
    }

    override fun hashCode(): Int {
        return ID.hashCode()
    }

    companion object {
        private const val ICON_RATIO = 0.8f
        private const val ID = "ru.perevozka24.perevozka24.ui.main.base.MapPinTransformation"
        private val ID_BYTES = ID.toByteArray(Key.CHARSET)

        val pinBg: Bitmap by weak {
            val drawable =
                ResourcesCompat.getDrawable(App.instance.resources, R.drawable.map_item_bg, null)
            (drawable as BitmapDrawable).bitmap
        }
        val clusterBg: Bitmap by weak {
            val drawable =
                ResourcesCompat.getDrawable(App.instance.resources, R.drawable.map_cluster_bg, null)
            (drawable as BitmapDrawable).bitmap
        }

        private fun crop(toTransform: Bitmap): Bitmap {
            val side: Int = max(toTransform.width, toTransform.height)
            val halfSide = side / 2f

            val result = Bitmap.createBitmap(side, side, Bitmap.Config.ARGB_8888)
            val canvas = Canvas(result)
            val p = Paint()
            p.shader = BitmapShader(toTransform, Shader.TileMode.CLAMP, Shader.TileMode.CLAMP)
            canvas.drawCircle(halfSide, halfSide, halfSide, p)
            canvas.setBitmap(null)

            return result
        }
    }
}
