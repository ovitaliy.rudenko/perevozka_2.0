package ru.perevozka24.perevozka24.ui.filters.models

import ru.perevozka24.perevozka24.data.features.location.entity.SimpleAddress

data class AddressDataModel(
    val address: SimpleAddress,
    val country: CountryModel?,
    val region: RegionModel?,
    val city: CityModel?
)
