package ru.perevozka24.perevozka24.ui.geolocation

import android.os.Bundle
import androidx.fragment.app.commit
import kotlinx.android.synthetic.main.activity_fragment_container.*
import ru.perevozka24.perevozka24.R
import ru.perevozka24.perevozka24.ui.base.BaseFragmentActivity

class GeolocationActivity : BaseFragmentActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        toolbarTitleView.setText(R.string.geolocation_title)
        if (savedInstanceState == null) {
            supportFragmentManager.commit {
                replace(R.id.container, GeolocationFragment())
            }
        }
    }
}
