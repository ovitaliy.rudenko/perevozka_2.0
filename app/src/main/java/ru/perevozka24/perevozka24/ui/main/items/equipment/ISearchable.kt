package ru.perevozka24.perevozka24.ui.main.items.equipment

import ru.perevozka24.perevozka24.ui.filters.models.FilterModel

interface ISearchable {
    fun search(q: String)
    fun refreshAfterFilterChange(old: FilterModel?, new: FilterModel?)
}
