package ru.perevozka24.perevozka24.ui.main.items.equipment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.navigation.fragment.findNavController
import kotlinx.android.synthetic.main.dialog_equipment.*
import ru.perevozka24.perevozka24.R
import ru.perevozka24.perevozka24.ui.add.AddOrderActivity
import ru.perevozka24.perevozka24.ui.base.FullScreenDialog
import ru.perevozka24.perevozka24.ui.common.ext.call
import ru.perevozka24.perevozka24.ui.main.items.detailed.ItemDetailedActivityDirections
import ru.perevozka24.perevozka24.ui.main.items.models.ItemModel
import ru.perevozka24.perevozka24.ui.main.items.widget.EquipmentItemHelper
import ru.perevozka24.perevozka24.utils.trackEvent

class EquipmentDialog : FullScreenDialog() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.dialog_equipment, container)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val item: ItemModel = checkNotNull(arguments?.getParcelable(ITEM))
        equipmentDialogClose.setOnClickListener { dismiss() }
        view.setOnClickListener { dismiss() }

        EquipmentItemHelper.setUp(equipmentDialogContent, item)
        equipmentDialogCall.setOnClickListener { it.context.call(item.phone) }
        equipmentDialogCreateOrder.setOnClickListener { v ->
            v.context.run {
                trackEvent(
                    "create_order",
                    mapOf("type" to "popup-ad")
                )
                startActivity(
                    AddOrderActivity.create(
                        this,
                        category = arguments?.getString(CATEGORY),
                        subcategory = arguments?.getString(SUB_CATEGORY)
                    )
                )
            }
        }

        equipmentDialogContainer.setOnClickListener {
            findNavController().navigate(
                ItemDetailedActivityDirections.actionGlobalItemDetailedActivity(item)
            )
        }
    }

    companion object {
        private const val ITEM = "item"
        private const val CATEGORY = "category"
        private const val SUB_CATEGORY = "sub_category"

        fun create(item: ItemModel, category: String?, subCategoryId: String?) =
            EquipmentDialog().apply {
                arguments = bundleOf(
                    ITEM to item,
                    CATEGORY to category,
                    SUB_CATEGORY to subCategoryId
                )
            }
    }
}
