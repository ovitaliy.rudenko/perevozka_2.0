package ru.perevozka24.perevozka24.ui.add

import org.kodein.di.Kodein
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.provider
import ru.perevozka24.perevozka24.ui.add.address.AddressStepViewModel
import ru.perevozka24.perevozka24.ui.add.category.CategoryStepViewModel
import ru.perevozka24.perevozka24.ui.add.contacts.ContactsStepViewModel
import ru.perevozka24.perevozka24.ui.add.extra.ExtraInfoStepViewModel
import ru.perevozka24.perevozka24.ui.add.photo.AddPhotoViewModel
import ru.perevozka24.perevozka24.ui.add.subcategory.SubCategoryStepViewModel
import ru.perevozka24.perevozka24.ui.add.task.TaskStepViewModel

val addOrderViewModelModule = Kodein.Module(name = "addOrderViewModelModule") {
    bind<AddOrderViewModel>() with provider {
        AddOrderViewModel(instance(), instance(), instance(), instance(), instance())
    }

    bind<CategoryStepViewModel>() with provider {
        CategoryStepViewModel(instance(), instance(), instance())
    }
    bind<SubCategoryStepViewModel>() with provider {
        SubCategoryStepViewModel(instance(), instance(), instance())
    }
    bind<ExtraInfoStepViewModel>() with provider {
        ExtraInfoStepViewModel(instance(), instance())
    }
    bind<ContactsStepViewModel>() with provider {
        ContactsStepViewModel(instance())
    }

    bind<AddressStepViewModel>() with provider {
        AddressStepViewModel(instance(), instance(), instance())
    }
    bind<TaskStepViewModel>() with provider {
        TaskStepViewModel(instance())
    }
    bind<AddPhotoViewModel>() with provider {
        AddPhotoViewModel(instance())
    }
}
