package ru.perevozka24.perevozka24.ui.support.items

import com.xwray.groupie.kotlinandroidextensions.GroupieViewHolder
import com.xwray.groupie.kotlinandroidextensions.Item
import kotlinx.android.synthetic.main.item_support_content.view.*
import kotlinx.android.synthetic.main.item_support_row.view.*
import ru.perevozka24.perevozka24.R
import ru.perevozka24.perevozka24.ui.common.ext.setVisibleText
import ru.perevozka24.perevozka24.ui.common.ext.visible
import ru.perevozka24.perevozka24.ui.support.SupportItemAction

data class SupportRowItem(
    private val icon: Int,
    private val text: Int,
    private val action: Int?,
    private val itemAction: SupportItemAction
) : Item() {
    override fun getId() = 0L
    override fun getLayout() = R.layout.item_support_row
    override fun bind(viewHolder: GroupieViewHolder, position: Int) {
        with(viewHolder.itemView) {
            itemSupportIcon.setImageResource(icon)
            itemSupportText.text = resources.getString(text)
            if (action != null) {
                itemSupportAction.setVisibleText(resources.getString(action))
                itemSupportAction.setOnClickListener { itemAction.action(text) }
            } else {
                itemSupportAction.visible = false
            }
        }
    }
}
