package ru.perevozka24.perevozka24.ui.filters.picker

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_fragment_container.backButton
import kotlinx.android.synthetic.main.activity_line_picker.*
import org.kodein.di.android.kodein
import ru.perevozka24.perevozka24.R
import ru.perevozka24.perevozka24.ui.base.BaseActivity

class AddressLinePickerActivity : BaseActivity() {

    override val kodein by kodein()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_line_picker)
        backButton.setOnClickListener { onBackPressed() }

        addressLineValue.setText(intent.getStringExtra(LINE) ?: "")
        addressLineSave.setOnClickListener {
            setResult(
                Activity.RESULT_OK,
                Intent().apply { putExtra(LINE, addressLineValue.text?.toString()) }
            )
            finish()
        }
    }

    companion object {
        const val LINE = "line"

        fun create(context: Context, line: String): Intent {
            return Intent(context, AddressLinePickerActivity::class.java).apply {
                putExtra(LINE, line)
            }
        }
    }
}
