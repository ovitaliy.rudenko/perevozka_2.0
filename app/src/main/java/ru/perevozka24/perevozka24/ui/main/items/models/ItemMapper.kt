package ru.perevozka24.perevozka24.ui.main.items.models

import ru.perevozka24.perevozka24.data.features.items.entities.ItemEntity
import ru.perevozka24.perevozka24.data.utils.escape
import ru.perevozka24.perevozka24.ui.common.Mapper

object ItemMapper : Mapper<ItemEntity, ItemModel> {
    override fun map(e: ItemEntity): ItemModel {
        return ItemModel(
            id = checkNotNull(e.id),
            name = checkNotNull(e.name).escape(),
            phone = checkNotNull(e.phone),
            userName = e.userName?.escape(),
            userId = e.userId,
            coordX = e.coordX,
            coordY = e.coordY,
            image = e.image,
            opts = e.opts?.let { OptionMapper.map(it.toList()) } ?: emptyList(),
            costs = e.costs?.map { it.escape() }?.toList() ?: emptyList(),
            text = e.text?.escape(),
            rating = e.rating,
            shipment = e.shipment,
            categoryId = e.categoryId,
            parentCategory = e.parentCategory,
            countryId = e.country,
            countryName = e.countryName,
            regionId = e.regionId,
            regionName = e.regionName,
            cityId = e.cityId,
            cityName = e.cityName,
            itemTime = e.itemTime,
            address = e.address?.escape(),
            showOffer = e.showOfferBool,
            favorite = e.favorite,
            categoryIcon = checkNotNull(e.categoryIcon)
        )
    }
}
