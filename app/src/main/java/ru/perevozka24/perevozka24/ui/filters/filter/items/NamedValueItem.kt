package ru.perevozka24.perevozka24.ui.filters.filter.items

import com.xwray.groupie.kotlinandroidextensions.GroupieViewHolder
import com.xwray.groupie.kotlinandroidextensions.Item
import kotlinx.android.synthetic.main.item_filter_named_value.view.*
import ru.perevozka24.perevozka24.R
import ru.perevozka24.perevozka24.ui.base.INameModel

data class NamedValueItem(
    private val title: Int,
    private val icon: Int,
    private val value: INameModel?,
    private val type: Class<*>,
    private val onClick: (type: Class<*>, value: Any?) -> Unit,
    private val defaultValueTitle: Int = 0
) : Item() {
    override fun getId(): Long = title.toLong()
    override fun getLayout(): Int = R.layout.item_filter_named_value

    override fun bind(viewHolder: GroupieViewHolder, position: Int) {
        with(viewHolder.itemView) {
            setOnClickListener {
                onClick(type, value)
            }
            namedIcon.setImageResource(icon)
            namedTitle.setText(title)
            namedTitle2.text = value?.name ?: resources.getString(R.string.filter_status_any)
        }
    }
}
