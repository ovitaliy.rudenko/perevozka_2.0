package ru.perevozka24.perevozka24.ui.main.orders.base

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import ru.perevozka24.perevozka24.DEFAULT_REQUEST_DELAY
import ru.perevozka24.perevozka24.data.ResourceProvider
import ru.perevozka24.perevozka24.data.Result
import ru.perevozka24.perevozka24.data.features.offers.OffersRepository
import ru.perevozka24.perevozka24.data.features.offers.updateFavoriteValue
import ru.perevozka24.perevozka24.ui.common.livedata.postValue
import ru.perevozka24.perevozka24.ui.filters.models.CategorySubCategoryModel
import ru.perevozka24.perevozka24.ui.filters.models.FilterModel
import ru.perevozka24.perevozka24.ui.main.ViewMode
import ru.perevozka24.perevozka24.ui.main.base.BaseContentViewModel
import ru.perevozka24.perevozka24.ui.main.orders.AddToFavoriteOrderDelegate
import ru.perevozka24.perevozka24.ui.main.orders.models.OfferModel
import ru.perevozka24.perevozka24.ui.main.orders.models.OffersModel

@SuppressWarnings("TooManyFunctions")
abstract class BaseOrderListViewModel(
    private val offersRepository: OffersRepository,
    resourceProvider: ResourceProvider
) : BaseContentViewModel<OffersModel>(resourceProvider), AddToFavoriteOrderDelegate {

    val mode: LiveData<ViewMode> = MutableLiveData()
    val changeModeVisible: LiveData<Boolean> = MutableLiveData()

    val itemsModel: LiveData<OrderListModel> = Transformations.map(itemsList) {
        OrderListModel(
            list = it.data,
            isLast = query.isNotEmpty() || (it.data.isEmpty() || it.data.size % PAGE_SIZE > 0)
        )
    }

    abstract suspend fun loadPage(offset: Int): OffersModel

    private var loadingJob: Job? = null

    override fun refresh() {
        load(0)
    }

    fun loadNext() {
        if (query.isEmpty()) {
            load(itemsList.value?.data?.size ?: 0)
        }
    }

    fun load(offset: Int) {
        if (items.value is Result.Loading) return
        if (offset == 0) items.postValue(Result.Loading)
        loadingJob?.cancel()
        loadingJob = GlobalScope.launch {
            val result = safeExecute {
                if (query.isEmpty()) {
                    delay(DEFAULT_REQUEST_DELAY)
                    loadPage(offset)
                } else {
                    offersRepository.search(query)
                }
            }
            val values = itemsList.value?.data?.takeIf { offset != 0 } ?: emptyList()
            if (result is Result.Success) {
                val combined = mutableListOf<OfferModel>().apply {
                    addAll(values)
                    addAll(result.data.data)
                }
                val offers = result.data.copy(data = combined)
                itemsList.postValue(offers)
                items.postValue(result.copy(data = offers))
            } else {
                if (loadingJob?.isCancelled == true) items.postValue(result)
            }
        }
    }

    override fun addToFavorite(model: OfferModel) {
        val itemId = model.id
        val favorite = model.favorite
        val offers = checkNotNull(itemsList.value)
        itemsList.postValue(offers.updateFavoriteValue(itemId, !favorite))
        launchGlobalWork(items) {
            if (favorite) {
                offersRepository.removeFavorite(itemId)
            } else {
                offersRepository.addFavorite(itemId)
            }
        }
    }

    fun removeFromList(offer: OfferModel) {
        val offerList = (itemsList.value?.data ?: emptyList())
            .filter { it.id != offer.id }
        val offers = checkNotNull(itemsList.value).copy(data = offerList)
        itemsList.postValue(offers)
    }

    val categorySubCategory: LiveData<CategorySubCategoryModel> = MutableLiveData()
    abstract suspend fun loadSubCategory(): CategorySubCategoryModel
    abstract suspend fun saveSubCategory(model: CategorySubCategoryModel)
    protected fun updateFilterValue() {
        GlobalScope.launch {
            val subCategory = loadSubCategory()
            categorySubCategory.postValue(subCategory)
        }
    }

    override fun refreshAfterFilterChange(initial: FilterModel?, changed: FilterModel?) {
        super.refreshAfterFilterChange(initial, changed)
        updateFilterValue()
    }

    fun subCategoryChanged(model: CategorySubCategoryModel) {
        GlobalScope.launch {
            saveSubCategory(model)
            refresh()
            updateFilterValue()
        }
    }

    private companion object {
        const val PAGE_SIZE = 30
    }
}
