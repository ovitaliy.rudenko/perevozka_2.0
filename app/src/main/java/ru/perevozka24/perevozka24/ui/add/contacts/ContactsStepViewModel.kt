package ru.perevozka24.perevozka24.ui.add.contacts

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import ru.perevozka24.perevozka24.data.ResourceProvider
import ru.perevozka24.perevozka24.ui.base.BaseViewModel
import ru.perevozka24.perevozka24.ui.common.livedata.Combined2LiveData
import ru.perevozka24.perevozka24.ui.common.livedata.postValue

class ContactsStepViewModel(
    resourceProvider: ResourceProvider
) : BaseViewModel(resourceProvider) {

    private val username: LiveData<String> = MutableLiveData("")
    private val phoneNumber: LiveData<String> = MutableLiveData("")

    val enabled = Combined2LiveData(username, phoneNumber) { u, p ->
        u.isNullOrEmpty().not() && p.isNullOrEmpty().not()
    }

    fun usernameChanged(value: String) {
        username.postValue(value)
    }

    fun phoneNumberChanged(value: String) {
        phoneNumber.postValue(value)
    }
}
