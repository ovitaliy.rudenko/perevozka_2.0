package ru.perevozka24.perevozka24.ui.common.ext

import timber.log.Timber

@SuppressWarnings("TooGenericExceptionCaught")
inline fun <T : Any> safe(
    silent: Boolean = false,
    crossinline block: () -> T?

): T? {
    return try {
        block()
    } catch (ex: Exception) {
        if (!silent) Timber.e(ex)
        null
    }
}

@SuppressWarnings("TooGenericExceptionCaught")
suspend inline fun <T : Any> safeSuspended(
    crossinline block: suspend () -> T?
): T? {
    return try {
        block()
    } catch (ex: Exception) {
        Timber.e(ex)
        null
    }
}
