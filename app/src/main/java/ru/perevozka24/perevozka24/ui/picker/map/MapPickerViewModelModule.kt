package ru.perevozka24.perevozka24.ui.picker.map

import org.kodein.di.Kodein
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.singleton

val mapPickerViewModelModule = Kodein.Module(name = "mapPickerViewModelModule") {
    bind<MapPickerViewModel>() with singleton {
        MapPickerViewModel(instance(), instance())
    }
}
