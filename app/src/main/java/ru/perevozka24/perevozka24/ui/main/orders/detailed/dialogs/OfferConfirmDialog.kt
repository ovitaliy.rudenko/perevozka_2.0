package ru.perevozka24.perevozka24.ui.main.orders.detailed.dialogs

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.core.text.HtmlCompat
import kotlinx.android.synthetic.main.dialog_offer_confirm.*
import ru.perevozka24.perevozka24.R
import ru.perevozka24.perevozka24.ui.base.FullScreenDialog
import ru.perevozka24.perevozka24.ui.common.ext.call
import ru.perevozka24.perevozka24.ui.main.orders.models.OfferModel
import ru.perevozka24.perevozka24.utils.trackEvent

class OfferConfirmDialog : FullScreenDialog() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.dialog_offer_confirm, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val offer: OfferModel = checkNotNull(arguments?.getParcelable(OFFER))
        val contacts = checkNotNull(offer.contacts)
        val duration = DURATION // Duration.ofMillis(offer.remainingTime * SECOND - System.currentTimeMillis())
        dialogOfferConfirmCall.setOnClickListener {
            context?.call(contacts)
            trackEvent("call_from_popup")
        }
        dialogOfferConfirmText.text = HtmlCompat.fromHtml(
            getString(R.string.offer_confirm_message, duration/* duration.toMinutes()*/),
            HtmlCompat.FROM_HTML_MODE_LEGACY
        )
        dialogOfferConfirmClose.setOnClickListener { dismiss() }
    }

    companion object {
        private const val OFFER = "offer"
        private const val DURATION = 15
        fun create(offer: OfferModel) = OfferConfirmDialog().apply {
            arguments = bundleOf(OFFER to offer)
        }
    }
}
