package ru.perevozka24.perevozka24.ui.main.toolbar

import android.content.Context
import androidx.annotation.StringRes

sealed class ToolbarMode {
    class Search(@StringRes private val hint: Int) : ToolbarMode() {
        override fun crateToolbarView(context: Context) = ToolbarSearchView(context, hint)
    }

    class Title(@StringRes private val title: Int) : ToolbarMode() {
        override fun crateToolbarView(context: Context) = ToolbarTitleView(context, title)
    }

    abstract fun crateToolbarView(context: Context): ToolbarView
}
