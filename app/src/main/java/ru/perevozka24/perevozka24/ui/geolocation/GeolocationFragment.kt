package ru.perevozka24.perevozka24.ui.geolocation

import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import kotlinx.android.synthetic.main.fragment_geolocation.*
import org.kodein.di.Kodein
import org.kodein.di.android.x.kodein
import ru.perevozka24.perevozka24.R
import ru.perevozka24.perevozka24.data.Result
import ru.perevozka24.perevozka24.services.geolocation.LocationTrackerWorker
import ru.perevozka24.perevozka24.ui.base.BaseContentLoaderFragment
import ru.perevozka24.perevozka24.ui.common.widgets.onCheckChanged
import ru.perevozka24.perevozka24.ui.filters.models.PickerModel
import ru.perevozka24.perevozka24.ui.filters.picker.dialog.FilterDialogPickerFragment
import ru.perevozka24.perevozka24.ui.filters.picker.dialog.FilterPickerCheckListener
import ru.perevozka24.perevozka24.ui.notification.item.NotificationEnableBackgroundModeItem
import ru.perevozka24.perevozka24.ui.viewModel
import ru.perevozka24.perevozka24.utils.trackEvent
import java.util.concurrent.atomic.AtomicReference

class GeolocationFragment : BaseContentLoaderFragment(), FilterPickerCheckListener {

    override val kodein: Kodein by kodein()
    val viewModel: GeolocationViewModel by viewModel()

    override val contentLayoutId: Int
        get() = R.layout.fragment_geolocation

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        trackEvent("visited_geolocation")
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        subscribe(viewModel.result)
        NotificationEnableBackgroundModeItem.setupItem(
            geolocationWarn,
            R.string.geolocation_warn_title_1,
            R.string.geolocation_warn_title_2
        )

        viewModel.enabled.observe(viewLifecycleOwner, Observer {
            geolocationEnabled.checked = it
        })
        geolocationEnabled.onCheckChanged {
            viewModel.setEnabledChanged(it)
        }

        viewModel.result.observe(viewLifecycleOwner, Observer {
            val item = (it as? Result.Success)?.data?.get()
            geolocationItemTitle.text = item?.name ?: getString(R.string.geolocation_select)
        })

        geolocationItem.setOnClickListener {
            val item =
                (viewModel.result.value as? Result.Success<AtomicReference<GeolocationModel>>)?.data?.get()
            FilterDialogPickerFragment.create(
                getString(R.string.geolocation_pick_title),
                GeolocationModel::class.java, item
            ).show(childFragmentManager, "picker")
        }

        viewModel.enableEvent.observe(viewLifecycleOwner, Observer {
            if (it) LocationTrackerWorker.start(requireContext())
            else LocationTrackerWorker.cancel(requireContext())
        })
    }

    override fun onFilterPickerChangedListener(
        type: Class<out PickerModel>,
        newValue: PickerModel?
    ) {
        viewModel.setCurrentItem(newValue as? GeolocationModel)
    }
}
