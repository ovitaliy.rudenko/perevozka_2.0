package ru.perevozka24.perevozka24.ui.common.widgets;

import android.text.Editable;
import android.text.TextWatcher;

public abstract class SimpleTextWatcher implements TextWatcher {
    /**
     * {@inheritDoc}
     */
    @Override
    @SuppressWarnings({"PMD.UncommentedEmptyMethodBody",
            "PMD.EmptyMethodInAbstractClassShouldBeAbstract"})
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        // do nothing
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @SuppressWarnings({"PMD.UncommentedEmptyMethodBody",
            "PMD.EmptyMethodInAbstractClassShouldBeAbstract"})
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        // do nothing
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @SuppressWarnings({"PMD.UncommentedEmptyMethodBody",
            "PMD.EmptyMethodInAbstractClassShouldBeAbstract"})
    public void afterTextChanged(Editable editable) {
        // do nothing
    }
}
