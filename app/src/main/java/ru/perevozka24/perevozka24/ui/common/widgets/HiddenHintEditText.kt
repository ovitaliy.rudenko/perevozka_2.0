package ru.perevozka24.perevozka24.ui.common.widgets

import android.content.Context
import android.graphics.Rect
import android.util.AttributeSet
import androidx.appcompat.widget.AppCompatEditText
import java.util.concurrent.atomic.AtomicReference

class HiddenHintEditText @JvmOverloads constructor(context: Context, attrs: AttributeSet? = null) :
    AppCompatEditText(context, attrs) {

    private var hiddenHint: AtomicReference<CharSequence>? = null

    override fun onFocusChanged(focused: Boolean, direction: Int, previouslyFocusedRect: Rect?) {
        super.onFocusChanged(focused, direction, previouslyFocusedRect)

        if (hiddenHint == null) {
            hiddenHint = AtomicReference(hint ?: "")
        }

        hint = if (focused) {
            ""
        } else {
            hiddenHint?.get()
        }
    }

    fun setCustomHint(customHint: CharSequence?) {
        hiddenHint = AtomicReference(customHint ?: "")
        hint = if (hasFocus()) {
            ""
        } else {
            customHint
        }
    }
}
