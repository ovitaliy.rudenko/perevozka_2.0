package ru.perevozka24.perevozka24.ui.main.items.detailed.items

import com.xwray.groupie.kotlinandroidextensions.GroupieViewHolder
import com.xwray.groupie.kotlinandroidextensions.Item
import kotlinx.android.synthetic.main.item_detailed_vertical_info.view.*
import ru.perevozka24.perevozka24.R
import ru.perevozka24.perevozka24.data.utils.escape
import ru.perevozka24.perevozka24.ui.common.ext.setVisibleText

data class ItemDetailedVerticalInfo(
    private val title: String?,
    private val data: String?
) : Item() {

    override fun getId(): Long = 0
    override fun getLayout(): Int = R.layout.item_detailed_vertical_info

    override fun bind(viewHolder: GroupieViewHolder, position: Int) {
        with(viewHolder.itemView) {
            itemDetailedInfoTitle.setVisibleText(title?.escape())
            itemDetailedInfoData.setVisibleText(data?.escape())
        }
    }
}
