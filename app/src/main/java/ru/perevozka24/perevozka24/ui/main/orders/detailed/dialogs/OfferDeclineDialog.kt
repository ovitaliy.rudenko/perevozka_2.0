package ru.perevozka24.perevozka24.ui.main.orders.detailed.dialogs

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.core.widget.addTextChangedListener
import androidx.lifecycle.Observer
import kotlinx.android.synthetic.main.dialog_offer_decline.*
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.kodein
import ru.perevozka24.perevozka24.R
import ru.perevozka24.perevozka24.ui.base.FullScreenDialog
import ru.perevozka24.perevozka24.ui.common.ext.setVisibleText
import ru.perevozka24.perevozka24.ui.common.ext.showSnackBar
import ru.perevozka24.perevozka24.ui.main.orders.detailed.OfferDetailedViewModel
import ru.perevozka24.perevozka24.ui.main.orders.models.OfferModel
import ru.perevozka24.perevozka24.ui.viewModel

class OfferDeclineDialog : FullScreenDialog(), KodeinAware {

    override val kodein: Kodein by kodein()
    val detailed: OfferDetailedViewModel by viewModel()
    val viewModel: OfferDeclineViewModel by viewModel()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.dialog_offer_decline, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val offer: OfferModel = checkNotNull(arguments?.getParcelable(OFFER))
        viewModel.init(offer)
        // field
        dialogOfferDeclineValue.addTextChangedListener {
            viewModel.setValue(it?.toString() ?: "")
        }
        viewModel.inputFieldError.observe(viewLifecycleOwner, Observer {
            dialogOfferDeclineErrorView.setVisibleText(it)
        })

        // button
        dialogOfferDeclineConfirm.setOnClickListener { viewModel.submit() }
        dialogOfferDeclineCancel.setOnClickListener { dismiss() }

        // events
        viewModel.submited.observe(viewLifecycleOwner, Observer {
            detailed.reload()
            dismiss()
        })
        viewModel.error.observe(viewLifecycleOwner, Observer {
            showSnackBar(it)
        })
    }

    companion object {
        private const val OFFER = "offer"
        fun create(offer: OfferModel) = OfferDeclineDialog().apply {
            arguments = bundleOf(OFFER to offer)
        }
    }
}
