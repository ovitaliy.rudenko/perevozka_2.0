package ru.perevozka24.perevozka24.ui.filters.picker.dialog

import android.os.Bundle
import android.view.View
import androidx.core.os.bundleOf
import androidx.lifecycle.Observer
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.GroupieViewHolder
import kotlinx.android.synthetic.main.content_dialog_filter_picker.*
import ru.perevozka24.perevozka24.R
import ru.perevozka24.perevozka24.ui.base.BaseContentLoadingDialogFragment
import ru.perevozka24.perevozka24.ui.base.INameModel
import ru.perevozka24.perevozka24.ui.filters.models.PickerModel
import ru.perevozka24.perevozka24.ui.viewModel
import kotlin.math.min

class FilterDialogPickerFragment : BaseContentLoadingDialogFragment() {

    private val viewModel: FilterDialogPickerViewModel by viewModel()
    override val contentLayoutId: Int = R.layout.content_dialog_filter_picker
    private val adapter = GroupAdapter<GroupieViewHolder>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel.init(
            requireArguments().getSerializable(TYPE) as Class<in PickerModel>,
            requireArguments().getParcelable(CURRENT) as? PickerModel
        )
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        filterDialogTitle.text = requireArguments().getString(TITLE)
        subscribe(viewModel.result)
        filterDialogList.adapter = adapter
        viewModel.items.observe(viewLifecycleOwner, Observer {
            val mapped = it.first.map { item ->
                CheckedItem(
                    item as INameModel,
                    item == it.second
                ) { newValue, checked ->
                    viewModel.select(newValue, checked)
                }
            }
            filterDialogList.layoutParams.height =
                resources.getDimensionPixelOffset(R.dimen.filter_item_height) *
                        min(MAX_ALLOWED_COUNT, mapped.size)
            adapter.update(mapped)
        })

        filterDialogOk.setOnClickListener {
            ((parentFragment ?: activity) as FilterPickerCheckListener)
                .onFilterPickerChangedListener(
                    requireArguments().getSerializable(TYPE) as Class<out PickerModel>,
                    viewModel.selected.value
                )
            dismiss()
        }
        filterDialogCancel.setOnClickListener { dismiss() }
    }

    companion object {
        private const val TITLE = "title"
        private const val TYPE = "type"
        private const val CURRENT = "current"
        private const val CAN_CLEAR = "can_clear"

        private const val MAX_ALLOWED_COUNT = 7

        fun create(title: String, type: Class<out PickerModel>, current: PickerModel?) =
            FilterDialogPickerFragment()
                .apply {
                    arguments = bundleOf(TITLE to title, TYPE to type, CURRENT to current)
                }
    }
}
