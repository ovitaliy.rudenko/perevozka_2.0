package ru.perevozka24.perevozka24.ui.profile

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import androidx.core.widget.doAfterTextChanged
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import com.yandex.metrica.YandexMetrica
import kotlinx.android.synthetic.main.fragment_profile.*
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.kodein
import ru.perevozka24.perevozka24.R
import ru.perevozka24.perevozka24.ui.common.ext.toast
import ru.perevozka24.perevozka24.ui.common.ext.visible
import ru.perevozka24.perevozka24.ui.filters.models.CategoryModel
import ru.perevozka24.perevozka24.ui.filters.models.CityModel
import ru.perevozka24.perevozka24.ui.filters.models.CountryModel
import ru.perevozka24.perevozka24.ui.filters.models.RegionModel
import ru.perevozka24.perevozka24.ui.filters.picker.FilterPickerActivity
import ru.perevozka24.perevozka24.ui.profile.models.ProfileCompanyType
import ru.perevozka24.perevozka24.ui.profile.models.ProfileModel

class ProfileFragment : Fragment(R.layout.fragment_profile), KodeinAware {

    override val kodein: Kodein by kodein()
    val viewModel: ProfileViewModel by lazy {
        (activity as ProfileActivity).viewModel
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        YandexMetrica.reportEvent("visited_profile")
        setHasOptionsMenu(true)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.profile, menu)
        menu.findItem(R.id.profileSave).isVisible = viewModel.changed.value ?: false
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.profileSave -> viewModel.save()
            else -> false
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel.changed.observe(viewLifecycleOwner, Observer {
            activity?.invalidateOptionsMenu()
        })
        viewModel.profileData.observe(viewLifecycleOwner, Observer {
            showProfile(it)
        })
        viewModel.error.observe(viewLifecycleOwner, Observer {
            toast(it)
        })
        viewModel.innEditable.observe(viewLifecycleOwner, Observer {
            profileCodeId.isEnabled = it
        })
        addPickerEvents()
        addTextChangeListeners()
    }

    private fun showProfile(profile: ProfileModel) {
        profileUsername.setSilentText(profile.name)
        profilePhoneValue.setSilentText(profile.phone)
        profileCountryValue.text =
            profile.countryModel.name ?: getString(R.string.filter_country_info_default)

        profileRegionContainer.visible = profile.countryModel.id != null
        profileRegionValue.text =
            profile.regionModel.name ?: getString(R.string.filter_region_info_default)

        profileCityContainer.visible = profile.regionModel.id != null
        profileCityValue.text =
            profile.cityModel.name ?: getString(R.string.filter_city_info_default)
        profileAddressValue.setSilentText(profile.address)
        profileCategoryValue.text =
            profile.categoryModel.name ?: getString(R.string.profile_specialization_missed)
        profileActivityDescriptionValue.setSilentText(profile.description)
        profileCodeId.setSilentText(profile.inn)
        profileKpp.setSilentText(profile.kpp)
        profileOrgn.setSilentText(profile.orgn)

        profileKpp.visible = profile.mode == ProfileCompanyType.Company.value
        profileKppTitle.visible = profileKpp.visible
        profileOrgn.visible = profile.mode != ProfileCompanyType.Individual.value
        profileOrgnTitle.visible = profileOrgn.visible

        profileUsername.setHint(
            when (profile.mode) {
                ProfileCompanyType.Company.value -> R.string.profile_company_hint
                else -> R.string.profile_username_hint
            }
        )
        when (profile.mode) {
            ProfileCompanyType.Entrepreneur.value -> {
                profileOrgnTitle.setText(R.string.profile_orgnip)
                profileOrgn.setHint(R.string.profile_orgnip_hint)
            }
            else -> {
                profileOrgnTitle.setText(R.string.profile_orgn)
                profileOrgn.setHint(R.string.profile_orgn_hint)
            }
        }
    }

    private fun addTextChangeListeners() {
        profileUsername.doAfterTextChanged { viewModel.userNameChange(it.toString()) }
        profilePhoneValue.doAfterTextChanged { viewModel.phoneChange(it.toString()) }
        profileAddressValue.doAfterTextChanged { viewModel.addressChange(it.toString()) }
        profileActivityDescriptionValue.doAfterTextChanged { viewModel.descriptionChange(it.toString()) }
        profileCodeId.doAfterTextChanged { viewModel.innChange(it.toString()) }
        profileKpp.doAfterTextChanged { viewModel.kppChange(it.toString()) }
        profileOrgn.doAfterTextChanged {
            viewModel.orgnChange(it.toString())
        }
    }

    private fun addPickerEvents() {
        profileCountryContainer.setOnClickListener {
            val model = checkNotNull(viewModel.profileData.value?.countryModel)
            startActivityForResult(
                FilterPickerActivity.create(
                    requireContext(),
                    model,
                    0
                ), PICK_COUNTRY
            )
        }

        profileRegionContainer.setOnClickListener {
            val model = checkNotNull(viewModel.profileData.value?.regionModel)
            startActivityForResult(
                FilterPickerActivity.create(
                    requireContext(),
                    model,
                    0
                ), PICK_REGION
            )
        }
        profileCityContainer.setOnClickListener {
            val model = checkNotNull(viewModel.profileData.value?.cityModel)
            startActivityForResult(
                FilterPickerActivity.create(
                    context = requireContext(),
                    model = model,
                    0
                ), PICK_CITY
            )
        }
        profileCategoryContainer.setOnClickListener {
            val model = checkNotNull(viewModel.profileData.value?.categoryModel)
            startActivityForResult(
                FilterPickerActivity.create(
                    requireContext(),
                    model,
                    0
                ), PICK_CATEGORY
            )
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == PICK_COUNTRY && resultCode == Activity.RESULT_OK) {
            val value = data?.getParcelableExtra<CountryModel>(FilterPickerActivity.MODEL)
            viewModel.countryChanged(checkNotNull(value))
        }
        if (requestCode == PICK_REGION && resultCode == Activity.RESULT_OK) {
            val value = data?.getParcelableExtra<RegionModel>(FilterPickerActivity.MODEL)
            viewModel.regionChanged(checkNotNull(value))
        }
        if (requestCode == PICK_CITY && resultCode == Activity.RESULT_OK) {
            val value = data?.getParcelableExtra<CityModel>(FilterPickerActivity.MODEL)
            viewModel.cityChanged(checkNotNull(value))
        }
        if (requestCode == PICK_CATEGORY && resultCode == Activity.RESULT_OK) {
            val value = data?.getParcelableExtra<CategoryModel>(FilterPickerActivity.MODEL)
            viewModel.categoryChange(checkNotNull(value))
        }
    }

    private companion object {
        const val PICK_COUNTRY = 100
        const val PICK_REGION = 101
        const val PICK_CITY = 102
        const val PICK_CATEGORY = 103
    }
}
