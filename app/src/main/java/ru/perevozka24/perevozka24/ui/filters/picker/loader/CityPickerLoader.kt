package ru.perevozka24.perevozka24.ui.filters.picker.loader

import ru.perevozka24.perevozka24.data.features.filter.FilterLocationPickerRepository
import ru.perevozka24.perevozka24.ui.filters.models.CityModel
import ru.perevozka24.perevozka24.ui.filters.models.PickerModel

class CityPickerLoader(
    private val current: CityModel,
    private val pickerRepository: FilterLocationPickerRepository
) : PickerLoader<CityModel> {
    override suspend fun load(): List<CityModel> {
        val countryId = checkNotNull(current.countryId)
        val regionId = checkNotNull(current.regionId)
        return pickerRepository.getCities(countryId, regionId)
            .map { it.copy(direction = current.direction) }
    }

    override fun <T : PickerModel> filter(list: List<T>, q: String): List<CityModel> {
        return list.filter {
            it as CityModel
            it.name?.contains(q, true) == true
        }.map { it as CityModel }
    }
}
