package ru.perevozka24.perevozka24.ui.filters.picker.dialog

import ru.perevozka24.perevozka24.ui.filters.models.PickerModel

interface FilterPickerCheckListener {
    fun onFilterPickerChangedListener(
        type: Class<out PickerModel>,
        newValue: PickerModel?
    )
}
