package ru.perevozka24.perevozka24.ui.main.orders.detailed.item

import com.xwray.groupie.kotlinandroidextensions.GroupieViewHolder
import com.xwray.groupie.kotlinandroidextensions.Item
import kotlinx.android.synthetic.main.item_offer_detailed_contact.view.*
import ru.perevozka24.perevozka24.R
import ru.perevozka24.perevozka24.ui.common.ext.blur
import ru.perevozka24.perevozka24.ui.common.ext.clearBlur
import ru.perevozka24.perevozka24.ui.common.ext.visible
import ru.perevozka24.perevozka24.ui.main.orders.detailed.OfferDetailedAction
import ru.perevozka24.perevozka24.ui.main.orders.models.OfferModel

data class OfferContactItem(
    private val offer: OfferModel,
    private val action: OfferDetailedAction
) : Item() {

    override fun getId() = 0L
    override fun getLayout(): Int = R.layout.item_offer_detailed_contact

    override fun bind(viewHolder: GroupieViewHolder, position: Int) {
        with(viewHolder.itemView) {
            val code = offer.code

            itemOfferContactPhoneCall.visible = code.callVisible
            itemOfferContactPhoneCall.setOnClickListener {
                action.call(offer)
            }

            itemOfferContactName.text = offer.name
            itemOfferContactPhone.text =
                offer.contacts ?: resources.getString(R.string.offer_blured_phone)

            if (code.contactsBlur) {
                itemOfferContactName.blur()
                itemOfferContactPhone.blur()
            } else {
                itemOfferContactName.clearBlur()
                itemOfferContactPhone.clearBlur()
            }
        }
    }
}
