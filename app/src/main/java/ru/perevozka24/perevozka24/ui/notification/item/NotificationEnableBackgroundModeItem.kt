package ru.perevozka24.perevozka24.ui.notification.item

import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.view.View
import androidx.core.text.HtmlCompat
import com.xwray.groupie.kotlinandroidextensions.GroupieViewHolder
import com.xwray.groupie.kotlinandroidextensions.Item
import kotlinx.android.synthetic.main.item_notification_enabled_background_mode.view.*
import ru.perevozka24.perevozka24.R
import ru.perevozka24.perevozka24.ui.common.TextViewLinkHandler
import ru.perevozka24.perevozka24.ui.common.ext.visible
import java.util.Locale

object NotificationEnableBackgroundModeItem : Item() {
    override fun getId(): Long = 1
    override fun getLayout(): Int = R.layout.item_notification_enabled_background_mode

    override fun bind(viewHolder: GroupieViewHolder, position: Int) {
        setupItem(
            viewHolder.itemView,
            R.string.notification_warn_title_1,
            R.string.notification_warn_title_2
        )
    }

    override fun hashCode(): Int = getId().toInt()

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false
        return true
    }

    private fun permissionButtonShouldBeDisplayed(manufacture: String): Boolean {
        val deviceManufacture: String? = Build.MANUFACTURER?.toLowerCase(Locale.ROOT)
        return deviceManufacture != null && deviceManufacture.contains(
            manufacture.toLowerCase(
                Locale.ROOT
            )
        )
    }

    private val huaweiAutoStart: ComponentName
        get() = ComponentName(
            "com.huawei.systemmanager",
            "com.huawei.systemmanager.optimize.process.ProtectActivity"
        )
    private val xiaomiAutoStart: ComponentName
        get() = ComponentName(
            "com.miui.securitycenter",
            "com.miui.permcenter.autostart.AutoStartManagementActivity"
        )
    private val xiaomiPowerkeeper: ComponentName
        get() = ComponentName(
            "com.miui.powerkeeper",
            "com.miui.powerkeeper.ui.HiddenAppsContainerManagementActivity"
        )

    private val isXiaomi = permissionButtonShouldBeDisplayed("xiaomi")
    private val isHuawei = permissionButtonShouldBeDisplayed("huawei")

    private fun isCallable(context: Context, intent: Intent) =
        context.packageManager.queryIntentActivities(
            intent,
            PackageManager.MATCH_DEFAULT_ONLY
        ).isNotEmpty()

    fun needWarning() = isXiaomi || isHuawei

    fun setupItem(view: View, t1: Int, t2: Int) {
        with(view) {
            itemNotificationWarnTitle1.setText(t1)
            itemNotificationWarnTitle2.setText(t2)
            val linkHandler = object : TextViewLinkHandler() {
                override fun onLinkClick(url: String?) {
                    val intent = Intent(Intent.ACTION_MAIN)
                    intent.component = when (url) {
                        "autostart" -> when (isHuawei) {
                            true -> huaweiAutoStart
                            false -> xiaomiAutoStart
                        }
                        "background" -> xiaomiPowerkeeper
                        else -> error("unknown link $url")
                    }
                    if (isCallable(context, intent)) {
                        context.startActivity(intent)
                    }
                }
            }
            itemNotificationWarnS1Text.text = HtmlCompat.fromHtml(
                context.getString(R.string.notification_warn_s1_text),
                HtmlCompat.FROM_HTML_MODE_LEGACY
            )
            itemNotificationWarnS1Text.movementMethod = linkHandler

            if (isHuawei) {
                itemNotificationWarnS1.visible = false
                itemNotificationWarnS2.visible = false
                itemNotificationWarnS2Text.visible = false
            } else {
                itemNotificationWarnS2Text.text = HtmlCompat.fromHtml(
                    context.getString(R.string.notification_warn_s2_text),
                    HtmlCompat.FROM_HTML_MODE_LEGACY
                )
                itemNotificationWarnS2Text.movementMethod = linkHandler
            }
        }
    }
}
