package ru.perevozka24.perevozka24.ui.add.task

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import androidx.core.widget.addTextChangedListener
import androidx.lifecycle.Observer
import kotlinx.android.synthetic.main.fragment_add_order_step_task.*
import ru.perevozka24.perevozka24.R
import ru.perevozka24.perevozka24.ui.add.BaseCreateOrderStepFragment
import ru.perevozka24.perevozka24.ui.viewModel

class TaskStepFragment : BaseCreateOrderStepFragment() {
    override val title: Int = R.string.create_order_step_task_title

    private val viewModel: TaskStepViewModel by viewModel()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_add_order_step_task, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        addOrderTask.addTextChangedListener { viewModel.taskChanged(it?.toString() ?: "") }

        addOrderTaskNext.setOnClickListener {
            addOrderViewModel.taskSelected(
                checkNotNull(viewModel.task.value)
            )
        }
        viewModel.nextEnabled.observe(viewLifecycleOwner, Observer {
            addOrderTaskNext.isEnabled = it
        })
    }

    override fun onResume() {
        super.onResume()
        activity?.window?.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE)
    }

    override fun onPause() {
        super.onPause()
        activity?.window?.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_NOTHING)
    }
}
