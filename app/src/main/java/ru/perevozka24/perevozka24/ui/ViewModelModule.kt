package ru.perevozka24.perevozka24.ui

import androidx.lifecycle.ViewModelProvider
import org.kodein.di.Kodein
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.provider
import org.kodein.di.generic.singleton
import ru.perevozka24.perevozka24.ui.add.addOrderViewModelModule
import ru.perevozka24.perevozka24.ui.auth.authViewModelModule
import ru.perevozka24.perevozka24.ui.distance.input.DistanceInputViewModel
import ru.perevozka24.perevozka24.ui.distance.result.DistanceResultViewModel
import ru.perevozka24.perevozka24.ui.filters.filter.filterViewModelModule
import ru.perevozka24.perevozka24.ui.filters.picker.FilterPickerViewModel
import ru.perevozka24.perevozka24.ui.filters.picker.dialog.FilterDialogPickerViewModel
import ru.perevozka24.perevozka24.ui.geolocation.GeolocationViewModel
import ru.perevozka24.perevozka24.ui.leftMenu.LeftMenuViewModel
import ru.perevozka24.perevozka24.ui.main.MainViewModel
import ru.perevozka24.perevozka24.ui.main.base.EquipmentButtonVieModel
import ru.perevozka24.perevozka24.ui.main.items.abuse.AbuseViewModel
import ru.perevozka24.perevozka24.ui.main.items.detailed.ItemDetailedViewModel
import ru.perevozka24.perevozka24.ui.main.items.equipment.EquipmentListViewModel
import ru.perevozka24.perevozka24.ui.main.items.equipment.EquipmentMapViewModel
import ru.perevozka24.perevozka24.ui.main.items.equipment.EquipmentViewModel
import ru.perevozka24.perevozka24.ui.main.items.favorite.FavoriteViewModel
import ru.perevozka24.perevozka24.ui.main.items.my.MyItemsViewModel
import ru.perevozka24.perevozka24.ui.main.orders.cargo.CargoViewModel
import ru.perevozka24.perevozka24.ui.main.orders.detailed.OfferDetailedLoadViewModel
import ru.perevozka24.perevozka24.ui.main.orders.detailed.OfferDetailedViewModel
import ru.perevozka24.perevozka24.ui.main.orders.detailed.dialogs.OfferDeclineViewModel
import ru.perevozka24.perevozka24.ui.main.orders.detailed.dialogs.OfferSetPriceViewModel
import ru.perevozka24.perevozka24.ui.main.orders.favorite.FavoriteOrderViewModel
import ru.perevozka24.perevozka24.ui.main.orders.my.MyOrderViewModel
import ru.perevozka24.perevozka24.ui.main.orders.orders.OrdersViewModel
import ru.perevozka24.perevozka24.ui.notification.NotificationViewModel
import ru.perevozka24.perevozka24.ui.profile.profileViewModelModule
import ru.perevozka24.perevozka24.ui.splash.SplashViewModel
import ru.perevozka24.perevozka24.ui.welcome.WelcomeViewModel

val viewModelModule = Kodein.Module(name = "viewModelModule") {
    bind<ViewModelProvider.Factory>() with singleton {
        KodeinViewModelFactory(kodein)
    }
    import(authViewModelModule)
    import(filterViewModelModule)
    import(addOrderViewModelModule)
    import(profileViewModelModule)

    bind<MainViewModel>() with singleton { MainViewModel(instance()) }
    bind<SplashViewModel>() with provider { SplashViewModel(instance()) }
    bind<WelcomeViewModel>() with provider { WelcomeViewModel(instance()) }
    bind<EquipmentViewModel>() with singleton {
        EquipmentViewModel(instance())
    }
    bind<EquipmentListViewModel>() with singleton {
        EquipmentListViewModel(
            instance(),
            instance()
        )
    }
    bind<EquipmentMapViewModel>() with singleton {
        EquipmentMapViewModel(
            instance(),
            instance(),
            instance(),
            instance(),
            instance()
        )
    }
    bind<OrdersViewModel>() with singleton { OrdersViewModel(instance(), instance(), instance()) }
    bind<CargoViewModel>() with singleton { CargoViewModel(instance(), instance(), instance()) }
    bind<ItemDetailedViewModel>() with provider { ItemDetailedViewModel(instance(), instance()) }
    bind<AbuseViewModel>() with provider { AbuseViewModel(instance(), instance()) }
    bind<FavoriteViewModel>() with provider { FavoriteViewModel(instance(), instance()) }
    bind<LeftMenuViewModel>() with provider {
        LeftMenuViewModel(
            instance(),
            instance(),
            instance(),
            instance(),
            instance()
        )
    }
    bind<DistanceInputViewModel>() with provider { DistanceInputViewModel(instance(), instance()) }
    bind<DistanceResultViewModel>() with provider { DistanceResultViewModel(instance()) }
    bind<FilterPickerViewModel>() with provider {
        FilterPickerViewModel(instance(), instance(), instance())
    }
    bind<FavoriteOrderViewModel>() with provider {
        FavoriteOrderViewModel(instance(), instance())
    }
    bind<NotificationViewModel>() with provider {
        NotificationViewModel(instance(), instance())
    }
    bind<OfferDetailedLoadViewModel>() with provider {
        OfferDetailedLoadViewModel(instance(), instance())
    }
    bind<OfferDetailedViewModel>() with provider {
        OfferDetailedViewModel(
            instance(),
            instance(),
            instance(),
            instance(),
            instance()
        )
    }
    bind<OfferSetPriceViewModel>() with provider {
        OfferSetPriceViewModel(
            instance(),
            instance()
        )
    }
    bind<OfferDeclineViewModel>() with provider {
        OfferDeclineViewModel(
            instance(),
            instance()
        )
    }
    bind<GeolocationViewModel>() with provider {
        GeolocationViewModel(instance(), instance())
    }
    bind<MyOrderViewModel>() with provider {
        MyOrderViewModel(instance(), instance())
    }
    bind<MyItemsViewModel>() with provider {
        MyItemsViewModel(instance(), instance())
    }
    bind<FilterDialogPickerViewModel>() with provider {
        FilterDialogPickerViewModel(instance(), instance(), instance())
    }
    bind<EquipmentButtonVieModel>() with provider {
        EquipmentButtonVieModel(instance(), instance())
    }
}
