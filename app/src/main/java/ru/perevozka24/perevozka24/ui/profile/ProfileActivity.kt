package ru.perevozka24.perevozka24.ui.profile

import android.os.Bundle
import androidx.fragment.app.commit
import androidx.lifecycle.Observer
import kotlinx.android.synthetic.main.activity_fragment_container.*
import ru.perevozka24.perevozka24.R
import ru.perevozka24.perevozka24.data.Result
import ru.perevozka24.perevozka24.ui.base.BaseContentLoaderFragmentActivity
import ru.perevozka24.perevozka24.ui.common.ext.hideKeyboard
import ru.perevozka24.perevozka24.ui.common.ext.toast
import ru.perevozka24.perevozka24.ui.profile.models.ProfileModel
import ru.perevozka24.perevozka24.ui.viewModel
import ru.perevozka24.perevozka24.utils.trackEvent

class ProfileActivity : BaseContentLoaderFragmentActivity<ProfileModel>() {

    override val viewModel: ProfileViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        trackEvent("visited_profile")
        toolbarTitleView.text = getString(R.string.profile_title)

        viewModel.result.observe(this, Observer {
            val errorMessage = (it as? Result.Error)?.message ?: return@Observer
            toast(errorMessage)
            finish()
        })
        viewModel.openProfileInfo.observe(this, Observer {
            openProfileInfo()
        })
    }

    override fun onPause() {
        super.onPause()
        hideKeyboard(this)
    }

    override fun showLoaded(value: ProfileModel) {
        if (value.mode == null) {
            supportFragmentManager.commit {
                replace(R.id.container, ProfileCompanyTypeFragment())
            }
        } else {
            openProfileInfo()
        }
    }

    private fun openProfileInfo() {
        supportFragmentManager.commit {
            replace(R.id.container, ProfileFragment())
        }
    }
}
