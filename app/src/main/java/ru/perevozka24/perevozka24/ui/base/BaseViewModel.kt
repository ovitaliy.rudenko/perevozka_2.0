package ru.perevozka24.perevozka24.ui.base

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import ru.perevozka24.perevozka24.R
import ru.perevozka24.perevozka24.data.ResourceProvider
import ru.perevozka24.perevozka24.data.Result
import ru.perevozka24.perevozka24.data.base.ApiException
import ru.perevozka24.perevozka24.ui.common.ext.safeSuspended
import ru.perevozka24.perevozka24.ui.common.livedata.postValue
import timber.log.Timber
import java.io.IOException
import java.net.UnknownHostException

@SuppressWarnings("TooGenericExceptionCaught")
abstract class BaseViewModel(val resourceProvider: ResourceProvider) : ViewModel() {

    val error: LiveData<String> = MutableLiveData()

    inline fun <T : Any> doWork(
        liveData: LiveData<Result<T>>,
        updateLoading: Boolean = true,
        crossinline block: suspend () -> T
    ) {
        viewModelScope.launch {
            if (updateLoading) liveData.postValue(Result.Loading)
            liveData.postValue(safeExecute(block))
        }
    }

    inline fun <T : Any> doGlobalWork(
        liveData: LiveData<Result<T>>,
        updateLoading: Boolean = true,
        crossinline block: suspend () -> T
    ): Job = GlobalScope.launch {
        if (updateLoading) liveData.postValue(Result.Loading)
        liveData.postValue(safeExecute(block))
    }

    inline fun <T : Any> launchGlobalWork(
        liveData: LiveData<Result<T>>,
        crossinline block: suspend () -> Unit
    ) {
        GlobalScope.launch {
            try {
                withContext(Dispatchers.IO) { block() }
            } catch (ex: ApiException) {
                Timber.e(ex)
                liveData.postValue(Result.Error(checkNotNull(ex.message)))
            } catch (ex: IOException) {
                Timber.e(ex)
                liveData.postValue(Result.Error(resourceProvider.getString(R.string.error_no_internet_connection)))
            } catch (ex: UnknownHostException) {
                Timber.e(ex)
                liveData.postValue(Result.Error(resourceProvider.getString(R.string.error_no_internet_connection)))
            } catch (ex: Exception) {
                Timber.e(ex)
                liveData.postValue(Result.Error(resourceProvider.getString(R.string.error_something_wrong)))
            }
        }
    }

    suspend inline fun <T : Any> safeExecute(
        crossinline block: suspend () -> T
    ): Result<T> {
        return try {
            val r: T = withContext(Dispatchers.IO) { block() }
            Result.Success(r)
        } catch (ex: ApiException) {
            Timber.e(ex)
            Result.Error(checkNotNull(ex.message))
        } catch (ex: IOException) {
            Timber.e(ex)
            Result.Error(resourceProvider.getString(R.string.error_no_internet_connection))
        } catch (ex: UnknownHostException) {
            Timber.e(ex)
            Result.Error(resourceProvider.getString(R.string.error_no_internet_connection))
        } catch (ex: Exception) {
            Timber.e(ex)
            Result.Error(resourceProvider.getString(R.string.error_something_wrong))
        }
    }

    suspend inline fun <T : Any> safe(
        crossinline block: suspend () -> T?
    ): T? {
        return safeSuspended(block)
    }

    inline fun <T : Any> Result<T>.traceError(): Result<T> {
        if (this is Result.Error) {
            error.postValue(this.message)
        }
        return this
    }

    inline fun <T : Any> Result<T>.doOnSuccess(callback: (T) -> Unit): Result<T> {
        if (this is Result.Success) {
            callback(this.data)
        }
        return this
    }
}
