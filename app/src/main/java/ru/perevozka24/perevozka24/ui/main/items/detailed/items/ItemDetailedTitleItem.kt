package ru.perevozka24.perevozka24.ui.main.items.detailed.items

import com.xwray.groupie.kotlinandroidextensions.GroupieViewHolder
import com.xwray.groupie.kotlinandroidextensions.Item
import kotlinx.android.synthetic.main.item_detailed_title.view.*
import ru.perevozka24.perevozka24.R
import ru.perevozka24.perevozka24.ui.main.items.models.ItemModel

data class ItemDetailedTitleItem(
    private val item: ItemModel
) : Item() {
    override fun getId(): Long = 0
    override fun getLayout(): Int = R.layout.item_detailed_title

    override fun bind(viewHolder: GroupieViewHolder, position: Int) {
        with(viewHolder.itemView) {
            itemDetailedTitle.text = item.name
        }
    }
}
