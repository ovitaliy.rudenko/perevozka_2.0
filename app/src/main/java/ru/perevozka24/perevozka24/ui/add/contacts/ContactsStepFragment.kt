package ru.perevozka24.perevozka24.ui.add.contacts

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import kotlinx.android.synthetic.main.fragment_add_order_step_contacts.*
import kotlinx.android.synthetic.main.fragment_add_order_step_extra_info.*
import ru.perevozka24.perevozka24.R
import ru.perevozka24.perevozka24.ui.add.BaseCreateOrderStepFragment
import ru.perevozka24.perevozka24.ui.common.widgets.form.addTextChangedListener
import ru.perevozka24.perevozka24.ui.viewModel

class ContactsStepFragment : BaseCreateOrderStepFragment() {
    override val title: Int = R.string.create_order_step_contacts_title
    private val stepViewModel: ContactsStepViewModel by viewModel()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_add_order_step_contacts, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        addOrderContactsUsername.addTextChangedListener { stepViewModel.usernameChanged(it?.toString() ?: "") }
        addOrderContactsPhone.addTextChangedListener { stepViewModel.phoneNumberChanged(it?.toString() ?: "") }

        stepViewModel.enabled.observe(viewLifecycleOwner, Observer {
                addOrderContactsNext.isEnabled = it
        })

        addOrderContactsNext.setOnClickListener {
            addOrderViewModel.contactsSelected(
                checkNotNull(addOrderContactsUsername.text?.toString()),
                checkNotNull(addOrderContactsPhone.text?.toString())
            )
        }
    }
}
