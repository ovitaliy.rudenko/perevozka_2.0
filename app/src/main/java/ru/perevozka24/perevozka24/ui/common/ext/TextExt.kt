package ru.perevozka24.perevozka24.ui.common.ext

import android.graphics.BlurMaskFilter
import android.view.View
import android.widget.TextView

private const val BLUR_KOEF = 3f

fun TextView.blur() {
    setLayerType(View.LAYER_TYPE_SOFTWARE, null)
    val radius = textSize / BLUR_KOEF
    val filter = BlurMaskFilter(radius, BlurMaskFilter.Blur.NORMAL)
    paint.maskFilter = filter
}
fun TextView.clearBlur() {
    paint.maskFilter = null
}

/**
 * Sets text. If new value is empty the view is hiding
 */
fun TextView.setVisibleText(text: CharSequence?, invisibility: Int = View.GONE) {
    this.text = text
    visible(text.isNullOrEmpty().not(), invisibility)
}

fun Array<out View>.setVisibleText(text: CharSequence?, setFirstTextOnly: Boolean = false) {
    if (text.isNullOrEmpty()) {
        this.forEach { it.visible = false }
    } else {
        var firstSet = false
        this.forEach {
            it.visible = true

            if (!firstSet && it is TextView) {
                it.text = text

                firstSet = setFirstTextOnly
            }
        }
    }
}
