package ru.perevozka24.perevozka24.ui.leftMenu

import ru.perevozka24.perevozka24.ui.auth.models.LoginModel
import ru.perevozka24.perevozka24.ui.geolocation.GeolocationModel

data class LeftMenuParams(
    val loginModel: LoginModel?,
    val geolocationItems: List<GeolocationModel>?,
    val hasItems: Boolean,
    val hasOrders: Boolean
) {
    val authorized: Boolean
        get() = loginModel != null

    val hasGeolocationItems: Boolean
        get() = geolocationItems.isNullOrEmpty().not()
}
