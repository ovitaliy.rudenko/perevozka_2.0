package ru.perevozka24.perevozka24.ui.notification

import android.os.Bundle
import androidx.fragment.app.commit
import kotlinx.android.synthetic.main.activity_fragment_container.*
import ru.perevozka24.perevozka24.R
import ru.perevozka24.perevozka24.ui.base.BaseFragmentActivity

class NotificationActivity : BaseFragmentActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        toolbarTitleView.text = getString(R.string.notification_title)

        if (savedInstanceState == null) {
            supportFragmentManager.commit {
                replace(R.id.container, NotificationFragment())
            }
        }
    }
}
