package ru.perevozka24.perevozka24.ui.auth.register.step1

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_auth_register_contact.*
import ru.perevozka24.perevozka24.R
import ru.perevozka24.perevozka24.ui.base.BaseFragment
import ru.perevozka24.perevozka24.ui.common.widgets.form.addTextChangedListener

abstract class BaseRegisterContactFragment<VM : BaseRegisterContactViewModel> : BaseFragment() {

    abstract val viewModel: VM

    abstract fun getContactViewHint(): Int
    abstract fun getContactViewTitle(): Int
    abstract fun getOtherWayButtonTitle(): Int
    abstract fun getOtherWayClickAction()
    abstract fun getProceedClickAction(contact: String)
    abstract fun getInputType(): Int

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_auth_register_contact, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        with(contactView) {
            hint = resources.getString(getContactViewHint())
            title = resources.getString(getContactViewTitle())
        }

        with(registerUseOtherWay) {
            text = getString(getOtherWayButtonTitle())
            setOnClickListener { getOtherWayClickAction() }
        }
        registerButton.setOnClickListener { viewModel.proceed() }

        // contact
        contactView.addTextChangedListener { viewModel.contactChange(it.toString()) }
        contactView.inputType = getInputType()
        viewModel.contactFieldError.observe(viewLifecycleOwner, {
            contactView.error(it)
        })

        //
        viewModel.proceedEvent.observe(viewLifecycleOwner) {
            getProceedClickAction(it)
        }
    }
}
