package ru.perevozka24.perevozka24.ui.auth

import org.kodein.di.Kodein
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.provider
import ru.perevozka24.perevozka24.ui.auth.login.LoginViewModel
import ru.perevozka24.perevozka24.ui.auth.register.step1.RegisterEmailViewModel
import ru.perevozka24.perevozka24.ui.auth.register.step1.RegisterPhoneViewModel
import ru.perevozka24.perevozka24.ui.auth.register.step2.EmailRegisterViewModel
import ru.perevozka24.perevozka24.ui.auth.register.step2.PhoneRegisterViewModel
import ru.perevozka24.perevozka24.ui.auth.restore.RestorePasswordViewModel

val authViewModelModule = Kodein.Module(name = "authViewModelModule") {
    bind<LoginViewModel>() with provider { LoginViewModel(instance(), instance()) }
    bind<RegisterPhoneViewModel>() with provider { RegisterPhoneViewModel(instance()) }
    bind<RegisterEmailViewModel>() with provider { RegisterEmailViewModel(instance()) }
    bind<EmailRegisterViewModel>() with provider { EmailRegisterViewModel(instance(), instance()) }
    bind<PhoneRegisterViewModel>() with provider { PhoneRegisterViewModel(instance(), instance()) }
    bind<RestorePasswordViewModel>() with provider {
        RestorePasswordViewModel(instance(), instance())
    }
}
