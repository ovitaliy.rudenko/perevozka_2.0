package ru.perevozka24.perevozka24.ui.main.items.favorite

import ru.perevozka24.perevozka24.data.ResourceProvider
import ru.perevozka24.perevozka24.data.features.items.ItemsRepository
import ru.perevozka24.perevozka24.ui.common.livedata.postValue
import ru.perevozka24.perevozka24.ui.main.items.AddToFavoriteViewModel

class FavoriteViewModel(
    private val repository: ItemsRepository,
    resourceProvider: ResourceProvider
) : AddToFavoriteViewModel(repository, resourceProvider) {
    init {
        load()
    }

    override val searchReportName: String
        get() = error("not used")

    override fun refresh() = load()

    private fun load() {
        doWork(items) {
            val result = repository.getFavorites()
            itemsList.postValue(result)
            result
        }
    }
}
