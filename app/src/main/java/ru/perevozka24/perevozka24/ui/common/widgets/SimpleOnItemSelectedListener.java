package ru.perevozka24.perevozka24.ui.common.widgets;

import android.widget.AdapterView;

public abstract class SimpleOnItemSelectedListener implements AdapterView.OnItemSelectedListener {

    /**
     * Do nothing if nothing selected
     *
     * @param parent target adapter view
     */
    @Override
    @SuppressWarnings({"PMD.UncommentedEmptyMethodBody",
            "PMD.EmptyMethodInAbstractClassShouldBeAbstract"})
    public void onNothingSelected(AdapterView<?> parent) {
        // do nothing
    }
}
