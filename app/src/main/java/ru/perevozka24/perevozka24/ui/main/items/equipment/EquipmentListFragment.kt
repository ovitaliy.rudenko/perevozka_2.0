package ru.perevozka24.perevozka24.ui.main.items.equipment

import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.fragment_list_equipment.*
import org.kodein.di.Kodein
import org.kodein.di.android.x.closestKodein
import ru.perevozka24.perevozka24.R
import ru.perevozka24.perevozka24.ui.add.AddOrderActivity
import ru.perevozka24.perevozka24.ui.common.ext.updateOrReplace
import ru.perevozka24.perevozka24.ui.common.ext.visible
import ru.perevozka24.perevozka24.ui.filters.models.FilterModel
import ru.perevozka24.perevozka24.ui.main.base.BaseListFragment
import ru.perevozka24.perevozka24.ui.main.items.AddToBlacklistDelegate
import ru.perevozka24.perevozka24.ui.main.items.AddToFavoriteDelegate
import ru.perevozka24.perevozka24.ui.main.items.ItemAction
import ru.perevozka24.perevozka24.ui.main.items.ItemActionImpl
import ru.perevozka24.perevozka24.ui.main.items.items.EquipmentMapper
import ru.perevozka24.perevozka24.ui.main.items.models.ItemModel
import ru.perevozka24.perevozka24.utils.trackEvent
import kotlin.math.abs

class EquipmentListFragment : BaseListFragment<EquipmentListViewModel>(), AddToFavoriteDelegate,
    AddToBlacklistDelegate, ISearchable {
    override val kodein: Kodein by closestKodein()

    override val viewModel: EquipmentListViewModel
        get() = (parentFragment as EquipmentFragment).listViewModel

    override val layoutId: Int
        get() = R.layout.fragment_list_equipment

    override fun addToFavorite(item: ItemModel) = viewModel.addToFavorite(item)

    override fun addToBlacklist(item: ItemModel) = viewModel.addToBlacklist(item)

    private val hasExtraActions = true
    private val actions: ItemAction = ItemActionImpl(this, this, this)

    override fun changeFilterClick() {
        (parentFragment as? EquipmentFragment)?.openFilter()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        recyclerView.adapter = adapter
        viewModel.itemsList.observe(viewLifecycleOwner, Observer {
            it ?: return@Observer
            adapter.updateOrReplace(EquipmentMapper.map(it, actions, hasExtraActions))
        })
        mainMapAddOrder.setOnClickListener { v ->
            trackEvent(
                "create_order",
                mapOf("type" to "main-screen")
            )
            v.context.run { startActivity(AddOrderActivity.create(this)) }
        }

        setupEquipmentButton(mainMapAddEquipment)

        recyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                if (abs(dy) > resources.getDimension(R.dimen.dp24)) {
                    val buttonVisible = dy < 0
                    mainMapAddOrder.visible = buttonVisible
                    mainMapAddEquipment.visible = buttonVisible
                }
            }
        })
    }

    override fun search(q: String) {
        viewModel.search(q)
    }

    override fun refreshAfterFilterChange(old: FilterModel?, new: FilterModel?) {
        viewModel.refreshAfterFilterChange(old, new)
    }
}
