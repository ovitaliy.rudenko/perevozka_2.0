package ru.perevozka24.perevozka24.ui.welcome

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.viewpager2.adapter.FragmentStateAdapter
import androidx.viewpager2.widget.ViewPager2
import kotlinx.android.synthetic.main.activity_welcome.*
import ru.perevozka24.perevozka24.BuildConfig
import ru.perevozka24.perevozka24.MainActivity
import ru.perevozka24.perevozka24.R
import ru.perevozka24.perevozka24.ui.base.BaseActivity
import ru.perevozka24.perevozka24.ui.common.ext.requestLocationPermissionIgnoreResult
import ru.perevozka24.perevozka24.ui.common.ext.toast
import ru.perevozka24.perevozka24.ui.viewModel
import ru.perevozka24.perevozka24.utils.trackEvent

class WelcomeActivity : BaseActivity() {

    private val viewModel: WelcomeViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_welcome)
        val adapter = ScreenSlidePagerAdapter(this)
        stepView.steps = adapter.itemCount
        welcomeViewPager.adapter = adapter
        welcomeViewPager.registerOnPageChangeCallback(object : ViewPager2.OnPageChangeCallback() {
            override fun onPageSelected(position: Int) {
                stepView.step = position + 1
            }
        })

        if (BuildConfig.DEBUG) {
            val heightDp = resources.displayMetrics.run { heightPixels / density }
            val widthDp = resources.displayMetrics.run { widthPixels / density }
            toast("$widthDp x $heightDp")
        }

        viewModel.onBoardingShown()
    }

    fun onClick(v: View) {
        requestLocationPermissionIgnoreResult {
            finish()
            val redirect = when (v.id) {
                R.id.welcomeStartAppPage2 -> {
                    trackEvent("tutorial_second_screen")
                    0
                }
                R.id.welcomeStartAppPage3 -> {
                    trackEvent("tutorial_finished")
                    0
                }
                else -> error("unknown view id")
            }
            startActivity(MainActivity.create(this, redirect))
        }
    }

    // An equivalent ViewPager2 adapter class
    class ScreenSlidePagerAdapter(fa: FragmentActivity) : FragmentStateAdapter(fa) {
        private val pages = arrayOf(
            R.layout.item_welcome_1,
            R.layout.item_welcome_2,
            R.layout.item_welcome_3
        )

        override fun getItemCount(): Int = pages.size

        override fun createFragment(position: Int): Fragment =
            WelcomePageFragment.create(pages[position])
    }
}
