package ru.perevozka24.perevozka24.ui.main.items.equipment

import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import ru.perevozka24.perevozka24.DEFAULT_REQUEST_DELAY
import ru.perevozka24.perevozka24.data.ResourceProvider
import ru.perevozka24.perevozka24.data.features.items.ItemsRepository
import ru.perevozka24.perevozka24.data.features.items.toModel
import ru.perevozka24.perevozka24.data.features.items.updateFavoriteValue
import ru.perevozka24.perevozka24.ui.common.livedata.postValue
import ru.perevozka24.perevozka24.ui.main.base.BaseContentViewModel
import ru.perevozka24.perevozka24.ui.main.items.AddToBlacklistDelegate
import ru.perevozka24.perevozka24.ui.main.items.AddToFavoriteDelegate
import ru.perevozka24.perevozka24.ui.main.items.models.ItemModel
import ru.perevozka24.perevozka24.ui.main.items.models.ItemsList

class EquipmentListViewModel(
    private val repository: ItemsRepository,
    resourceProvider: ResourceProvider
) : BaseContentViewModel<ItemsList>(resourceProvider), AddToFavoriteDelegate,
    AddToBlacklistDelegate {

    override val searchReportName: String = "main_screen_search"

    private var loadingJob: Job? = null

    init {
        load()
    }

    override fun refresh() = load()

    fun load() {
        loadingJob?.cancel()
        loadingJob = doGlobalWork(items) {
            Thread.sleep(DEFAULT_REQUEST_DELAY)
            if (query.isEmpty()) {
                repository.getEquipment().apply {
                    itemsList.postValue(this)
                }
            } else {
                repository.search(query).apply {
                    itemsList.postValue(this)
                }
            }
        }
    }

    override fun addToFavorite(item: ItemModel) {
        val itemId = item.id
        val favorite = item.favorite
        val oldList = itemsList.value ?: ItemsList()
        itemsList.postValue(oldList.updateFavoriteValue(itemId, !favorite))
        GlobalScope.launch {
            safeExecute {
                if (favorite) {
                    repository.removeFavorite(itemId)
                } else {
                    repository.addFavorite(itemId)
                }
            }.traceError()
        }
    }

    override fun addToBlacklist(item: ItemModel) {
        val userId = item.userId

        GlobalScope.launch {
            safeExecute {
                repository.addToBlackList(userId)
                val oldList = itemsList.value ?: ItemsList()
                itemsList.postValue(oldList.filterNot { userId == it.userId }.toModel())
            }.traceError()
        }
    }
}
