package ru.perevozka24.perevozka24.ui.profile

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import ru.perevozka24.perevozka24.data.ResourceProvider
import ru.perevozka24.perevozka24.data.Result
import ru.perevozka24.perevozka24.data.features.profile.ProfileRepository
import ru.perevozka24.perevozka24.ui.LiveEvent
import ru.perevozka24.perevozka24.ui.base.BaseContentLoaderViewModel
import ru.perevozka24.perevozka24.ui.common.livedata.Combined2LiveData
import ru.perevozka24.perevozka24.ui.common.livedata.distinctUntilChanged
import ru.perevozka24.perevozka24.ui.common.livedata.postValue
import ru.perevozka24.perevozka24.ui.filters.models.CategoryModel
import ru.perevozka24.perevozka24.ui.filters.models.CityModel
import ru.perevozka24.perevozka24.ui.filters.models.CountryModel
import ru.perevozka24.perevozka24.ui.filters.models.RegionModel
import ru.perevozka24.perevozka24.ui.profile.models.ProfileModel

@SuppressWarnings("TooManyFunctions")
class ProfileViewModel(
    private val profileRepository: ProfileRepository,
    resourceProvider: ResourceProvider
) : BaseContentLoaderViewModel<ProfileModel>(resourceProvider) {

    var profileData: LiveData<ProfileModel> = MutableLiveData()

    var openProfileInfo = LiveEvent<Unit>()

    val innEditable: LiveData<Boolean> = MutableLiveData()

    val changed: LiveData<Boolean> = Combined2LiveData(result, profileData) { p1, p2 ->
        p1 is Result.Success && p1.data != p2
    }.distinctUntilChanged()

    init {
        execute {
            profileRepository.getProfile().also {
                profileData.postValue(it)
                innEditable.postValue(it.inn.isNullOrBlank())
            }
        }
    }

    fun countryChanged(countryModel: CountryModel) {
        val newData = checkNotNull(profileData.value).copy(
            countryModel = countryModel,
            regionModel = RegionModel.default(countryModel.id),
            cityModel = CityModel.default()
        )
        profileData.postValue(newData)
    }

    fun regionChanged(regionModel: RegionModel) {
        val newData = checkNotNull(profileData.value).copy(
            regionModel = regionModel,
            cityModel = CityModel.default(countryId = regionModel.countryId, regionId = regionModel.id)
        )
        profileData.postValue(newData)
    }

    fun cityChanged(cityModel: CityModel) {
        val newData = checkNotNull(profileData.value).copy(
            cityModel = cityModel
        )
        profileData.postValue(newData)
    }

    fun categoryChange(categoryModel: CategoryModel) {
        val newData = checkNotNull(profileData.value).copy(
            categoryModel = categoryModel
        )
        profileData.postValue(newData)
    }

    fun userNameChange(username: String) {
        val newData = checkNotNull(profileData.value).copy(
            name = username
        )
        profileData.postValue(newData)
    }

    fun phoneChange(phone: String) {
        val newData = checkNotNull(profileData.value).copy(
            phone = phone
        )
        profileData.postValue(newData)
    }

    fun addressChange(address: String) {
        val newData = checkNotNull(profileData.value).copy(
            address = address
        )
        profileData.postValue(newData)
    }

    fun descriptionChange(description: String) {
        val newData = checkNotNull(profileData.value).copy(
            description = description
        )
        profileData.postValue(newData)
    }

    fun innChange(inn: String) {
        val newData = checkNotNull(profileData.value).copy(
            inn = inn
        )
        profileData.postValue(newData)
    }

    fun kppChange(value: String) {
        val newData = checkNotNull(profileData.value).copy(
            kpp = value
        )
        profileData.postValue(newData)
    }

    fun orgnChange(value: String) {
        val newData = checkNotNull(profileData.value).copy(
            orgn = value
        )
        profileData.postValue(newData)
    }

    fun modeChanged(value: Int) {
        val newData = checkNotNull(profileData.value).copy(
            mode = value
        )
        profileData.postValue(newData)
    }

    fun save(): Boolean {
        GlobalScope.launch {
            val r = safeExecute {
                profileRepository.save(checkNotNull(profileData.value?.validate()))
                profileRepository.getProfile()
            }.doOnSuccess {
                profileData.postValue(it)
                result.postValue(Result.Success(it))
            }.traceError()
        }

        return true
    }

    fun next() {
        openProfileInfo.postValue(Unit)
    }
}
