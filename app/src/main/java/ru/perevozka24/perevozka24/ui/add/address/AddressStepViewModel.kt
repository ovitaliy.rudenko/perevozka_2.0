package ru.perevozka24.perevozka24.ui.add.address

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.map
import androidx.lifecycle.viewModelScope
import com.yandex.mapkit.map.CameraPosition
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import ru.perevozka24.perevozka24.data.ResourceProvider
import ru.perevozka24.perevozka24.data.features.filter.FilterLocationPickerRepository
import ru.perevozka24.perevozka24.data.features.location.GeoLocationRepository
import ru.perevozka24.perevozka24.data.features.location.entity.SimpleAddress
import ru.perevozka24.perevozka24.ui.LiveEvent
import ru.perevozka24.perevozka24.ui.base.BaseViewModel
import ru.perevozka24.perevozka24.ui.common.livedata.postValue
import ru.perevozka24.perevozka24.ui.common.livedata.setValue
import ru.perevozka24.perevozka24.ui.filters.models.CityModel
import ru.perevozka24.perevozka24.ui.filters.models.CountryModel
import ru.perevozka24.perevozka24.ui.filters.models.RegionModel

class AddressStepViewModel(
    private val pickerLocationRepository: FilterLocationPickerRepository,
    private val geoLocationDataSource: GeoLocationRepository,
    resourceProvider: ResourceProvider
) : BaseViewModel(resourceProvider) {

    private val defaultCountry = CountryModel.default()
    private val defaultRegion = RegionModel.default(null)
    private val defaultCity = CityModel.default(null)

    val country: LiveData<CountryModel> = MutableLiveData(defaultCountry)
    val region: LiveData<RegionModel> = MutableLiveData(defaultRegion)
    val city: LiveData<CityModel> = MutableLiveData(defaultCity)
    val address: LiveData<String> = MutableLiveData("")
    var simpleAddress = SimpleAddress()

    fun initCurrentLocation(detect: Boolean) {
        if (!detect) return
        viewModelScope.launch {
            safe {
                val data = geoLocationDataSource.getLocationData()
                data.country?.let {
                    country.postValue(it)
                }
                data.region?.let {
                    region.postValue(it)
                }
                data.city?.let {
                    city.postValue(it)
                }
                simpleAddress = data.address
            }
        }
    }

    val nextEnabled = city.map { it.id != null }

    val location = LiveEvent<CameraPosition>()

    fun countryChanged(country: CountryModel) {
        this.country.setValue(country)
        this.region.setValue(RegionModel.default(countryId = country.id))
        this.city.setValue(CityModel.default())
        addressChanged("")
    }

    fun regionChanged(region: RegionModel) {
        this.region.setValue(region)
        this.city.setValue(CityModel.default(region.countryId, region.id))
        addressChanged("")
    }

    fun cityChanged(city: CityModel) {
        this.city.setValue(city)
        addressChanged("")
    }

    fun addressChanged(address: String?) {
        this.address.setValue(address ?: "")
    }

    fun addressSelected(newAddress: SimpleAddress) {
        GlobalScope.launch {
            country.postValue(
                pickerLocationRepository.getCountryByAddress(newAddress) ?: defaultCountry
            )
            val (r, c) = pickerLocationRepository.getRegionByAddress(newAddress)
            region.postValue(r ?: defaultRegion)
            city.postValue(c ?: defaultCity)
            address.postValue(newAddress.line ?: "")
            newAddress.toCameraPosition()?.let {
                location.postValue(it)
            }
            simpleAddress = newAddress
        }
    }
}
