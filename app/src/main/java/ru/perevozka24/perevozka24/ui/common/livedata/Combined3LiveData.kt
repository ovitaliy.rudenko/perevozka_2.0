package ru.perevozka24.perevozka24.ui.common.livedata

import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import timber.log.Timber

class Combined3LiveData<T1, T2, T3, R>(
    t1LiveData: LiveData<T1>,
    t2LiveData: LiveData<T2>,
    t3LiveData: LiveData<T3>,
    private val combine: (T1?, T2?, T3?) -> R
) : MediatorLiveData<R>() {
    private var t1: T1? = null
    private var t2: T2? = null
    private var t3: T3? = null

    init {
        addSource(t1LiveData) { t: T1 ->
            t1 = t
            notifyOnChange()
        }
        addSource(t2LiveData) { t: T2 ->
            t2 = t
            notifyOnChange()
        }
        addSource(t3LiveData) { t: T3 ->
            t3 = t
            notifyOnChange()
        }
    }

    private fun notifyOnChange() {
        try {
            postValue(combine.invoke(t1, t2, t3))
        } catch (e: IllegalArgumentException) {
            Timber.e(e)
        }
    }
}
