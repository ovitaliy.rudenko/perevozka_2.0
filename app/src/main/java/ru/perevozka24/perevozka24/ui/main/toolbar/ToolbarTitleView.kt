package ru.perevozka24.perevozka24.ui.main.toolbar

import android.annotation.SuppressLint
import android.content.Context
import androidx.annotation.StringRes
import androidx.core.content.res.ResourcesCompat
import kotlinx.android.synthetic.main.toolbar_main_title.view.*
import ru.perevozka24.perevozka24.R

@SuppressLint("ViewConstructor")
class ToolbarTitleView(context: Context, @StringRes title: Int) : ToolbarView(context) {
    init {
        setBackgroundColor(ResourcesCompat.getColor(resources, R.color.yellow, null))
        inflate(context, R.layout.toolbar_main_title, this)
        toolbarTitle.setText(title)
    }

    override fun setupOnDrawerClick(callback: () -> Unit) {
        openDrawerView.setOnClickListener { callback() }
    }
}
