package ru.perevozka24.perevozka24.ui.auth.login

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import ru.perevozka24.perevozka24.data.ResourceProvider
import ru.perevozka24.perevozka24.data.Result
import ru.perevozka24.perevozka24.data.features.auth.AuthRepository
import ru.perevozka24.perevozka24.data.features.auth.LoginParams
import ru.perevozka24.perevozka24.ui.base.BaseViewModel
import ru.perevozka24.perevozka24.ui.common.livedata.postValue
import ru.perevozka24.perevozka24.ui.common.widgets.form.EmptyFieldValidator
import ru.perevozka24.perevozka24.ui.common.widgets.form.InputField
import ru.perevozka24.perevozka24.utils.trackEvent

class LoginViewModel(
    private val authRepository: AuthRepository,
    resourceProvider: ResourceProvider
) : BaseViewModel(resourceProvider) {

    val usernameFieldError: LiveData<String> = MutableLiveData()
    private val usernameField =
        MutableLiveData(
            InputField(
                "",
                EmptyFieldValidator(),
                { usernameFieldError.postValue(it) }
            )
        )

    val passwordError: LiveData<String> = MutableLiveData()
    private val passwordField = MutableLiveData(
        InputField(
            "",
            EmptyFieldValidator(),
            { passwordError.postValue(it) }
        ))

    val login: LiveData<Result<Unit>> = MutableLiveData()

    fun userNameChange(username: String) {
        usernameField.value?.value = username
    }

    fun passwordChange(password: String) {
        passwordField.value?.value = password
    }

    fun login() {
        val fields = arrayOf(usernameField, passwordField)

        val isValid = fields.map {
            it.value?.validate()
            (it.value?.hasError ?: true).not()
        }
            .all { it }

        if (isValid) {
            doWork(login) {
                val params = LoginParams(
                    username = checkNotNull(usernameField.value?.value),
                    password = checkNotNull(passwordField.value?.value)
                )
                authRepository.login(params)
                trackEvent(
                    "auth_successful",
                    mapOf("email" to (usernameField.value?.value ?: ""))
                )
            }
        }
    }
}
