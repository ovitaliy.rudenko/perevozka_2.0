package ru.perevozka24.perevozka24.ui.main.items

import ru.perevozka24.perevozka24.ui.main.items.models.ItemModel

interface AddToFavoriteDelegate {
    fun addToFavorite(item: ItemModel)
}
