package ru.perevozka24.perevozka24.ui.main.items.favorite

import android.os.Bundle
import org.kodein.di.Kodein
import org.kodein.di.android.x.kodein
import ru.perevozka24.perevozka24.ui.main.items.base.BaseItemsFragment
import ru.perevozka24.perevozka24.ui.viewModel
import ru.perevozka24.perevozka24.utils.trackEvent

class FavoriteItemsFragment : BaseItemsFragment<FavoriteViewModel>() {
    override val kodein: Kodein by kodein()
    override val viewModel: FavoriteViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        trackEvent("visited_fav_ads")
    }
}
