package ru.perevozka24.perevozka24.ui.common.ext

import android.app.Activity
import android.content.Intent
import ru.perevozka24.perevozka24.MainActivity

fun Activity.restartApp() {
    val intent = Intent(this, MainActivity::class.java).restart()
    finishAffinity()
    startActivity(intent)
}
