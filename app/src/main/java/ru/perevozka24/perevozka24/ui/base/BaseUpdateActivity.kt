package ru.perevozka24.perevozka24.ui.base

import android.annotation.SuppressLint
import android.app.Activity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.snackbar.Snackbar
import com.google.android.play.core.appupdate.AppUpdateInfo
import com.google.android.play.core.appupdate.AppUpdateManagerFactory
import com.google.android.play.core.install.InstallState
import com.google.android.play.core.install.InstallStateUpdatedListener
import com.google.android.play.core.install.model.AppUpdateType
import com.google.android.play.core.install.model.AppUpdateType.IMMEDIATE
import com.google.android.play.core.install.model.InstallStatus
import com.google.android.play.core.install.model.UpdateAvailability
import org.kodein.di.KodeinAware
import org.kodein.di.KodeinContext
import org.kodein.di.android.kodein
import org.kodein.di.generic.kcontext
import ru.perevozka24.perevozka24.R

abstract class BaseUpdateActivity : AppCompatActivity(), KodeinAware {
    override val kodeinContext: KodeinContext<*> = kcontext(this)
    override val kodein by kodein()

    private var hasUpdateSnake: Snackbar? = null
    private var progressSnackbar: Snackbar? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        checkForUpdate(this)
    }

    private fun checkForUpdate(activity: Activity) {
        // Creates instance of the manager.
        val appUpdateManager = AppUpdateManagerFactory.create(activity)

        // Returns an intent object that you use to check for an update.
        val appUpdateInfoTask = appUpdateManager.appUpdateInfo

        // Checks that the platform will allow the specified type of update.
        appUpdateInfoTask.addOnSuccessListener { appUpdateInfo ->
            if (appUpdateInfo.updateAvailability() == UpdateAvailability.UPDATE_AVAILABLE &&
                // For a flexible update, use AppUpdateType.FLEXIBLE
                appUpdateInfo.isUpdateTypeAllowed(AppUpdateType.FLEXIBLE)
            ) {
                showUpdateInfo(appUpdateInfo)
            }

            if (appUpdateInfo.updateAvailability() == UpdateAvailability.DEVELOPER_TRIGGERED_UPDATE_IN_PROGRESS) {
                appUpdateManager.registerListener(updateListener)
                appUpdateManager.startUpdateFlowForResult(
                    appUpdateInfo,
                    IMMEDIATE,
                    activity,
                    INSTALL_REQUEST_CODE
                )
            }
        }
    }

    private fun startUpdate(updateInfo: AppUpdateInfo) {
        val appUpdateManager = AppUpdateManagerFactory.create(this)
        // Before starting an update, register a listener for updates.
        appUpdateManager.registerListener(updateListener)
        appUpdateManager.startUpdateFlowForResult(
            updateInfo,
            AppUpdateType.FLEXIBLE,
            this,
            INSTALL_REQUEST_CODE
        )
    }

    private fun installUpdate() {
        val appUpdateManager = AppUpdateManagerFactory.create(this)
        appUpdateManager.completeUpdate()
    }

    private val updateListener = object : InstallStateUpdatedListener {

        override fun onStateUpdate(state: InstallState) {
            if (state.installStatus() == InstallStatus.DOWNLOADING || state.installStatus() == InstallStatus.PENDING) {
                val updateProgress = state.bytesDownloaded().toDouble() / 1.0.coerceAtLeast(
                    state.totalBytesToDownload().toDouble()
                )
                showProgressInfo(updateProgress)
            } else {
                if (state.installStatus() == InstallStatus.DOWNLOADED) {
                    installUpdate()
                }

                if (arrayOf(
                        InstallStatus.DOWNLOADED,
                        InstallStatus.CANCELED,
                        InstallStatus.INSTALLING,
                        InstallStatus.INSTALLED
                    ).any { it == state.installStatus() }
                ) {
                    showProgressInfo(null)
                    AppUpdateManagerFactory.create(this@BaseUpdateActivity).unregisterListener(this)
                }
            }
        }
    }

    private fun showUpdateInfo(updateInfo: AppUpdateInfo?) {
        hasUpdateSnake = if (updateInfo != null) {
            Snackbar.make(
                findViewById(android.R.id.content),
                R.string.update_has,
                Snackbar.LENGTH_INDEFINITE
            ).apply {
                setAction(R.string.update_to_download) { startUpdate(updateInfo) }
                show()
            }
        } else {
            hasUpdateSnake?.dismiss()
            null
        }
    }

    @SuppressLint("SetTextI18n")
    private fun showProgressInfo(progress: Double?) {
        if (progress != null) {
            if (progressSnackbar == null) {
                progressSnackbar = Snackbar.make(
                    findViewById(android.R.id.content), R.string.update_inprogress,
                    Snackbar.LENGTH_INDEFINITE
                ).apply {
                    val contentLay = view.findViewById<View>(
                        com.google.android.material.R.id.snackbar_text
                    ).parent as ViewGroup
                    val item = LayoutInflater.from(this@BaseUpdateActivity).inflate(
                        R.layout.part_progressed_bar, contentLay, false
                    )
                    contentLay.addView(item)
                    show()
                }
            }
            progressSnackbar?.let {
                val contentLay = it.view.findViewById<View>(
                    com.google.android.material.R.id.snackbar_text
                ).parent as ViewGroup
                contentLay.findViewById<TextView>(R.id.progressValue).apply {
                    text = "${(progress * MAX_PROGRESS).toInt()} %"
                }
            }
        } else {
            progressSnackbar?.dismiss()
            progressSnackbar = null
        }
    }

    private companion object {
        const val INSTALL_REQUEST_CODE = 999
        const val MAX_PROGRESS = 100.0
    }
}
