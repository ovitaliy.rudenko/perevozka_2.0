package ru.perevozka24.perevozka24.ui.main.items.items

import com.xwray.groupie.Group
import ru.perevozka24.perevozka24.ui.common.ItemMapper
import ru.perevozka24.perevozka24.ui.main.items.ItemAction
import ru.perevozka24.perevozka24.ui.main.items.models.ItemModel

object EquipmentMapper : ItemMapper<ItemModel, ItemAction, Group> {
    override fun map(e: ItemModel, callback: ItemAction, params: Any): Group {
        return EquipmentItem(e, callback, params as Boolean)
    }
}
