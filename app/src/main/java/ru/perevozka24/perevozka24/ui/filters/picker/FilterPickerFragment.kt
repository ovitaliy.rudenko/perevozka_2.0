package ru.perevozka24.perevozka24.ui.filters.picker

import android.os.Bundle
import android.view.View
import androidx.core.os.bundleOf
import androidx.core.widget.addTextChangedListener
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.GroupieViewHolder
import kotlinx.android.synthetic.main.content_filter_picker.*
import ru.perevozka24.perevozka24.R
import ru.perevozka24.perevozka24.ui.base.BaseContentLoaderFragment
import ru.perevozka24.perevozka24.ui.common.ext.updateOrReplace
import ru.perevozka24.perevozka24.ui.filters.OnPickerValueSelected
import ru.perevozka24.perevozka24.ui.filters.models.PickerModel
import ru.perevozka24.perevozka24.ui.filters.picker.loader.PickerClearMapper
import ru.perevozka24.perevozka24.ui.filters.picker.loader.PickerMapper
import ru.perevozka24.perevozka24.ui.viewModel

class FilterPickerFragment : BaseContentLoaderFragment() {

    private val viewModel: FilterPickerViewModel by viewModel()
    override val contentLayoutId: Int = R.layout.content_filter_picker
    private val adapter = GroupAdapter<GroupieViewHolder>()
    private val item by lazy(LazyThreadSafetyMode.NONE) {
        checkNotNull(arguments?.getParcelable(TYPE) as? PickerModel)
    }
    private val onPickerValueSelected by lazy(LazyThreadSafetyMode.NONE) {
        requireActivity() as OnPickerValueSelected
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel.init(item, requireArguments().getBoolean(CARGO_ONLY))
    }

    private val onClickListener = View.OnClickListener { v ->
        onPickerValueSelected.onSelectPickerValue(v.tag as PickerModel)
        activity?.onBackPressed()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        subscribe(viewModel.result)
        filterPickerList.adapter = adapter
        val clearTitle = requireArguments().getInt(CLEAR_TITLE, 0)
        val clear = if (clearTitle != 0) {
            PickerClearMapper.clearItem(item, clearTitle, onClickListener)
        } else {
            null
        }
        viewModel.filtered.observe(viewLifecycleOwner, {
            val mapped = PickerMapper.map(it, clear, onClickListener)
            adapter.updateOrReplace(mapped)
        })
        filterQuery.addTextChangedListener { viewModel.filterChanged(it.toString()) }
        filterQuery.setHint(PickerMapper.mapToHint(item))
    }

    companion object {
        private const val TYPE = "type"
        private const val CLEAR_TITLE = "clear_title"
        private const val CARGO_ONLY = "cargo_only"

        fun create(item: PickerModel, clearTitle: Int, cargoOnly: Boolean) =
            FilterPickerFragment().apply {
                arguments =
                    bundleOf(TYPE to item, CLEAR_TITLE to clearTitle, CARGO_ONLY to cargoOnly)
            }
    }
}
