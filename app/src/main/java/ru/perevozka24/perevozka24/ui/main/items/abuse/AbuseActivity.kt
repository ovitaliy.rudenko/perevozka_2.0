package ru.perevozka24.perevozka24.ui.main.items.abuse

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.commit
import kotlinx.android.synthetic.main.activity_fragment_container.*
import ru.perevozka24.perevozka24.R
import ru.perevozka24.perevozka24.ui.base.BaseFragmentActivity

class AbuseActivity : BaseFragmentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        toolbarTitleView.setText(R.string.abuse_title)
        if (savedInstanceState == null) {
            val itemId = checkNotNull(intent.getStringExtra(ITEM_ID))
            supportFragmentManager.commit {
                replace(R.id.container, AbuseFragment.newInstance(itemId))
            }
        }
    }

    companion object {
        private const val ITEM_ID = "item_id"

        fun create(context: Context, itemId: String): Intent {
            return Intent(context, AbuseActivity::class.java).apply {
                putExtra(ITEM_ID, itemId)
            }
        }
    }
}
