package ru.perevozka24.perevozka24.ui.main

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.navigation.NavDirections
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import ru.perevozka24.perevozka24.data.features.auth.AuthRepository
import ru.perevozka24.perevozka24.ui.LiveEvent
import ru.perevozka24.perevozka24.ui.common.livedata.distinctUntilChanged
import ru.perevozka24.perevozka24.ui.common.livedata.postValue

class MainViewModel(
    private val authRepository: AuthRepository
) : ViewModel() {

    private val _screenMode: LiveData<MainScreenMode> = MutableLiveData(MainScreenMode.Equipment)
    val screenMode: LiveData<MainScreenMode> = _screenMode.distinctUntilChanged()
    val openProfileEvent = LiveEvent<NavDirections>()

    fun changeMode(mode: MainScreenMode) {
        _screenMode.postValue(mode)
    }

    fun openProfile() {
        GlobalScope.launch {
            val clazz = when (authRepository.getUser()) {
                null -> MainFragmentDirections.actionGlobalLoginActivity()
                else -> MainFragmentDirections.actionGlobalProfileActivity()
            }
            openProfileEvent.postValue(clazz)
        }
    }
}
