package ru.perevozka24.perevozka24.ui.auth.register.step2

import androidx.navigation.fragment.navArgs
import ru.perevozka24.perevozka24.ui.viewModel

class PhoneRegisterFragment : BaseRegisterFragment<PhoneRegisterViewModel>() {

    private val arg: PhoneRegisterFragmentArgs by navArgs()
    override val contact: String
        get() = arg.contact
    protected override val viewModel: PhoneRegisterViewModel by viewModel()
}
