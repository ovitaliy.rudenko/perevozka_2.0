package ru.perevozka24.perevozka24.ui.main.orders.favorite

import android.os.Bundle
import org.kodein.di.Kodein
import org.kodein.di.android.x.kodein
import ru.perevozka24.perevozka24.ui.main.orders.base.BaseOrdersFragment
import ru.perevozka24.perevozka24.ui.viewModel
import ru.perevozka24.perevozka24.utils.trackEvent

class FavoriteOrdersFragment : BaseOrdersFragment<FavoriteOrderViewModel>() {

    override val kodein: Kodein by kodein()
    override val viewModel: FavoriteOrderViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        trackEvent("visited_fav_orders")
    }
}
