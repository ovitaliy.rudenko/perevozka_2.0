package ru.perevozka24.perevozka24.ui.main.orders.models

import ru.perevozka24.perevozka24.data.features.offers.entity.OfferEntity
import ru.perevozka24.perevozka24.data.features.offers.entity.OffersEntity
import ru.perevozka24.perevozka24.data.utils.escape
import ru.perevozka24.perevozka24.ui.common.Mapper
import ru.perevozka24.perevozka24.ui.filters.models.CategoryMapper

object OfferMapper : Mapper<OfferEntity, OfferModel> {
    override fun map(e: OfferEntity): OfferModel {
        return OfferModel(
            id = e.id,
            name = e.name.trim().escape(),
            shipment = e.shipment,
            cityId = e.cityId,
            categoryId = e.categoryId,
            offer = e.offer?.trim()?.escape(),
            cityName = e.cityName?.trim(),
            dateAdded = e.dateAdded,
            contacts = e.contacts?.trim()?.escape(),
            country = e.country?.trim(),
            message = e.message?.trim(),
            abuseCount = e.abuseCount,
            categoryImage = e.categoryImage,
            remainingTime = e.remainingTime,
            price = e.price,
            payRate = e.payRate,
            payType = e.payType,
            favorite = e.isFavorite,
            cityNameTo = e.cityTo,
            status = e.status?.let { OfferProgressStatus.Mapper.map(e.status) }
                ?: OfferProgressStatus.OPENED,
            gallery = e.gallery?.let { ImageModel.Mapper.map(it) } ?: emptyList(),
            address = e.address?.escape(),
            addressTo = e.addressTo?.escape(),
            companyName = e.companyName?.escape(),
            companyUrl = e.companyUrl,
            showContact = e.showContacts ?: false,
            code = OfferStatus.NONE,
            performTime = getPerformTime(e),
            category = CategoryMapper.map(checkNotNull(e.parentCategory))
        )
    }

    private const val PERIOD = 2
    private const val FIXED = 3
    private const val BEFORE = 4
    private fun getPerformTime(e: OfferEntity): PerformTime {
        val type = e.performDateType
        val date1 = e.performDateDate1
        val date2 = e.performDateDate2
        return when {
            type == PERIOD && date1 != null && date2 != null -> PerformTime.Period(date1, date2)
            type == FIXED && date1 != null -> PerformTime.Fixed(date1)
            type == BEFORE && date1 != null -> PerformTime.Before(date1)
            else -> PerformTime.AnyTime
        }
    }
}

object OffersMapper : Mapper<OffersEntity, OffersModel> {
    override fun map(e: OffersEntity): OffersModel {
        return OffersModel(
            data = OfferMapper.map(e.data ?: emptyList()),
            status = OfferStatus.NONE,
            viewsCount = e.viewsCount
        )
    }
}
