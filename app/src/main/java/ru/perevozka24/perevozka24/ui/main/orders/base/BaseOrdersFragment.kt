package ru.perevozka24.perevozka24.ui.main.orders.base

import android.graphics.Color
import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.GroupieViewHolder
import kotlinx.android.synthetic.main.content_list.*
import ru.perevozka24.perevozka24.R
import ru.perevozka24.perevozka24.ui.base.BaseContentLoaderFragment
import ru.perevozka24.perevozka24.ui.common.ext.updateOrReplace
import ru.perevozka24.perevozka24.ui.main.orders.AddToFavoriteOrderDelegate
import ru.perevozka24.perevozka24.ui.main.orders.OrderAction
import ru.perevozka24.perevozka24.ui.main.orders.OrderActionImpl
import ru.perevozka24.perevozka24.ui.main.orders.items.OrderItemMapper
import ru.perevozka24.perevozka24.ui.main.orders.models.OfferModel

abstract class BaseOrdersFragment<T : BaseOrdersViewModel> :
    BaseContentLoaderFragment(), AddToFavoriteOrderDelegate {

    override val contentLayoutId: Int = R.layout.content_list

    abstract val viewModel: T

    private val adapter = GroupAdapter<GroupieViewHolder>()
    protected open val actions: OrderAction = OrderActionImpl(this, this)
    protected open val hasExtraActions = true
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        view.setBackgroundColor(Color.WHITE)
        subscribe(viewModel.items)
        viewModel.itemsList.observe(viewLifecycleOwner, Observer {
            val items = it ?: return@Observer
            adapter.updateOrReplace(OrderItemMapper.map(items.data, actions, hasExtraActions))
        })
        contentList.adapter = adapter
    }

    override fun addToFavorite(model: OfferModel) {
        viewModel.addToFavorite(model)
    }
}
