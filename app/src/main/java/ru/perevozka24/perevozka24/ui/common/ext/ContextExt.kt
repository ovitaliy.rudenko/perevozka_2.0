package ru.perevozka24.perevozka24.ui.common.ext

import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import androidx.core.content.ContextCompat
import timber.log.Timber

fun Context.openLink(link: String?) {
    try {
        val fixedLink = link?.run {
            if (!startsWith("http")) "https://$link"
            else link
        }
        val uri = Uri.parse(fixedLink)
        startActivity(Intent(Intent.ACTION_VIEW, uri))
    } catch (ex: ActivityNotFoundException) {
        Timber.e(ex)
        toast("Unable to open: $link")
    }
}

fun Context.call(phone: String) {
    try {
        val intent = Intent(Intent.ACTION_DIAL)
        intent.data = Uri.parse("tel:$phone")
        startActivity(intent)
    } catch (ex: ActivityNotFoundException) {
        Timber.e(ex)
        toast("Unable to call: $phone")
    }
}

fun Context.email(email: String) {
    try {
        val intent = Intent(Intent.ACTION_SENDTO)
        intent.data = Uri.parse("mailto:$email")
        startActivity(intent)
    } catch (ex: ActivityNotFoundException) {
        Timber.e(ex)
        toast("Unable to mail: $email")
    }
}

fun Context.hasPermission(vararg permission: String): Boolean {
    return permission.map {
        ContextCompat.checkSelfPermission(this, it) == PackageManager.PERMISSION_GRANTED
    }.all {
        it == true
    }
}
