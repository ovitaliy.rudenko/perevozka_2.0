package ru.perevozka24.perevozka24.ui.filters.models

import kotlinx.android.parcel.Parcelize
import ru.perevozka24.perevozka24.ICON_DEFAULT

@Parcelize
data class CategorySubCategoryModel(
    val subCategoryId: String?,
    val icon: String?,
    val subCategoryName: String?,
    val categoryName: String?,
    val categoryId: String?,
    val categoryIcon: Int,
    val shipment: Int,
    val sale: Int
) : PickerModel {

    fun toCategory() = CategoryModel(
        id = categoryId,
        name = categoryName,
        shipment = shipment,
        sale = sale,
        icon = categoryIcon
    )

    fun toSubCategory() = SubCategoryModel(
        id = subCategoryId,
        icon = icon,
        name = subCategoryName,
        categoryId = categoryId,
        shipment = shipment
    )

    companion object {
        fun default(categoryId: String? = null, categoryName: String? = null) =
            CategorySubCategoryModel(
                subCategoryId = null,
                icon = ICON_DEFAULT,
                subCategoryName = null,
                categoryName = categoryName,
                categoryId = categoryId,
                shipment = 0,
                sale = 0,
                categoryIcon = 0
            )

        fun create(
            category: CategoryModel,
            subCategory: SubCategoryModel
        ): CategorySubCategoryModel {
            return CategorySubCategoryModel(
                categoryId = category.id,
                categoryName = category.name,
                shipment = category.shipment,
                sale = category.sale,
                subCategoryId = subCategory.id,
                icon = subCategory.icon,
                subCategoryName = subCategory.name,
                categoryIcon = category.icon
            )
        }
    }
}
