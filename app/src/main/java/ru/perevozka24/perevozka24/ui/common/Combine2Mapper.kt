package ru.perevozka24.perevozka24.ui.common

interface Combine2Mapper<E1, E2, M> {
    fun map(e1: E1, e2: E2): M
    fun map(e1: E1, list: List<E2>): List<M> = list.map { map(e1, it) }
}
