package ru.perevozka24.perevozka24.ui.common.ext

import android.graphics.Bitmap
import android.widget.ImageView
import androidx.swiperefreshlayout.widget.CircularProgressDrawable
import com.bumptech.glide.Glide
import com.bumptech.glide.load.Transformation
import ru.perevozka24.perevozka24.R

const val STROKE_WIDTH = 5f
const val CENTER_RADIUS = 30f

fun ImageView.load(
    path: String?,
    centerCrop: Boolean = false,
    transformations: Transformation<Bitmap>? = null
) {
    path?.let {
        val circularProgressDrawable = CircularProgressDrawable(context)
        circularProgressDrawable.strokeWidth =
            STROKE_WIDTH
        circularProgressDrawable.centerRadius =
            CENTER_RADIUS
        circularProgressDrawable.start()
        Glide.with(this)
            .load(it)
            .error(R.drawable.ic_error_outline_black_24dp)
            .placeholder(circularProgressDrawable)
            .dontAnimate()
            .apply {
                if (centerCrop) centerCrop()
                transformations?.let { tr ->
                    transform(tr)
                }
            }
            .into(this)
    } ?: {
        setImageDrawable(null)
    }()
}
