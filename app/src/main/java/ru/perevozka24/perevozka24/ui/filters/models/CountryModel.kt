package ru.perevozka24.perevozka24.ui.filters.models

import kotlinx.android.parcel.Parcelize

@Parcelize
data class CountryModel(
    val id: String?,
    val name: String?,
    val direction: Direction
) : PickerModel {
    companion object {
        fun default(direction: Direction = Direction.NONE) = CountryModel(
            id = null,
            name = null,
            direction = direction
        )
    }
}
