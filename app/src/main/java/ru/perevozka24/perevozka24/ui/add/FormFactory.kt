package ru.perevozka24.perevozka24.ui.add

import ru.perevozka24.perevozka24.data.features.filter.FilterPickerDataSource
import ru.perevozka24.perevozka24.ui.add.address.AddressCargoFromStepFragment
import ru.perevozka24.perevozka24.ui.add.address.AddressCargoToStepFragment
import ru.perevozka24.perevozka24.ui.add.address.AddressEquipmentSellStepFragment
import ru.perevozka24.perevozka24.ui.add.address.AddressStepFragment
import ru.perevozka24.perevozka24.ui.add.category.CategoryStepFragment
import ru.perevozka24.perevozka24.ui.add.contacts.ContactsStepFragment
import ru.perevozka24.perevozka24.ui.add.extra.ExtraInfoStepFragment
import ru.perevozka24.perevozka24.ui.add.photo.AddPhotoStepFragment
import ru.perevozka24.perevozka24.ui.add.subcategory.SubCategoryStepFragment
import ru.perevozka24.perevozka24.ui.add.task.TaskStepFragment
import ru.perevozka24.perevozka24.ui.common.weak
import ru.perevozka24.perevozka24.ui.filters.models.CategoryModel

abstract class FormFactory {
    abstract val steps: Array<Class<out ICreateOrderStep>>
    val count: Int get() = steps.size

    companion object {
        private val sellEquipmentFactory: FormFactory by weak { SellEquipmentFactory() }
        private val cargoFactory: FormFactory by weak { CargoFactory() }
        private val passengerFactory: FormFactory by weak { PassengerFactory() }
        private val dostFactory: FormFactory by weak { DostFactory() }
        private val notCargoFactory: FormFactory by weak { DefaultFactory() }

        fun create(category: CategoryModel?): FormFactory = when {
            category?.id == FilterPickerDataSource.SPEC_TYPE && category.sale == 1 -> sellEquipmentFactory
            category?.id == FilterPickerDataSource.CARGO_TYPE -> cargoFactory
            category?.id == FilterPickerDataSource.PASSENGER_TYPE -> passengerFactory
            category?.id == FilterPickerDataSource.DOST_TYPE -> dostFactory
            else -> notCargoFactory
        }
    }

    private class SellEquipmentFactory : FormFactory() {
        override val steps: Array<Class<out ICreateOrderStep>> = arrayOf(
            CategoryStepFragment::class.java,
            SubCategoryStepFragment::class.java,
            AddressEquipmentSellStepFragment::class.java,
            TaskStepFragment::class.java,
            ExtraInfoStepFragment::class.java,
            ContactsStepFragment::class.java
        )
    }

    private class CargoFactory : FormFactory() {
        override val steps: Array<Class<out ICreateOrderStep>> = arrayOf(
            CategoryStepFragment::class.java,
            SubCategoryStepFragment::class.java,
            AddressCargoFromStepFragment::class.java,
            AddressCargoToStepFragment::class.java,
            TaskStepFragment::class.java,
            AddPhotoStepFragment::class.java,
            ExtraInfoStepFragment::class.java,
            ContactsStepFragment::class.java
        )
    }

    private class PassengerFactory : FormFactory() {
        override val steps: Array<Class<out ICreateOrderStep>> = arrayOf(
            CategoryStepFragment::class.java,
            SubCategoryStepFragment::class.java,
            AddressCargoFromStepFragment::class.java,
            AddressCargoToStepFragment::class.java,
            TaskStepFragment::class.java,
            ExtraInfoStepFragment::class.java,
            ContactsStepFragment::class.java
        )
    }

    private class DostFactory : FormFactory() {
        override val steps: Array<Class<out ICreateOrderStep>> = arrayOf(
            CategoryStepFragment::class.java,
            SubCategoryStepFragment::class.java,
            AddressStepFragment::class.java,
            TaskStepFragment::class.java,
            ExtraInfoStepFragment::class.java,
            ContactsStepFragment::class.java
        )
    }

    private class DefaultFactory : FormFactory() {
        override val steps: Array<Class<out ICreateOrderStep>> = arrayOf(
            CategoryStepFragment::class.java,
            SubCategoryStepFragment::class.java,
            AddressStepFragment::class.java,
            TaskStepFragment::class.java,
            AddPhotoStepFragment::class.java,
            ExtraInfoStepFragment::class.java,
            ContactsStepFragment::class.java
        )
    }
}
