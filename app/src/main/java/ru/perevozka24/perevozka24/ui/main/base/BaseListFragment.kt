package ru.perevozka24.perevozka24.ui.main.base

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.text.HtmlCompat
import androidx.lifecycle.Observer
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.kotlinandroidextensions.GroupieViewHolder
import ru.perevozka24.perevozka24.R
import ru.perevozka24.perevozka24.data.Result
import ru.perevozka24.perevozka24.data.isEmpty
import ru.perevozka24.perevozka24.ui.base.BaseFragment
import ru.perevozka24.perevozka24.ui.common.ext.openLink
import ru.perevozka24.perevozka24.ui.common.ext.showOnly
import ru.perevozka24.perevozka24.ui.viewModel
import ru.perevozka24.perevozka24.utils.trackEvent

abstract class BaseListFragment<VM : BaseContentViewModel<out ContentModel>> : BaseFragment() {

    protected abstract val viewModel: VM
    protected abstract val layoutId: Int
    protected val adapter = GroupAdapter<GroupieViewHolder>()

    private val equipmentVieModel: EquipmentButtonVieModel by viewModel()

    final override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? =
        inflater.inflate(layoutId, container, false)

    abstract fun changeFilterClick()

    protected fun setupEquipmentButton(view: View) {
        equipmentVieModel.equipmentLink.observe(viewLifecycleOwner) { linkId ->
            view.setOnClickListener {
                trackEvent(
                    "add_ad",
                    mapOf("type" to "main-screen")
                )
                view.context.openLink(getString(linkId))
            }
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val recyclerSwipeToRefreshView =
            view.findViewById<SwipeRefreshLayout>(R.id.recyclerSwipeToRefreshView)

        recyclerSwipeToRefreshView.setOnRefreshListener { viewModel.refresh() }

        view.findViewById<TextView>(R.id.emptyMessageText).apply {
            text = HtmlCompat.fromHtml(
                getString(R.string.try_to_change_filter),
                HtmlCompat.FROM_HTML_MODE_LEGACY
            )
            setOnClickListener { changeFilterClick() }
        }

        viewModel.items.observe(viewLifecycleOwner, Observer {
            val r = it ?: return@Observer

            when (r) {
                is Result.Success -> {
                    if (r.isEmpty()) {
                        showOnly(
                            view,
                            R.id.contentEmpty,
                            R.id.contentContainer,
                            R.id.contentError,
                            R.id.contentLoader
                        )
                    } else {
                        showOnly(
                            view,
                            R.id.contentContainer,
                            R.id.contentEmpty,
                            R.id.contentError,
                            R.id.contentLoader
                        )
                    }

                    recyclerSwipeToRefreshView.isRefreshing = false
                }
                is Result.Error -> {
                    showOnly(
                        view,
                        R.id.contentError,
                        R.id.contentEmpty,
                        R.id.contentContainer,
                        R.id.contentLoader
                    )
                    recyclerSwipeToRefreshView.isRefreshing = false
                }
                is Result.Loading -> {
                    showOnly(
                        view,
                        R.id.contentLoader,
                        R.id.contentEmpty,
                        R.id.contentContainer,
                        R.id.contentError
                    )
                }
            }
        })
    }
}
