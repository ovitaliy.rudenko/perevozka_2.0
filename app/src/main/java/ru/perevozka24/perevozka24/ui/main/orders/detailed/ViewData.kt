package ru.perevozka24.perevozka24.ui.main.orders.detailed

import com.yandex.mapkit.geometry.Point
import com.yandex.mapkit.geometry.Polyline
import ru.perevozka24.perevozka24.ui.main.orders.models.OfferAbuseModel
import ru.perevozka24.perevozka24.ui.main.orders.models.OfferModel

data class ViewData(
    val showFavorite: Boolean,
    val offer: OfferModel,
    val routeData: RouteData?,
    val abuse: List<OfferAbuseModel>?,
    val showContacts: Boolean,
    val loggedIn: Boolean
)

data class RouteData(
    val distance: String,
    val start: Point,
    val end: Point,
    val polyline: Polyline
)
