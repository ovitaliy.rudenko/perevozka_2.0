package ru.perevozka24.perevozka24.ui.filters.filter

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.liveData
import androidx.lifecycle.switchMap
import ru.perevozka24.perevozka24.data.ResourceProvider
import ru.perevozka24.perevozka24.data.features.filter.FilterRepository
import ru.perevozka24.perevozka24.ui.base.BaseViewModel
import ru.perevozka24.perevozka24.ui.common.livedata.postValue
import ru.perevozka24.perevozka24.ui.filters.models.FilterCargoModel
import ru.perevozka24.perevozka24.ui.filters.models.FilterItemsEquipmentModel
import ru.perevozka24.perevozka24.ui.filters.models.FilterModel
import ru.perevozka24.perevozka24.ui.filters.models.FilterOfferModel
import ru.perevozka24.perevozka24.ui.filters.models.PickerModel

class FilterSelectionViewModel(
    private val filterRepository: FilterRepository,
    resourceProvider: ResourceProvider
) : BaseViewModel(resourceProvider) {

    val type: LiveData<FilterType> = MutableLiveData()
    val selection: LiveData<FilterModel> =
        type.switchMap {
            liveData {
                val data = filterRepository.getFilterItems(it)
                if (initialData == null) initialData = data
                emit(data)
            }
        }

    var initialData: FilterModel? = null

    fun init(type: FilterType) {
        this.type.postValue(type)
    }

    fun pickerValueChanged(value: PickerModel) {
        val filter = selection.value ?: return
        val newFilter = when (filter) {
            is FilterItemsEquipmentModel -> filter.updated(value)
            is FilterCargoModel -> filter.updated(value)
            is FilterOfferModel -> filter.updated(value)
            else -> error("unknown")
        }
        selection.postValue(newFilter)
    }

    fun pickerValueChanged(type: Class<out PickerModel>, value: PickerModel?) {
        val filter = selection.value ?: return
        val newFilter = when (filter) {
            is FilterCargoModel -> filter.updated(type, value)
            is FilterOfferModel -> filter.updated(type, value)
            else -> error("unknown")
        }
        selection.postValue(newFilter)
    }

    fun saveFilter() {
        val type = type.value ?: return
        val filter = selection.value ?: return
        filterRepository.setFilterItems(type, filter)
    }
}
