package ru.perevozka24.perevozka24.ui.leftMenu.items

import android.view.View
import com.xwray.groupie.kotlinandroidextensions.GroupieViewHolder
import com.xwray.groupie.kotlinandroidextensions.Item
import kotlinx.android.synthetic.main.item_menu_auth.view.*
import ru.perevozka24.perevozka24.R

class AuthDoLoginMenuItem(
    private val loginClick: View.OnClickListener,
) : Item() {
    override fun getId(): Long = 0
    override fun getLayout(): Int = R.layout.item_menu_auth

    override fun bind(viewHolder: GroupieViewHolder, position: Int) {
        with(viewHolder.itemView) {
            itemMenuAuthLogin.setOnClickListener(loginClick)
        }
    }
}
