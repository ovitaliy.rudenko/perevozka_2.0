package ru.perevozka24.perevozka24.ui.auth.register.step2

import ru.perevozka24.perevozka24.data.ResourceProvider
import ru.perevozka24.perevozka24.data.features.auth.AuthRepository
import ru.perevozka24.perevozka24.data.features.auth.RegistrationParams
import ru.perevozka24.perevozka24.data.utils.toInt
import ru.perevozka24.perevozka24.ui.auth.models.LoginModel

class PhoneRegisterViewModel(
    private val authRepository: AuthRepository,
    resourceProvider: ResourceProvider
) : BaseRegisterViewModel(resourceProvider) {

    override suspend fun registerDo(): LoginModel {
        val params = RegistrationParams(
            firstname = checkNotNull(usernameField.value?.value),
            email = contact,
            password = checkNotNull(passwordField.value?.value),
            passconf = checkNotNull(passwordField.value?.value),
            agreement = checkNotNull(agreementCheckedField.value?.value).toInt()
        )
        return authRepository.registerPhone(params)
    }
}
