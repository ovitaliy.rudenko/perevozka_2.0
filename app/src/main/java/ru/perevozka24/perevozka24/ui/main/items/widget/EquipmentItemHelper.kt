package ru.perevozka24.perevozka24.ui.main.items.widget

import android.view.View
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import kotlinx.android.synthetic.main.part_equipment.view.*
import ru.perevozka24.perevozka24.R
import ru.perevozka24.perevozka24.ui.common.ext.load
import ru.perevozka24.perevozka24.ui.common.ext.setVisibleText
import ru.perevozka24.perevozka24.ui.main.items.models.ItemModel

object EquipmentItemHelper {
    fun setUp(
        view: View,
        model: ItemModel
    ) {
        with(view) {
            equipmentImage.load(
                model.image,
                true,
                RoundedCorners(resources.getDimensionPixelOffset(R.dimen.round_corners))
            )
            equipmentName.setVisibleText(model.name)
            arrayOf(equipmentUserNameIcon, equipmentUserName).setVisibleText(model.userName)
            arrayOf(equipmentUserPhoneIcon, equipmentUserPhone).setVisibleText(model.phone)
            arrayOf(equipmentUserAddressIcon, equipmentUserAddress).setVisibleText(model.cityName)
            equipmentUserRating.rating = model.rating
            equipmentUserPrice.setVisibleText(model.costs.firstOrNull())
        }
    }
}
