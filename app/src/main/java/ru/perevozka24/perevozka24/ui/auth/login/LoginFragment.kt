package ru.perevozka24.perevozka24.ui.auth.login

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.Navigation
import kotlinx.android.synthetic.main.fragment_auth_login.*
import ru.perevozka24.perevozka24.R
import ru.perevozka24.perevozka24.data.Result
import ru.perevozka24.perevozka24.ui.auth.RegistrationActivity
import ru.perevozka24.perevozka24.ui.base.BaseFragment
import ru.perevozka24.perevozka24.ui.common.ext.restartApp
import ru.perevozka24.perevozka24.ui.common.ext.showSnackBar
import ru.perevozka24.perevozka24.ui.common.ext.visible
import ru.perevozka24.perevozka24.ui.common.widgets.form.addTextChangedListener
import ru.perevozka24.perevozka24.ui.viewModel

class LoginFragment : BaseFragment() {

    private val viewModel: LoginViewModel by viewModel()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(R.layout.fragment_auth_login, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        usernameView.addTextChangedListener { viewModel.userNameChange(it.toString().trim()) }
        viewModel.usernameFieldError.observe(viewLifecycleOwner, {
            usernameView.error(it)
        })
        passwordView.addTextChangedListener { viewModel.passwordChange(it.toString()) }
        viewModel.passwordError.observe(viewLifecycleOwner, {
            passwordView.error(it)
        })
        loginButton.setOnClickListener { viewModel.login() }
        registerButton.setOnClickListener {
            startActivity(
                Intent(requireContext(), RegistrationActivity::class.java)
            )
        }
        forgotPasswordButton.setOnClickListener(
            Navigation.createNavigateOnClickListener(
                R.id.action_loginFragment_to_restorePasswordFragment
            )
        )
        viewModel.login.observe(viewLifecycleOwner, {
            when (it) {
                is Result.Loading -> loginProgressBar.visible = true
                is Result.Error -> {
                    loginProgressBar.visible = false
                    showSnackBar(it.message)
                }
                is Result.Success -> {
                    loginProgressBar.visible = false
                    activity?.restartApp()
                }
            }
        })
    }
}
