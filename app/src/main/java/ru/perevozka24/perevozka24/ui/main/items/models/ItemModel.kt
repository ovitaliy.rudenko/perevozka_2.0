package ru.perevozka24.perevozka24.ui.main.items.models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize
import ru.perevozka24.perevozka24.ui.main.base.ContentModel

@Parcelize
data class ItemModel(
    val id: String,
    val name: String,
    val phone: String,
    val userName: String?,
    val userId: String,
    val coordX: Double?,
    val coordY: Double?,
    val image: String?,
    val countryId: String?,
    val countryName: String?,
    val regionId: String?,
    val regionName: String?,
    val cityId: String?,
    val cityName: String?,
    val opts: List<OptionModel>,
    val costs: List<String>,
    val text: String?,
    val shipment: Long?,
    val rating: Float,
    val categoryId: Long?,
    val parentCategory: String?,
    val itemTime: String?,
    val address: String?,
    val showOffer: Boolean,
    val favorite: Boolean,
    val categoryIcon: String
) : Parcelable

class ItemsList : ArrayList<ItemModel>(), ContentModel
