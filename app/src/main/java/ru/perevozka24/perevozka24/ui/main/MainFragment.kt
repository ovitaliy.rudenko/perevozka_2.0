package ru.perevozka24.perevozka24.ui.main

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.widget.EditText
import android.widget.TextView.OnEditorActionListener
import androidx.fragment.app.commit
import androidx.lifecycle.Observer
import com.yandex.mapkit.MapKitFactory
import kotlinx.android.synthetic.main.fragment_main.*
import ru.perevozka24.perevozka24.R
import ru.perevozka24.perevozka24.ui.base.BaseFragment
import ru.perevozka24.perevozka24.ui.common.DrawerHolder
import ru.perevozka24.perevozka24.ui.common.ext.hideKeyboard
import ru.perevozka24.perevozka24.ui.main.base.ISearchContainer
import ru.perevozka24.perevozka24.ui.viewModel
import ru.perevozka24.perevozka24.utils.trackEvent

class MainFragment : BaseFragment(), ISearchContainer {

    private val viewModel: MainViewModel by viewModel()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? =
        inflater.inflate(R.layout.fragment_main, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        MapKitFactory.initialize(activity)

        viewModel.screenMode.observe(viewLifecycleOwner, Observer {
            mainToolbarContainer.removeAllViews()
            val toolbar = it.toolbarType.crateToolbarView(requireContext()).apply {
                setupOnDrawerClick { (activity as? DrawerHolder)?.openLeftDrawer() }
                trackEvent("show_main_menu", mapOf("type" to "search-panel"))
            }
            mainToolbarContainer.addView(toolbar)

            childFragmentManager.commit {
                replace(R.id.mainContentContainer, it.fragment())
            }
        })
    }

    override fun subscribeSearch(value: String, queryChanged: (String) -> Unit) {
        val searchField = view?.findViewById<EditText>(R.id.mainTopBarSearchFiled)
        searchField?.setText(value)
        fun performSearch() {
            val query = searchField?.text?.toString() ?: ""
            queryChanged(query)
            hideKeyboard(requireActivity())
            mainContentContainer.requestFocus()
        }
        view?.findViewById<View>(R.id.mainTopBarSearchButton)?.setOnClickListener {
            performSearch()
        }
        searchField?.setOnEditorActionListener(OnEditorActionListener { _, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                performSearch()
                true
            } else {
                false
            }
        })
    }

    override fun clearSearch() {
        view?.findViewById<EditText>(R.id.mainTopBarSearchFiled)?.setText("")
    }
}
