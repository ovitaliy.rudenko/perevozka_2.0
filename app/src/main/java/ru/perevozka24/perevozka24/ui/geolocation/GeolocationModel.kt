package ru.perevozka24.perevozka24.ui.geolocation

import kotlinx.android.parcel.Parcelize
import ru.perevozka24.perevozka24.data.features.geolocation.LocationTrackerEntity
import ru.perevozka24.perevozka24.ui.base.INameModel

@Parcelize
data class GeolocationModel(
    val id: String?,
    override val name: String
) : INameModel {
    object Mapper :
        ru.perevozka24.perevozka24.ui.common.Mapper<LocationTrackerEntity, GeolocationModel> {
        override fun map(e: LocationTrackerEntity): GeolocationModel {
            return GeolocationModel(e.id, e.name)
        }
    }

    companion object {
        fun default() = GeolocationModel(null, "")
    }
}
