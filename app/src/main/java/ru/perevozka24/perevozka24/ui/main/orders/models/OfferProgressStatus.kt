package ru.perevozka24.perevozka24.ui.main.orders.models

import ru.perevozka24.perevozka24.R
import ru.perevozka24.perevozka24.data.features.offers.entity.OfferProgressStatusEntity

enum class OfferProgressStatus {
    OPENED {
        override val title: Int
            get() = R.string.order_status_opened
        override val color: Int
            get() = R.drawable.dr_offer_status_open
    },
    IN_WORK {
        override val title: Int
            get() = R.string.order_status_confirmation
        override val color: Int
            get() = R.drawable.dr_offer_status_in_work
    },
    CLOSED {
        override val title: Int
            get() = R.string.order_status_closed
        override val color: Int
            get() = R.drawable.dr_offer_status_closed
    };

    abstract val title: Int
    abstract val color: Int

    object Mapper :
        ru.perevozka24.perevozka24.ui.common.Mapper<OfferProgressStatusEntity, OfferProgressStatus> {
        override fun map(e: OfferProgressStatusEntity): OfferProgressStatus {
            return when (e) {
                OfferProgressStatusEntity.CLOSED -> CLOSED
                OfferProgressStatusEntity.OPENED -> OPENED
                OfferProgressStatusEntity.IN_WORK -> IN_WORK
            }
        }
    }
}
