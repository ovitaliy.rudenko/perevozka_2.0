package ru.perevozka24.perevozka24.ui.filters.models

import kotlinx.android.parcel.IgnoredOnParcel
import kotlinx.android.parcel.Parcelize

@Parcelize
data class FilterCargoModel(
    val category: CategoryModel,
    val subCategory: SubCategoryModel,
    val country: CountryModel,
    val region: RegionModel,
    val countryTo: CountryModel,
    val regionTo: RegionModel,
    val cargoBody: CargoBodyModel,
    val status: OfferStatusModel?
) : FilterModel {
    @IgnoredOnParcel
    val shipment: Long = 0

    override fun updated(value: PickerModel): FilterModel {
        return when (value) {
            is CategoryModel -> copy(
                category = value,
                subCategory = SubCategoryModel.default(categoryId = value.id)
            )
            is SubCategoryModel -> copy(subCategory = value)
            is CountryModel -> {
                if (value.direction == Direction.FROM) {
                    copy(
                        country = value,
                        region = RegionModel.default(
                            direction = Direction.FROM,
                            countryId = value.id
                        )
                    )
                } else {
                    copy(
                        countryTo = value,
                        regionTo = RegionModel.default(
                            direction = Direction.TO,
                            countryId = value.id
                        )

                    )
                }
            }
            is RegionModel -> {
                if (value.direction == Direction.FROM) {
                    copy(region = value)
                } else {
                    copy(regionTo = value)
                }
            }
            else -> error("unknown type $value")
        }
    }

    override fun updated(type: Class<out PickerModel>, value: PickerModel?): FilterModel {
        return when (type) {
            CargoBodyModel::class.java -> copy(
                cargoBody = value as? CargoBodyModel ?: CargoBodyModel.default()
            )
            OfferStatusModel::class.java -> copy(status = value as? OfferStatusModel)
            else -> error("unknown type $type")
        }
    }
}
