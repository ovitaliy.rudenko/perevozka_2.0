package ru.perevozka24.perevozka24.ui.main.orders.base

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.core.content.res.ResourcesCompat
import androidx.core.view.postDelayed
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.fragment_tab_order.*
import kotlinx.android.synthetic.main.include_filter_order.*
import ru.perevozka24.perevozka24.R
import ru.perevozka24.perevozka24.data.api.SECOND
import ru.perevozka24.perevozka24.ui.common.ext.load
import ru.perevozka24.perevozka24.ui.common.ext.safe
import ru.perevozka24.perevozka24.ui.common.ext.toast
import ru.perevozka24.perevozka24.ui.common.ext.updateOrReplace
import ru.perevozka24.perevozka24.ui.common.ext.visible
import ru.perevozka24.perevozka24.ui.common.items.LoadingItem
import ru.perevozka24.perevozka24.ui.common.widgets.EndlessRecyclerOnScrollListener
import ru.perevozka24.perevozka24.ui.filters.filter.FilterSelectionFragment.Companion.CHANGED_VALUE
import ru.perevozka24.perevozka24.ui.filters.filter.FilterSelectionFragment.Companion.INITIAL_VALUE
import ru.perevozka24.perevozka24.ui.filters.models.CategorySubCategoryModel
import ru.perevozka24.perevozka24.ui.filters.models.FilterModel
import ru.perevozka24.perevozka24.ui.filters.models.PickerModel
import ru.perevozka24.perevozka24.ui.filters.picker.FilterPickerActivity
import ru.perevozka24.perevozka24.ui.filters.picker.loader.PickerClearMapper
import ru.perevozka24.perevozka24.ui.main.base.BaseListFragment
import ru.perevozka24.perevozka24.ui.main.base.DefaultFilterTitleFormatter
import ru.perevozka24.perevozka24.ui.main.base.FilterTitleFormatter
import ru.perevozka24.perevozka24.ui.main.base.ISearchContainer
import ru.perevozka24.perevozka24.ui.main.orders.AddToFavoriteOrderDelegate
import ru.perevozka24.perevozka24.ui.main.orders.OrderAction
import ru.perevozka24.perevozka24.ui.main.orders.OrderActionImpl
import ru.perevozka24.perevozka24.ui.main.orders.items.OrderItemMapper
import ru.perevozka24.perevozka24.ui.main.orders.models.OfferModel
import kotlin.math.abs

abstract class BaseOrderContentFragment<VM : BaseOrderListViewModel> : BaseListFragment<VM>(),
    AddToFavoriteOrderDelegate, AddOrderActionListener {
    override val layoutId: Int = R.layout.fragment_tab_order

    private val orderAction: OrderAction by lazy { OrderActionImpl(this, this) }
    protected open val hasExtraActions = true

    private var scrollEndListener: EndlessRecyclerOnScrollListener? = null

    protected open val filterTitleFormatter: FilterTitleFormatter
        get() = DefaultFilterTitleFormatter

    override fun changeFilterClick() = openFilter()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        (parentFragment as? ISearchContainer)?.subscribeSearch(viewModel.query) { query ->
            viewModel.search(query)
        }

        setupFilter()
        contentFilter.setOnClickListener { openFilter() }

        viewModel.error.observe(viewLifecycleOwner, Observer {
            toast(it)
        })

        checkNotNull(recyclerView)
        recyclerView.adapter = adapter
        val layoutManager = recyclerView.layoutManager as LinearLayoutManager
        scrollEndListener = object : EndlessRecyclerOnScrollListener(layoutManager) {
            override fun onLoadMore(currentPage: Int) {
                safe {
                    if (isVisible) {
                        viewModel.loadNext()
                    }
                }
            }
        }

        recyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                if (isVisible && abs(dy) > resources.getDimension(R.dimen.dp24)) {
                    val buttonVisible = dy < 0
                    itemOfferCreateButton.visible = buttonVisible
                }
            }
        })

        with(itemOfferCreateButton) {
            text = getString(getAddOrderActionTitle())
            setOnClickListener(getAddOrderAction())
        }

        viewModel.itemsModel.observe(viewLifecycleOwner, Observer {
            val orderList = it ?: return@Observer
            scrollEndListener?.let { listener -> recyclerView.removeOnScrollListener(listener) }
            val result = OrderItemMapper.map(
                orderList.list,
                orderAction,
                hasExtraActions
            ).toMutableList()
            if (!orderList.isLast) {
                result.add(LoadingItem.horizontal())
            }
            adapter.updateOrReplace(result) {
                scrollEndListener?.let { listener ->
                    recyclerView.postDelayed(SECOND) {
                        if (isVisible) recyclerView.addOnScrollListener(listener)
                    }
                }
            }
        })

        viewModel.categorySubCategory.observe(viewLifecycleOwner, Observer {
            contentFilterSubCategoryIcon.load(it.icon)
            val categoryName = filterTitleFormatter.format(requireContext(), it)
            contentFilterSubCategoryText.text =
                categoryName ?: resources.getString(R.string.filter_category_default)

            val colorRes = if (categoryName == null) R.color.grey_87 else R.color.black
            val color = ResourcesCompat.getColor(resources, colorRes, null)
            contentFilterSubCategoryText.setTextColor(color)
        })
    }

    override fun onDestroyView() {
        scrollEndListener?.let { recyclerView.removeOnScrollListener(it) }
        super.onDestroyView()
    }

    open fun openFilter() {
        startActivityForResult(
            createFilterIntent(),
            CODE
        )
    }

    open fun openQuickFilter(model: PickerModel) {
        startActivityForResult(
            createQuickFilterIntent(model), CODE_PICKER
        )
    }

    private fun setupFilter() {
        contentFilterSubCategory.setOnClickListener {
            val model = viewModel.categorySubCategory.value ?: CategorySubCategoryModel.default()
            if (model.categoryId == null) {
                openFilter()
            } else {
                openQuickFilter(model)
            }
        }

        viewModel.categorySubCategory.observe(viewLifecycleOwner, Observer {
            contentFilterSubCategoryIcon.load(it.icon)
            val categoryName = filterTitleFormatter.format(requireContext(), it)
            contentFilterSubCategoryText.text =
                categoryName ?: resources.getString(R.string.filter_category_default)

            val colorRes = if (categoryName == null) R.color.grey_87 else R.color.black
            val color = ResourcesCompat.getColor(resources, colorRes, null)
            contentFilterSubCategoryText.setTextColor(color)
        })
    }

    protected abstract fun createFilterIntent(): Intent

    protected open fun createQuickFilterIntent(model: PickerModel) =
        FilterPickerActivity.create(requireContext(), model, PickerClearMapper.mainTitle(model))

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == CODE && resultCode == Activity.RESULT_OK) {
            viewModel.clearSearch()
            (parentFragment as? ISearchContainer)?.clearSearch()
            viewModel.refreshAfterFilterChange(
                data?.getParcelableExtra(INITIAL_VALUE) as? FilterModel,
                data?.getParcelableExtra(CHANGED_VALUE) as? FilterModel
            )
        }
        if (requestCode == CODE_PICKER && resultCode == Activity.RESULT_OK) {
            viewModel.clearSearch()
            (parentFragment as? ISearchContainer)?.clearSearch()
            val value =
                data?.getParcelableExtra<CategorySubCategoryModel>(FilterPickerActivity.MODEL)
            viewModel.subCategoryChanged(checkNotNull(value))
        }

        if (requestCode == REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            data?.getParcelableExtra<OfferModel>(OFFER_TO_REMOVE)?.let {
                viewModel.removeFromList(it)
            }
        }
    }

    override fun addToFavorite(model: OfferModel) {
        viewModel.addToFavorite(model)
    }

    companion object {
        private const val CODE = 1002
        private const val CODE_PICKER = 1003

        const val REQUEST_CODE = 991
        const val OFFER_TO_REMOVE = "offerToRemove"
    }
}
