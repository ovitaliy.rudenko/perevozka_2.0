package ru.perevozka24.perevozka24.ui.add.address

import kotlinx.android.synthetic.main.fragment_add_order_step_address.*
import kotlinx.android.synthetic.main.item_filter_location.view.*
import ru.perevozka24.perevozka24.R
import ru.perevozka24.perevozka24.ui.common.ext.visible

class AddressEquipmentSellStepFragment : BaseAddressStepFragment() {
    override val title: Int = R.string.create_order_step_equipment_sell_title

    override fun initAddressCity() {
        with(addOrderCity) {
            setBackgroundResource(R.drawable.underlined_fa)
            locationIcon.setImageResource(R.drawable.ic_city)
            val defaultTitle = context.getString(R.string.filter_city_info_default)
            locationTitle.text = defaultTitle
            viewModel.city.observe(viewLifecycleOwner, {
                locationTitle.text = it?.name ?: defaultTitle
            })
            setOnClickListener { startPickingCity() }
        }
    }

    override fun initAddressView() {
        addOrderAddress.visible = false
    }

    override fun submit() {
        addOrderViewModel.locationSelected(
            checkNotNull(viewModel.country.value),
            checkNotNull(viewModel.region.value),
            checkNotNull(viewModel.city.value),
            checkNotNull("")
        )
    }
}
