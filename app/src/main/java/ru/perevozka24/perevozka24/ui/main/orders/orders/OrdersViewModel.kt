package ru.perevozka24.perevozka24.ui.main.orders.orders

import ru.perevozka24.perevozka24.data.ResourceProvider
import ru.perevozka24.perevozka24.data.features.filter.FilterRepository
import ru.perevozka24.perevozka24.data.features.offers.OffersRepository
import ru.perevozka24.perevozka24.ui.common.livedata.postValue
import ru.perevozka24.perevozka24.ui.filters.filter.FilterType
import ru.perevozka24.perevozka24.ui.filters.models.CategorySubCategoryModel
import ru.perevozka24.perevozka24.ui.filters.models.FilterOfferModel
import ru.perevozka24.perevozka24.ui.main.ViewMode
import ru.perevozka24.perevozka24.ui.main.orders.base.BaseOrderListViewModel

class OrdersViewModel(
    private val repository: OffersRepository,
    private val filterRepository: FilterRepository,
    resourceProvider: ResourceProvider
) : BaseOrderListViewModel(repository, resourceProvider) {
    override val searchReportName: String = "orders_screen_search"

    init {
        mode.postValue(ViewMode.LIST)
        changeModeVisible.postValue(false)
        updateFilterValue()
        load(0)
    }

    override suspend fun loadPage(offset: Int) = repository.getOffers(offset)

    override suspend fun loadSubCategory(): CategorySubCategoryModel {
        val filter =
            filterRepository.getFilterItems(FilterType.ORDER) as FilterOfferModel
        return CategorySubCategoryModel.create(filter.category, filter.subCategory)
    }

    override suspend fun saveSubCategory(model: CategorySubCategoryModel) {
        val filter = filterRepository.getFilterItems(FilterType.ORDER)
            .updated(model.toCategory())
            .updated(model.toSubCategory())
        filterRepository.setFilterItems(FilterType.ORDER, filter)
    }
}
