package ru.perevozka24.perevozka24.ui.filters.picker.item

import android.view.View
import com.xwray.groupie.kotlinandroidextensions.GroupieViewHolder
import kotlinx.android.synthetic.main.item_filter_loader_region.view.*
import ru.perevozka24.perevozka24.R
import ru.perevozka24.perevozka24.ui.filters.models.RegionModel

data class RegionItem(val model: RegionModel, val onClick: View.OnClickListener) :
    BasePickerItem(onClick) {
    override fun getLayout(): Int = R.layout.item_filter_loader_region
    override fun getId(): Long = model.id?.toLongOrNull() ?: 0

    override fun bind(viewHolder: GroupieViewHolder, position: Int) {
        with(viewHolder.itemView) {
            tag = model
            filterLoaderRegionText.text =
                model.name ?: resources.getString(R.string.filter_region_default)
        }
    }
}
