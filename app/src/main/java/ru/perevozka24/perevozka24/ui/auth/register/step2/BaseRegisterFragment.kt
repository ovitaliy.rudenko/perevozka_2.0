package ru.perevozka24.perevozka24.ui.auth.register.step2

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import kotlinx.android.synthetic.main.fragment_auth_register.*
import ru.perevozka24.perevozka24.R
import ru.perevozka24.perevozka24.data.Result
import ru.perevozka24.perevozka24.ui.base.BaseFragment
import ru.perevozka24.perevozka24.ui.common.ext.restartApp
import ru.perevozka24.perevozka24.ui.common.ext.setVisibleText
import ru.perevozka24.perevozka24.ui.common.ext.showSnackBar
import ru.perevozka24.perevozka24.ui.common.ext.visible
import ru.perevozka24.perevozka24.ui.common.widgets.form.addTextChangedListener

abstract class BaseRegisterFragment<VM : BaseRegisterViewModel> : BaseFragment() {

    protected abstract val viewModel: VM
    protected abstract val contact: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel.init(contact)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(R.layout.fragment_auth_register, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        // username
        usernameView.addTextChangedListener { viewModel.userNameChange(it.toString()) }
        viewModel.usernameFieldError.observe(viewLifecycleOwner, {
            usernameView.error(it)
        })

        // password
        passwordView.addTextChangedListener { viewModel.passwordChange(it.toString()) }
        viewModel.passwordError.observe(viewLifecycleOwner, {
            passwordView.error(it)
        })
        // agreement
        registrationAgreementCheck.setOnCheckedChangeListener { _, isChecked ->
            viewModel.agreementCheckChanged(isChecked)
        }
        viewModel.agreementError.observe(viewLifecycleOwner, Observer {
            registrationAgreementError.setVisibleText(it)
        })

        // do registration
        registerButton.setOnClickListener { viewModel.register() }
        viewModel.registering.observe(viewLifecycleOwner, Observer {
            when (it) {
                is Result.Loading -> registerProgressBar.visible = true
                is Result.Error -> {
                    registerProgressBar.visible = false
                    showSnackBar(it.message)
                }
                is Result.Success -> {
                    registerProgressBar.visible = false
                    activity?.restartApp()
                }
            }
        })
    }
}
