package ru.perevozka24.perevozka24.ui.main.base

import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import com.yandex.mapkit.MapKitFactory
import com.yandex.mapkit.geometry.Point
import com.yandex.mapkit.map.CameraPosition
import com.yandex.mapkit.map.MapObject
import com.yandex.mapkit.map.MapObjectCollection
import kotlinx.android.synthetic.main.fragment_main_equipment_map.*
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import org.kodein.di.generic.instance
import ru.perevozka24.perevozka24.data.Result
import ru.perevozka24.perevozka24.data.features.location.GeoLocationDataSource
import ru.perevozka24.perevozka24.data.features.location.LocationParams.Companion.ZOOM_ADDRESS
import ru.perevozka24.perevozka24.ui.base.BaseFragment
import ru.perevozka24.perevozka24.ui.common.ext.requestLocationPermission
import ru.perevozka24.perevozka24.ui.common.ext.safe
import ru.perevozka24.perevozka24.ui.common.postRetry

abstract class BaseMapFragment<VM : BaseContentViewModel<out ContentModel>> : BaseFragment() {

    protected abstract val viewModel: VM

    private val locationDataSource by instance<GeoLocationDataSource>()

    protected val mapObjects: MapObjectCollection
        get() = mapview.map.mapObjects

    private val pins = mutableListOf<MapObject>()

    abstract fun addMapItems(
        content: ContentModel
    ): Collection<MapObject>

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mainMapFocusMyLocation.setOnClickListener {
            requestLocationPermission { requestLocation() }
        }

        viewModel.items.observe(viewLifecycleOwner, Observer { result ->
            val r = result as? Result.Success ?: return@Observer
            if (isVisible) addPins(r.data)
        })
    }

    private fun addPins(content: ContentModel) {
        safe(true) {
            pins.forEach { mapObjects.remove(it) }
        }
        pins.clear()

        postRetry {
            pins.addAll(addMapItems(content))
        }
    }

    override fun onResume() {
        super.onResume()
        MapKitFactory.getInstance().onStart()
        mapview.onStart()

        (viewModel.items.value as? Result.Success)?.data?.let {
            addPins(it)
        }
    }

    override fun onPause() {
        mapview.onStop()
        MapKitFactory.getInstance().onStop()
        super.onPause()
    }

    fun centerMap(point: Point, zoom: Float) {
        mapview.map.move(
            CameraPosition(
                point,
                zoom, 0f, 0f
            )
        )
    }

    private fun requestLocation() {
        if (!isVisible) return
        viewLifecycleOwner.lifecycleScope.launch {
            locationDataSource.getLocationFlow().collect {
                if (!isVisible) return@collect
                centerMap(it, ZOOM_ADDRESS)
            }
        }
    }
}
