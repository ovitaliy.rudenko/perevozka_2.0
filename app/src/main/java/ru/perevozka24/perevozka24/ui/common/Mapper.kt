package ru.perevozka24.perevozka24.ui.common

interface Mapper<E, M> {
    fun map(e: E): M
    fun map(list: List<E>): List<M> = list.map { map(it) }
}
