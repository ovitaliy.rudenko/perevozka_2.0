package ru.perevozka24.perevozka24.ui.filters.filter.items

import com.xwray.groupie.kotlinandroidextensions.GroupieViewHolder
import kotlinx.android.synthetic.main.item_filter_location.view.*
import ru.perevozka24.perevozka24.R
import ru.perevozka24.perevozka24.ui.filters.filter.FilterItem
import ru.perevozka24.perevozka24.ui.filters.models.CityModel

data class FilterCityItem(
    private val city: CityModel,
    override val defaultTitle: Int,
    override val onClick: (Any?) -> Unit
) : FilterItem(city, defaultTitle, onClick) {
    override fun getId(): Long = city.id?.toLong() ?: 0
    override fun getLayout(): Int = R.layout.item_filter_location

    override fun bind(viewHolder: GroupieViewHolder, position: Int) {
        super.bind(viewHolder, position)
        with(viewHolder.itemView) {
            locationIcon.setImageResource(R.drawable.ic_city)
            isEnabled = city.regionId != null
            locationTitle.text =
                when {
                    city.name != null -> city.name
                    else -> context.getString(defaultTitle)
                }
        }
    }
}
