package ru.perevozka24.perevozka24.ui.main.items

import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.widget.PopupWindow
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.NavHostFragment.findNavController
import ru.perevozka24.perevozka24.R
import ru.perevozka24.perevozka24.ui.common.ext.inflate
import ru.perevozka24.perevozka24.ui.main.items.abuse.AbuseActivity
import ru.perevozka24.perevozka24.ui.main.items.detailed.ItemDetailedActivityDirections
import ru.perevozka24.perevozka24.ui.main.items.models.ItemModel

class ItemActionImpl(
    private val fragment: Fragment,
    private val addToFavorite: AddToFavoriteDelegate,
    private val addToBlacklistDelegate: AddToBlacklistDelegate
) : ItemAction {

    override fun addToFavorite(itemModel: ItemModel, view: View) {
        addToFavorite.addToFavorite(itemModel)
    }

    override fun showMore(itemModel: ItemModel, view: View) {
        val root = fragment.view ?: return
        val viewLocation = intArrayOf(0, 0)
        view.getLocationInWindow(viewLocation)
        val v = inflate<View>(
            root as ViewGroup,
            R.layout.popup_equipment_item
        )

        v.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED)

        val popup = PopupWindow(
            v,
            ViewGroup.LayoutParams.WRAP_CONTENT,
            ViewGroup.LayoutParams.WRAP_CONTENT
        ).apply {
            elevation = fragment.resources.getDimension(R.dimen.default_elevation)
            isOutsideTouchable = true
            showAsDropDown(
                view,
                -v.measuredWidth - view.width,
                -view.height,
                Gravity.END or Gravity.TOP
            )
        }

        v.findViewById<View>(R.id.itemMoreAbuse).setOnClickListener {
            popup.dismiss()
            openAbuser(itemModel, view)
        }
        v.findViewById<View>(R.id.itemMoreHide).setOnClickListener {
            popup.dismiss()
            addToBlackList(itemModel, view)
        }
    }

    override fun openDetailed(itemModel: ItemModel, view: View) {
        findNavController(fragment).navigate(
            ItemDetailedActivityDirections.actionGlobalItemDetailedActivity(
                itemModel
            )
        )
    }

    override fun openAbuser(itemModel: ItemModel, view: View) {
        val context = view.context
        context.startActivity(AbuseActivity.create(context, itemModel.id))
    }

    override fun addToBlackList(itemModel: ItemModel, view: View) {
        addToBlacklistDelegate.addToBlacklist(itemModel)
    }
}
