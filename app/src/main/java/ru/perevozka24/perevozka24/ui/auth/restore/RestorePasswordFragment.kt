package ru.perevozka24.perevozka24.ui.auth.restore

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import kotlinx.android.synthetic.main.fragment_auth_restore_password.*
import ru.perevozka24.perevozka24.R
import ru.perevozka24.perevozka24.data.Result
import ru.perevozka24.perevozka24.ui.base.BaseFragment
import ru.perevozka24.perevozka24.ui.common.ext.showAlert
import ru.perevozka24.perevozka24.ui.common.ext.showSnackBar
import ru.perevozka24.perevozka24.ui.common.ext.visible
import ru.perevozka24.perevozka24.ui.common.widgets.form.addTextChangedListener
import ru.perevozka24.perevozka24.ui.viewModel

class RestorePasswordFragment : BaseFragment() {
    private val viewModel: RestorePasswordViewModel by viewModel()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(R.layout.fragment_auth_restore_password, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        // email
        emailView.addTextChangedListener { viewModel.emailChange(it.toString().trim()) }
        viewModel.emailFieldError.observe(viewLifecycleOwner, Observer {
            emailView.error(it)
        })

        // do
        restorePasswordButton.setOnClickListener { viewModel.restore() }
        viewModel.restoring.observe(viewLifecycleOwner, Observer {
            when (it) {
                is Result.Loading -> restorePasswordProgressBar.visible = true
                is Result.Error -> {
                    restorePasswordProgressBar.visible = false
                    showSnackBar(it.message)
                }
                is Result.Success -> {
                    restorePasswordProgressBar.visible = false
                    context?.showAlert(it.data) { activity?.onBackPressed() }
                }
            }
        })
    }
}
