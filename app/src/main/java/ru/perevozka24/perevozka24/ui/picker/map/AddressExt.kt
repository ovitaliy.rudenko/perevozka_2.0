package ru.perevozka24.perevozka24.ui.picker.map

import android.location.Address
import com.yandex.mapkit.GeoObjectCollection
import com.yandex.mapkit.search.ToponymObjectMetadata
import ru.perevozka24.perevozka24.data.features.location.LocationParams
import ru.perevozka24.perevozka24.data.features.location.entity.SimpleAddress
import com.yandex.mapkit.search.Address as YAnderss

fun Address.toText(): String {
    val result = (0..maxAddressLineIndex).joinToString(",") { getAddressLine(it) }
    return if (result.isNotEmpty()) {
        result
    } else {
        countryName
    }
}

fun SimpleAddress.toText(): String {
    return listOfNotNull(country, region, city, line).joinToString(", ").trim()
}

fun String.isPostal() = toIntOrNull() != null && length >= POSTAL_LENGTH

private const val POSTAL_LENGTH = 5

fun Address.toSimple(): SimpleAddress {
    return SimpleAddress(
        latitude = latitude,
        longitude = longitude,
        country = countryName,
        region = adminArea,
        city = locality,
        line = maxAddressLineIndex.takeIf { it > -1 }?.let { getAddressLine(0) }
    )
}

fun GeoObjectCollection.Item.toSimple(): SimpleAddress {
    val a = obj?.metadataContainer?.getItem(ToponymObjectMetadata::class.java)?.address

    return SimpleAddress(
        latitude = obj?.geometry?.firstOrNull()?.point?.latitude ?: 0.0,
        longitude = obj?.geometry?.firstOrNull()?.point?.longitude ?: 0.0,
        country = a?.components?.firstOrNull { it.kinds.contains(YAnderss.Component.Kind.COUNTRY) }?.name,
        region = a?.components?.firstOrNull { it.kinds.contains(YAnderss.Component.Kind.REGION) }?.name,
        city = a?.components?.firstOrNull { it.kinds.contains(YAnderss.Component.Kind.LOCALITY) }?.name,
        line = a?.components?.firstOrNull { it.kinds.contains(YAnderss.Component.Kind.STREET) }?.name
            ?.plus(" ")
            ?.plus(
                a.components.firstOrNull { it.kinds.contains(YAnderss.Component.Kind.HOUSE) }?.name
                    ?: ""
            )
            ?.trim()
    )
}

@SuppressWarnings("ReturnCount")
fun List<Address>.findBest(query: String): Address? {
    var items = this
    val parts = query.split(",").map { it.trim() }
    val country = parts.takeIf { it.isNotEmpty() }?.get(0)
    val region = parts.takeIf { it.size > 1 }?.get(1)
    val city = parts.takeIf { it.size > 2 }?.get(2)?.replace(LocationParams.CITY_SUFFIX, "")?.trim()

    if (city != null) {
        val foundCity = items.firstOrNull { it.featureName == city }
        if (foundCity != null) {
            return foundCity
        }
    }

    if (country != null) {
        val filtered = items.matchCountry(country)
        if (filtered.isNotEmpty()) {
            items = filtered
        } else {
            return items.firstOrNull()
        }
    }
    if (region != null) {
        val filtered = items.matchRegion(region)
        if (filtered.isNotEmpty()) {
            items = filtered
        } else {
            return items.firstOrNull()
        }
    }
    if (city != null) {
        val filtered = items.matchCity(city)
        if (filtered.isNotEmpty()) {
            items = filtered
        } else {
            return items.firstOrNull()
        }
    }

    return items.firstOrNull()
}

private fun List<Address>.matchCountry(country: String): List<Address> {
    return filter { it.countryName == country }
}

private fun List<Address>.matchRegion(region: String): List<Address> {
    return filter { it.adminArea == region }
}

private fun List<Address>.matchCity(city: String): List<Address> {
    return filter { it.maxAddressLineIndex >= 0 && it.getAddressLine(0).contains(city) }
}
