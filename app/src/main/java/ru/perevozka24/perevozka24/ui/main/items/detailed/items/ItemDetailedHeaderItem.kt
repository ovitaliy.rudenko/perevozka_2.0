package ru.perevozka24.perevozka24.ui.main.items.detailed.items

import com.xwray.groupie.kotlinandroidextensions.GroupieViewHolder
import com.xwray.groupie.kotlinandroidextensions.Item
import kotlinx.android.synthetic.main.item_detailed_header.view.*
import ru.perevozka24.perevozka24.R
import ru.perevozka24.perevozka24.ui.common.ext.load
import ru.perevozka24.perevozka24.ui.common.ext.visible
import ru.perevozka24.perevozka24.ui.main.items.detailed.ItemDetailedAction
import ru.perevozka24.perevozka24.ui.main.items.models.ItemModel

data class ItemDetailedHeaderItem(
    private val item: ItemModel,
    private val showActions: Boolean,
    private val actions: ItemDetailedAction
) : Item() {
    override fun getId(): Long = 0
    override fun getLayout(): Int = R.layout.item_detailed_header

    override fun bind(viewHolder: GroupieViewHolder, position: Int) {
        with(viewHolder.itemView) {
            itemDetailedImage.load(item.image)
            itemDetailedContainer.visible = showActions
            if (showActions) {
                itemDetailedFavorite.isSelected = item.favorite
                itemDetailedFavorite.setOnClickListener { actions.addToFavorite(item) }
                itemDetailedAbuse.setOnClickListener { actions.addAbuse(item) }
                itemDetailedBlock.setOnClickListener { actions.block(item) }
            }
        }
    }
}
