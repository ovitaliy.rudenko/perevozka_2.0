package ru.perevozka24.perevozka24.ui.main.orders.detailed.item

import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.PagerSnapHelper
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.kotlinandroidextensions.GroupieViewHolder
import com.xwray.groupie.kotlinandroidextensions.Item
import kotlinx.android.synthetic.main.item_offer_detailed_gallery.view.*
import kotlinx.android.synthetic.main.item_offer_detailed_gallery_item.view.*
import ru.perevozka24.perevozka24.R
import ru.perevozka24.perevozka24.ui.common.ext.load
import ru.perevozka24.perevozka24.ui.common.ext.visible
import ru.perevozka24.perevozka24.ui.main.orders.detailed.OfferDetailedAction
import ru.perevozka24.perevozka24.ui.main.orders.models.OfferModel

data class OfferGalleryItem(
    private val offer: OfferModel,
    private val showFavorite: Boolean,
    private val actions: OfferDetailedAction
) : Item() {

    override fun getId() = 0L
    override fun getLayout(): Int = R.layout.item_offer_detailed_gallery

    override fun createViewHolder(itemView: View): GroupieViewHolder {
        with(itemView) {
            itemOfferGalleryList.setHasFixedSize(true)
            PagerSnapHelper().attachToRecyclerView(itemOfferGalleryList)
        }
        return super.createViewHolder(itemView)
    }

    override fun bind(viewHolder: GroupieViewHolder, position: Int) {
        with(viewHolder.itemView) {
            itemOfferGalleryList.layoutManager =
                LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
            val adapter = GroupAdapter<GroupieViewHolder>()
            itemOfferGalleryList.adapter = adapter

            val imageItems = offer.gallery.map { it.link }.map { OfferGalleryImageItem(it) }
            val items = imageItems.takeIf { it.isNotEmpty() } ?: listOf(DefaultImage)
            adapter.update(items)

            itemOfferGalleryFavorite.visible = showFavorite
            itemOfferGalleryFavorite.isSelected = offer.favorite
            itemOfferGalleryFavorite.setOnClickListener { actions.addToFavorite(offer) }
        }
    }

    data class OfferGalleryImageItem(private val link: String) : Item() {
        override fun getId() = link.hashCode().toLong()
        override fun getLayout(): Int = R.layout.item_offer_detailed_gallery_item

        override fun bind(viewHolder: GroupieViewHolder, position: Int) {
            with(viewHolder.itemView) {
                itemOfferGalleryListImage.load(link)
            }
        }
    }

    object DefaultImage : Item() {
        override fun getId() = 0L
        override fun getLayout(): Int = R.layout.item_offer_detailed_gallery_item

        override fun bind(viewHolder: GroupieViewHolder, position: Int) {
            with(viewHolder.itemView) {
                itemOfferGalleryListImage.setImageResource(R.drawable.ic_order_placeholder)
            }
        }
    }
}
