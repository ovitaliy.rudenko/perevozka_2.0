package ru.perevozka24.perevozka24.ui.base

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import ru.perevozka24.perevozka24.data.ResourceProvider
import ru.perevozka24.perevozka24.data.Result
import ru.perevozka24.perevozka24.ui.LiveEvent

abstract class BaseContentLoaderViewModel<T : Any>(
    resourceProvider: ResourceProvider
) : BaseViewModel(resourceProvider) {

    val result: LiveData<Result<T>> = MutableLiveData()
    val loaded = LiveEvent<T>()

    fun execute(load: suspend () -> T) {
        doWork(result) {
            val r = load()
            loaded.postValue(r)
            r
        }
    }
}
