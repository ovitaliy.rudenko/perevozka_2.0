package ru.perevozka24.perevozka24.ui.common.widgets

import android.content.Context
import android.text.method.LinkMovementMethod
import android.util.AttributeSet
import android.view.View
import android.widget.CompoundButton
import android.widget.LinearLayout
import androidx.core.text.HtmlCompat
import kotlinx.android.synthetic.main.merge_checkbox.view.*
import ru.perevozka24.perevozka24.R

class CheckedButton @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : LinearLayout(context, attrs, defStyleAttr) {

    private var listener: CompoundButton.OnCheckedChangeListener? = null

    var checked: Boolean
        get() = checkboxIcon.isChecked
        set(value) {
            checkboxIcon.setOnCheckedChangeListener(null)
            checkboxIcon.isChecked = value
            checkboxIcon.setOnCheckedChangeListener(listener)
        }

    init {
        orientation = HORIZONTAL

        val set = intArrayOf(
            android.R.attr.checked, // idx 0
            android.R.attr.text,
            R.attr.layout // idx 2
        )
        val a = context.obtainStyledAttributes(attrs, set)
        val checked = a.getBoolean(CHECKED_INDEX, false)
        val title = a.getString(TEXT_INDEX) ?: ""
        val layoutId = a.getResourceId(LAYOUT_INDEX, R.layout.merge_checkbox)
        a.recycle()
        View.inflate(context, layoutId, this)
        checkboxIcon.isChecked = checked
        checkboxText.text = HtmlCompat.fromHtml(title, HtmlCompat.FROM_HTML_MODE_LEGACY)
        checkboxText.movementMethod = LinkMovementMethod.getInstance()
        checkboxText.setOnClickListener {
            checkboxIcon.isChecked = !checkboxIcon.isChecked
        }
        setOnClickListener {
            checkboxIcon.isChecked = !checkboxIcon.isChecked
        }
    }

    fun setOnCheckedChangeListener(listener: CompoundButton.OnCheckedChangeListener?) {
        this.listener = listener
        checkboxIcon.setOnCheckedChangeListener(listener)
    }

    fun setTitle(title: Int) {
        checkboxText.setText(title)
    }

    fun setTitle(title: String) {
        checkboxText.text = title
    }

    private companion object {
        const val CHECKED_INDEX = 0
        const val TEXT_INDEX = 1
        const val LAYOUT_INDEX = 2
    }
}

inline fun CheckedButton.onCheckChanged(
    crossinline onChange: (value: Boolean) -> Unit
) {
    setOnCheckedChangeListener(CompoundButton.OnCheckedChangeListener { _, isChecked ->
        onChange(isChecked)
    })
}
