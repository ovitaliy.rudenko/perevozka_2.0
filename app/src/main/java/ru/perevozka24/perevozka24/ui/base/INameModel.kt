package ru.perevozka24.perevozka24.ui.base

import ru.perevozka24.perevozka24.ui.filters.models.PickerModel

interface INameModel : PickerModel {
    val name: String
}
