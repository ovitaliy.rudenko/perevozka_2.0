package ru.perevozka24.perevozka24.ui.common.widgets

import android.graphics.drawable.Drawable
import com.bumptech.glide.request.target.CustomTarget

abstract class SimpleCustomTarget<T> : CustomTarget<T> {
    constructor() : super()
    constructor(width: Int, height: Int) : super(width, height)

    @SuppressWarnings("EmptyFunctionBlock")
    override fun onLoadCleared(placeholder: Drawable?) {
    }
}
