package ru.perevozka24.perevozka24.ui.main.items.detailed

import android.os.Bundle
import androidx.fragment.app.commit
import androidx.navigation.navArgs
import kotlinx.android.synthetic.main.activity_fragment_container.*
import ru.perevozka24.perevozka24.R
import ru.perevozka24.perevozka24.ui.base.BaseFragmentActivity

class ItemDetailedActivity : BaseFragmentActivity() {

    private val args: ItemDetailedActivityArgs by navArgs()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        toolbarTitleView.text = getString(R.string.item_detailed_title, args.item.id)
        if (savedInstanceState == null) {
            supportFragmentManager.commit {
                replace(R.id.container, ItemDetailedFragment.create(args.item, args.isMine))
            }
        }
    }
}
