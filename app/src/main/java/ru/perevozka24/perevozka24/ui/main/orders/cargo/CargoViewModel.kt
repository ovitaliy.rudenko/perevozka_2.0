package ru.perevozka24.perevozka24.ui.main.orders.cargo

import ru.perevozka24.perevozka24.data.ResourceProvider
import ru.perevozka24.perevozka24.data.features.filter.FilterRepository
import ru.perevozka24.perevozka24.data.features.offers.OffersRepository
import ru.perevozka24.perevozka24.ui.filters.filter.FilterType
import ru.perevozka24.perevozka24.ui.filters.models.CategorySubCategoryModel
import ru.perevozka24.perevozka24.ui.filters.models.FilterCargoModel
import ru.perevozka24.perevozka24.ui.main.orders.base.BaseOrderListViewModel
import ru.perevozka24.perevozka24.ui.main.orders.models.OffersModel

class CargoViewModel(
    private val repository: OffersRepository,
    private val filterRepository: FilterRepository,
    resourceProvider: ResourceProvider
) : BaseOrderListViewModel(repository, resourceProvider) {
    override val searchReportName: String = "cargo_screen_search"
    init {
        updateFilterValue()
        load(0)
    }

    override suspend fun loadPage(offset: Int): OffersModel = repository.getCargo(offset)

    override suspend fun loadSubCategory(): CategorySubCategoryModel {
        val filter =
            filterRepository.getFilterItems(FilterType.CARGO) as FilterCargoModel
        return CategorySubCategoryModel.create(filter.category, filter.subCategory)
    }

    override suspend fun saveSubCategory(model: CategorySubCategoryModel) {
        val filter = filterRepository.getFilterItems(FilterType.CARGO)
            .updated(model.toCategory())
            .updated(model.toSubCategory())
        filterRepository.setFilterItems(FilterType.CARGO, filter)
    }
}
