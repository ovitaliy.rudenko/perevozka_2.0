package ru.perevozka24.perevozka24.ui.filters.filter

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.lifecycle.Observer
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.GroupieViewHolder
import kotlinx.android.synthetic.main.fragment_filter.*
import ru.perevozka24.perevozka24.R
import ru.perevozka24.perevozka24.ui.base.BaseFragment
import ru.perevozka24.perevozka24.ui.filters.models.PickerModel
import ru.perevozka24.perevozka24.ui.filters.picker.FilterPickerActivity
import ru.perevozka24.perevozka24.ui.filters.picker.dialog.FilterDialogPickerFragment
import ru.perevozka24.perevozka24.ui.filters.picker.dialog.FilterPickerCheckListener
import ru.perevozka24.perevozka24.ui.filters.picker.loader.PickerMapper

class FilterSelectionFragment : BaseFragment(), FilterPickerCheckListener {

    private val viewModel: FilterSelectionViewModel by lazy {
        (requireActivity() as FilterActivity).viewModel
    }

    private val type by lazy { checkNotNull(arguments?.getSerializable(TYPE)) as FilterType }

    private val adapter = GroupAdapter<GroupieViewHolder>()

    private val onActivityPickerClick = fun(value: Any?) {
        val item = value as? FilterItem ?: return
        val picker = item.item
        val title = item.defaultTitle
        startActivityForResult(
            FilterPickerActivity.create(requireContext(), picker, title),
            REQUEST_CODE
        )
    }

    private val onDialogPickerClick = fun(type: Class<*>, value: Any?) {
        FilterDialogPickerFragment.create(
            getString(PickerMapper.mapToDialogTitle(type)),
            type as Class<out PickerModel>,
            value as? PickerModel
        ).show(childFragmentManager, "picker")
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(R.layout.fragment_filter, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        filterSaveButton.setText(type.button)
        filterSaveButton.setOnClickListener {
            viewModel.saveFilter()
            activity?.setResult(Activity.RESULT_OK, Intent().apply {
                putExtra(INITIAL_VALUE, viewModel.initialData)
                putExtra(CHANGED_VALUE, viewModel.selection.value)
            })
            activity?.finish()
        }
        filterValuesList.adapter = adapter
        viewModel.selection.observe(viewLifecycleOwner, Observer {
            val filter = it ?: return@Observer

            adapter.updateAsync(
                FilterGroupBuilder.create(filter, onActivityPickerClick, onDialogPickerClick)
            )
        })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            val value = data?.getParcelableExtra<PickerModel>(FilterPickerActivity.MODEL)
            viewModel.pickerValueChanged(checkNotNull(value))
        }
    }

    override fun onFilterPickerChangedListener(
        type: Class<out PickerModel>,
        newValue: PickerModel?
    ) {
        viewModel.pickerValueChanged(type, newValue)
    }

    companion object {
        private const val TYPE = "type"
        const val INITIAL_VALUE = "initial"
        const val CHANGED_VALUE = "changed"
        private const val REQUEST_CODE = 1000
        fun create(type: FilterType) = FilterSelectionFragment().apply {
            arguments = bundleOf(TYPE to type)
        }
    }
}
