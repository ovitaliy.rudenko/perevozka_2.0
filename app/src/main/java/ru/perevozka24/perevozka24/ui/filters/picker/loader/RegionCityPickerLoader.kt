package ru.perevozka24.perevozka24.ui.filters.picker.loader

import ru.perevozka24.perevozka24.data.features.filter.FilterLocationPickerRepository
import ru.perevozka24.perevozka24.ui.filters.models.PickerModel
import ru.perevozka24.perevozka24.ui.filters.models.RegionCityModel

class RegionCityPickerLoader(
    private val current: RegionCityModel,
    private val pickerRepository: FilterLocationPickerRepository
) : PickerLoader<RegionCityModel> {
    override suspend fun load(): List<RegionCityModel> {
        val id = checkNotNull(current.region.countryId)
        return pickerRepository.getRegionCity(id).map { it.copy(direction = current.direction) }
    }

    override fun <T : PickerModel> filter(list: List<T>, q: String): List<RegionCityModel> {
        return list.filter {
            it as RegionCityModel
            if (it.city == null) {
                it.region.name?.contains(q, true) == true
            } else {
                it.city.name?.contains(q, true) ?: false
            }
        }
            .map { it as RegionCityModel }
    }
}
