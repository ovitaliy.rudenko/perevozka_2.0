package ru.perevozka24.perevozka24.ui.support.items

import com.xwray.groupie.kotlinandroidextensions.GroupieViewHolder
import com.xwray.groupie.kotlinandroidextensions.Item
import ru.perevozka24.perevozka24.R

object SupportTitleItem : Item() {
    override fun getId() = 0L
    override fun getLayout() = R.layout.item_support_title
    override fun bind(viewHolder: GroupieViewHolder, position: Int) = Unit
}
