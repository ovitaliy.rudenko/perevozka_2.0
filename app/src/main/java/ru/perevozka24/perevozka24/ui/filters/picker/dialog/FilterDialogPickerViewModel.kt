package ru.perevozka24.perevozka24.ui.filters.picker.dialog

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import ru.perevozka24.perevozka24.data.ResourceProvider
import ru.perevozka24.perevozka24.data.Result
import ru.perevozka24.perevozka24.data.features.filter.FilterPickerRepository
import ru.perevozka24.perevozka24.data.features.geolocation.LocationTrackerRepository
import ru.perevozka24.perevozka24.ui.base.BaseViewModel
import ru.perevozka24.perevozka24.ui.base.INameModel
import ru.perevozka24.perevozka24.ui.common.livedata.Combined2LiveData
import ru.perevozka24.perevozka24.ui.common.livedata.setValue
import ru.perevozka24.perevozka24.ui.filters.models.PickerModel
import ru.perevozka24.perevozka24.ui.filters.picker.loader.PickerLoader
import ru.perevozka24.perevozka24.ui.filters.picker.loader.PickerLoaderFactory

open class FilterDialogPickerViewModel(
    private val pickerRepo: FilterPickerRepository,
    private val locationTrackerRepository: LocationTrackerRepository,
    resourceProvider: ResourceProvider
) : BaseViewModel(resourceProvider) {
    lateinit var loader: PickerLoader<PickerModel>

    val result: LiveData<Result<List<PickerModel>>> = MutableLiveData()

    val selected: LiveData<PickerModel?> = MutableLiveData()
    val items: LiveData<Pair<List<PickerModel>, PickerModel?>> =
        Combined2LiveData(result, selected) { r, c ->
            val list = (r as? Result.Success<List<PickerModel>>)?.data ?: emptyList()
            Pair(list, c)
        }

    fun init(type: Class<in PickerModel>, current: PickerModel?) {
        selected.setValue(current)
        loader = PickerLoaderFactory.create(
            type,
            pickerRepo,
            locationTrackerRepository
        )

        doWork(result) {
            loader.load()
        }
    }

    fun select(value: INameModel, checked: Boolean) {
        val currentValue = selected.value
        if (value == currentValue && !checked) {
            selected.setValue(null)
        } else {
            selected.setValue(value)
        }
    }
}
