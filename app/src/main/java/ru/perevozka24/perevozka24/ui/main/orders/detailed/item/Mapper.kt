package ru.perevozka24.perevozka24.ui.main.orders.detailed.item

import android.content.Context
import com.xwray.groupie.kotlinandroidextensions.Item
import org.threeten.bp.format.DateTimeFormatter
import ru.perevozka24.perevozka24.DATE_FORMAT
import ru.perevozka24.perevozka24.R
import ru.perevozka24.perevozka24.data.features.filter.FilterPickerDataSource.Companion.CARGO_TYPE
import ru.perevozka24.perevozka24.data.features.filter.FilterPickerDataSource.Companion.DOST_TYPE
import ru.perevozka24.perevozka24.data.features.filter.FilterPickerDataSource.Companion.PASSENGER_TYPE
import ru.perevozka24.perevozka24.data.features.filter.FilterPickerDataSource.Companion.SPEC_TYPE
import ru.perevozka24.perevozka24.data.features.filter.FilterPickerDataSource.Companion.STRO_TYPE
import ru.perevozka24.perevozka24.data.features.filter.FilterPickerDataSource.Companion.USLG_TYPE
import ru.perevozka24.perevozka24.ui.common.items.DividerItem
import ru.perevozka24.perevozka24.ui.main.orders.detailed.OfferDetailedAction
import ru.perevozka24.perevozka24.ui.main.orders.detailed.ViewData
import ru.perevozka24.perevozka24.ui.main.orders.models.OfferAbuseModel
import ru.perevozka24.perevozka24.ui.main.orders.models.OfferModel
import ru.perevozka24.perevozka24.ui.main.orders.models.PerformTime
import kotlin.math.roundToInt

object Mapper {

    @SuppressWarnings("LongMethod")
    fun map(
        context: Context,
        viewData: ViewData,
        actions: OfferDetailedAction
    ): List<Item> {
        val offer = viewData.offer
        val route = viewData.routeData
        val divider = DividerItem()
        return mutableListOf<Item>().apply {
            add(OfferGalleryItem(offer, viewData.showFavorite, actions))
            add(OfferTextItem(offer))
            add(divider)
            add(OfferRowStatusItem(offer))
            add(divider)
            (offer.companyUrl ?: offer.companyName)?.let {
                add(
                    OfferRowLinkItem(
                        context.getString(R.string.offer_performer),
                        offer.companyName ?: it,
                        it
                    )
                )
            }
            add(divider)

            performDateItem(context, offer)?.let {
                add(it)
                add(divider)
            }

            offer.payRate?.roundToInt()?.takeIf { it > 0 }?.let {
                add(
                    OfferRowItem(
                        context.getString(R.string.offer_price),
                        context.getString(R.string.offer_price_value, it)
                    )
                )
                add(divider)

                offer.payType?.let { rateType ->
                    add(OfferRowItem(context.getString(R.string.offer_rate_type), rateType))
                    add(divider)
                }
            }

            route?.let {
                add(OfferRowItem(context.getString(R.string.offer_distance), it.distance))
                add(divider)
            }

            offer.address?.let {
                add(OfferColumnItem(context.getString(R.string.offer_address), it))
                add(divider)
            }

            route?.let {
                add(OfferRouteItem(offer))
                add(OfferMapItem(it))
            }

            viewData.abuse?.let {
                add(OfferAbuseItem(it))
            }

            if (viewData.showContacts && offer.code.contactsVisible) {
                add(OfferContactItem(offer, actions))
            }

            mapBubble(viewData)?.let { add(it) }
        }
    }

    private fun mapBubble(viewData: ViewData): OfferBubbleItem? =
        if (!viewData.loggedIn) {
            OfferBubbleItem(R.string.offer_login_description)
        } else if (viewData.showContacts && viewData.offer.code.contactsVisible) {
            viewData.offer.code.hint?.let {
                OfferBubbleItem(it)
            }
        } else {
            null
        }

    fun mapAbuses(items: List<OfferAbuseModel>) =
        items.mapIndexed { index, abuse -> OfferAbuseSubItem(abuse, index % 2 == 0) }

    private fun performDateItem(context: Context, itemModel: OfferModel): Item? {
        val id = itemModel.category.id
        val sale = itemModel.category.sale
        if (id == SPEC_TYPE && sale == 1 || itemModel.performTime is PerformTime.AnyTime) return null
        val title: String = when {
            id == CARGO_TYPE -> context.getString(R.string.offer_perform_date_cargo)
            id == SPEC_TYPE && sale == 0 -> context.getString(R.string.offer_perform_date_spec_rental)
            id == PASSENGER_TYPE -> context.getString(R.string.offer_perform_date_pass)
            id == STRO_TYPE -> context.getString(R.string.offer_perform_date_stro)
            id == DOST_TYPE -> context.getString(R.string.offer_perform_date_dost)
            id == USLG_TYPE -> context.getString(R.string.offer_perform_date_uslg)
            else -> error("unsupported $itemModel")
        }
        val performTime = itemModel.performTime
        val formatter = DateTimeFormatter.ofPattern(DATE_FORMAT)
        val value: String = when (performTime) {
            is PerformTime.AnyTime -> context.getString(R.string.offer_perform_date_any_time)
            is PerformTime.Period -> context.getString(
                R.string.offer_perform_date_period,
                formatter.format(performTime.start), formatter.format(performTime.end)
            )
            is PerformTime.Fixed -> formatter.format(performTime.date)
            is PerformTime.Before -> context.getString(
                R.string.offer_perform_date_before,
                formatter.format(performTime.date)
            )
        }
        return OfferRowItem(title, value)
    }
}
