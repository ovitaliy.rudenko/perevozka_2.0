package ru.perevozka24.perevozka24.ui.main.items.models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize
import ru.perevozka24.perevozka24.data.features.items.entities.ItemSimpleEntity
import ru.perevozka24.perevozka24.ui.main.base.ContentModel

@Parcelize
data class ItemSimpleModel(
    val id: String,
    val coordX: Double?,
    val coordY: Double?,
    val categoryId: Long?,
    val categoryIcon: String,
    val uri: String
) : Parcelable {
    companion object Mapper :
        ru.perevozka24.perevozka24.ui.common.Mapper<ItemSimpleEntity, ItemSimpleModel> {
        override fun map(e: ItemSimpleEntity): ItemSimpleModel {
            return ItemSimpleModel(
                id = checkNotNull(e.id),
                coordX = e.coordX,
                coordY = e.coordY,
                categoryId = e.categoryId,
                categoryIcon = checkNotNull(e.categoryIcon),
                uri = checkNotNull(e.uri)
            )
        }
    }
}

class ItemSimplesList : ArrayList<ItemSimpleModel>(), ContentModel
