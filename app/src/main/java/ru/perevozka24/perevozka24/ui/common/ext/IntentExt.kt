package ru.perevozka24.perevozka24.ui.common.ext

import android.content.Intent

fun Intent.restart() =
    this.apply {
        flags =
            Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK
    }
