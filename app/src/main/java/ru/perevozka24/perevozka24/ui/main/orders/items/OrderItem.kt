package ru.perevozka24.perevozka24.ui.main.orders.items

import com.xwray.groupie.kotlinandroidextensions.GroupieViewHolder
import com.xwray.groupie.kotlinandroidextensions.Item
import kotlinx.android.synthetic.main.item_order.view.*
import ru.perevozka24.perevozka24.R
import ru.perevozka24.perevozka24.ui.common.ext.formatToDefault
import ru.perevozka24.perevozka24.ui.common.ext.load
import ru.perevozka24.perevozka24.ui.common.ext.setVisibleText
import ru.perevozka24.perevozka24.ui.common.ext.visible
import ru.perevozka24.perevozka24.ui.main.orders.OrderAction
import ru.perevozka24.perevozka24.ui.main.orders.models.OfferModel

class OrderItem(
    private val model: OfferModel,
    private val callback: OrderAction,
    private val extraActions: Boolean
) : Item() {
    override fun getId() = model.id.toLongOrNull() ?: 0
    override fun getLayout(): Int = R.layout.item_order
    override fun bind(viewHolder: GroupieViewHolder, position: Int) {
        with(viewHolder.itemView) {
            val route = model.cityNameTo?.let {
                resources.getString(R.string.order_route, model.cityName, it)
            }
            itemOrderCategoryIcon.load(model.categoryImage)
            itemOrderCityName.text = model.cityName
            itemOrderDate.text = model.dateAdded?.formatToDefault(context)
            itemOrderStatus.text = resources.getString(model.status.title)
            itemOrderStatus.setBackgroundResource(model.status.color)
            itemOrderRoute.setVisibleText(route)
            itemOrderDescription.setVisibleText(model.offer)

            setOnClickListener {
                callback.openDetailed(model, it)
            }

            itemOrderFavorite.visible = extraActions
            itemOrderFavorite.isSelected = model.favorite
            itemOrderFavorite.setOnClickListener {
                callback.addToFavorite(
                    model,
                    viewHolder.itemView
                )
            }
        }
    }
}
