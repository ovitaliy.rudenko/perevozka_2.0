package ru.perevozka24.perevozka24.ui.main.orders.cargo

import android.content.Context
import ru.perevozka24.perevozka24.ui.filters.models.CategorySubCategoryModel
import ru.perevozka24.perevozka24.ui.main.base.FilterTitleFormatter

object CargoFilterTitleFormatter : FilterTitleFormatter {
    override fun format(context: Context, m: CategorySubCategoryModel): String? {
        val category = m.subCategoryName?.run {
            val parts = split(" ")
            parts.subList(2, parts.size).joinToString(" ")
        }
        return category ?: m.categoryName
    }
}
