package ru.perevozka24.perevozka24.ui.common.ext

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

fun Array<View>.visible(visible: Boolean, invisibility: Int = View.GONE) {
    forEach { it.visible(visible, invisibility) }
}

@Suppress("UNCHECKED_CAST")
fun <T : View> inflate(parent: ViewGroup, layoutId: Int): T =
    LayoutInflater.from(parent.context).inflate(
        layoutId, parent, false
    ) as T
