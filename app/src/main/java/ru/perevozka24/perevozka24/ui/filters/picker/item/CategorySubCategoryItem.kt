package ru.perevozka24.perevozka24.ui.filters.picker.item

import android.view.View
import com.xwray.groupie.kotlinandroidextensions.GroupieViewHolder
import kotlinx.android.synthetic.main.item_filter_loader_sub_category.view.*
import ru.perevozka24.perevozka24.R
import ru.perevozka24.perevozka24.ui.common.ext.load
import ru.perevozka24.perevozka24.ui.common.ext.visible
import ru.perevozka24.perevozka24.ui.filters.models.CategorySubCategoryModel

data class CategorySubCategoryItem(
    val model: CategorySubCategoryModel,
    val onClick: View.OnClickListener
) :
    BasePickerItem(onClick) {
    override fun getLayout(): Int = R.layout.item_filter_loader_sub_category
    override fun getId(): Long = model.subCategoryId?.toLongOrNull() ?: 0

    override fun bind(viewHolder: GroupieViewHolder, position: Int) {
        with(viewHolder.itemView) {
            tag = model
            filterLoaderSubCategoryText.text = when {
                model.subCategoryName == null -> resources.getString(R.string.filter_category_default)
                model.sale == 1 -> "${model.subCategoryName} ${resources.getString(R.string.filter_sale)}"
                else -> model.subCategoryName
            }
            filterLoaderSubCategoryIcon.visible = model.icon != null
            filterLoaderSubCategoryIcon.load(model.icon)
        }
    }
}
