package ru.perevozka24.perevozka24.ui.support

import ru.perevozka24.perevozka24.R
import ru.perevozka24.perevozka24.ui.base.BaseHostActivity

class SupportActivity : BaseHostActivity() {

    override val graph: Int = R.navigation.nav_support
}
