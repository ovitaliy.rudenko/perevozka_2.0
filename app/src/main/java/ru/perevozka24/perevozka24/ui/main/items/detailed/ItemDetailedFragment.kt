package ru.perevozka24.perevozka24.ui.main.items.detailed

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.lifecycle.Observer
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.GroupieViewHolder
import kotlinx.android.synthetic.main.fragment_item_detailed.*
import org.kodein.di.Kodein
import org.kodein.di.android.x.kodein
import ru.perevozka24.perevozka24.R
import ru.perevozka24.perevozka24.data.Result
import ru.perevozka24.perevozka24.ui.add.AddOrderActivity
import ru.perevozka24.perevozka24.ui.base.BaseFragment
import ru.perevozka24.perevozka24.ui.common.ext.call
import ru.perevozka24.perevozka24.ui.common.ext.toast
import ru.perevozka24.perevozka24.ui.common.ext.updateOrReplace
import ru.perevozka24.perevozka24.ui.common.ext.visible
import ru.perevozka24.perevozka24.ui.main.items.abuse.AbuseActivity
import ru.perevozka24.perevozka24.ui.main.items.detailed.items.Mapper
import ru.perevozka24.perevozka24.ui.main.items.models.ItemModel
import ru.perevozka24.perevozka24.ui.viewModel
import ru.perevozka24.perevozka24.utils.trackEvent

class ItemDetailedFragment : BaseFragment() {

    override val kodein: Kodein by kodein()
    val viewModel: ItemDetailedViewModel by viewModel()

    private val item: ItemModel by lazy { checkNotNull(arguments?.getParcelable(ITEM) as? ItemModel) }
    private val adapter = GroupAdapter<GroupieViewHolder>()

    private val action = object : ItemDetailedAction {
        override fun addToFavorite(item: ItemModel) {
            viewModel.addFavorite(item)
        }

        override fun addAbuse(item: ItemModel) {
            startActivity(AbuseActivity.create(requireContext(), item.id))
        }

        override fun block(item: ItemModel) {
            viewModel.block(item)
        }

        override fun call(item: ItemModel) {
            callPhone(item)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel.init(item, requireArguments().getBoolean(IS_MINE))
        trackEvent("single_ad_visited")
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_item_detailed, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.isMine.observe(viewLifecycleOwner, Observer {
            itemDetailedContainer.visible = !it
        })
        viewModel.error.observe(viewLifecycleOwner, Observer {
            toast(it)
        })
        viewModel.item.observe(viewLifecycleOwner, Observer {
            val item = (it as? Result.Success)?.data ?: return@Observer
            adapter.updateOrReplace(
                Mapper.map(
                    requireContext(),
                    item,
                    viewModel.isMine.value ?: false,
                    action
                )
            )
        })
        viewModel.blocked.observe(viewLifecycleOwner, Observer {
            if (it == true) activity?.finish()
        })
        itemDetailedList.adapter = adapter
        itemDetailedList.itemAnimator = null

        itemDetailedCall.setOnClickListener { callPhone(item) }
        itemDetailedCreateOrder.setOnClickListener { v ->
            val userId = item.takeIf { it.showOffer }?.userId
            trackEvent(
                "create_order",
                mapOf("type" to "personal")
            )
            v.context.run { startActivity(AddOrderActivity.create(this, userId)) }
        }
    }

    private fun callPhone(item: ItemModel) {
        context?.call(item.phone)
    }

    companion object {
        private const val ITEM = "item"
        private const val IS_MINE = "isMine"
        fun create(item: ItemModel, isMine: Boolean): ItemDetailedFragment {
            return ItemDetailedFragment().apply {
                arguments = bundleOf(ITEM to item, IS_MINE to isMine)
            }
        }
    }
}
