package ru.perevozka24.perevozka24.ui.common.widgets.form

import java.util.regex.Pattern

class EmailValidator : InputFieldValidator<String> {
    override fun validate(input: String): String? {
        if (!isValid(input)) {
            return "Неправильный формат email"
        }
        return null
    }

    private fun isValid(email: String?): Boolean {
        val emailRegex = "^[a-zA-Z0-9_+&*-]+(?:\\." +
                "[a-zA-Z0-9_+&*-]+)*@" +
                "(?:[a-zA-Z0-9-]+\\.)+[a-z" +
                "A-Z]{2,7}$"

        val pat = Pattern.compile(emailRegex)
        return if (email == null) false else pat.matcher(email).matches()
    }
}
