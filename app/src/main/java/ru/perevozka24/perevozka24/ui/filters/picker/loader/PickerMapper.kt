package ru.perevozka24.perevozka24.ui.filters.picker.loader

import android.view.View
import com.xwray.groupie.Group
import ru.perevozka24.perevozka24.R
import ru.perevozka24.perevozka24.ui.filters.models.CargoBodyModel
import ru.perevozka24.perevozka24.ui.filters.models.CategoryModel
import ru.perevozka24.perevozka24.ui.filters.models.CategorySubCategoryModel
import ru.perevozka24.perevozka24.ui.filters.models.CityModel
import ru.perevozka24.perevozka24.ui.filters.models.CountryModel
import ru.perevozka24.perevozka24.ui.filters.models.OfferStatusModel
import ru.perevozka24.perevozka24.ui.filters.models.PickerModel
import ru.perevozka24.perevozka24.ui.filters.models.RegionCityModel
import ru.perevozka24.perevozka24.ui.filters.models.RegionModel
import ru.perevozka24.perevozka24.ui.filters.models.SubCategoryModel
import ru.perevozka24.perevozka24.ui.filters.picker.item.CategoryAddOrderItem
import ru.perevozka24.perevozka24.ui.filters.picker.item.CategoryItem
import ru.perevozka24.perevozka24.ui.filters.picker.item.CategorySubCategoryItem
import ru.perevozka24.perevozka24.ui.filters.picker.item.CityItem
import ru.perevozka24.perevozka24.ui.filters.picker.item.CountryItem
import ru.perevozka24.perevozka24.ui.filters.picker.item.RegionCityItem
import ru.perevozka24.perevozka24.ui.filters.picker.item.RegionItem
import ru.perevozka24.perevozka24.ui.filters.picker.item.SubCategoryItem

object PickerMapper {

    fun map(
        list: List<PickerModel>,
        clear: Group?,
        onClickListener: View.OnClickListener
    ): List<Group> {
        return mutableListOf<Group>().apply {
            if (clear != null) add(clear)
            addAll(
                list.map { mapItem(it, onClickListener) }
            )
        }
    }

    private fun mapItem(model: PickerModel, onClickListener: View.OnClickListener): Group {
        return when (model) {
            is CategoryModel -> CategoryItem(model, onClickListener)
            is SubCategoryModel -> SubCategoryItem(model, onClickListener)
            is CategorySubCategoryModel -> CategorySubCategoryItem(
                model,
                onClickListener
            )
            is CountryModel -> CountryItem(model, onClickListener)
            is RegionCityModel -> RegionCityItem(model, onClickListener)
            is RegionModel -> RegionItem(model, onClickListener)
            is CityModel -> CityItem(model, onClickListener)
            else -> error("unknown class")
        }
    }

    fun mapAddNewOrder(
        list: List<PickerModel>,
        onClickListener: View.OnClickListener
    ): List<Group> {
        return list.map { model ->
            when (model) {
                is CategoryModel -> CategoryAddOrderItem(model, onClickListener)
                else -> mapItem(model, onClickListener)
            }
        }
    }

    fun mapToTitle(model: PickerModel): Int {
        return when (model) {
            is CategoryModel -> R.string.filter_title_category
            is CategorySubCategoryModel -> R.string.filter_title_sub_category
            is SubCategoryModel -> R.string.filter_title_sub_category
            is CountryModel -> R.string.filter_title_country
            is RegionCityModel -> R.string.filter_title_region
            is RegionModel -> R.string.filter_title_region
            is CityModel -> R.string.filter_title_city
            else -> error("unknown class")
        }
    }

    fun mapToDialogTitle(type: Class<*>): Int {
        return when (type) {
            CargoBodyModel::class.java -> R.string.filter_group_cargo_body_type
            OfferStatusModel::class.java -> R.string.filter_group_status
            else -> error("unknown class $type")
        }
    }

    fun mapToHint(model: PickerModel): Int {
        return when (model) {
            is CategoryModel -> R.string.filter_picker_hint_category
            is CategorySubCategoryModel -> R.string.filter_picker_hint
            is SubCategoryModel -> R.string.filter_picker_hint_sub_category
            is CountryModel -> R.string.filter_picker_hint_country
            is RegionCityModel -> R.string.filter_picker_hint_city
            is RegionModel -> R.string.filter_picker_hint_region
            is CityModel -> R.string.filter_picker_hint_city
            else -> error("unknown class")
        }
    }
}
