package ru.perevozka24.perevozka24.ui.base

import android.os.Bundle
import kotlinx.android.synthetic.main.activity_fragment_container.*
import ru.perevozka24.perevozka24.R

abstract class BaseFragmentActivity : BaseActivity() {
    open val contentViewId = R.layout.activity_fragment_container
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(contentViewId)
        setSupportActionBar(toolbar)
        title = ""
        backButton.setOnClickListener { onBackPressed() }
    }
}
