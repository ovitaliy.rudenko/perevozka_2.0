package ru.perevozka24.perevozka24.ui.main.orders.items

import com.xwray.groupie.Group
import ru.perevozka24.perevozka24.ui.common.ItemMapper
import ru.perevozka24.perevozka24.ui.main.orders.OrderAction
import ru.perevozka24.perevozka24.ui.main.orders.models.OfferModel

object OrderItemMapper : ItemMapper<OfferModel, OrderAction, Group> {

    override fun map(e: OfferModel, callback: OrderAction, params: Any): Group {
        return OrderItem(e, callback, params as Boolean)
    }
}
