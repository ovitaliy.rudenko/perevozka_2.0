package ru.perevozka24.perevozka24.ui.common

import java.lang.ref.WeakReference
import kotlin.properties.ReadOnlyProperty
import kotlin.reflect.KProperty

class Weak<T : Any?, R>(private val initializer: () -> R) : ReadOnlyProperty<T, R> {

    private var weakReference: WeakReference<R>? = null

    override fun getValue(thisRef: T, property: KProperty<*>): R {
        val result = weakReference?.get()
        return if (result == null) {
            val newvalue = initializer()
            weakReference = WeakReference(newvalue)
            newvalue
        } else {
            result
        }
    }
}

fun <T : Any, R> weak(initializer: () -> R): Weak<T, R> = Weak(initializer)
