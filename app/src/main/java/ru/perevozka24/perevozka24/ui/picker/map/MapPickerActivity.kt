package ru.perevozka24.perevozka24.ui.picker.map

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.commit
import kotlinx.android.synthetic.main.activity_fragment_container.*
import org.kodein.di.Copy
import org.kodein.di.android.kodein
import org.kodein.di.android.retainedSubKodein
import ru.perevozka24.perevozka24.R
import ru.perevozka24.perevozka24.data.features.location.entity.SimpleAddress
import ru.perevozka24.perevozka24.ui.base.BaseFragmentActivity
import ru.perevozka24.perevozka24.ui.viewModel

class MapPickerActivity : BaseFragmentActivity() {
    override val kodein by retainedSubKodein(kodein(), copy = Copy.All) {
        import(mapPickerViewModelModule)
    }
    private val viewModel: MapPickerViewModel by viewModel()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        toolbarTitleView.setText(R.string.picker_map_title)
        viewModel.init(intent.getParcelableExtra(ADDRESS) as SimpleAddress)
        if (savedInstanceState == null) {
            supportFragmentManager.commit {
                replace(R.id.container, MapPickerFragment())
            }
        }
    }

    companion object {
        const val ADDRESS = "address"
        fun create(context: Context, address: SimpleAddress) =
            Intent(context, MapPickerActivity::class.java)
                .putExtra(ADDRESS, address)
    }
}
