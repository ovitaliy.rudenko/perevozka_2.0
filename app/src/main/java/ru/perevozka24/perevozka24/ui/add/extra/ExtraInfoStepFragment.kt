package ru.perevozka24.perevozka24.ui.add.extra

import android.app.DatePickerDialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import androidx.lifecycle.Observer
import kotlinx.android.synthetic.main.fragment_add_order_step_extra_info.*
import org.threeten.bp.LocalDate
import org.threeten.bp.format.DateTimeFormatter
import ru.perevozka24.perevozka24.DATE_FORMAT
import ru.perevozka24.perevozka24.R
import ru.perevozka24.perevozka24.ui.add.BaseCreateOrderStepFragment
import ru.perevozka24.perevozka24.ui.common.ext.onItemSelected
import ru.perevozka24.perevozka24.ui.common.ext.setVisibleText
import ru.perevozka24.perevozka24.ui.common.ext.visible
import ru.perevozka24.perevozka24.ui.common.widgets.SimpleOnItemSelectedListener
import ru.perevozka24.perevozka24.ui.common.widgets.form.addTextChangedListener
import ru.perevozka24.perevozka24.ui.filters.models.ExecutionPeriodModel
import ru.perevozka24.perevozka24.ui.filters.models.ExecutionPeriodType
import ru.perevozka24.perevozka24.ui.viewModel
import java.util.Calendar

class ExtraInfoStepFragment : BaseCreateOrderStepFragment() {
    override val title: Int = R.string.create_order_step_extra_info_title
    private val stepViewModel: ExtraInfoStepViewModel by viewModel()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_add_order_step_extra_info, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupDateFields()
        setupRateFields()
        addOrderExtraNext.setOnClickListener {
            stepViewModel.onNextClicked()
        }

        stepViewModel.moveNext.observe(viewLifecycleOwner, Observer {
            addOrderViewModel.extraInfoSelected(
                ExecutionPeriodModel(
                    stepViewModel.periodType.value,
                    stepViewModel.date1Field.value?.value,
                    stepViewModel.date2Field.value?.value
                ),
                stepViewModel.selectedPrice.value,
                stepViewModel.selectedType.value
            )
        })
    }

    private fun setupDateFields() {
        addOrderExecutionPeriodType.onItemSelected<ExecutionPeriodType> {
            stepViewModel.changeExecutionType(it)
        }
        addOrderExecutionPeriodType.adapter = ExecutionPeriodAdapter()

        stepViewModel.date1FieldError.observe(viewLifecycleOwner, Observer {
            addOrderExecutionPeriodDate1Error.setVisibleText(it)
        })
        stepViewModel.date2FieldError.observe(viewLifecycleOwner, Observer {
            addOrderExecutionPeriodDate2Error.setVisibleText(it)
        })

        stepViewModel.dateFieldVisible.observe(viewLifecycleOwner, Observer {
            addOrderExecutionPeriodTitle1.visible = it
            addOrderExecutionPeriodDate1.visible = it
        })
        stepViewModel.dateField2Visible.observe(viewLifecycleOwner, Observer {
            addOrderExecutionPeriodTitle2.visible = it
            addOrderExecutionPeriodDate2.visible = it
        })
        stepViewModel.dateTitle.observe(viewLifecycleOwner, Observer {
            addOrderExecutionPeriodTitle1.setText(it)
        })

        stepViewModel.date1Field.observe(viewLifecycleOwner, Observer {
            addOrderExecutionPeriodDate1.text = it.value
        })

        stepViewModel.date2Field.observe(viewLifecycleOwner, Observer {
            addOrderExecutionPeriodDate2.text = it.value
        })

        addOrderExecutionPeriodDate1.setOnClickListener {
            val cal = Calendar.getInstance()
            DatePickerDialog(
                it.context, 0, { _, year, month, dayOfMonth ->
                    val date = LocalDate.of(year, month + 1, dayOfMonth)
                    stepViewModel.date1Changed(date.format(DateTimeFormatter.ofPattern(DATE_FORMAT)))
                }, cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH)
            )
                .apply {
                    datePicker.minDate = cal.timeInMillis
                }
                .show()
        }

        addOrderExecutionPeriodDate2.setOnClickListener {
            val cal = Calendar.getInstance()
            DatePickerDialog(
                it.context, 0,
                DatePickerDialog.OnDateSetListener { _, year, month, dayOfMonth ->
                    val date = LocalDate.of(year, month + 1, dayOfMonth)
                    stepViewModel.date2Changed(date.format(DateTimeFormatter.ofPattern(DATE_FORMAT)))
                }, cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH)
            )
                .apply {
                    datePicker.minDate = cal.timeInMillis
                }
                .show()
        }
    }

    private fun setupRateFields() {
        addOrderPriceType.onItemSelectedListener = object : SimpleOnItemSelectedListener() {
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                (parent?.adapter as? PriceTypeAdapter)?.getItem(position)?.let {
                    stepViewModel.typeChanged(it)
                }
            }
        }
        stepViewModel.rateTypes.observe(viewLifecycleOwner, Observer {
            addOrderPriceType.adapter = PriceTypeAdapter(it ?: emptyList())
        })

        addOrderPrice.addTextChangedListener { stepViewModel.priceChanged(it?.toString() ?: "") }
    }
}
