package ru.perevozka24.perevozka24.ui.filters.picker.loader

import ru.perevozka24.perevozka24.ui.filters.models.PickerModel

interface PickerLoader<out C : PickerModel> {
    suspend fun load(): List<PickerModel>
    fun <T : PickerModel> filter(list: List<T>, q: String): List<C>
}
