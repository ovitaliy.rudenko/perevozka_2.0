package ru.perevozka24.perevozka24.ui.filters.picker.loader

import android.view.View
import com.xwray.groupie.Group
import ru.perevozka24.perevozka24.R
import ru.perevozka24.perevozka24.ui.filters.models.CategoryModel
import ru.perevozka24.perevozka24.ui.filters.models.CategorySubCategoryModel
import ru.perevozka24.perevozka24.ui.filters.models.CityModel
import ru.perevozka24.perevozka24.ui.filters.models.CountryModel
import ru.perevozka24.perevozka24.ui.filters.models.PickerModel
import ru.perevozka24.perevozka24.ui.filters.models.RegionCityModel
import ru.perevozka24.perevozka24.ui.filters.models.RegionModel
import ru.perevozka24.perevozka24.ui.filters.models.SubCategoryModel
import ru.perevozka24.perevozka24.ui.filters.picker.item.ClearItem

object PickerClearMapper {

    fun clearItem(model: PickerModel, title: Int, onClickListener: View.OnClickListener): Group {
        val m = when (model) {
            is CategoryModel -> CategoryModel.default()
            is SubCategoryModel -> SubCategoryModel.default(categoryId = model.categoryId)
            is CountryModel -> CountryModel.default(model.direction)
            is RegionModel -> RegionModel.default(model.countryId, model.direction)
            is CityModel -> CityModel.default(model.countryId, model.regionId, model.direction)
            is RegionCityModel -> RegionCityModel.default(
                direction = model.direction,
                countryId = model.region.countryId
            )
            is CategorySubCategoryModel ->
                CategorySubCategoryModel.default(
                    model.categoryId, model.categoryName
                )
            else -> error("unknown class $model")
        }
        return ClearItem(m, title, onClickListener)
    }

    fun mainTitle(model: PickerModel): Int {
        return when (model) {
            is CategoryModel -> R.string.filter_category_main_default
            is SubCategoryModel -> R.string.filter_sub_main_default
            is CategorySubCategoryModel -> R.string.filter_sub_main_default
            is CountryModel -> R.string.filter_country_main_default
            is RegionModel -> R.string.filter_region_main_default
            is CityModel -> R.string.filter_city_main_default
            else -> error("unknown class $model")
        }
    }
}
