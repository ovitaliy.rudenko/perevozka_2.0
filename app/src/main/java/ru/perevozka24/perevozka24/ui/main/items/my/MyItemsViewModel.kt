package ru.perevozka24.perevozka24.ui.main.items.my

import ru.perevozka24.perevozka24.data.ResourceProvider
import ru.perevozka24.perevozka24.data.features.items.ItemsRepository
import ru.perevozka24.perevozka24.ui.common.livedata.postValue
import ru.perevozka24.perevozka24.ui.main.items.AddToFavoriteViewModel

class MyItemsViewModel(
    private val repository: ItemsRepository,
    resourceProvider: ResourceProvider
) : AddToFavoriteViewModel(repository, resourceProvider) {
    override val searchReportName: String
        get() = error("not used")

    init {
        load()
    }

    override fun refresh() = load()

    private fun load() {
        doWork(items) {
            val result = repository.getMyItems()
            itemsList.postValue(result)
            result
        }
    }
}
