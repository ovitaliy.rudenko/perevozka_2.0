package ru.perevozka24.perevozka24.ui.filters.picker.item

import android.view.View
import com.xwray.groupie.kotlinandroidextensions.GroupieViewHolder
import kotlinx.android.synthetic.main.item_filter_loader_sub_category.view.*
import ru.perevozka24.perevozka24.R
import ru.perevozka24.perevozka24.ui.common.ext.load
import ru.perevozka24.perevozka24.ui.common.ext.visible
import ru.perevozka24.perevozka24.ui.filters.models.SubCategoryModel

data class SubCategoryItem(val model: SubCategoryModel, val onClick: View.OnClickListener) :
    BasePickerItem(onClick) {
    override fun getLayout(): Int = R.layout.item_filter_loader_sub_category
    override fun getId(): Long = model.id?.toLongOrNull() ?: 0

    override fun bind(viewHolder: GroupieViewHolder, position: Int) {
        with(viewHolder.itemView) {
            tag = model
            filterLoaderSubCategoryText.text = model.name
                ?: resources.getString(R.string.filter_sub_default)
            filterLoaderSubCategoryIcon.visible = model.icon != null
            filterLoaderSubCategoryIcon.load(model.icon)
        }
    }
}
