package ru.perevozka24.perevozka24.ui.base

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.ViewStub
import android.widget.TextView
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import kotlinx.android.synthetic.main.fragment_content_loader.*
import ru.perevozka24.perevozka24.R
import ru.perevozka24.perevozka24.data.Result
import ru.perevozka24.perevozka24.data.isEmpty
import ru.perevozka24.perevozka24.ui.common.ext.showOnly

abstract class BaseContentLoaderFragment : BaseFragment() {

    abstract val contentLayoutId: Int

    private lateinit var contentView: View

    final override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(R.layout.fragment_content_loader, container, false).apply {
        findViewById<ViewStub>(R.id.containerContent).apply {
            layoutResource = contentLayoutId
            contentView = inflate()
        }
    }

    fun <T : Any> subscribe(result: LiveData<Result<T>>) {
        result.observe(viewLifecycleOwner, Observer {
            it ?: return@Observer
            when (it) {
                is Result.Loading -> showOnly(
                    containerLoading,
                    containerError, contentView, contentEmpty
                )
                is Result.Error -> {
                    containerError.findViewById<TextView>(R.id.errorMessageText).text = it.message
                    showOnly(
                        containerError,
                        containerLoading, contentView, contentEmpty
                    )
                }
                is Result.Success ->
                    if (it.isEmpty()) {
                        showOnly(
                            contentEmpty,
                            contentView, containerError, containerLoading
                        )
                    } else {
                        showOnly(
                            contentView,
                            contentEmpty, containerError, containerLoading
                        )
                    }
            }
        })
    }
}
