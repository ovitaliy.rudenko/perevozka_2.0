package ru.perevozka24.perevozka24.ui.notification.item

import com.xwray.groupie.kotlinandroidextensions.GroupieViewHolder
import com.xwray.groupie.kotlinandroidextensions.Item
import kotlinx.android.synthetic.main.item_notification_settings.view.*
import ru.perevozka24.perevozka24.R
import ru.perevozka24.perevozka24.ui.common.widgets.onCheckChanged

data class NotificationCheckboxItem(
    private val title: Int,
    private val checked: Boolean,
    private val onChecked: (Boolean) -> Unit
) : Item() {
    override fun getId(): Long = title.toLong()

    override fun getLayout(): Int = R.layout.item_notification_settings

    override fun bind(viewHolder: GroupieViewHolder, position: Int) {
        with(viewHolder.itemView) {
            notificationCheckbox.checked = checked
            notificationCheckbox.setTitle(title)
            notificationCheckbox.onCheckChanged(onChecked)
        }
    }
}
