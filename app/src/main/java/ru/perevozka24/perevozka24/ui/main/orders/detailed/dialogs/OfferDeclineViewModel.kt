package ru.perevozka24.perevozka24.ui.main.orders.detailed.dialogs

import ru.perevozka24.perevozka24.data.ResourceProvider
import ru.perevozka24.perevozka24.data.features.offers.OfferCancelParams
import ru.perevozka24.perevozka24.data.features.offers.OfferRepository

class OfferDeclineViewModel(
    private val offerRepository: OfferRepository,
    resourceProvider: ResourceProvider
) : BaseOfferDialogActionViewModel(resourceProvider) {

    override suspend fun proceed() {
        val params = OfferCancelParams(offer.id, checkNotNull(inputField.value?.value))
        offerRepository.cancelOffer(params)
    }
}
