package ru.perevozka24.perevozka24.ui.main.orders.detailed.dialogs

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.launch
import ru.perevozka24.perevozka24.data.ResourceProvider
import ru.perevozka24.perevozka24.data.Result
import ru.perevozka24.perevozka24.ui.LiveEvent
import ru.perevozka24.perevozka24.ui.base.BaseViewModel
import ru.perevozka24.perevozka24.ui.common.livedata.postValue
import ru.perevozka24.perevozka24.ui.common.widgets.form.EmptyFieldValidator
import ru.perevozka24.perevozka24.ui.common.widgets.form.InputField
import ru.perevozka24.perevozka24.ui.main.orders.models.OfferModel

abstract class BaseOfferDialogActionViewModel(
    resourceProvider: ResourceProvider
) : BaseViewModel(resourceProvider) {

    protected lateinit var offer: OfferModel

    val submited = LiveEvent<Unit>()
    val inputFieldError: LiveData<String> = MutableLiveData()
    protected val inputField = MutableLiveData(InputField("", EmptyFieldValidator(),
        { inputFieldError.postValue(it) }
    ))

    fun init(value: OfferModel) {
        offer = value
    }

    fun setValue(price: String) {
        inputField.value?.value = price
    }

    fun submit() {
        inputField.value?.validate()
        val valid = !(inputField.value?.hasError ?: true)

        if (valid) {
            viewModelScope.launch {
                val result = safeExecute { proceed() }

                if (result is Result.Success) {
                    submited.postValue(Unit)
                } else if (result is Result.Error) {
                    error.postValue(result.message)
                }
            }
        }
    }

    abstract suspend fun proceed()
}
