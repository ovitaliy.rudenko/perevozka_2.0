package ru.perevozka24.perevozka24.ui.main.items.models

import ru.perevozka24.perevozka24.data.features.items.entities.OptionEntity
import ru.perevozka24.perevozka24.data.utils.escape
import ru.perevozka24.perevozka24.ui.common.Mapper

object OptionMapper : Mapper<OptionEntity, OptionModel> {
    override fun map(e: OptionEntity): OptionModel {
        return OptionModel(
            label = checkNotNull(e.label).escape(),
            value = checkNotNull(e.value).escape()
        )
    }
}
