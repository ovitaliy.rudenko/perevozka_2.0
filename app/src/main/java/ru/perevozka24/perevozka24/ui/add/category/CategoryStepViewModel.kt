package ru.perevozka24.perevozka24.ui.add.category

import ru.perevozka24.perevozka24.data.ResourceProvider
import ru.perevozka24.perevozka24.data.features.filter.FilterLocationPickerRepository
import ru.perevozka24.perevozka24.data.features.filter.FilterPickerRepository
import ru.perevozka24.perevozka24.ui.filters.models.CategoryModel
import ru.perevozka24.perevozka24.ui.filters.picker.FilterPickerViewModel

class CategoryStepViewModel(
    pickerRepository: FilterPickerRepository,
    pickerLocationRepository: FilterLocationPickerRepository,
    resourceProvider: ResourceProvider
) : FilterPickerViewModel(pickerRepository, pickerLocationRepository, resourceProvider) {
    init {
        init(CategoryModel.default())
    }
}
