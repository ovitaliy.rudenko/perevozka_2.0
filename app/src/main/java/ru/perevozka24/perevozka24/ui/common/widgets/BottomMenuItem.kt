package ru.perevozka24.perevozka24.ui.common.widgets

import android.content.Context
import android.util.AttributeSet
import android.view.Gravity
import android.view.View
import android.widget.LinearLayout
import androidx.annotation.StyleableRes
import kotlinx.android.synthetic.main.merge_bottom_menu_item.view.*
import ru.perevozka24.perevozka24.R

class BottomMenuItem @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : LinearLayout(context, attrs, defStyleAttr) {
    init {
        gravity = Gravity.BOTTOM or Gravity.CENTER_HORIZONTAL
        orientation = VERTICAL
        View.inflate(context, R.layout.merge_bottom_menu_item, this)

        val set = intArrayOf(
            android.R.attr.src, // idx 0
            android.R.attr.text // idx 1
        )
        @StyleableRes val iconIndex = 0
        @StyleableRes val textIndex = 1
        val a = context.obtainStyledAttributes(attrs, set)
        val icon = a.getDrawable(iconIndex)
        val title = a.getText(textIndex)
        a.recycle()

        titleView.text = title
        iconView.setImageDrawable(icon)
    }
}
