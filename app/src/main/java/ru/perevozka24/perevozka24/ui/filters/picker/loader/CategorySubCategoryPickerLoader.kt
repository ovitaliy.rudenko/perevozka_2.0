package ru.perevozka24.perevozka24.ui.filters.picker.loader

import ru.perevozka24.perevozka24.data.features.filter.FilterPickerDataSource
import ru.perevozka24.perevozka24.data.features.filter.FilterPickerRepository
import ru.perevozka24.perevozka24.ui.filters.models.CategorySubCategoryModel
import ru.perevozka24.perevozka24.ui.filters.models.PickerModel

class CategorySubCategoryPickerLoader(
    private val current: CategorySubCategoryModel,
    private val pickerRepository: FilterPickerRepository,
    private val extra: Array<out Any>
) : PickerLoader<CategorySubCategoryModel> {
    override suspend fun load(): List<CategorySubCategoryModel> {
        val cargoOnly = extra.takeIf { it.isNotEmpty() }?.get(0) == true
        return pickerRepository.getCategorySubCategories(current.shipment)
            .run {
                if (current.categoryId != null) {
                    filter { it.categoryId == current.categoryId }
                } else {
                    this
                }
            }
            .filter {
                when (cargoOnly) {
                    true -> it.categoryId == FilterPickerDataSource.CARGO_TYPE
                    false -> true
                }
            }
    }

    override fun <T : PickerModel> filter(
        list: List<T>,
        q: String
    ): List<CategorySubCategoryModel> {
        return list.asSequence().filter {
            it as CategorySubCategoryModel
            it.subCategoryName?.contains(q, true) == true
        }
            .map { it as CategorySubCategoryModel }.toList()
    }
}
