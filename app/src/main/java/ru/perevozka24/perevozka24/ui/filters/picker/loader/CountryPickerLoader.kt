package ru.perevozka24.perevozka24.ui.filters.picker.loader

import ru.perevozka24.perevozka24.data.features.filter.FilterLocationPickerRepository
import ru.perevozka24.perevozka24.ui.filters.models.CountryModel
import ru.perevozka24.perevozka24.ui.filters.models.PickerModel

class CountryPickerLoader(
    private val current: CountryModel,
    private val pickerRepository: FilterLocationPickerRepository
) : PickerLoader<CountryModel> {
    override suspend fun load(): List<CountryModel> {
        return pickerRepository.getCounties().map { it.copy(direction = current.direction) }
    }

    override fun <T : PickerModel> filter(list: List<T>, q: String): List<CountryModel> {
        return list
            .filter { (it as CountryModel).name?.contains(q, true) == true }
            .map { it as CountryModel }
    }
}
