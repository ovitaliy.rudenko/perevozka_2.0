package ru.perevozka24.perevozka24.ui.main

import androidx.fragment.app.Fragment
import ru.perevozka24.perevozka24.R
import ru.perevozka24.perevozka24.ui.main.items.equipment.EquipmentFragment
import ru.perevozka24.perevozka24.ui.main.items.favorite.FavoriteItemsFragment
import ru.perevozka24.perevozka24.ui.main.items.my.MyItemsFragment
import ru.perevozka24.perevozka24.ui.main.orders.cargo.CargoFragment
import ru.perevozka24.perevozka24.ui.main.orders.favorite.FavoriteOrdersFragment
import ru.perevozka24.perevozka24.ui.main.orders.my.MyOrdersFragment
import ru.perevozka24.perevozka24.ui.main.orders.orders.OrdersFragment
import ru.perevozka24.perevozka24.ui.main.toolbar.ToolbarMode

sealed class MainScreenMode(
    val fragment: () -> Fragment,
    val toolbarType: ToolbarMode
) {
    object Equipment : MainScreenMode(
        fragment = { EquipmentFragment() },
        toolbarType = ToolbarMode.Search(R.string.search_equipment_hint)
    )

    object FavoriteItems : MainScreenMode(
        fragment = { FavoriteItemsFragment() },
        toolbarType = ToolbarMode.Title(R.string.item_favorite_title)
    )

    object MyItems : MainScreenMode(
        fragment = { MyItemsFragment() },
        toolbarType = ToolbarMode.Title(R.string.item_my)
    )

    object Cargo : MainScreenMode(
        fragment = { CargoFragment() },
        toolbarType = ToolbarMode.Search(R.string.search_orders)
    )

    object Order : MainScreenMode(
        fragment = { OrdersFragment() },
        toolbarType = ToolbarMode.Search(R.string.search_orders)
    )

    object FavoriteOrders : MainScreenMode(
        fragment = { FavoriteOrdersFragment() },
        toolbarType = ToolbarMode.Title(R.string.favorite_orders)
    )

    object MyOrders : MainScreenMode(
        fragment = { MyOrdersFragment() },
        toolbarType = ToolbarMode.Title(R.string.orders_my)
    )
}
