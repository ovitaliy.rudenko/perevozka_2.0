package ru.perevozka24.perevozka24.ui.common.items

import android.view.View
import android.view.ViewGroup
import androidx.annotation.ColorRes
import androidx.annotation.DimenRes
import androidx.core.content.res.ResourcesCompat
import com.xwray.groupie.kotlinandroidextensions.GroupieViewHolder
import com.xwray.groupie.kotlinandroidextensions.Item
import ru.perevozka24.perevozka24.R

data class DividerItem(
    @ColorRes private val color: Int = R.color.grey_f1,
    @DimenRes private val size: Int = R.dimen.dp1,
    @DimenRes private val marginStart: Int = 0,
    @DimenRes private val marginEnd: Int = 0,
    @DimenRes private val marginTop: Int = 0,
    @DimenRes private val marginBottom: Int = 0
) : Item() {
    override fun getId() = 0L

    override fun getLayout() = R.layout.item_divider

    override fun bind(viewHolder: GroupieViewHolder, position: Int) = Unit

    override fun createViewHolder(itemView: View): GroupieViewHolder {
        return super.createViewHolder(itemView).apply {
            with(itemView) {
                val lp = layoutParams as ViewGroup.MarginLayoutParams
                lp.height = resources.getDimensionPixelOffset(size)
                if (marginStart > 0) {
                    lp.marginStart = resources.getDimensionPixelOffset(marginStart)
                }
                if (marginEnd > 0) {
                    lp.marginEnd = resources.getDimensionPixelOffset(marginEnd)
                }
                if (marginTop > 0) {
                    lp.topMargin = resources.getDimensionPixelOffset(marginTop)
                }
                if (marginBottom > 0) {
                    lp.bottomMargin = resources.getDimensionPixelOffset(marginBottom)
                }
                setBackgroundColor(ResourcesCompat.getColor(resources, color, null))
            }
        }
    }
}
