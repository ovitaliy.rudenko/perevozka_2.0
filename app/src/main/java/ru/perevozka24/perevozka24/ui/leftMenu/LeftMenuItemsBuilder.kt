package ru.perevozka24.perevozka24.ui.leftMenu

import com.xwray.groupie.Group
import ru.perevozka24.perevozka24.R
import ru.perevozka24.perevozka24.ui.add.AddOrderActivity
import ru.perevozka24.perevozka24.ui.common.ext.openLink
import ru.perevozka24.perevozka24.ui.leftMenu.items.AuthDoLoginMenuItem
import ru.perevozka24.perevozka24.ui.leftMenu.items.AuthLoggedInMenuItem
import ru.perevozka24.perevozka24.ui.leftMenu.items.DividerMenuItem
import ru.perevozka24.perevozka24.ui.leftMenu.items.MenuItem
import ru.perevozka24.perevozka24.ui.main.MainScreenMode
import ru.perevozka24.perevozka24.utils.trackEvent

@SuppressWarnings("LongMethod")
object LeftMenuItemsBuilder {

    fun buildItems(
        params: LeftMenuParams,
        action: LeftMenuAction
    ): List<Group> {
        val list = mutableListOf<Group>()
        list.addAll(authItems(params, action))
        list.addAll(itemsItems(params, action))
        list.addAll(orderItems(params, action))
        list.addAll(commonItems(params, action))
        return list
    }

    private fun authItems(
        params: LeftMenuParams,
        action: LeftMenuAction
    ): List<Group> =
        listOf(
            if (params.loginModel == null) {
                AuthDoLoginMenuItem(
                    loginClick = action.loginClick,
                )
            } else {
                AuthLoggedInMenuItem(logout = action.logoutClick, login = params.loginModel)
            },
            DividerMenuItem()
        )

    private fun itemsItems(
        params: LeftMenuParams,
        action: LeftMenuAction
    ): List<Group> {
        val list = mutableListOf<Group>()
        list.add(
            MenuItem(
                R.string.order_create,
                R.drawable.ic_add_in_circle
            ) { v ->
                trackEvent(
                    "create_order",
                    mapOf("type" to "main_menu")
                )
                v.context.run { startActivity(AddOrderActivity.create(this)) }
            })
        if (params.hasOrders) {
            list.add(
                MenuItem(
                    R.string.orders_my,
                    R.drawable.ic_my_orders,
                    action.switchTabClick(MainScreenMode.MyOrders)
                )
            )
        }
        list.add(
            MenuItem(
                R.string.search_equipment_hint,
                R.drawable.ic_left_menu_equipment,
                action.switchTabClick(MainScreenMode.Equipment)
            )
        )
        if (params.authorized) {
            list.add(
                MenuItem(
                    R.string.favorite_items,
                    R.drawable.ic_left_menu_heart,
                    action.switchTabClick(MainScreenMode.FavoriteItems)
                )
            )
        }
        list.add(DividerMenuItem())

        return list
    }

    private fun orderItems(
        params: LeftMenuParams,
        action: LeftMenuAction
    ): List<Group> {
        val list = mutableListOf<Group>()

        list.add(MenuItem(
            R.string.add_equipment,
            R.drawable.ic_add_in_circle
        ) { v ->
            trackEvent(
                "add_ad",
                mapOf("type" to "main-menu")
            )
            v.context.run {
                val link = if (params.authorized) {
                    R.string.link_add_equipment_logged_in
                } else {
                    R.string.link_add_equipment_not_logged_in
                }
                openLink(getString(link))
            }
        })
        if (params.hasItems) {
            list.add(
                MenuItem(
                    R.string.item_my,
                    R.drawable.ic_my_items,
                    action.switchTabClick(MainScreenMode.MyItems)
                )
            )
        }
        list.add(
            MenuItem(
                R.string.menu_orders,
                R.drawable.ic_left_menu_orders,
                action.switchTabClick(MainScreenMode.Order)
            )
        )
        list.add(
            MenuItem(
                R.string.menu_cargo,
                R.drawable.ic_left_menu_cargo,
                action.switchTabClick(MainScreenMode.Cargo)
            )
        )

        if (params.authorized) {
            list.add(
                MenuItem(
                    R.string.favorite_orders,
                    R.drawable.ic_left_menu_favorite,
                    action.switchTabClick(MainScreenMode.FavoriteOrders)
                )
            )
        }
        if (params.hasGeolocationItems) {
            list.add(
                MenuItem(
                    R.string.geolocation_title,
                    R.drawable.ic_left_menu_geolocation,
                    when (params.authorized) {
                        true -> action.redirectClick(R.id.action_global_geolocationActivity)
                        false -> action.loginClick
                    }
                )
            )
        }

        list.add(
            MenuItem(
                R.string.calculate_distance_title,
                R.drawable.ic_left_menu_distance,
                action.redirectClick(R.id.action_global_distanceActivity)
            )
        )

        list.add(DividerMenuItem())
        return list
    }

    private fun commonItems(
        params: LeftMenuParams,
        action: LeftMenuAction
    ): List<Group> =
        listOf(
            MenuItem(
                R.string.profile,
                R.drawable.ic_left_menu_profile,
                action.profileClick
            ),
            MenuItem(
                R.string.notification,
                R.drawable.ic_left_menu_notifications,
                when (params.authorized) {
                    true -> action.redirectClick(R.id.action_global_notificationActivity)
                    false -> action.loginClick
                }
            ),
            MenuItem(
                R.string.help,
                R.drawable.ic_left_menu_help,
                action.rulesClickLister
            ),
            MenuItem(
                R.string.support,
                R.drawable.ic_left_menu_support,
                action.redirectClick(R.id.action_global_supportActivity)
            )
        )
}
