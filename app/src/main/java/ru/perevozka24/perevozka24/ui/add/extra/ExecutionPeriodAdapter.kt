package ru.perevozka24.perevozka24.ui.add.extra

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import kotlinx.android.synthetic.main.item_address.view.*
import ru.perevozka24.perevozka24.R
import ru.perevozka24.perevozka24.ui.filters.models.ExecutionPeriodType

class ExecutionPeriodAdapter : BaseAdapter() {

    private val values = ExecutionPeriodType.values()

    override fun getCount(): Int = values.size

    override fun getItem(position: Int) = values[position]

    override fun getItemId(position: Int): Long = position.toLong()

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        return getDropDownView(position, convertView, parent).apply {
            background = null
        }
    }

    override fun getDropDownView(position: Int, convertView: View?, parent: ViewGroup): View {
        val view = convertView ?: LayoutInflater.from(parent.context)
            .inflate(R.layout.item_address, parent, false)
        with(view) {
            itemAddressTitle.setText(getItem(position).title)
        }
        return view
    }
}
