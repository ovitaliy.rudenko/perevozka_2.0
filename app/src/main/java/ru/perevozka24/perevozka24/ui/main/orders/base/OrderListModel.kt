package ru.perevozka24.perevozka24.ui.main.orders.base

import ru.perevozka24.perevozka24.ui.main.orders.models.OfferModel

data class OrderListModel(
    val list: List<OfferModel>,
    val isLast: Boolean
)
