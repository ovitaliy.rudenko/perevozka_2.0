package ru.perevozka24.perevozka24.ui.common.ext

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.provider.Settings
import androidx.fragment.app.Fragment
import com.google.android.material.snackbar.Snackbar
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.listener.multi.BaseMultiplePermissionsListener
import ru.perevozka24.perevozka24.R

fun Fragment.requestLocationPermission(granted: () -> Unit) {
    val view = view ?: return
    Dexter.withActivity(requireActivity())
        .withPermissions(
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.ACCESS_COARSE_LOCATION
        )
        .withListener(object : BaseMultiplePermissionsListener() {
            override fun onPermissionsChecked(report: MultiplePermissionsReport) {
                if (report.areAllPermissionsGranted()) {
                    granted()
                } else {
                    val permanentlyDenied =
                        report.deniedPermissionResponses.first().isPermanentlyDenied
                    Snackbar.make(view, R.string.location_permission, Snackbar.LENGTH_LONG)
                        .apply {
                            setAction(android.R.string.ok) {
                                if (!permanentlyDenied) {
                                    requestLocationPermission(granted)
                                } else {
                                    startSettings()
                                }
                            }
                        }.show()
                }
            }
        })
        .onSameThread()
        .check()
}

inline fun Activity.requestLocationPermissionIgnoreResult(crossinline onRequested: () -> Unit) {
    Dexter.withActivity(this)
        .withPermissions(
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.ACCESS_COARSE_LOCATION
        )
        .withListener(object : BaseMultiplePermissionsListener() {
            override fun onPermissionsChecked(report: MultiplePermissionsReport) {
                onRequested()
            }
        })
        .onSameThread()
        .check()
}

inline fun Fragment.startSettings() {
    val view = view ?: return
    val context = view.context
    val appSettings = Intent(
        Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
        Uri.parse("package:" + context.packageName)
    )
    appSettings.addCategory(Intent.CATEGORY_DEFAULT)
    appSettings.flags = Intent.FLAG_ACTIVITY_NEW_TASK
    context.startActivity(appSettings)
}
