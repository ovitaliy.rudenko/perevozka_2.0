package ru.perevozka24.perevozka24.ui.main.items.items

import com.xwray.groupie.kotlinandroidextensions.GroupieViewHolder
import com.xwray.groupie.kotlinandroidextensions.Item
import kotlinx.android.synthetic.main.item_equipment.view.*
import ru.perevozka24.perevozka24.R
import ru.perevozka24.perevozka24.ui.common.ext.visible
import ru.perevozka24.perevozka24.ui.main.items.ItemAction
import ru.perevozka24.perevozka24.ui.main.items.models.ItemModel
import ru.perevozka24.perevozka24.ui.main.items.widget.EquipmentItemHelper

data class EquipmentItem(
    private val model: ItemModel,
    private val action: ItemAction,
    private val showExtraActions: Boolean
) : Item() {
    override fun getId() = model.id.toLongOrNull() ?: 0
    override fun getLayout(): Int = R.layout.item_equipment

    override fun bind(viewHolder: GroupieViewHolder, position: Int) {
        with(viewHolder.itemView) {
            EquipmentItemHelper.setUp(this, model)

            setOnClickListener { action.openDetailed(model, it) }

            equipmentLike.visible = showExtraActions
            equipmentMore.visible = showExtraActions
            if (showExtraActions) {
                equipmentLike.isSelected = model.favorite
                equipmentLike.setOnClickListener {
                    action.addToFavorite(model, it)
                }
                equipmentMore.setOnClickListener { action.showMore(model, it) }
            }
        }
    }
}
