package ru.perevozka24.perevozka24.ui.base

import android.os.Bundle
import android.widget.TextView
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import kotlinx.android.synthetic.main.activity_fragment_container.*
import kotlinx.android.synthetic.main.fragment_content_loader.*
import ru.perevozka24.perevozka24.R
import ru.perevozka24.perevozka24.data.Result
import ru.perevozka24.perevozka24.data.isEmpty
import ru.perevozka24.perevozka24.ui.common.ext.showOnly

abstract class BaseContentLoaderFragmentActivity<T : Any> : BaseFragmentActivity() {

    override val contentViewId = R.layout.activity_fragment_loading_container

    abstract val viewModel: BaseContentLoaderViewModel<T>
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        subscribe(viewModel.result)
        viewModel.loaded.observe(this, Observer { showLoaded(it) })
    }

    fun <T : Any> subscribe(result: LiveData<Result<T>>) {
        result.observe(this, Observer {
            it ?: return@Observer
            when (it) {
                is Result.Loading -> showOnly(
                    containerLoading,
                    containerError, container, contentEmpty
                )
                is Result.Error -> {
                    containerError.findViewById<TextView>(R.id.errorMessageText).text = it.message
                    showOnly(
                        containerError,
                        containerLoading, container, contentEmpty
                    )
                }
                is Result.Success ->
                    if (it.isEmpty()) {
                        showOnly(
                            contentEmpty,
                            container, containerError, containerLoading
                        )
                    } else {
                        showOnly(
                            container,
                            contentEmpty, containerError, containerLoading
                        )
                    }
            }
        })
    }

    abstract fun showLoaded(value: T)
}
