package ru.perevozka24.perevozka24.ui.add

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.fragment.app.commit
import androidx.lifecycle.Observer
import kotlinx.android.synthetic.main.activity_add_order.*
import ru.perevozka24.perevozka24.MainActivity
import ru.perevozka24.perevozka24.R
import ru.perevozka24.perevozka24.ui.base.BaseActivity
import ru.perevozka24.perevozka24.ui.common.ext.hideKeyboard
import ru.perevozka24.perevozka24.ui.common.ext.restart
import ru.perevozka24.perevozka24.ui.common.ext.toast
import ru.perevozka24.perevozka24.ui.common.ext.visible
import ru.perevozka24.perevozka24.ui.viewModel

class AddOrderActivity : BaseActivity() {

    val viewModel: AddOrderViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_order)
        updateStepsCount()
        addOrderToolbarBackButton.setOnClickListener { onBackPressed() }
        viewModel.init(
            intent.getStringExtraOnce(CATEGORY),
            intent.getStringExtraOnce(SUB_CATEGORY)
        )
        viewModel.userId = intent.getStringExtra(USER_ID)
        viewModel.stepNext.observe(this, Observer {
            supportFragmentManager.commit {
                updateStepsCount()
                val formFactory = FormFactory.create(viewModel.categoryModel)
                val fragment = formFactory.steps[it].newInstance()
                if (it != 0) addToBackStack("step_$it")
                addOrderStepTitle.text = getString(fragment.title)
                addOrderStepBar.step = it + 1
                replace(R.id.addOrderContainer, fragment as Fragment)
            }
        })
        viewModel.loading.observe(this, {
            addOrderProgressBar.visible = it
        })
        viewModel.error.observe(this, {
            toast(it)
        })
        viewModel.saved.observe(this, {
            hideKeyboard(this)
            toast(getString(R.string.create_order_saved))
            val action = if (it) MainActivity.MY_ORDERS else MainActivity.VIEW_ORDER
            val intent = MainActivity.create(this, action)
                .restart()
            startActivity(intent)
        })
    }

    override fun onBackPressed() {
        super.onBackPressed()
        setupHeader()
    }

    override fun onPause() {
        hideKeyboard(this)
        super.onPause()
    }

    private fun updateStepsCount() {
        val formFactory = FormFactory.create(viewModel.categoryModel)
        addOrderStepBar.steps = formFactory.count
        viewModel.setStepsCount(formFactory.count)
    }

    private fun setupHeader() {
        viewModel.stepChanged(supportFragmentManager.backStackEntryCount)
        val tId = (supportFragmentManager.fragments.lastOrNull() as? ICreateOrderStep)?.title
        addOrderStepTitle.text = tId?.let { getString(it) } ?: ""
        addOrderStepBar.step = supportFragmentManager.backStackEntryCount + 1
    }

    private fun Intent.getStringExtraOnce(name: String): String? {
        val value = getStringExtra(name)
        removeExtra(name)
        return value
    }

    companion object {
        private const val USER_ID = "userId"
        const val CATEGORY = "category"
        const val SUB_CATEGORY = "subcategory"
        fun create(
            context: Context,
            userId: String? = null,
            category: String? = null,
            subcategory: String? = null
        ): Intent {
            return Intent(context, AddOrderActivity::class.java)
                .putExtra(USER_ID, userId)
                .putExtra(CATEGORY, category)
                .putExtra(SUB_CATEGORY, subcategory)
        }
    }
}
