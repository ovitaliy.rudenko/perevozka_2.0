package ru.perevozka24.perevozka24.ui.splash

import android.content.Intent
import android.os.Bundle
import androidx.lifecycle.Observer
import ru.perevozka24.perevozka24.MainActivity
import ru.perevozka24.perevozka24.ui.base.BaseActivity
import ru.perevozka24.perevozka24.ui.common.ext.requestLocationPermissionIgnoreResult
import ru.perevozka24.perevozka24.ui.viewModel
import ru.perevozka24.perevozka24.ui.welcome.WelcomeActivity

class SplashActivity : BaseActivity() {
    private val viewModel: SplashViewModel by viewModel()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val self = this
        viewModel.showWelcome.observe(this, Observer {
            startActivity(Intent(self, WelcomeActivity::class.java))
        })

        viewModel.showMain.observe(this, Observer {
            requestLocationPermissionIgnoreResult {
                startActivity(Intent(self, MainActivity::class.java))
            }
        })
    }
}
