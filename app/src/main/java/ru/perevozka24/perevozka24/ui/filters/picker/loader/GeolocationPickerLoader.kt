package ru.perevozka24.perevozka24.ui.filters.picker.loader

import ru.perevozka24.perevozka24.data.features.geolocation.LocationTrackerRepository
import ru.perevozka24.perevozka24.ui.filters.models.PickerModel
import ru.perevozka24.perevozka24.ui.geolocation.GeolocationModel

class GeolocationPickerLoader(
    private val pickerRepository: LocationTrackerRepository
) : PickerLoader<GeolocationModel> {
    override suspend fun load(): List<GeolocationModel> {
        return pickerRepository.getItems()
    }

    override fun <T : PickerModel> filter(list: List<T>, q: String): List<GeolocationModel> =
        list.map { it as GeolocationModel }
}
