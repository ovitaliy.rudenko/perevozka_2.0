package ru.perevozka24.perevozka24.ui.filters.models

import kotlinx.android.parcel.IgnoredOnParcel
import kotlinx.android.parcel.Parcelize

@Parcelize
data class FilterItemsEquipmentModel(
    val category: CategoryModel,
    val subCategory: SubCategoryModel,
    val country: CountryModel,
    val region: RegionModel,
    val city: CityModel
) : FilterModel {
    @IgnoredOnParcel
    val shipment: Long = 0

    override fun updated(value: PickerModel): FilterModel {
        return when (value) {
            is CategoryModel -> copy(
                category = value,
                subCategory = SubCategoryModel.default(categoryId = value.id)
            )
            is SubCategoryModel -> copy(subCategory = value)
            is CountryModel -> copy(
                country = value,
                region = RegionModel.default(countryId = value.id),
                city = CityModel.default()
            )
            is RegionModel -> copy(region = value, city = CityModel.default(value.countryId, value.id))
            is CityModel -> copy(city = value)
            else -> error("unknown type")
        }
    }
}
