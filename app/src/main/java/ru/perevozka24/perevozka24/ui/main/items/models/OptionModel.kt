package ru.perevozka24.perevozka24.ui.main.items.models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class OptionModel(val label: String, val value: String) : Parcelable
