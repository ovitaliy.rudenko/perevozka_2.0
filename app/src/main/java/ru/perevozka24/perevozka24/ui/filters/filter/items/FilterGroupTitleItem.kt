package ru.perevozka24.perevozka24.ui.filters.filter.items

import com.xwray.groupie.kotlinandroidextensions.GroupieViewHolder
import com.xwray.groupie.kotlinandroidextensions.Item
import kotlinx.android.synthetic.main.item_filter_group_title.view.*
import ru.perevozka24.perevozka24.R

data class FilterGroupTitleItem(
    private val titleId: Int
) : Item() {
    override fun getId(): Long = 0
    override fun getLayout(): Int = R.layout.item_filter_group_title

    override fun bind(viewHolder: GroupieViewHolder, position: Int) {
        with(viewHolder.itemView) {
            groupTitle.setText(titleId)
        }
    }
}
