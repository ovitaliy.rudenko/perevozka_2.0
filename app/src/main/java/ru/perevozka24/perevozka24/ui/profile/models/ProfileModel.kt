package ru.perevozka24.perevozka24.ui.profile.models

import ru.perevozka24.perevozka24.App
import ru.perevozka24.perevozka24.R
import ru.perevozka24.perevozka24.data.base.ApiException
import ru.perevozka24.perevozka24.ui.filters.models.CategoryModel
import ru.perevozka24.perevozka24.ui.filters.models.CityModel
import ru.perevozka24.perevozka24.ui.filters.models.CountryModel
import ru.perevozka24.perevozka24.ui.filters.models.RegionModel

data class ProfileModel(
    val name: String?,
    val phone: String?,
    val countryModel: CountryModel,
    val regionModel: RegionModel,
    val cityModel: CityModel,
    val address: String?,
    val categoryModel: CategoryModel,
    val description: String?,
    val mode: Int?,
    val inn: String?,
    val kpp: String?,
    val orgn: String?
) {
    @SuppressWarnings("ComplexMethod")
    fun validate(): ProfileModel {
        val error = mutableListOf<String>()
        if (name.isNullOrBlank()) {
            if (mode == ProfileCompanyType.Company.value) {
                error.add(required(R.string.profile_company_hint))
            } else {
                error.add(required(R.string.profile_username))
            }
        } else {
            if (mode != ProfileCompanyType.Company.value && name.split(" ").size < FIO_LENGHT) {
                val msg = App.instance.resources.getString(R.string.profile_validation_user_name)
                error.add(msg)
            }
        }

        if (phone.isNullOrBlank()) {
            error.add(required(R.string.profile_phone))
        }

        if (regionModel.id == null) {
            error.add(required(R.string.profile_region))
        }
        if (cityModel.id == null) {
            error.add(required(R.string.profile_city))
        }

        if (address.isNullOrBlank()) {
            error.add(required(R.string.profile_address))
        }

        if (categoryModel.id == null) {
            error.add(required(R.string.profile_specialization))
        }

        if (inn.isNullOrBlank()) {
            error.add(required(R.string.profile_code_id))
        }

        if (mode == ProfileCompanyType.Company.value) {
            if (kpp.isNullOrBlank()) {
                error.add(required(R.string.profile_kpp))
            }
        }
        if (mode == ProfileCompanyType.Company.value) {
            if (orgn.isNullOrBlank()) {
                error.add(required(R.string.profile_orgn))
            }
        }

        if (mode == ProfileCompanyType.Entrepreneur.value) {
            if (orgn.isNullOrBlank()) {
                error.add(required(R.string.profile_orgnip))
            }
        }

        if (error.isNotEmpty()) {
            throw ApiException(error.joinToString("\n"))
        }

        return this
    }

    private fun required(name: Int) = required(App.instance.getString(name))
    private fun required(name: String) =
        App.instance.resources.getString(R.string.profile_validation_required, name)

    companion object {
        private const val FIO_LENGHT = 3
    }
}
