package ru.perevozka24.perevozka24.ui.add.photo

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.FileProvider
import androidx.lifecycle.Observer
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.GroupieViewHolder
import kotlinx.android.synthetic.main.fragment_add_order_step_photo.*
import ru.perevozka24.perevozka24.BuildConfig
import ru.perevozka24.perevozka24.R
import ru.perevozka24.perevozka24.ui.add.BaseCreateOrderStepFragment
import ru.perevozka24.perevozka24.ui.add.photo.items.AddPhotoItem
import ru.perevozka24.perevozka24.ui.add.photo.items.GridSpacingItemDecoration
import ru.perevozka24.perevozka24.ui.viewModel
import java.io.File
import java.text.SimpleDateFormat
import java.util.Date
import kotlin.math.ceil

class AddPhotoStepFragment : BaseCreateOrderStepFragment() {
    override val title: Int = R.string.create_order_step_add_photo_title

    private val viewModel: AddPhotoViewModel by viewModel()

    private val adapter = GroupAdapter<GroupieViewHolder>()
    private var photoPath: File? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_add_order_step_photo, container, false)
    }

    @SuppressWarnings("FunctionNaming")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        addOrderPickedPhotoList.adapter = adapter
        addOrderPickedPhotoList.addItemDecoration(
            GridSpacingItemDecoration(
                2,
                resources.getDimensionPixelOffset(R.dimen.item_detailed_padding)
            )
        )

        val deleteFun = fun(p: String) = viewModel.removeFile(p)
        viewModel.items.observe(viewLifecycleOwner, Observer {
            val c = (ceil(it.size / 2.0) * 2).toInt()
            val arr: Array<String> = it.toTypedArray()
            val items = (0 until c).map { index ->
                val path = if (index < arr.size) arr[index] else null
                AddPhotoItem(path, deleteFun)
            }
            adapter.updateAsync(items)

            addOrderTaskNext.setText(
                if (items.isEmpty()) {
                    R.string.create_order_add_photo_proceed_no_photo
                } else {
                    R.string.create_order_add_photo_proceed
                }
            )
        })

        addOrderPickCameraPhoto.setOnClickListener {
            val path = createPath(requireContext())
            photoPath = path
            val takePictureIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
            val photoURI: Uri = FileProvider.getUriForFile(
                requireContext(),
                BuildConfig.APPLICATION_ID + ".fileprovider",
                path
            )
            if (takePictureIntent.resolveActivity(requireContext().packageManager) != null) {
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI)
                startActivityForResult(takePictureIntent, CAMERA)
            }
        }

        addOrderPickGallery.setOnClickListener {
            val intent = Intent()
            intent.type = "image/*"
            intent.action = Intent.ACTION_GET_CONTENT
            startActivityForResult(
                Intent.createChooser(
                    intent,
                    getString(R.string.create_order_add_photo_pick_gallery)
                ),
                GALLERY
            )
        }

        addOrderTaskNext.setOnClickListener {
            addOrderViewModel.photoSelected(viewModel.items.value?.toTypedArray())
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == CAMERA && resultCode == Activity.RESULT_OK) {
            photoPath?.absolutePath?.let { viewModel.addFile(it) }
        }

        if (requestCode == GALLERY && resultCode == Activity.RESULT_OK) {
            data?.data?.toString()?.let { viewModel.addFile(it) }
        }
    }

    private companion object {
        const val CAMERA = 1
        const val GALLERY = 2

        fun createPath(context: Context): File {
            val timeStamp: String = SimpleDateFormat("yyyyMMdd_HHmmss").format(Date())
            val storageDir: File =
                checkNotNull(context.getExternalFilesDir(Environment.DIRECTORY_PICTURES)).also {
                    it.mkdirs()
                }

            return File.createTempFile(
                "JPEG_${timeStamp}_", /* prefix */
                ".jpg", /* suffix */
                storageDir /* directory */
            )
        }
    }
}
