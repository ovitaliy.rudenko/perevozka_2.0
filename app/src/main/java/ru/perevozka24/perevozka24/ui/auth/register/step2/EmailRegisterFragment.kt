package ru.perevozka24.perevozka24.ui.auth.register.step2

import androidx.navigation.fragment.navArgs
import ru.perevozka24.perevozka24.ui.viewModel

class EmailRegisterFragment : BaseRegisterFragment<EmailRegisterViewModel>() {

    private val arg: EmailRegisterFragmentArgs by navArgs()
    override val contact: String
        get() = arg.contact
    override val viewModel: EmailRegisterViewModel by viewModel()
}
