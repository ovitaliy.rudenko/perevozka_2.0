package ru.perevozka24.perevozka24.ui.main.orders.detailed

import ru.perevozka24.perevozka24.ui.main.orders.models.OfferModel

interface OfferDetailedAction {
    fun addToFavorite(offer: OfferModel)
    fun pay(offer: OfferModel)
    fun call(offer: OfferModel)
}
