package ru.perevozka24.perevozka24.ui.main.orders.detailed

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.channels.ReceiveChannel
import kotlinx.coroutines.channels.ticker
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import ru.perevozka24.perevozka24.MIN_SEC_FORMAT
import ru.perevozka24.perevozka24.R
import ru.perevozka24.perevozka24.data.ResourceProvider
import ru.perevozka24.perevozka24.data.Result
import ru.perevozka24.perevozka24.data.api.SECOND
import ru.perevozka24.perevozka24.data.features.auth.AuthRepository
import ru.perevozka24.perevozka24.data.features.location.LocationRepository
import ru.perevozka24.perevozka24.data.features.offers.OfferCancelParams
import ru.perevozka24.perevozka24.data.features.offers.OfferRepository
import ru.perevozka24.perevozka24.data.features.offers.OffersRepository
import ru.perevozka24.perevozka24.ui.LiveEvent
import ru.perevozka24.perevozka24.ui.base.BaseViewModel
import ru.perevozka24.perevozka24.ui.common.livedata.postValue
import ru.perevozka24.perevozka24.ui.main.orders.models.OfferModel
import ru.perevozka24.perevozka24.ui.main.orders.models.OfferStatus
import ru.perevozka24.perevozka24.ui.main.orders.models.OfferStatus.Companion.REMAINING_TIME
import ru.perevozka24.perevozka24.utils.trackEvent
import java.util.Date

@SuppressWarnings("TooManyFunctions")
class OfferDetailedViewModel(
    private val authRepository: AuthRepository,
    private val offerRepository: OfferRepository,
    private val offersRepository: OffersRepository,
    private val locationRepository: LocationRepository,
    resourceProvider: ResourceProvider
) : BaseViewModel(resourceProvider) {
    val data: LiveData<ViewData> = MutableLiveData()
    val showButtonsContainer: LiveData<Boolean> = MutableLiveData()
    val showPayButton: LiveData<Boolean> = MutableLiveData()
    val showTakeButton: LiveData<Boolean> = MutableLiveData()
    val showAddEquipmentButton: LiveData<Boolean> = MutableLiveData()
    val showLoginButton: LiveData<Boolean> = MutableLiveData()
    val showConfirmButton: LiveData<Boolean> = MutableLiveData()
    val showDeclineButton: LiveData<Boolean> = MutableLiveData()
    val showRequireConfirmationMassage = LiveEvent<OfferModel>()
    val onAbused = LiveEvent<OfferModel>()
    val isAuthorized: LiveData<Boolean> = MutableLiveData(authRepository.isAuthorized())

    val confirmationTime: LiveData<String> = MutableLiveData()
    private var tickerTimerJob: ReceiveChannel<Unit>? = null
    private var confirmationTimerJob: Job? = null

    private var isMine: Boolean = false

    fun init(offer: OfferModel, isMine: Boolean) {
        data.postValue(ViewData(!isMine, offer, null, null, !isMine, authRepository.isAuthorized()))
        this.isMine = isMine

        initButtons(offer)
        initConfirmationTimer(offer)

        loadRoute(offer)
        load(offer.id)

        loadAbuses(offer.id)
    }

    private fun loadRoute(offer: OfferModel) {
        val address = offer.address ?: return
        val addressTo = offer.addressTo ?: return

        GlobalScope.launch {
            val result = safeExecute {
                locationRepository.getRoute(address, addressTo)
            }
            if (result is Result.Success) {
                if (result.data.isNotEmpty()) {
                    withContext(Dispatchers.Main) {
                        val route = result.data.first()
                        val routeData = RouteData(
                            distance = route.metadata.weight.distance.text,
                            start = route.geometry.points.first(),
                            end = route.geometry.points.last(),
                            polyline = route.geometry
                        )
                        val value = checkNotNull(data.value).copy(routeData = routeData)
                        data.postValue(value)
                    }
                } else {
                    error.postValue(resourceProvider.getString(R.string.route_not_found))
                }
            } else if (result is Result.Error) {
                error.postValue(result.message)
            }
        }
    }

    private fun initButtons(offer: OfferModel) {
        if (isMine || offer.code == OfferStatus.NONE) {
            showButtonsContainer.postValue(false)
        } else {
            val loggedIn = authRepository.isAuthorized()
            showButtonsContainer.postValue(true)
            showPayButton.postValue(loggedIn && offer.price > 0)
            showTakeButton.postValue(loggedIn && offer.code == OfferStatus.CAN_TAKE)
            showAddEquipmentButton.postValue(loggedIn && offer.code == OfferStatus.ADD_EQUIPMENT)
            showConfirmButton.postValue(loggedIn && offer.code == OfferStatus.CONFIRM)
            showDeclineButton.postValue(loggedIn && offer.code == OfferStatus.CONFIRM)
            showLoginButton.postValue(!loggedIn)
        }
    }

    fun addFavorite(offer: OfferModel) {
        GlobalScope.launch {
            val favorite = offer.favorite
            val newData = data.value?.copy(
                offer = offer.copy(favorite = !offer.favorite)
            )
            newData?.let {
                data.postValue(it)
            }
            if (favorite) {
                offersRepository.addFavorite(offer.id)
            } else {
                offersRepository.removeFavorite(offer.id)
            }
        }
    }

    fun takeOffer() {
        val takeOffer = checkNotNull(data.value?.offer?.id)
        GlobalScope.launch {
            val result = safeExecute { offerRepository.getContacts(takeOffer) }
            if (result is Result.Success) {
                refreshOffer(result)
                showRequireConfirmationMassage.postValue(result.data)
            } else if (result is Result.Error) {
                error.postValue(result.message)
            }
        }
    }

    private fun load(offerId: String) {
        GlobalScope.launch {
            val result = safeExecute { offerRepository.getOffer(offerId) }
            if (result is Result.Success) {
                refreshOffer(result)
            }
        }
    }

    private fun loadAbuses(offerId: String) {
        GlobalScope.launch {
            safeExecute { offerRepository.loadAbuses(offerId) }
                .doOnSuccess {
                    if (it.isNotEmpty()) {
                        data.value?.let { value ->
                            data.postValue(value.copy(abuse = it))
                        }
                    }
                }
        }
    }

    private fun refreshOffer(result: Result.Success<OfferModel>) {
        data.value?.let { value ->
            data.postValue(value.copy(offer = result.data))

            initButtons(result.data)
            initConfirmationTimer(result.data)
        }
    }

    fun reload() {
        data.value?.offer?.id?.let { load(it) }
    }

    override fun onCleared() {
        removeConfirmationTimer()

        super.onCleared()
    }

    private fun initConfirmationTimer(offer: OfferModel) {
        val timerIsRunning = offer.message?.contains(REMAINING_TIME) ?: false
        removeConfirmationTimer()
        val delay = offer.remainingTime * SECOND - System.currentTimeMillis()

        updateConfirmationTimerChanged(delay)

        if (timerIsRunning && delay > 0) {
            tickerTimerJob = ticker(SECOND, SECOND, Dispatchers.IO)
            viewModelScope.launch {
                val channel = checkNotNull(tickerTimerJob)
                for (event in channel) {
                    updateConfirmationTimerChanged(offer.remainingTime * SECOND - System.currentTimeMillis())
                }
            }

            confirmationTimerJob = viewModelScope.launch {
                delay(delay)
                reload()
                confirmationTime.postValue("")
                removeConfirmationTimer()
                trackEvent("order_ timer_is_over")
            }
        }
    }

    private fun updateConfirmationTimerChanged(delay: Long) {
        if (delay > 0) {
            val formatted = MIN_SEC_FORMAT.format(Date(delay))
            confirmationTime.postValue(formatted.format(delay))
        }
    }

    private fun removeConfirmationTimer() {
        tickerTimerJob?.cancel()
        tickerTimerJob = null
        confirmationTimerJob?.cancel()
        confirmationTimerJob = null
    }

    fun decline() {
        val offer = data.value?.offer ?: return
        val id = offer.id

        GlobalScope.launch {
            safeExecute {
                val params = OfferCancelParams(id, "default abuse")
                offerRepository.cancelOffer(params)
            }
                .traceError()
                .doOnSuccess {
                    onAbused.postValue(offer)
                }
        }
    }
}
