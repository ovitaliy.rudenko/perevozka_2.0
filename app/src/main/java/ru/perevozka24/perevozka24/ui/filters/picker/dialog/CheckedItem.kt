package ru.perevozka24.perevozka24.ui.filters.picker.dialog

import com.xwray.groupie.kotlinandroidextensions.GroupieViewHolder
import com.xwray.groupie.kotlinandroidextensions.Item
import kotlinx.android.synthetic.main.item_filter_checked.view.*
import ru.perevozka24.perevozka24.R
import ru.perevozka24.perevozka24.ui.base.INameModel
import ru.perevozka24.perevozka24.ui.common.widgets.onCheckChanged

data class CheckedItem<T : INameModel>(
    val model: T,
    val checked: Boolean,
    val onCheck: (T, Boolean) -> Unit
) : Item() {
    override fun getLayout(): Int = R.layout.item_filter_checked
    override fun getId(): Long = model.hashCode().toLong()

    override fun bind(viewHolder: GroupieViewHolder, position: Int) {
        with(viewHolder.itemView) {
            tag = model
            checkbox.setTitle(model.name)
            checkbox.checked = checked
            checkbox.onCheckChanged {
                onCheck(model, it)
            }
        }
    }
}
