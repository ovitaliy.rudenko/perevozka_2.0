package ru.perevozka24.perevozka24.ui.main.orders.detailed

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import ru.perevozka24.perevozka24.R
import ru.perevozka24.perevozka24.data.ResourceProvider
import ru.perevozka24.perevozka24.data.features.offers.OfferRepository
import ru.perevozka24.perevozka24.ui.base.BaseContentLoaderViewModel
import ru.perevozka24.perevozka24.ui.common.livedata.postValue
import ru.perevozka24.perevozka24.ui.main.orders.models.OfferModel

class OfferDetailedLoadViewModel(
    private val offerRepository: OfferRepository,
    resourceProvider: ResourceProvider
) : BaseContentLoaderViewModel<OfferModel>(resourceProvider) {

    val title: LiveData<String> = MutableLiveData()

    fun init(offer: OfferModel?, id: String?) {
        val r = resourceProvider.getString(
            R.string.offer_detailed_title,
            (offer?.id ?: id ?: error(noIdError))
        )
        title.postValue(
            r
        )

        execute {
            offer ?: id?.let { offerRepository.getOffer(it) } ?: error(noIdError)
        }
    }

    private val noIdError = "No offer or offer id passed"
}
