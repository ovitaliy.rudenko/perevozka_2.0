package ru.perevozka24.perevozka24.ui.distance.input

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.postDelayed
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.GroupieViewHolder
import com.yandex.mapkit.MapKitFactory
import kotlinx.android.synthetic.main.fragment_calculate_distance_input.*
import org.kodein.di.Kodein
import org.kodein.di.android.x.kodein
import ru.perevozka24.perevozka24.R
import ru.perevozka24.perevozka24.data.Result
import ru.perevozka24.perevozka24.ui.base.BaseFragment
import ru.perevozka24.perevozka24.ui.common.ext.setVisibleText
import ru.perevozka24.perevozka24.ui.common.ext.showSnackBar
import ru.perevozka24.perevozka24.ui.common.ext.toast
import ru.perevozka24.perevozka24.ui.common.ext.visible
import ru.perevozka24.perevozka24.ui.common.widgets.form.addTextChangedListener
import ru.perevozka24.perevozka24.ui.filters.OnPickerValueSelected
import ru.perevozka24.perevozka24.ui.filters.filter.FilterGroupBuilder
import ru.perevozka24.perevozka24.ui.filters.filter.FilterItem
import ru.perevozka24.perevozka24.ui.filters.models.PickerModel
import ru.perevozka24.perevozka24.ui.filters.picker.FilterPickerActivity
import ru.perevozka24.perevozka24.ui.viewModel
import ru.perevozka24.perevozka24.utils.YSafeInit
import ru.perevozka24.perevozka24.utils.trackEvent

class DistanceInputFragment : BaseFragment(), OnPickerValueSelected {
    override val kodein: Kodein by kodein()
    private val viewModel: DistanceInputViewModel by viewModel()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(R.layout.fragment_calculate_distance_input, container, false)

    private val adapter = GroupAdapter<GroupieViewHolder>()

    private val onClick = fun(v: Any?) {
        val item = v as? FilterItem ?: return
        val picker = item.item
        startActivityForResult(
            FilterPickerActivity.create(requireContext(), picker, 0),
            REQUEST_CODE
        )
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        trackEvent("visited_distance")
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        view.postDelayed(DELAY) {
            distanceCalculatorConsumption.text = viewModel.consumption?.toString()
            distanceCalculatorConsumption.addTextChangedListener {
                viewModel.consumptionChange(it.toString().toIntOrNull())
            }
        }
        distanceCalculatorPrice.text = viewModel.price?.toString()
        YSafeInit.map(requireContext())
        YSafeInit.directions(requireContext())

        distanceCalculatorShow.setOnClickListener { viewModel.calculate() }

        distanceCalculatorPrice.addTextChangedListener {
            viewModel.priceChange(it.toString().toIntOrNull())
        }

        distanceCalculatorList.adapter = adapter
        viewModel.selection.observe(viewLifecycleOwner, Observer {
            val filter = it ?: return@Observer
            adapter.updateAsync(FilterGroupBuilder.create(filter, onClick))
        })

        viewModel.calculation.observe(viewLifecycleOwner, Observer {
            val result = it ?: return@Observer

            when (result) {
                is Result.Loading -> distanceCalculatorProgressBar.visible = true
                is Result.Error -> {
                    distanceCalculatorProgressBar.visible = false
                    showSnackBar(result.message)
                }
                is Result.Success -> distanceCalculatorProgressBar.visible = false
            }
        })

        viewModel.showCalculation.observe(viewLifecycleOwner, Observer {
            it ?: return@Observer
            findNavController().navigate(
                DistanceInputFragmentDirections.actionDistanceInputFragmentToDistanceResultFragment(
                    result = it
                )
            )
        })

        viewModel.inputError.observe(viewLifecycleOwner, Observer {
            distanceCalculatorInputError.setVisibleText(it, View.INVISIBLE)
        })

        viewModel.error.observe(viewLifecycleOwner, Observer {
            toast(it)
        })
    }

    override fun onSelectPickerValue(value: PickerModel) {
        viewModel.pickerValueChanged(value)
    }

    override fun onResume() {
        super.onResume()
        MapKitFactory.getInstance().onStart()
    }

    override fun onPause() {
        super.onPause()
        MapKitFactory.getInstance().onStop()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            val value = data?.getParcelableExtra<PickerModel>(FilterPickerActivity.MODEL)
            viewModel.pickerValueChanged(checkNotNull(value))
        }
    }

    private companion object {
        const val DELAY = 100L
        const val REQUEST_CODE = 1000
    }
}
