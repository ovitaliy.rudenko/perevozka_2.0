package ru.perevozka24.perevozka24.ui.leftMenu.items

import com.xwray.groupie.kotlinandroidextensions.GroupieViewHolder
import com.xwray.groupie.kotlinandroidextensions.Item
import ru.perevozka24.perevozka24.R

class DividerMenuItem : Item() {
    override fun getId(): Long = 0
    override fun getLayout(): Int = R.layout.item_menu_divider

    override fun bind(viewHolder: GroupieViewHolder, position: Int) = Unit
}
