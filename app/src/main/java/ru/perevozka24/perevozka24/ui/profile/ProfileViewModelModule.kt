package ru.perevozka24.perevozka24.ui.profile

import org.kodein.di.Kodein
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.provider

val profileViewModelModule = Kodein.Module(name = "profileViewModelModule") {
    bind<ProfileViewModel>() with provider() {
        ProfileViewModel(
            instance(), instance()
        )
    }
}
