package ru.perevozka24.perevozka24.ui.support.items

import com.xwray.groupie.kotlinandroidextensions.GroupieViewHolder
import com.xwray.groupie.kotlinandroidextensions.Item
import kotlinx.android.synthetic.main.item_support_content.view.*
import ru.perevozka24.perevozka24.R
import ru.perevozka24.perevozka24.ui.common.ext.visible
import ru.perevozka24.perevozka24.ui.support.SupportItemAction

data class SupportContentItem(
    private val username: Int,
    private val phoneNumber: Int,
    private val email: Int,
    private val hasWhasapp: Boolean,
    private val callAction: SupportItemAction,
    private val emailAction: SupportItemAction
) : Item() {
    override fun getId() = 0L
    override fun getLayout() = R.layout.item_support_content
    override fun bind(viewHolder: GroupieViewHolder, position: Int) {
        with(viewHolder.itemView) {
            itemSupportContentUserName.text = resources.getString(username)
            itemSupportContentNumberValue.text = resources.getString(phoneNumber)
            itemSupportContentEmailValue.text = resources.getString(email)
            itemSupportContentWhatsapp.visible = hasWhasapp

            itemSupportContentNumberValue.setOnClickListener { callAction.action(phoneNumber) }
            itemSupportContentEmailValue.setOnClickListener { emailAction.action(email) }
        }
    }
}
