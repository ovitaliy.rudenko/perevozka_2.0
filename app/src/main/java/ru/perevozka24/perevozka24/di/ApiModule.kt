package ru.perevozka24.perevozka24.di

import com.google.gson.Gson
import org.kodein.di.Kodein
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.singleton
import org.kodein.di.weakReference
import retrofit2.Retrofit
import ru.perevozka24.perevozka24.API_URL
import ru.perevozka24.perevozka24.IP_API_URL
import ru.perevozka24.perevozka24.data.api.GsonFactory
import ru.perevozka24.perevozka24.data.api.RetrofitFactory
import ru.perevozka24.perevozka24.data.features.auth.AuthApi
import ru.perevozka24.perevozka24.data.features.filter.FilterApi
import ru.perevozka24.perevozka24.data.features.geolocation.LocationTrackerApi
import ru.perevozka24.perevozka24.data.features.items.ItemsApi
import ru.perevozka24.perevozka24.data.features.location.IpApi
import ru.perevozka24.perevozka24.data.features.notification.NotificationApi
import ru.perevozka24.perevozka24.data.features.offers.OffersApi
import ru.perevozka24.perevozka24.data.features.profile.ProfileApi
import ru.perevozka24.perevozka24.data.features.upload.UploadApi

val apiModule = Kodein.Module("api") {
    bind<Gson>() with singleton(ref = weakReference) { GsonFactory.create() }
    bind<Retrofit>() with singleton(ref = weakReference) {
        RetrofitFactory.create(
            instance(),
            API_URL
        )
    }
    bind<AuthApi>() with singleton { instance<Retrofit>().create(AuthApi::class.java) }
    bind<ItemsApi>() with singleton { instance<Retrofit>().create(ItemsApi::class.java) }
    bind<FilterApi>() with singleton { instance<Retrofit>().create(FilterApi::class.java) }
    bind<OffersApi>() with singleton { instance<Retrofit>().create(OffersApi::class.java) }
    bind<UploadApi>() with singleton { instance<Retrofit>().create(UploadApi::class.java) }
    bind<ProfileApi>() with singleton { instance<Retrofit>().create(ProfileApi::class.java) }
    bind<NotificationApi>() with singleton { instance<Retrofit>().create(NotificationApi::class.java) }
    bind<LocationTrackerApi>() with singleton { instance<Retrofit>().create(LocationTrackerApi::class.java) }

    //
    bind<Retrofit>("ipApi") with singleton(ref = weakReference) {
        RetrofitFactory.create(
            instance(),
            IP_API_URL
        )
    }
    bind<IpApi>() with singleton { instance<Retrofit>("ipApi").create(IpApi::class.java) }
}
