package ru.perevozka24.perevozka24.di

import org.kodein.di.Kodein
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.singleton
import org.kodein.di.weakReference
import ru.perevozka24.perevozka24.data.features.auth.AuthRepository
import ru.perevozka24.perevozka24.data.features.filter.FilterPickerRepository
import ru.perevozka24.perevozka24.data.features.filter.FilterRepository
import ru.perevozka24.perevozka24.data.features.geolocation.LocationTrackerRepository
import ru.perevozka24.perevozka24.data.features.items.ItemsRepository
import ru.perevozka24.perevozka24.data.features.location.GeoLocationRepository
import ru.perevozka24.perevozka24.data.features.location.LocationRepository
import ru.perevozka24.perevozka24.data.features.notification.NotificationRepository
import ru.perevozka24.perevozka24.data.features.offers.OfferRepository
import ru.perevozka24.perevozka24.data.features.offers.OffersRepository
import ru.perevozka24.perevozka24.data.features.profile.ProfileRepository
import ru.perevozka24.perevozka24.data.features.upload.UploadRepository

val repositoryModule = Kodein.Module("repository") {
    bind<AuthRepository>() with singleton(ref = weakReference) {
        AuthRepository(instance(), instance())
    }
    bind<FilterRepository>() with singleton(ref = weakReference) {
        FilterRepository(instance())
    }
    bind<ItemsRepository>() with singleton(ref = weakReference) {
        ItemsRepository(instance(), instance(), instance(), instance())
    }
    bind<FilterPickerRepository>() with singleton(ref = weakReference) {
        FilterPickerRepository(instance(), instance())
    }

    bind<OffersRepository>() with singleton(ref = weakReference) {
        OffersRepository(instance(), instance(), instance(), instance())
    }
    bind<OfferRepository>() with singleton(ref = weakReference) {
        OfferRepository(instance(), instance(), instance())
    }

    bind<LocationRepository>() with singleton(ref = weakReference) {
        LocationRepository(instance(), instance(), instance())
    }
    bind<GeoLocationRepository>() with singleton(ref = weakReference) {
        GeoLocationRepository(instance(), instance(), instance())
    }
    bind<UploadRepository>() with singleton(ref = weakReference) {
        UploadRepository(instance())
    }
    bind<ProfileRepository>() with singleton(ref = weakReference) {
        ProfileRepository(instance(), instance())
    }

    bind<NotificationRepository>() with singleton(ref = weakReference) {
        NotificationRepository(instance(), instance())
    }
    bind<LocationTrackerRepository>() with singleton(ref = weakReference) {
        LocationTrackerRepository(instance())
    }
}
