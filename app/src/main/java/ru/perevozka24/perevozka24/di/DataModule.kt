package ru.perevozka24.perevozka24.di

import android.content.Context
import android.content.SharedPreferences
import org.kodein.di.Kodein
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.singleton
import org.kodein.di.weakReference
import ru.perevozka24.perevozka24.data.ResourceProvider
import ru.perevozka24.perevozka24.data.cache.CacheProvider
import ru.perevozka24.perevozka24.data.cache.FileCacheStore
import ru.perevozka24.perevozka24.data.cache.GsonCacheMapper
import ru.perevozka24.perevozka24.data.features.auth.AuthDataSource
import ru.perevozka24.perevozka24.data.features.filter.FilterLocationPickerRepository
import ru.perevozka24.perevozka24.data.features.filter.FilterPickerDataSource
import ru.perevozka24.perevozka24.data.features.filter.FilterSelectionDataSource
import ru.perevozka24.perevozka24.data.features.geolocation.LocationTrackerDataSource
import ru.perevozka24.perevozka24.data.features.items.items
import ru.perevozka24.perevozka24.data.features.location.DistanceDataSource
import ru.perevozka24.perevozka24.data.features.location.FusedLocationDataSource
import ru.perevozka24.perevozka24.data.features.location.GeoLocationDataSource
import ru.perevozka24.perevozka24.data.features.location.IpApiLocationDataSource
import ru.perevozka24.perevozka24.data.features.location.LocationDataSource
import ru.perevozka24.perevozka24.data.features.notification.NotificationDataSource
import ru.perevozka24.perevozka24.data.features.notification.NotificationLocalDataSource
import ru.perevozka24.perevozka24.data.features.notification.NotificationRemoteDataSource
import ru.perevozka24.perevozka24.data.features.offers.offers
import ru.perevozka24.perevozka24.data.features.profile.ProfileDataSource
import ru.perevozka24.perevozka24.data.features.rate.RateDataSource
import ru.perevozka24.perevozka24.data.features.upload.UploadDataSource
import ru.perevozka24.perevozka24.data.features.user.UserDataSource
import ru.perevozka24.perevozka24.data.prefs.PrefsSourceFactory
import java.io.File

val dataModule = Kodein.Module("data") {
    bind<SharedPreferences>() with singleton(ref = weakReference) {
        PrefsSourceFactory.create(instance())
    }
    bind<CacheProvider>() with singleton(ref = weakReference) {
        val cacheDir: File = instance<Context>().cacheDir.apply { mkdirs() }
        CacheProvider(FileCacheStore(cacheDir), GsonCacheMapper)
    }
    bind<ResourceProvider>() with singleton(ref = weakReference) {
        ResourceProvider(instance())
    }
    bind<UserDataSource>() with singleton(ref = weakReference) {
        UserDataSource(instance())
    }
    bind<AuthDataSource>() with singleton(ref = weakReference) {
        AuthDataSource(instance())
    }
    bind<FilterPickerDataSource>() with singleton(ref = weakReference) {
        FilterPickerDataSource(instance(), instance())
    }
    bind<FilterSelectionDataSource>() with singleton(ref = weakReference) {
        FilterSelectionDataSource(
            instance(),
            instance(),
            instance(),
            instance(),
            instance()
        )
    }
    bind<LocationDataSource>() with singleton(ref = weakReference) { LocationDataSource(instance()) }
    bind<DistanceDataSource>() with singleton(ref = weakReference) { DistanceDataSource() }
    bind<RateDataSource>() with singleton(ref = weakReference) { RateDataSource(instance()) }
    bind<UploadDataSource>() with singleton(ref = weakReference) {
        UploadDataSource(
            instance(),
            instance()
        )
    }
    bind<FilterLocationPickerRepository>() with singleton(ref = weakReference) {
        FilterLocationPickerRepository(
            instance()
        )
    }
    bind<GeoLocationDataSource>() with singleton(ref = weakReference) {
        GeoLocationDataSource(instance(), instance())
    }
    bind<FusedLocationDataSource>() with singleton(ref = weakReference) {
        FusedLocationDataSource(instance())
    }
    bind<ProfileDataSource>() with singleton(ref = weakReference) {
        ProfileDataSource(instance(), instance())
    }
    bind<IpApiLocationDataSource>() with singleton(ref = weakReference) {
        IpApiLocationDataSource(instance(), instance())
    }
    bind<NotificationRemoteDataSource>() with singleton(ref = weakReference) {
        NotificationRemoteDataSource(instance(), instance())
    }
    bind<NotificationLocalDataSource>() with singleton(ref = weakReference) {
        NotificationLocalDataSource(instance(), instance())
    }
    bind<NotificationDataSource>() with singleton(ref = weakReference) {
        NotificationDataSource(instance(), instance())
    }
    bind<LocationTrackerDataSource>() with singleton(ref = weakReference) {
        LocationTrackerDataSource(instance(), instance(), instance())
    }

    import(items)
    import(offers)
}
