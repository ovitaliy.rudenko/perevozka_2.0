package ru.perevozka24.perevozka24.data.features.offers.entity

import com.google.gson.annotations.SerializedName

data class OffersEntity(
    @SerializedName("data")
    val data: List<OfferEntity>? = null,

    @SerializedName("code")
    val status: OfferStatusEntity? = null,

    @SerializedName("offers_views")
    val viewsCount: Int = 0
)
