package ru.perevozka24.perevozka24.data.features.location

import com.yandex.mapkit.geometry.Point
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import ru.perevozka24.perevozka24.data.features.filter.FilterPickerDataSource
import ru.perevozka24.perevozka24.data.features.location.LocationParams.Companion.ZOOM_ADDRESS
import ru.perevozka24.perevozka24.ui.filters.models.AddressDataModel
import ru.perevozka24.perevozka24.ui.filters.models.CityMapper
import ru.perevozka24.perevozka24.ui.filters.models.CountryMapper
import ru.perevozka24.perevozka24.ui.filters.models.RegionMapper

class GeoLocationRepository(
    private val geoLocationDataSource: GeoLocationDataSource,
    private val locationDataSource: LocationDataSource,
    private val pickerDataSource: FilterPickerDataSource
) {

    suspend fun getLocationPoint(): Point? = withContext(Dispatchers.IO) {
        geoLocationDataSource.getLocation()
    }

    suspend fun getLocationData(): AddressDataModel {
        return withContext(Dispatchers.IO) {
            val location = checkNotNull(getLocationPoint())
            val address = withContext(Dispatchers.Main) {
                locationDataSource.getAddress(location, ZOOM_ADDRESS)
            }.first()

            val country = pickerDataSource.getCountryFromAddress(address)
            val (region, city) = pickerDataSource.getCityByAddress(address) ?: Pair(null, null)

            AddressDataModel(
                address,
                country?.let { CountryMapper.map(it) },
                region?.let { RegionMapper.map(it) },
                city?.let { CityMapper.map(it) }
            )
        }
    }
}
