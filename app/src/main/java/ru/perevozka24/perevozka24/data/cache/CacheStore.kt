package ru.perevozka24.perevozka24.data.cache

interface CacheStore {
    fun put(key: String, value: String?)
    fun get(key: String): String?
}
