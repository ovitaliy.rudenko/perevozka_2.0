package ru.perevozka24.perevozka24.data.features.items.favorite

import ru.perevozka24.perevozka24.data.features.items.ItemsApi
import ru.perevozka24.perevozka24.data.features.items.entities.ItemEntity
import ru.perevozka24.perevozka24.data.features.user.UserDataSource

class FavoritesItemsRemoteDataSource constructor(
    private val userDataSource: UserDataSource,
    private val api: ItemsApi
) : FavoritesItemsDataSource {

    override suspend fun getFavorites(): List<ItemEntity> =
        api.getFavorite(userDataSource.authToken, 1)
            .results
            .map { it.copy(favorite = true) }

    override suspend fun addFavorite(itemId: String) {
        api.favoriteAdd(userDataSource.authToken, itemId)
    }

    override suspend fun removeFavorite(itemId: String) {
        api.favoriteRemove(userDataSource.authToken, itemId)
    }
}
