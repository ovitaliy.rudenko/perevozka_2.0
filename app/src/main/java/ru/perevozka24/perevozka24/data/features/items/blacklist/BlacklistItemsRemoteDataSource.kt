package ru.perevozka24.perevozka24.data.features.items.blacklist

import ru.perevozka24.perevozka24.data.base.checkIsOk
import ru.perevozka24.perevozka24.data.features.items.ItemsApi
import ru.perevozka24.perevozka24.data.features.user.UserDataSource

class BlacklistItemsRemoteDataSource(
    private val userDataSource: UserDataSource,
    private val api: ItemsApi
) : BlacklistItemsDataSource {
    override suspend fun addToBlackList(itemId: String) {
        api.blacklist(userDataSource.authToken, itemId).checkIsOk()
    }

    override fun getBlackList(): List<String>? = null
}
