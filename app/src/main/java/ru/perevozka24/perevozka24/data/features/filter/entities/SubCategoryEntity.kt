package ru.perevozka24.perevozka24.data.features.filter.entities

data class SubCategoryEntity(
    val id: String?,
    private val image: String?,
    val name: String?,
    val categoryId: String?,
    val shipment: Int?
) {
    val icon: String?
        get() = image?.let {
            if (it.startsWith("http")) {
                it
            } else {
                "https://perevozka24.ru$it"
            }
        }

    class List : ArrayList<SubCategoryEntity>()
}
