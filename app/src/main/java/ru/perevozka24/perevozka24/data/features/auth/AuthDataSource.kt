package ru.perevozka24.perevozka24.data.features.auth

import ru.perevozka24.perevozka24.data.base.checkIsOk
import ru.perevozka24.perevozka24.data.features.auth.responces.LoginResponse
import ru.perevozka24.perevozka24.data.features.auth.responces.RemindResponse

class AuthDataSource(private val api: AuthApi) {

    suspend fun login(username: String, password: String): LoginResponse =
        api.login(username, password)
            .checkIsOk()

    suspend fun register(
        firstname: String,
        email: String,
        password: String,
        passconf: String,
        agreement: Int
    ): LoginResponse =
        api.registration(firstname, email, password, passconf, agreement)
            .checkIsOk()

    suspend fun registrationPhone(
        firstname: String,
        phone: String,
        password: String,
        passconf: String,
        agreement: Int
    ): LoginResponse =
        api.registrationPhone(firstname, phone, password, passconf, agreement)
            .checkIsOk()

    suspend fun restore(
        email: String
    ): RemindResponse =
        api.remind(email)
            .results
            .checkIsOk()
}
