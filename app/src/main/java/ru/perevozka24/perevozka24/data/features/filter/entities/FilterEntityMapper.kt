package ru.perevozka24.perevozka24.data.features.filter.entities

import ru.perevozka24.perevozka24.data.features.offers.entity.OfferProgressStatusEntity
import ru.perevozka24.perevozka24.ui.common.Mapper
import ru.perevozka24.perevozka24.ui.filters.models.Direction
import ru.perevozka24.perevozka24.ui.filters.models.FilterCargoModel
import ru.perevozka24.perevozka24.ui.filters.models.FilterDistanceCalculatorModel
import ru.perevozka24.perevozka24.ui.filters.models.FilterItemsEquipmentModel
import ru.perevozka24.perevozka24.ui.filters.models.FilterModel
import ru.perevozka24.perevozka24.ui.filters.models.FilterOfferModel

object FilterEntityMapper : Mapper<FilterModel, FilterEntity> {
    override fun map(e: FilterModel): FilterEntity =
        when (e) {
            is FilterItemsEquipmentModel -> FilterItemsEquipmentMapper.map(e)
            is FilterCargoModel -> FilterCargoMapper.map(e)
            is FilterOfferModel -> FilterOfferMapper.map(e)
            is FilterDistanceCalculatorModel -> FilterDistanceCalculatorMapper.map(e)
            else -> error("unsupported type")
        }

    private object FilterItemsEquipmentMapper :
        Mapper<FilterItemsEquipmentModel, FilterItemsEquipmentEntity> {
        override fun map(e: FilterItemsEquipmentModel) =
            FilterItemsEquipmentEntity(
                category = e.category.let { CategoryMapper.map(it) },
                subCategory = e.subCategory.let { SubCategoryMapper.map(it) },
                country = e.country.let { CountryMapper.map(it) },
                region = e.region.let { RegionMapper.map(it) },
                city = e.city.let { CityMapper.map(it) }
            )
    }

    private object FilterCargoMapper : Mapper<FilterCargoModel, FilterCargoEntity> {
        override fun map(e: FilterCargoModel) =
            FilterCargoEntity(
                subCategory = e.subCategory.let { SubCategoryMapper.map(it) },
                country = e.country.copy(direction = Direction.FROM)
                    .run { CountryMapper.map(this) },
                region = e.region.copy(direction = Direction.FROM).run { RegionMapper.map(this) },
                countryTo = e.countryTo.copy(direction = Direction.TO)
                    .run { CountryMapper.map(this) },
                regionTo = e.regionTo.copy(direction = Direction.TO).run { RegionMapper.map(this) },
                cargoBody = e.cargoBody.let { CargoBodyEntity.Mapper.map(it) },
                status = e.status?.status?.let { OfferProgressStatusEntity.Mapper.map(it) }
            )
    }

    private object FilterOfferMapper : Mapper<FilterOfferModel, FilterOfferEntity> {
        override fun map(e: FilterOfferModel) =
            FilterOfferEntity(
                category = e.category.let { CategoryMapper.map(it) },
                subCategory = e.subCategory.let { SubCategoryMapper.map(it) },
                country = e.country.let { CountryMapper.map(it) },
                region = e.region.let { RegionMapper.map(it) },
                city = e.city.let { CityMapper.map(it) },
                status = e.status?.status?.let { OfferProgressStatusEntity.Mapper.map(it) }
            )
    }

    private object FilterDistanceCalculatorMapper :
        Mapper<FilterDistanceCalculatorModel, FilterDistanceCalculatorEntity> {
        override fun map(e: FilterDistanceCalculatorModel) =
            FilterDistanceCalculatorEntity(
                country = e.country.let { CountryMapper.map(it).copy(direction = Direction.FROM) },
                region = e.region.let { RegionMapper.map(it).copy(direction = Direction.FROM) },
                city = e.city.let { CityMapper.map(it).copy(direction = Direction.FROM) },
                countryTo = e.countryTo.let {
                    CountryMapper.map(it).copy(direction = Direction.TO)
                },
                regionTo = e.regionTo.let { RegionMapper.map(it).copy(direction = Direction.TO) },
                cityTo = e.cityTo.let { CityMapper.map(it).copy(direction = Direction.TO) }
            )
    }
}
