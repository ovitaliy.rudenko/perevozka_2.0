package ru.perevozka24.perevozka24.data.features.auth

import ru.perevozka24.perevozka24.data.features.user.UserDataSource
import ru.perevozka24.perevozka24.ui.auth.models.LoginModel

class AuthRepository(
    private val authDataSource: AuthDataSource,
    private val userDataSource: UserDataSource
) {

    fun logout() {
        userDataSource.authToken = null
        userDataSource.userId = null
        userDataSource.userEmail = null
    }

    suspend fun login(params: LoginParams): LoginModel {
        val result = authDataSource.login(params.username, params.password)
        userDataSource.authToken = result.authToken
        userDataSource.userId = result.userId
        userDataSource.userEmail = params.username
        userDataSource.userName = result.name

        return LoginModel.Mapper.map(userDataSource.getUserEntity())
    }

    suspend fun register(params: RegistrationParams): LoginModel {
        val result = authDataSource.register(
            firstname = params.firstname,
            email = params.email,
            password = params.password,
            passconf = params.passconf,
            agreement = params.agreement
        )

        userDataSource.authToken = result.authToken
        userDataSource.userId = result.userId
        userDataSource.userEmail = params.email
        userDataSource.userName = params.firstname

        return LoginModel.Mapper.map(userDataSource.getUserEntity())
    }

    suspend fun registerPhone(params: RegistrationParams): LoginModel {
        val result = authDataSource.registrationPhone(
            firstname = params.firstname,
            phone = params.email,
            password = params.password,
            passconf = params.passconf,
            agreement = params.agreement
        )

        userDataSource.authToken = result.authToken
        userDataSource.userId = result.userId
        userDataSource.userEmail = params.email
        userDataSource.userName = params.firstname

        return LoginModel.Mapper.map(userDataSource.getUserEntity())
    }

    suspend fun getUser(): LoginModel? =
        if (userDataSource.authorized) {
            LoginModel.Mapper.map(userDataSource.getUserEntity())
        } else {
            null
        }

    fun isAuthorized(): Boolean = userDataSource.authorized

    suspend fun restore(params: RestoreParams): String {
        return checkNotNull(authDataSource.restore(params.email).errorMessage)
    }

    fun isShowOnbording(): Boolean = userDataSource.onBoardingWasShown == 0
    fun setShowOnboarding() {
        userDataSource.onBoardingWasShown = 1
    }
}
