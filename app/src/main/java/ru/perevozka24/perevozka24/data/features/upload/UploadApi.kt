package ru.perevozka24.perevozka24.data.features.upload

import okhttp3.MultipartBody
import retrofit2.http.Multipart
import retrofit2.http.POST
import retrofit2.http.Part
import ru.perevozka24.perevozka24.data.features.upload.entity.UploadResponse

interface UploadApi {

    @Multipart
    @POST("api4/image_upload/offer")
    suspend fun uploadOfferImage(@Part image: MultipartBody.Part): UploadResponse
}
