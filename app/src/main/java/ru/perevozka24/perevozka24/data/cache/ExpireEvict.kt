package ru.perevozka24.perevozka24.data.cache

import org.threeten.bp.Duration

class ExpireEvict private constructor(
    private val duration: Duration,
    private val createdAt: Long = 0
) : CacheEviction {

    override fun evicted(): Boolean {
        return System.currentTimeMillis() > createdAt + duration.toMillis()
    }

    override fun buildToSave(): CacheEviction {
        return ExpireEvict(
            duration = this.duration,
            createdAt = System.currentTimeMillis()
        )
    }

    override fun toString(): String {
        return "ExpireEvict(duration=$duration, createdAt=$createdAt)"
    }

    companion object {
        fun create(duration: Duration) = ExpireEvict(duration)
    }
}
