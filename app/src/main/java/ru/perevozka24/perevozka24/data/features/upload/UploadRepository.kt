package ru.perevozka24.perevozka24.data.features.upload

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class UploadRepository(
    private val uploadDataSource: UploadDataSource
) {
    suspend fun uploadOfferImage(file: String): String {
        val response = withContext(Dispatchers.IO) {
            uploadDataSource.uploadOfferImage(file)
        }
        return checkNotNull(response.image)
    }
}
