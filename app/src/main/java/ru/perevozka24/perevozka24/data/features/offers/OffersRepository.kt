package ru.perevozka24.perevozka24.data.features.offers

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import ru.perevozka24.perevozka24.data.features.filter.FilterPickerDataSource
import ru.perevozka24.perevozka24.data.features.filter.FilterSelectionDataSource
import ru.perevozka24.perevozka24.data.features.filter.entities.FilterTypeEntity
import ru.perevozka24.perevozka24.data.features.offers.entity.OffersEntity
import ru.perevozka24.perevozka24.data.features.offers.favorite.FavoritesOffersDataSource
import ru.perevozka24.perevozka24.ui.main.orders.models.OffersMapper
import ru.perevozka24.perevozka24.ui.main.orders.models.OffersModel

class OffersRepository(
    private val favoritesDataSource: FavoritesOffersDataSource,
    private val dataSource: OffersDataSource,
    private val filterSelectionDataSource: FilterSelectionDataSource,
    private val filterPickerDataSource: FilterPickerDataSource
) {
    suspend fun getFavorites(): OffersModel =
        OffersMapper.map(favoritesDataSource.getFavorites().setupCategory())

    suspend fun addFavorite(itemId: String) = favoritesDataSource.addFavorite(itemId)

    suspend fun removeFavorite(itemId: String) = favoritesDataSource.removeFavorite(itemId)

    suspend fun getOffers(offset: Int): OffersModel = load(FilterTypeEntity.OFFERS, offset)

    suspend fun getCargo(offset: Int): OffersModel = load(FilterTypeEntity.CARGO, offset)

    suspend fun load(filterType: FilterTypeEntity, offset: Int): OffersModel {
        val selection = filterSelectionDataSource.getFilterItems(filterType)
        val params = FilterEntityToParamsMapper.map(selection)
            .copy(offset = offset)
        return OffersMapper.map(dataSource.getOffers(params).setupCategory())
            .applyFavorites(favoritesDataSource)
    }

    suspend fun getMyOffers(): OffersModel =
        OffersMapper.map(dataSource.getMyOffers().setupCategory())
            .applyFavorites(favoritesDataSource)

    suspend fun hasMyOffers() = dataSource.hasMyOffers()

    suspend fun search(query: String) = withContext(Dispatchers.IO) {
        OffersMapper.map(dataSource.search(query).setupCategory())
            .applyFavorites(favoritesDataSource)
    }

    fun OffersEntity.setupCategory(): OffersEntity {
        val items = data?.map {
            it.copy(
                parentCategory = filterPickerDataSource.getCategory(
                    it.typeOf,
                    it.sale
                )
            )
        }
        return copy(
            data = items ?: emptyList()
        )
    }
}
