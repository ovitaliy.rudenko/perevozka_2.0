package ru.perevozka24.perevozka24.data.features.filter

import org.threeten.bp.Duration
import ru.perevozka24.perevozka24.R
import ru.perevozka24.perevozka24.data.cache.CacheProvider
import ru.perevozka24.perevozka24.data.cache.ExpireEvict
import ru.perevozka24.perevozka24.data.cache.SimpleCacheKey
import ru.perevozka24.perevozka24.data.features.filter.entities.CargoBodyEntity
import ru.perevozka24.perevozka24.data.features.filter.entities.CategoryEntity
import ru.perevozka24.perevozka24.data.features.filter.entities.CityEntity
import ru.perevozka24.perevozka24.data.features.filter.entities.CountryEntity
import ru.perevozka24.perevozka24.data.features.filter.entities.RegionEntity
import ru.perevozka24.perevozka24.data.features.filter.entities.RegionsEntity
import ru.perevozka24.perevozka24.data.features.filter.entities.SubCategoryEntity
import ru.perevozka24.perevozka24.data.features.location.entity.SimpleAddress
import ru.perevozka24.perevozka24.data.features.offers.entity.OfferProgressStatusEntity

@SuppressWarnings("TooManyFunctions")
class FilterPickerDataSource(
    private val api: FilterApi,
    private val cacheProvider: CacheProvider
) {
    fun getCategory(id: String, sale: Int) = categories.first { it.id == id && it.sale == sale }

    fun getCategories() = categories

    suspend fun getSubCategories(shipment: Int, categoryId: String): List<SubCategoryEntity> =
        cacheProvider.proceed(
            SimpleCacheKey(
                "subcategories_$shipment $categoryId",
                SubCategoryEntity.List::class.java
            ),
            ExpireEvict.create(Duration.ofDays(1))
        ) {
            api.getCategories(shipment, categoryId)
                .results
                .map { it.copy(categoryId = categoryId, shipment = shipment) }
        }

    suspend fun getCounties(): List<CountryEntity> = cacheProvider.proceed(
        SimpleCacheKey("countries", CountryEntity.List::class.java),
        ExpireEvict.create(Duration.ofDays(1))
    ) { api.getCountries().results ?: emptyList() }

    suspend fun getCountyByCode(code: String): CountryEntity =
        getCounties().first { it.code == code }

    suspend fun getCountryFromAddress(address: SimpleAddress): CountryEntity? =
        getCounties().firstOrNull { it.name == address.country }

    suspend fun getRegions(countryId: String): List<RegionsEntity> =
        cacheProvider.proceed(
            SimpleCacheKey("region_$countryId", RegionsEntity.RegionsEntityList::class.java),
            ExpireEvict.create(Duration.ofDays(1))
        ) {
            (api.getRegions(countryId).results ?: emptyList())
                .map { r ->
                    r.copy(
                        countryId = countryId,
                        cities = r.cities.map { c -> c.copy(regionId = r.id) }
                    )
                }
        }

    suspend fun getCities(countryId: String, regionId: String): List<CityEntity> =
        getRegions(countryId)
            .first { it.id == regionId }
            .cities.map { it.copy(countryId = countryId, regionId = regionId) }

    suspend fun getCargoBodyType(): List<CargoBodyEntity> = cacheProvider.proceed(
        SimpleCacheKey("cargo_body_type", CargoBodyEntity.List::class.java),
        ExpireEvict.create(Duration.ofDays(1))
    ) {
        api.getCargoBody().results.map { e ->
            CargoBodyEntity(e.key, e.value)
        }
    }

    fun getStatuses() = OfferProgressStatusEntity.values()

    suspend fun getRegionCity(code: String, regionId: String) = getRegions(code)
        .first { it.id == regionId }

    suspend fun getRegion(code: String, regionId: String) = getRegions(code)
        .first { it.id == regionId }
        .run { RegionEntity(id, name, countryId, direction) }

    suspend fun getCity(code: String, regionId: String, cityId: String) = getCities(code, regionId)
        .first { it.id == cityId }

    suspend fun getCityByAddress(address: SimpleAddress): Pair<RegionEntity?, CityEntity?>? {
        return if (address.city != null) {
            val countryId = getCountryFromAddress(address)?.code ?: return null
            val regions = getRegions(countryId)

            val region = regions.find {
                it.id == it.cities.firstOrNull { it.name == address.city }?.regionId
            }
            val city = region?.cities?.firstOrNull { it.name == address.city }
            region?.let {
                Pair(
                    RegionEntity(
                        id = it.id,
                        name = it.name,
                        countryId = countryId,
                        direction = it.direction
                    ),
                    city?.copy(
                        countryId = countryId,
                        regionId = it.id,
                        direction = it.direction
                    )
                )
            }
        } else {
            null
        }
    }

    companion object {
        const val CARGO_TYPE = "gruz"
        const val SPEC_TYPE = "spec"
        const val PASSENGER_TYPE = "pass"
        const val STRO_TYPE = "stro"
        const val DOST_TYPE = "dost"
        const val USLG_TYPE = "uslg"
        private val categories = listOf(
            CategoryEntity(CARGO_TYPE, "Грузоперевозки", icon = R.drawable.ic_cat_cargo),
            CategoryEntity(
                PASSENGER_TYPE,
                "Пассажирские перевозки",
                icon = R.drawable.ic_cat_trans
            ),
            CategoryEntity(SPEC_TYPE, "Аренда спецтехники", icon = R.drawable.ic_cat_rent),
            CategoryEntity(SPEC_TYPE, "Продажа спецтехники", 1, icon = R.drawable.ic_cat_sale),
            CategoryEntity(
                STRO_TYPE,
                "Аренда строительного оборудования",
                icon = R.drawable.ic_cat_equipment
            ),
            CategoryEntity(DOST_TYPE, "Строительные материалы", icon = R.drawable.ic_cat_materials),
            CategoryEntity(USLG_TYPE, "Сопутствующие услуги", icon = R.drawable.ic_cat_service)
        )
    }
}
