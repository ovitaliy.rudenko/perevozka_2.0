package ru.perevozka24.perevozka24.data.features.filter.entities

data class FilterItemsEquipmentEntity(
    val category: CategoryEntity? = null,
    val subCategory: SubCategoryEntity? = null,
    override val country: CountryEntity? = null,
    override val region: RegionEntity? = null,
    val city: CityEntity? = null
) : FilterEntity {
    val shipment: Int = 0

    override fun setDefaultLocation(
        country: CountryEntity?,
        region: RegionEntity?,
        city: CityEntity?
    ): FilterEntity {
        return this.copy(country = country, region = region, city = city)
    }
}
