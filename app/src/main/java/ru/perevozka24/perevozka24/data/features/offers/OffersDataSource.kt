package ru.perevozka24.perevozka24.data.features.offers

import ru.perevozka24.perevozka24.data.base.checkIsOk
import ru.perevozka24.perevozka24.data.features.offers.entity.OfferAbuseEntity
import ru.perevozka24.perevozka24.data.features.offers.entity.OfferContactEntity
import ru.perevozka24.perevozka24.data.features.offers.entity.OfferStatusEntity
import ru.perevozka24.perevozka24.data.features.offers.entity.OffersEntity
import ru.perevozka24.perevozka24.data.features.user.UserDataSource

@SuppressWarnings("TooManyFunctions")
class OffersDataSource(
    private val userDataSource: UserDataSource,
    private val api: OffersApi
) {

    suspend fun getOffers(params: OfferParams): OffersEntity {
        return api.getOffers(
            token = userDataSource.authToken,
            shipment = params.shipment,
            category = params.category,
            subCategory = params.subCategory,
            country = params.country,
            region = params.region,
            city = params.city,
            countryIdTo = params.countryTo,
            regionIdTo = params.regionTo,
            cargoBodyType = params.cargoBodyType,
            status = params.status,
            offset = params.offset
        )
    }

    suspend fun getOffer(id: String): OffersEntity {
        return api.getOffer(userDataSource.authToken, id)
    }

    suspend fun getContacts(id: String): OfferContactEntity {
        val token = checkNotNull(userDataSource.authToken)
        return api.showOffer(token, id).checkIsOk()
    }

    suspend fun unTakeOffer(): String? {
        return api.untakenOffer(userDataSource.authToken).results
    }

    suspend fun takeOffer(params: OfferTakeParams) =
        api.takeOffer(userDataSource.authToken, params.offerId, params.price)
            .checkIsOk()

    suspend fun cancelOffer(params: OfferCancelParams) =
        api.cancelOffer(userDataSource.authToken, params.offerId, params.text)
            .checkIsOk()

    suspend fun loadAbuses(id: String): List<OfferAbuseEntity> =
        api.loadAbuses(id).checkIsOk().abuses

    suspend fun getMyOffers(): OffersEntity {
        val token = userDataSource.authToken ?: return OffersEntity(
            emptyList(),
            OfferStatusEntity.NOAUTH,
            0
        )
        return api.getMyOffers(token, "1")
    }

    suspend fun search(query: String): OffersEntity {
        val token = userDataSource.authToken
        return api.search(token, query)
    }

    suspend fun createOffer(params: OfferCreateParams) {
        val token = userDataSource.authToken
        api.addOffer(
            token = token,
            categoryId = params.categoryId,
            country = params.country,
            regionId = params.regionId,
            cityId = params.cityId,
            address = params.address,
            countryTo = params.countryTo,
            regionIdTo = params.regionIdTo,
            cityIdTo = params.cityIdTo,
            addressTo = params.addressTo,
            name = params.name,
            phone = params.phone,
            offer = params.offer,
            paymentType = params.paymentType,
            paymentRate = params.paymentRate,
            toUserId = params.toUserId,
            defaultImage = params.defaultImage,
            images = params.images,
            type = params.periodType,
            date1 = params.periodDate1,
            date2 = params.periodDate2,
            os = "1"
        ).checkIsOk()
    }

    suspend fun hasMyOffers(): Boolean {
        val myOffer = getMyOffers()
        return myOffer.data?.isNotEmpty() == true
    }
}
