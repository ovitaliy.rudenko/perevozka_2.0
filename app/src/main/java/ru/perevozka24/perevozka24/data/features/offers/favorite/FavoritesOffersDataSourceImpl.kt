package ru.perevozka24.perevozka24.data.features.offers.favorite

import ru.perevozka24.perevozka24.data.features.user.UserDataSource

class FavoritesOffersDataSourceImpl constructor(
    private val userDataSource: UserDataSource,
    private val localDataSource: FavoritesOffersDataSource,
    private val remoteDataSource: FavoritesOffersDataSource
) : FavoritesOffersDataSource {

    override suspend fun getFavorites() =
        if (userDataSource.authorized) remoteDataSource.getFavorites()
        else localDataSource.getFavorites()

    override suspend fun addFavorite(itemId: String) =
        if (userDataSource.authorized) remoteDataSource.addFavorite(itemId)
        else localDataSource.addFavorite(itemId)

    override suspend fun removeFavorite(itemId: String) =
        if (userDataSource.authorized) remoteDataSource.removeFavorite(itemId)
        else localDataSource.removeFavorite(itemId)
}
