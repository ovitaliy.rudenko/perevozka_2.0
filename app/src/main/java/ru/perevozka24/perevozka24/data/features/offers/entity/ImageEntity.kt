package ru.perevozka24.perevozka24.data.features.offers.entity

import com.google.gson.annotations.SerializedName

data class ImageEntity(
    @SerializedName("filename") val filename: String?,
    @SerializedName("is_default") val cover: Boolean
)
