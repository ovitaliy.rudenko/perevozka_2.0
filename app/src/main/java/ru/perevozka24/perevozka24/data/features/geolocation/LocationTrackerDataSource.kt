package ru.perevozka24.perevozka24.data.features.geolocation

import org.threeten.bp.Duration
import ru.perevozka24.perevozka24.data.cache.CacheProvider
import ru.perevozka24.perevozka24.data.cache.ExpireEvict
import ru.perevozka24.perevozka24.data.cache.SimpleCacheKey
import ru.perevozka24.perevozka24.data.features.user.UserDataSource

class LocationTrackerDataSource(
    private val userDataSource: UserDataSource,
    private val cacheProvider: CacheProvider,
    private val api: LocationTrackerApi
) {
    fun getEnabled(): Int {
        return userDataSource.geolocationEnabled
    }

    fun setEnabled(value: Int) {
        userDataSource.geolocationEnabled = value
    }

    suspend fun getCurrentItemId(): String? {
        return userDataSource.geolocationItem
    }

    suspend fun getCurrentItem(): LocationTrackerEntity? {
        return userDataSource.geolocationItem?.let { id ->
            getItems().firstOrNull { it.id == id }
        }
    }

    fun setCurrentItem(id: String?) {
        userDataSource.geolocationItem = id
    }

    suspend fun getItems(): List<LocationTrackerEntity> {
        return userDataSource.authToken?.let {
            cacheProvider.proceed(
                SimpleCacheKey(
                    "geolocation_item",
                    LocationTrackerEntity.List::class.java
                ),
                ExpireEvict.create(Duration.ofSeconds(ITEMS_CASHING_DURATION_SECONDS))
            ) {
                api.getGeolocationItems(it).results
            }
        } ?: emptyList()
    }

    suspend fun postLocation(id: String, lat: Double, ln: Double) =
        api.setGeolocationItem(userDataSource.authToken, id, lat, ln)

    companion object {
        private const val ITEMS_CASHING_DURATION_SECONDS = 10L
    }
}
