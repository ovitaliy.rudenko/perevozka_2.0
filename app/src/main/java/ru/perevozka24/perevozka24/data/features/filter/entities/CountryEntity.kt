package ru.perevozka24.perevozka24.data.features.filter.entities

import ru.perevozka24.perevozka24.ui.filters.models.Direction

data class CountryEntity(
    val code: String?,
    val name: String?,
    val direction: Direction?
) {
    class List : ArrayList<CountryEntity>()
}
