package ru.perevozka24.perevozka24.data.features.upload

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import androidx.core.net.toUri
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody
import ru.perevozka24.perevozka24.data.features.upload.entity.UploadResponse
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileOutputStream
import java.io.InputStream
import java.text.SimpleDateFormat
import java.util.Date
import java.util.Locale

class UploadDataSource(private val context: Context, private val upload: UploadApi) {

    suspend fun uploadOfferImage(image: String): UploadResponse {
        val inputStream: InputStream =
            checkNotNull(context.contentResolver.openInputStream(image.makeUri()))
        val bitmap = BitmapFactory.decodeStream(inputStream)

        val fileName = SimpleDateFormat("yyyyMMdd_HHmmss", Locale.ROOT).format(Date()) + ".jpg"

        val file = bitmapToFile(context, bitmap, fileName)

        val part = MultipartBody.Part.createFormData(
            "image", fileName,
            RequestBody.create(
                "image/jpeg".toMediaTypeOrNull(),
                file
            )
        )

        return upload.uploadOfferImage(part)
    }

    private fun String.makeUri(): Uri {
        val uri = toUri()
        return if (uri.host == null) "file://$this".toUri() else uri
    }

    private fun bitmapToFile(
        context: Context,
        bitmap: Bitmap,
        fileNameToSave: String
    ): File { // File name like "image.png"
        val file = File(context.externalCacheDir, fileNameToSave).apply { createNewFile() }

        // Convert bitmap to byte array
        val bos = ByteArrayOutputStream()
        bitmap.compress(
            Bitmap.CompressFormat.JPEG,
            JPEG_QUALITY,
            bos
        ) // YOU can also save it in JPEG
        val bitmapdata = bos.toByteArray()

        // write the bytes in file
        val fos = FileOutputStream(file)
        fos.write(bitmapdata)
        fos.flush()
        fos.close()
        return file
    }

    companion object {
        const val JPEG_QUALITY = 100
    }
}
