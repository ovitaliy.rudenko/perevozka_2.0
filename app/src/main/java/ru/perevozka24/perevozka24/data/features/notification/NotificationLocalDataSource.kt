package ru.perevozka24.perevozka24.data.features.notification

import android.content.SharedPreferences
import androidx.core.content.edit
import com.google.gson.Gson
import ru.perevozka24.perevozka24.data.features.notification.entity.NotificationEntity

class NotificationLocalDataSource(
    private val gson: Gson,
    private val sharedPreferences: SharedPreferences
) {
    fun getNotification(): NotificationEntity? {
        return sharedPreferences.getString(KEY, null)?.let {
            gson.fromJson(it, NotificationEntity::class.java)
        }
    }

    fun setNotification(params: NotificationEntity) {
        val json = gson.toJson(params)
        sharedPreferences.edit {
            putString(KEY, json)
        }
    }

    private companion object {
        const val KEY = "notification_settings"
    }
}
