package ru.perevozka24.perevozka24.data.features.items.favorite

import org.kodein.di.Kodein
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.singleton
import org.kodein.di.weakReference

val favorites = Kodein.Module("favorites") {

    bind<FavoritesItemsDataSource>("local") with singleton(ref = weakReference) {
        FavoritesItemsLocalDataSource(
            instance(),
            instance()
        )
    }
    bind<FavoritesItemsDataSource>("remote") with singleton(ref = weakReference) {
        FavoritesItemsRemoteDataSource(
            instance(),
            instance()
        )
    }
    bind<FavoritesItemsDataSource>() with singleton(ref = weakReference) {
        FavoritesItemsDataSourceImpl(
            instance(),
            instance("local"),
            instance("remote")
        )
    }
}
