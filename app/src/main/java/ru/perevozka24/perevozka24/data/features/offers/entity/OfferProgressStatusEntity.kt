package ru.perevozka24.perevozka24.data.features.offers.entity

import com.google.gson.annotations.SerializedName
import ru.perevozka24.perevozka24.ui.main.orders.models.OfferProgressStatus
import java.util.Locale

enum class OfferProgressStatusEntity {
    @SerializedName("opened")
    OPENED,

    @SerializedName("in_work")
    IN_WORK,

    @SerializedName("closed")
    CLOSED, ;

    override fun toString(): String {
        return super.toString().toLowerCase(Locale.ROOT)
    }

    object Mapper :
        ru.perevozka24.perevozka24.ui.common.Mapper<OfferProgressStatus, OfferProgressStatusEntity> {
        override fun map(e: OfferProgressStatus): OfferProgressStatusEntity {
            return values()[e.ordinal]
        }
    }
}
