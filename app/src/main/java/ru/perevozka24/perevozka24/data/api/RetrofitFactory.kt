package ru.perevozka24.perevozka24.data.api

import com.facebook.stetho.okhttp3.StethoInterceptor
import com.google.gson.Gson
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import ru.perevozka24.perevozka24.BuildConfig
import timber.log.Timber

object RetrofitFactory {
    private const val MAX_LOG_LENGTH = 4000
    private fun createLogging(): HttpLoggingInterceptor =
        HttpLoggingInterceptor(object : HttpLoggingInterceptor.Logger {
            override fun log(message: String) {
                val m = if (message.length > MAX_LOG_LENGTH) {
                    message.substring(0, MAX_LOG_LENGTH)
                } else {
                    message
                }
                Timber.tag("OkHttp").d(m)
            }
        }).apply {
            setLevel(HttpLoggingInterceptor.Level.BODY)
        }

    private fun createClient() = OkHttpClient().newBuilder().apply {
        addInterceptor(createLogging())
        if (BuildConfig.DEBUG) {
            addNetworkInterceptor(StethoInterceptor())
        }
    }

        .build()

    fun create(gson: Gson, apiUrl: String): Retrofit = Retrofit.Builder()
        .client(createClient())
        .baseUrl(apiUrl)
        .addConverterFactory(GsonConverterFactory.create(gson))
        .build()
}
