package ru.perevozka24.perevozka24.data.features.filter.entities

import java.lang.reflect.Type

enum class FilterTypeEntity {
    EQUIPMENT_ITEMS {
        override val key: String
            get() = "EQUIPMENT_ITEMS"
        override val type: Type
            get() = FilterItemsEquipmentEntity::class.java

        override fun createNew(): FilterEntity = FilterItemsEquipmentEntity()

        override val needDefaultCity: Boolean
            get() = true
    },
    CARGO {
        override val key: String
            get() = "CARGO"
        override val type: Type
            get() = FilterCargoEntity::class.java

        override fun createNew(): FilterEntity = FilterCargoEntity()
    },
    OFFERS {
        override val key: String
            get() = "OFFERS"
        override val type: Type
            get() = FilterOfferEntity::class.java

        override fun createNew(): FilterEntity = FilterOfferEntity()
    },
    DISTANCE {
        override val key: String
            get() = "DISTANCE"
        override val type: Type
            get() = FilterDistanceCalculatorEntity::class.java
        override val needDefaultLocation: Boolean
            get() = false

        override fun createNew(): FilterEntity = FilterDistanceCalculatorEntity()
    };

    abstract val key: String
    abstract val type: Type
    abstract fun createNew(): FilterEntity

    open val needDefaultLocation = true
    open val needDefaultCity = false
}
