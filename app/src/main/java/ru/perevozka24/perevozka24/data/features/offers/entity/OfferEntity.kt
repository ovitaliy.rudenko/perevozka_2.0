package ru.perevozka24.perevozka24.data.features.offers.entity

import com.google.gson.annotations.SerializedName
import org.threeten.bp.LocalDateTime
import ru.perevozka24.perevozka24.ICON_PATH
import ru.perevozka24.perevozka24.data.features.filter.entities.CategoryEntity
import java.io.Serializable

data class OfferEntity(
    val id: String,
    val name: String,
    val shipment: Long? = null,
    @SerializedName("city_id") val cityId: Long? = null,
    @SerializedName("cat_id") val categoryId: Long? = null,
    val offer: String? = null,
    @SerializedName("city_name") val cityName: String? = null,
    @SerializedName("date_added") val dateAdded: LocalDateTime? = null,
    @SerializedName("code") val offerStatus: OfferStatusEntity,
    val contacts: String? = null,
    val country: String? = null,
    val message: String? = null,
    @SerializedName("abuses_cnt") val abuseCount: Int = 0,
    @SerializedName("category_image") private val image: String?,
    @SerializedName("remaining_time") val remainingTime: Long = 0,
    @SerializedName("performer_price") val price: Long = 0,
    @SerializedName("pay_rate") val payRate: Double?,
    @SerializedName("pay_type") val payType: String?,
    @SerializedName("city_to") val cityTo: String? = null,
    val status: OfferProgressStatusEntity?,
    val isFavorite: Boolean,
    val gallery: List<ImageEntity>?,
    val address: String?,
    @SerializedName("address_to") val addressTo: String?,
    @SerializedName("company_name") val companyName: String?,
    @SerializedName("company_url") val companyUrl: String?,
    @SerializedName("show_contacts") val showContacts: Boolean?,
    @SerializedName("load_date_type") val performDateType: Int,
    @SerializedName("load_date_1") val performDateDate1: LocalDateTime?,
    @SerializedName("load_date_2") val performDateDate2: LocalDateTime?,
    @SerializedName("typeof") val typeOf: String,
    val parentCategory: CategoryEntity?,
    @SerializedName("sale") val sale: Int
) : Serializable {
    val categoryImage: String?
        get() = image?.let { "$ICON_PATH$it" }
}
