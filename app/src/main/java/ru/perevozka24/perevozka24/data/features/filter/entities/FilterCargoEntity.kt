package ru.perevozka24.perevozka24.data.features.filter.entities

import ru.perevozka24.perevozka24.data.features.offers.entity.OfferProgressStatusEntity

data class FilterCargoEntity(
    val subCategory: SubCategoryEntity? = null,
    override val country: CountryEntity? = null,
    override val region: RegionEntity? = null,
    val countryTo: CountryEntity? = null,
    val regionTo: RegionEntity? = null,
    val cargoBody: CargoBodyEntity? = null,
    val status: OfferProgressStatusEntity? = null
) : FilterEntity {
    val shipment: Int = 1

    override fun setDefaultLocation(
        country: CountryEntity?,
        region: RegionEntity?,
        city: CityEntity?
    ): FilterEntity {
        return this.copy(country = country, region = region)
    }
}
