package ru.perevozka24.perevozka24.data.features.offers

import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Query
import ru.perevozka24.perevozka24.data.base.Response
import ru.perevozka24.perevozka24.data.base.SimpleResponse
import ru.perevozka24.perevozka24.data.features.offers.entity.OfferAbuseResponse
import ru.perevozka24.perevozka24.data.features.offers.entity.OfferContactEntity
import ru.perevozka24.perevozka24.data.features.offers.entity.OffersEntity

@SuppressWarnings("LongParameterList", "TooManyFunctions")
interface OffersApi {

    @FormUrlEncoded
    @POST("api4/favorite_offer")
    suspend fun favoriteOfferAdd(
        @Field("token") authToken: String?,
        @Field("offer_id") favoriteId: String?
    ): SimpleResponse

    @FormUrlEncoded
    @POST("api4/favorite_offer_remove")
    suspend fun favoriteOfferRemove(
        @Field("token") authToken: String?,
        @Field("offer_id") favoriteId: String?
    ): SimpleResponse

    @GET("api5/offers")
    suspend fun favoriteOfferGet(
        @Query("token") authToken: String?,
        @Query("fav") favorite: String?
    ): OffersEntity

    @GET("api5/offers")
    suspend fun getOffers(
        @Query("token") token: String?,
        @Query("shipment") shipment: Int?,
        @Query("typeof") category: String?,
        @Query("ts_type") subCategory: String?,
        @Query("country") country: String?,
        @Query("region_id") region: String?,
        @Query("city_id") city: String?,
        @Query("country_to") countryIdTo: String?,
        @Query("region_to") regionIdTo: String?,
        @Query("gruz_kuzov_type") cargoBodyType: String?,
        @Query("status") status: String?,
        @Query("offset") offset: Int
    ): OffersEntity

    @GET("api5/offers")
    suspend fun getOffer(
        @Query("token") token: String?,
        @Query("offer_id") offerId: String?
    ): OffersEntity

    @GET("api5/offers")
    suspend fun getMyOffers(
        @Query("token") token: String?,
        @Query("user_offers") userOffers: String?
    ): OffersEntity

    @GET("api5/offers/search")
    suspend fun search(
        @Query("token") token: String?,
        @Query("q") query: String?
    ): OffersEntity

    @FormUrlEncoded
    @POST("api3/show_offer")
    suspend fun showOffer(
        @Field("token") token: String?,
        @Field("offer_id") offerId: String?
    ): OfferContactEntity

    @FormUrlEncoded
    @POST("api3/untake_offer")
    suspend fun untakenOffer(@Field("token") token: String?): Response<String>

    @FormUrlEncoded
    @POST("api3/take_offer")
    suspend fun takeOffer(
        @Field("token") token: String?,
        @Field("offer_id") offerId: String?,
        @Field("price") price: String?
    ): SimpleResponse

    @FormUrlEncoded
    @POST("api3/cancel_offer")
    suspend fun cancelOffer(
        @Field("token") token: String?,
        @Field("offer_id") offerId: String?,
        @Field("text") text: String?
    ): SimpleResponse

    @FormUrlEncoded
    @POST("api3/list_offer_abuses")
    suspend fun loadAbuses(
        @Field("offer_id") itemId: String?
    ): OfferAbuseResponse

    @FormUrlEncoded
    @POST("api3/offer_create")
    suspend fun addOffer(
        @Field("token") token: String?,
        @Field("cat_id") categoryId: String?,
        @Field("country_id") country: String?,
        @Field("region_id") regionId: String?,
        @Field("city_id") cityId: String?,
        @Field("phone") phone: String?,
        @Field("offer") offer: String?,
        @Field("name") name: String?,
        @Field("address") address: String?,
        @Field("country_to") countryTo: String?,
        @Field("region_id_to") regionIdTo: String?,
        @Field("city_id_to") cityIdTo: String?,
        @Field("address_to") addressTo: String?,
        @Field("to_user") toUserId: String?,
        @Field("pay_rate") paymentRate: String?,
        @Field("pay_type") paymentType: String?,
        @Field("image_default") defaultImage: String?,
        @Field("image[]") images: List<String>,
        @Field("load_date_type") type: String?,
        @Field("load_date_1") date1: String?,
        @Field("load_date_2") date2: String?,
        @Field("os") os: String
    ): SimpleResponse
}
