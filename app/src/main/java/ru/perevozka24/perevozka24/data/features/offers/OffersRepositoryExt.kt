package ru.perevozka24.perevozka24.data.features.offers

import ru.perevozka24.perevozka24.data.features.offers.favorite.FavoritesOffersDataSource
import ru.perevozka24.perevozka24.ui.main.orders.models.OfferModel
import ru.perevozka24.perevozka24.ui.main.orders.models.OffersModel

suspend fun OffersModel.applyFavorites(favoritesDataSource: FavoritesOffersDataSource): OffersModel {
    val favorites = favoritesDataSource.getFavorites().data?.map { it.id } ?: emptyList()
    return copy(data = data.map { it.copy(favorite = favorites.contains(it.id)) })
}

suspend fun OfferModel.applyFavorites(favoritesDataSource: FavoritesOffersDataSource): OfferModel {
    val favorites = favoritesDataSource.getFavorites().data?.map { it.id } ?: emptyList()
    return copy(favorite = favorites.contains(id))
}

fun OffersModel.updateFavoriteValue(id: String, favorite: Boolean): OffersModel {
    return copy(data = data.map {
        if (it.id == id) it.copy(favorite = favorite)
        else it
    })
}
