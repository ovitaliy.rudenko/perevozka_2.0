package ru.perevozka24.perevozka24.data.features.geolocation

import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST
import ru.perevozka24.perevozka24.data.base.Response
import ru.perevozka24.perevozka24.data.base.SimpleResponse

interface LocationTrackerApi {
    @FormUrlEncoded
    @POST("api3/get_user_items")
    suspend fun getGeolocationItems(@Field("token") token: String?): Response<List<LocationTrackerEntity>>

    @FormUrlEncoded
    @POST("api3/set_item_location")
    suspend fun setGeolocationItem(
        @Field("token") token: String?,
        @Field("item_id") itemId: String?,
        @Field("loc_x") x: Double,
        @Field("loc_y") y: Double
    ): SimpleResponse
}
