package ru.perevozka24.perevozka24.data.features.auth

import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST
import ru.perevozka24.perevozka24.data.base.Response
import ru.perevozka24.perevozka24.data.features.auth.responces.LoginResponse
import ru.perevozka24.perevozka24.data.features.auth.responces.RemindResponse

interface AuthApi {
    @FormUrlEncoded
    @POST("api3/login")
    suspend fun login(
        @Field("auth_login") login: String,
        @Field("auth_password") password: String
    ): LoginResponse

    @FormUrlEncoded
    @POST("api4/registration")
    suspend fun registration(
        @Field("firstname") firstname: String?,
        @Field("email") email: String?,
        @Field("password") password: String?,
        @Field("passconf") passconf: String?,
        @Field("agreement") agreement: Int
    ): LoginResponse

    @FormUrlEncoded
    @POST("api4/registration/phone")
    suspend fun registrationPhone(
        @Field("firstname") firstname: String?,
        @Field("phone") email: String?,
        @Field("password") password: String?,
        @Field("passconf") passconf: String?,
        @Field("agreement") agreement: Int
    ): LoginResponse

    @FormUrlEncoded
    @POST("api4/remind")
    suspend fun remind(
        @Field("email") email: String?
    ): Response<RemindResponse>
}
