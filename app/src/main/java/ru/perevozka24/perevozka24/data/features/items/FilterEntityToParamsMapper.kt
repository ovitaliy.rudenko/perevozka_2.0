package ru.perevozka24.perevozka24.data.features.items

import ru.perevozka24.perevozka24.data.features.filter.entities.FilterEntity
import ru.perevozka24.perevozka24.data.features.filter.entities.FilterItemsEquipmentEntity
import ru.perevozka24.perevozka24.ui.common.Mapper

object FilterEntityToParamsMapper : Mapper<FilterEntity, ItemsRequestParams> {
    override fun map(e: FilterEntity): ItemsRequestParams {
        return when (e) {
            is FilterItemsEquipmentEntity -> EquipmentMapper.map(e)
            else -> error("unsupported")
        }
    }

    private object EquipmentMapper : Mapper<FilterItemsEquipmentEntity, ItemsParams> {
        override fun map(e: FilterItemsEquipmentEntity): ItemsParams {
            return ItemsParams(
                shipment = e.shipment,
                type = e.category?.id,
                category = e.subCategory?.id,
                country = e.country?.code,
                region = e.region?.id,
                cityId = e.city?.id,
                sale = e.category?.sale,
                blackList = null
            )
        }
    }
}
