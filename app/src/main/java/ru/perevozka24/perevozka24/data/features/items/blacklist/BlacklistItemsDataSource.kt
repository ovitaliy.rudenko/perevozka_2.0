package ru.perevozka24.perevozka24.data.features.items.blacklist

interface BlacklistItemsDataSource {
    suspend fun addToBlackList(itemId: String)
    fun getBlackList(): List<String>?
}
