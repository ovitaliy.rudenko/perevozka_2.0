package ru.perevozka24.perevozka24.data.features.offers.entity

import com.google.gson.annotations.SerializedName
import org.threeten.bp.LocalDateTime

data class OfferAbuseEntity(
    @SerializedName("id") val id: String,
    @SerializedName("offer_id") val offerId: String,
    @SerializedName("user_id") val userId: String,
    @SerializedName("text") val text: String,
    @SerializedName("link") val link: String,
    @SerializedName("date") val date: LocalDateTime
)
