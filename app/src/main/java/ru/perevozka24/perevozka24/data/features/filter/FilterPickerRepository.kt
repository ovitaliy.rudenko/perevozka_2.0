package ru.perevozka24.perevozka24.data.features.filter

import ru.perevozka24.perevozka24.data.features.rate.RateDataSource
import ru.perevozka24.perevozka24.ui.filters.models.CargoBodyModel
import ru.perevozka24.perevozka24.ui.filters.models.CategoryMapper
import ru.perevozka24.perevozka24.ui.filters.models.CategoryModel
import ru.perevozka24.perevozka24.ui.filters.models.CategorySubCategoryMapper
import ru.perevozka24.perevozka24.ui.filters.models.CategorySubCategoryModel
import ru.perevozka24.perevozka24.ui.filters.models.OfferStatusModel
import ru.perevozka24.perevozka24.ui.filters.models.RateTypeModel
import ru.perevozka24.perevozka24.ui.filters.models.SubCategoryMapper
import ru.perevozka24.perevozka24.ui.filters.models.SubCategoryModel

class FilterPickerRepository(
    private val dataSource: FilterPickerDataSource,
    private val rateDataSource: RateDataSource
) {
    fun getCategories(): List<CategoryModel> = dataSource.getCategories()
        .map { CategoryMapper.map(it) }

    suspend fun getSubCategories(shipment: Int, categoryId: String): List<SubCategoryModel> =
        dataSource.getSubCategories(shipment, categoryId)
            .map { SubCategoryMapper.map(it) }

    suspend fun getCategorySubCategories(shipment: Int): List<CategorySubCategoryModel> =
        dataSource.getCategories().flatMap {
            val subCategories = dataSource.getSubCategories(shipment, checkNotNull(it.id))
            CategorySubCategoryMapper.map(it, subCategories)
        }

    suspend fun getRate() = rateDataSource.getRates().map { RateTypeModel.Mapper.map(it) }

    suspend fun getCargoBodyType() = dataSource.getCargoBodyType().map {
        CargoBodyModel.Mapper.map(it)
    }

    fun getStatuses() = dataSource.getStatuses().map { OfferStatusModel.Mapper.map(it) }
}
