package ru.perevozka24.perevozka24.data.features.items

interface ItemsRequestParams

data class ItemsParams(
    val shipment: Int?,
    val type: String?,
    val category: String?,
    val country: String?,
    val region: String?,
    val cityId: String?,
    val sale: Int?,
    val blackList: List<String>?
) : ItemsRequestParams

data class ItemAbuseParams(
    val itemId: String,
    val contact: String?,
    val text: String?
) : ItemsRequestParams
