package ru.perevozka24.perevozka24.data.features.location

import java.util.ArrayList

data class LocationParams(
    val country: String?,
    val region: String?,
    val city: String?,
    val address: String? = null
) {
    fun buildSearch(): String {
        val items = ArrayList<String>()

        country?.let { items.add(it) }
        region?.replace(REGION, "")?.trim()?.takeIf { it.isNotBlank() }
            ?.let { items.add("$REGION_SUFFIX $it") }
        city?.takeIf { it.isNotBlank() }?.let { items.add("$CITY_SUFFIX $it") }
        address?.takeIf { it.isNotBlank() }?.let { items.add(it) }
        return items.joinToString(", ")
    }

    fun zoomValue(): Float {
        return when {
            address != null -> ZOOM_ADDRESS
            city != null -> ZOOM_CITY
            region != null -> ZOOM_REGION
            else -> ZOOM_COUNTRY
        }
    }

    companion object {
        const val ZOOM_COUNTRY = 4f
        const val ZOOM_REGION = 8f
        const val ZOOM_CITY = 10f
        const val ZOOM_ADDRESS = 16f

        const val REGION_SUFFIX = "обл."
        const val REGION = "область"
        const val CITY_SUFFIX = "г."
    }
}

data class CalculateLocationParams(val from: LocationParams, val to: LocationParams)
