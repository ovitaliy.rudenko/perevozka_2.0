package ru.perevozka24.perevozka24.data.features.auth.responces

import com.google.gson.annotations.SerializedName
import ru.perevozka24.perevozka24.data.base.BaseResponse

class LoginResponse(
    @SerializedName("user_id")
    val userId: String?,

    @SerializedName("name")
    val name: String?,

    @SerializedName("x-auth-token")
    val authToken: String? = null
) : BaseResponse()
