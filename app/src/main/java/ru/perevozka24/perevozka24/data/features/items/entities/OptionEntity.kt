package ru.perevozka24.perevozka24.data.features.items.entities

data class OptionEntity(val label: String?, val value: String?)
