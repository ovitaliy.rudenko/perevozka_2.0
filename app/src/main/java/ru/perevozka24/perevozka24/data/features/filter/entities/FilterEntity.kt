package ru.perevozka24.perevozka24.data.features.filter.entities

interface FilterEntity {
    val country: CountryEntity?
    val region: RegionEntity?
    fun setDefaultLocation(
        country: CountryEntity?,
        region: RegionEntity?,
        city: CityEntity?
    ): FilterEntity = this
}
