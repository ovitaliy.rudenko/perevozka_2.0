package ru.perevozka24.perevozka24.data.features.offers.favorite

import org.kodein.di.Kodein
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.singleton
import org.kodein.di.weakReference

val favoriteOffers = Kodein.Module("favoriteOffers") {

    bind<FavoritesOffersDataSource>("local") with singleton(ref = weakReference) {
        FavoritesOffersLocalDataSource(
            instance(),
            instance()
        )
    }
    bind<FavoritesOffersDataSource>("remote") with singleton(ref = weakReference) {
        FavoritesOffersRemoteDataSource(
            instance(),
            instance()
        )
    }
    bind<FavoritesOffersDataSource>() with singleton(ref = weakReference) {
        FavoritesOffersDataSourceImpl(
            instance(),
            instance("local"),
            instance("remote")
        )
    }
}
