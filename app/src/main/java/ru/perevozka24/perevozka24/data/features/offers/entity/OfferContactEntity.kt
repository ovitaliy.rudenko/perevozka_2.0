package ru.perevozka24.perevozka24.data.features.offers.entity

import com.google.gson.annotations.SerializedName
import ru.perevozka24.perevozka24.data.base.BaseResponse

class OfferContactEntity(
    @SerializedName("contacts")
    val contacts: String? = null,

    @SerializedName("offers_views")
    val viewsCount: Int = 0
) : BaseResponse()
