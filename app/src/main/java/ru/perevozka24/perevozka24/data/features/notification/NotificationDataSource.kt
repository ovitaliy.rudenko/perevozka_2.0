package ru.perevozka24.perevozka24.data.features.notification

import ru.perevozka24.perevozka24.data.features.notification.entity.NotificationEntity

class NotificationDataSource(
    private val local: NotificationLocalDataSource,
    private val remote: NotificationRemoteDataSource
) {
    suspend fun getNotification(): NotificationEntity {
        return local.getNotification() ?: remote.getNotification().apply {
            local.setNotification(this)
            remote.setNotification(this)
        }
    }

    suspend fun setNotification(params: NotificationEntity) {
        local.setNotification(params)
        remote.setNotification(params)
    }
}
