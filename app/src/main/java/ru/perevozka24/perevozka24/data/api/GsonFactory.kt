package ru.perevozka24.perevozka24.data.api

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.google.gson.JsonDeserializationContext
import com.google.gson.JsonDeserializer
import com.google.gson.JsonElement
import com.google.gson.JsonObject
import com.google.gson.JsonPrimitive
import com.google.gson.reflect.TypeToken
import org.threeten.bp.LocalDateTime
import org.threeten.bp.format.DateTimeFormatter
import ru.perevozka24.perevozka24.DEFAULT_DATE_FORMAT
import ru.perevozka24.perevozka24.DEFAULT_DATE_FORMAT_2
import ru.perevozka24.perevozka24.DEFAULT_DATE_FORMAT_3
import ru.perevozka24.perevozka24.data.features.items.entities.OptionEntity
import ru.perevozka24.perevozka24.data.utils.longToDateTime
import ru.perevozka24.perevozka24.data.utils.safeParse
import ru.perevozka24.perevozka24.ui.common.ext.safe
import java.lang.reflect.Type
import java.text.SimpleDateFormat
import java.util.Date
import java.util.Locale

const val SECOND = 1000L

object GsonFactory {
    fun create(): Gson = GsonBuilder()
        .setLenient()
        .registerTypeAdapter(Boolean::class.java, booleanJsonDeserializer)
        .registerTypeAdapter(Date::class.java, dateJsonDeserializer)
        .registerTypeAdapter(LocalDateTime::class.java, localDateTimeJsonDeserializer)
        .registerTypeAdapter(Int::class.java, integerJsonDeserializer)
        .registerTypeAdapter(Int::class.javaPrimitiveType, integerJsonDeserializer)
        .registerTypeAdapter(Long::class.java, longJsonDeserializer)
        .registerTypeAdapter(Long::class.javaPrimitiveType, longJsonDeserializer)
        .registerTypeAdapter(Double::class.java, doubleJsonDeserializer)
        .registerTypeAdapter(Double::class.javaPrimitiveType, doubleJsonDeserializer)
        .registerTypeAdapter(Array<OptionEntity>::class.java, optionArrayEntityDeserializer)
        .registerTypeAdapter(
            (object : TypeToken<List<OptionEntity>>() {}).type,
            optionListEntityDeserializer
        )
        .registerTypeAdapter(Array<String>::class.java, stringArrayJsonDeserializer)
        .registerTypeAdapter(
            (object : TypeToken<List<String>?>() {}).type,
            stringListJsonDeserializer
        )
        .create()
}

private val dateJsonDeserializer =
    JsonDeserializer<Date?> { json: JsonElement, _: Type, _: JsonDeserializationContext ->
        if ((json as JsonPrimitive).isString) {
            SimpleDateFormat(
                DEFAULT_DATE_FORMAT,
                Locale.getDefault()
            ).safeParse(json.getAsString())
                ?: SimpleDateFormat(
                    DEFAULT_DATE_FORMAT_2,
                    Locale.getDefault()
                ).safeParse(json.getAsString())
        }
        null
    }
private val localDateTimeJsonDeserializer =
    JsonDeserializer<LocalDateTime?> { json: JsonElement, _: Type, _: JsonDeserializationContext ->
        val primitive = json as? JsonPrimitive ?: return@JsonDeserializer null
        when {
            primitive.isNumber -> longToDateTime(primitive.asLong * SECOND)
            primitive.isString -> {
                val string = json.asString
                string?.toLongOrNull()?.let {
                    return@JsonDeserializer longToDateTime(it * SECOND)
                }

                arrayOf(DEFAULT_DATE_FORMAT, DEFAULT_DATE_FORMAT_2, DEFAULT_DATE_FORMAT_3)
                    .asSequence()
                    .map {
                        DateTimeFormatter.ofPattern(it)
                    }.mapNotNull {
                        safe(true) { LocalDateTime.parse(string, it) }
                    }.firstOrNull()
            }
            else -> null
        }
    }
private val integerJsonDeserializer =
    JsonDeserializer { json: JsonElement, _: Type?, _: JsonDeserializationContext? ->
        try {
            json.asInt
        } catch (ignore: ClassCastException) {
            0
        }
    }
private val doubleJsonDeserializer =
    JsonDeserializer<Double> { json: JsonElement, _: Type?, _: JsonDeserializationContext? ->
        try {
            json.asDouble
        } catch (ignore: ClassCastException) {
            0.0
        }
    }
private val longJsonDeserializer =
    JsonDeserializer<Long> { json: JsonElement, typeOfT: Type?, context: JsonDeserializationContext? ->
        try {
            json.asLong
        } catch (ignore: ClassCastException) {
            dateJsonDeserializer.deserialize(json, typeOfT, context)?.time
        }
    }

private val booleanJsonDeserializer =
    JsonDeserializer<Boolean> { json: JsonElement, typeOfT: Type, _: JsonDeserializationContext? ->
        when {
            json.isJsonNull -> false
            (json as JsonPrimitive).isBoolean -> json.getAsBoolean()
            json.isNumber -> json.getAsInt() == 1
            json.isString -> json.asString == "1" || json.asString == "true"
            else -> throw IllegalArgumentException("can not convert $json $typeOfT to boolean")
        }
    }

private val optionArrayEntityDeserializer: JsonDeserializer<Array<OptionEntity>> =
    JsonDeserializer<Array<OptionEntity>> { json: JsonElement, type: Type?, context: JsonDeserializationContext? ->
        optionListEntityDeserializer.deserialize(json, type, context).toTypedArray()
    }
private val optionListEntityDeserializer: JsonDeserializer<List<OptionEntity>> =
    JsonDeserializer { json: JsonElement, _: Type?, _: JsonDeserializationContext? ->
        val optionEntities: ArrayList<OptionEntity> = ArrayList()
        if (json.isJsonArray) {
            val jsonArray = json.asJsonArray
            var i = 0
            while (i < jsonArray.size()) {
                val jsonObject = jsonArray[i] as JsonObject
                for ((key, value) in jsonObject.entrySet()) {
                    optionEntities.add(
                        OptionEntity(
                            key,
                            value.asString
                        )
                    )
                }
                i++
            }
        }
        optionEntities
    }

private val stringArrayJsonDeserializer: JsonDeserializer<Array<String>?> =
    JsonDeserializer { json: JsonElement, type: Type?, context: JsonDeserializationContext? ->
        stringListJsonDeserializer.deserialize(json, type, context)?.toTypedArray()
    }

private val stringListJsonDeserializer: JsonDeserializer<List<String>?> =
    JsonDeserializer { json: JsonElement, _: Type?, _: JsonDeserializationContext? ->
        val strings: ArrayList<String> = ArrayList()

        if (json.isJsonArray) {
            val jsonArray = json.asJsonArray
            var i = 0
            while (i < jsonArray.size()) {
                strings.add(jsonArray[i].asString)
                i++
            }
        }
        strings
    }
