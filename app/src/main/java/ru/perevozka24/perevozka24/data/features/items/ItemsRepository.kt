package ru.perevozka24.perevozka24.data.features.items

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import ru.perevozka24.perevozka24.data.base.checkIsOk
import ru.perevozka24.perevozka24.data.features.filter.FilterSelectionDataSource
import ru.perevozka24.perevozka24.data.features.filter.entities.FilterTypeEntity
import ru.perevozka24.perevozka24.data.features.items.blacklist.BlacklistItemsDataSource
import ru.perevozka24.perevozka24.data.features.items.favorite.FavoritesItemsDataSource
import ru.perevozka24.perevozka24.ui.main.items.models.ItemMapper
import ru.perevozka24.perevozka24.ui.main.items.models.ItemSimpleModel
import ru.perevozka24.perevozka24.ui.main.items.models.ItemSimplesList
import ru.perevozka24.perevozka24.ui.main.items.models.ItemsList

@SuppressWarnings("TooManyFunctions")
class ItemsRepository(
    private val itemsDataSource: ItemsDataSource,
    private val blacklistDataSource: BlacklistItemsDataSource,
    private val favoritesDataSource: FavoritesItemsDataSource,
    private val filterSelectionDataSource: FilterSelectionDataSource
) {

    suspend fun getEquipment(): ItemsList {
        return withContext(Dispatchers.IO) {
            val selection =
                filterSelectionDataSource.getFilterItems(FilterTypeEntity.EQUIPMENT_ITEMS)
            val params = FilterEntityToParamsMapper.map(selection) as ItemsParams
            getItems(params)
        }
    }

    suspend fun getMyItems(): ItemsList =
        withContext(Dispatchers.IO) {
            itemsDataSource.getMyItems().map { ItemMapper.map(it) }
                .applyFavorites(favoritesDataSource)
                .toModel()
        }

    suspend fun hasMyItems(): Boolean = getMyItems().isNotEmpty()

    private suspend fun getItems(params: ItemsParams): ItemsList =
        withContext(Dispatchers.IO) {
            itemsDataSource.getItems(params).map { ItemMapper.map(it) }
                .applyFavorites(favoritesDataSource)
                .toModel()
        }

    suspend fun getSimpleItem(): ItemSimplesList =
        withContext(Dispatchers.IO) {
            val selection = filterSelectionDataSource.getFilterItems(
                FilterTypeEntity.EQUIPMENT_ITEMS
            )

            val params = (FilterEntityToParamsMapper.map(selection) as ItemsParams)
                .copy(cityId = null) // we ignore city id
            itemsDataSource.getSimpleItems(params).map { ItemSimpleModel.map(it) }
                .toSimpleModel()
        }

    suspend fun getItemByUri(uri: String): ItemsList =
        withContext(Dispatchers.IO) {
            itemsDataSource.getItemByUri(uri).map { ItemMapper.map(it) }
                .applyFavorites(favoritesDataSource)
                .toModel()
        }

    suspend fun addToBlackList(userId: String) =
        withContext(Dispatchers.IO) { blacklistDataSource.addToBlackList(userId) }

    suspend fun addFavorite(itemId: String) =
        withContext(Dispatchers.IO) { favoritesDataSource.addFavorite(itemId) }

    suspend fun removeFavorite(itemId: String) =
        withContext(Dispatchers.IO) { favoritesDataSource.removeFavorite(itemId) }

    suspend fun getFavorites(): ItemsList =
        withContext(Dispatchers.IO) { favoritesDataSource.getFavorites() }
            .map { ItemMapper.map(it) }
            .toModel()

    suspend fun abuse(params: ItemAbuseParams) {
        withContext(Dispatchers.IO) { itemsDataSource.abuse(params).checkIsOk() }
    }

    suspend fun search(query: String): ItemsList = withContext(Dispatchers.IO) {
        itemsDataSource.search(query)
            .map { ItemMapper.map(it) }
            .applyFavorites(favoritesDataSource)
            .toModel()
    }

    suspend fun searchSimple(query: String): ItemSimplesList = withContext(Dispatchers.IO) {
        itemsDataSource.searchSimple(query)
            .map { ItemSimpleModel.map(it) }
            .toSimpleModel()
    }
}
