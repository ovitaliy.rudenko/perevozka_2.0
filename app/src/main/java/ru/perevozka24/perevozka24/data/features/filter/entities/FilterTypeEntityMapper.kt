package ru.perevozka24.perevozka24.data.features.filter.entities

import ru.perevozka24.perevozka24.ui.common.Mapper
import ru.perevozka24.perevozka24.ui.filters.filter.FilterType

object FilterTypeEntityMapper : Mapper<FilterType, FilterTypeEntity> {
    override fun map(e: FilterType): FilterTypeEntity {
        return when (e) {
            FilterType.EQUIPMENT -> FilterTypeEntity.EQUIPMENT_ITEMS
            FilterType.CARGO -> FilterTypeEntity.CARGO
            FilterType.ORDER -> FilterTypeEntity.OFFERS
        }
    }
}
