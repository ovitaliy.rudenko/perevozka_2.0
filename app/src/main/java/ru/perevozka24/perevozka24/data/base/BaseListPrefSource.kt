package ru.perevozka24.perevozka24.data.base

import android.content.SharedPreferences
import androidx.core.content.edit
import com.google.gson.Gson

@SuppressWarnings("UnnecessaryAbstractClass")
abstract class BaseListPrefSource<T>(
    private val sharedPreferences: SharedPreferences
) {

    protected open val key: String = javaClass.simpleName

    fun addTo(item: T) {
        val items = getList()
        items.add(item)
        sharedPreferences.edit {
            putString(key, Gson().toJson(items))
        }
    }

    fun remove(item: T) {
        val items = getList()
        items.remove(item)
        sharedPreferences.edit().putString(key, Gson().toJson(items)).apply()
    }

    fun contains(item: T): Boolean = getList().contains(item)

    fun getList() = sharedPreferences.getString(key, null)?.let {
        Gson().fromJson(it, ArrayList<T>().javaClass)
    }
        ?: ArrayList()
}
