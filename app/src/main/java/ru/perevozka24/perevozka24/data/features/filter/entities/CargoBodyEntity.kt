package ru.perevozka24.perevozka24.data.features.filter.entities

import ru.perevozka24.perevozka24.ui.filters.models.CargoBodyModel

data class CargoBodyEntity(
    val id: String?,
    val name: String
) {
    class List : ArrayList<CargoBodyEntity>()

    object Mapper : ru.perevozka24.perevozka24.ui.common.Mapper<CargoBodyModel, CargoBodyEntity> {
        override fun map(e: CargoBodyModel): CargoBodyEntity {
            return CargoBodyEntity(
                id = e.id,
                name = e.name
            )
        }
    }
}
