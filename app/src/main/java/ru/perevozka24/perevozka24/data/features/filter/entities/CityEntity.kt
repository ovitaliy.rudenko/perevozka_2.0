package ru.perevozka24.perevozka24.data.features.filter.entities

import com.google.gson.annotations.SerializedName
import ru.perevozka24.perevozka24.ui.filters.models.Direction

data class CityEntity(
    @SerializedName("city_id") val id: String?,
    val name: String?,
    val regionId: String?,
    val countryId: String?,
    val direction: Direction?
)
