package ru.perevozka24.perevozka24.data.features.items

import ru.perevozka24.perevozka24.data.features.items.favorite.FavoritesItemsDataSource
import ru.perevozka24.perevozka24.ui.main.items.models.ItemModel
import ru.perevozka24.perevozka24.ui.main.items.models.ItemSimpleModel
import ru.perevozka24.perevozka24.ui.main.items.models.ItemSimplesList
import ru.perevozka24.perevozka24.ui.main.items.models.ItemsList

fun List<ItemModel>.toModel(): ItemsList {
    val result = ItemsList()
    result.addAll(this)
    return result
}

fun List<ItemSimpleModel>.toSimpleModel(): ItemSimplesList {
    val result = ItemSimplesList()
    result.addAll(this)
    return result
}

suspend fun List<ItemModel>.applyFavorites(favoritesDataSource: FavoritesItemsDataSource): List<ItemModel> {
    val favorites = favoritesDataSource.getFavorites().map { it.id }
    return map { it.copy(favorite = favorites.contains(it.id)) }
}

fun ItemsList.updateFavoriteValue(id: String, favorite: Boolean): ItemsList {
    return map {
        if (it.id == id) it.copy(favorite = favorite)
        else it
    }.toModel()
}
