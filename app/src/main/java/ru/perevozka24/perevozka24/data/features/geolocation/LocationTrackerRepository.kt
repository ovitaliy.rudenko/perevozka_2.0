package ru.perevozka24.perevozka24.data.features.geolocation

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import ru.perevozka24.perevozka24.data.utils.toInt
import ru.perevozka24.perevozka24.ui.geolocation.GeolocationModel

class LocationTrackerRepository(private val geolocationDataSource: LocationTrackerDataSource) {

    suspend fun isEnabled() = geolocationDataSource.getEnabled() == 1
    suspend fun setEnabled(value: Boolean) = geolocationDataSource.setEnabled(value.toInt())

    suspend fun getCurrentId(): String? =
        withContext(Dispatchers.IO) {
            geolocationDataSource.getCurrentItemId()
        }
    suspend fun getCurrent(): GeolocationModel? =
        withContext(Dispatchers.IO) {
            geolocationDataSource.getCurrentItem()?.let {
                GeolocationModel.Mapper.map(it)
            }
        }

    fun selectItem(id: String?) = geolocationDataSource.setCurrentItem(id)

    suspend fun getItems(): List<GeolocationModel> =
        withContext(Dispatchers.IO) {
            GeolocationModel.Mapper.map(geolocationDataSource.getItems())
        }

    suspend fun postLocation(id: String, lat: Double, ln: Double) =
        withContext(Dispatchers.IO) {
            geolocationDataSource.postLocation(id, lat, ln)
        }
}
