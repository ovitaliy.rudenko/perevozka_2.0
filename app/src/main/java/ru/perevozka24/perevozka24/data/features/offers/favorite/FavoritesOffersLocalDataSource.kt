package ru.perevozka24.perevozka24.data.features.offers.favorite

import android.content.SharedPreferences
import ru.perevozka24.perevozka24.data.base.BaseListPrefSource
import ru.perevozka24.perevozka24.data.features.offers.OffersApi
import ru.perevozka24.perevozka24.data.features.offers.entity.OfferStatusEntity
import ru.perevozka24.perevozka24.data.features.offers.entity.OffersEntity

class FavoritesOffersLocalDataSource constructor(
    private val api: OffersApi,
    sharedPreferences: SharedPreferences
) :
    BaseListPrefSource<String>(sharedPreferences),
    FavoritesOffersDataSource {

    override suspend fun getFavorites(): OffersEntity =
        OffersEntity(data = emptyList(), status = OfferStatusEntity.NOAUTH) /*{
        val favoriteIds = getList()
        return favoriteIds
            .asFlow()
            .map { id -> api.getOffer(null, id) }
            .toList()
            .map {
                it.copy(
                    data = it.data?.map { offer -> offer.copy(isFavorite = true) }
                )
            }
            .takeIf { it.isNotEmpty() }?.let {
                it.reduce { acc, offersEntity ->
                    val newList: List<OfferEntity> =
                        (offersEntity.data ?: emptyList()) + (acc.data ?: emptyList())
                    return acc.copy(data = newList)
                }
            } ?: OffersEntity(data = emptyList(), status = OfferStatusEntity.NOAUTH)
    }*/

    override suspend fun addFavorite(itemId: String) = addTo(itemId)

    override suspend fun removeFavorite(itemId: String) = remove(itemId)
}
