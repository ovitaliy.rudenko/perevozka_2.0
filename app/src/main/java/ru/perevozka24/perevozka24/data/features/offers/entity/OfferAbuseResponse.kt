package ru.perevozka24.perevozka24.data.features.offers.entity

import ru.perevozka24.perevozka24.data.base.BaseResponse

data class OfferAbuseResponse(
    val abuses: List<OfferAbuseEntity>
) : BaseResponse()
