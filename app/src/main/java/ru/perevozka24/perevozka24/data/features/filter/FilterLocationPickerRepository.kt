package ru.perevozka24.perevozka24.data.features.filter

import ru.perevozka24.perevozka24.data.features.location.entity.SimpleAddress
import ru.perevozka24.perevozka24.ui.filters.models.CityMapper
import ru.perevozka24.perevozka24.ui.filters.models.CityModel
import ru.perevozka24.perevozka24.ui.filters.models.CountryMapper
import ru.perevozka24.perevozka24.ui.filters.models.CountryModel
import ru.perevozka24.perevozka24.ui.filters.models.RegionCityMapper
import ru.perevozka24.perevozka24.ui.filters.models.RegionCityModel
import ru.perevozka24.perevozka24.ui.filters.models.RegionMapper
import ru.perevozka24.perevozka24.ui.filters.models.RegionModel
import ru.perevozka24.perevozka24.ui.filters.models.RegionsMapper

class FilterLocationPickerRepository(
    private val dataSource: FilterPickerDataSource
) {

    suspend fun getCounties(): List<CountryModel> = dataSource.getCounties()
        .map { CountryMapper.map(it) }

    suspend fun getRegions(countryId: String): List<RegionModel> =
        dataSource.getRegions(countryId).run {
            RegionsMapper.map(this)
        }

    suspend fun getCities(countryId: String, regionId: String): List<CityModel> =
        dataSource.getCities(countryId, regionId)
            .run {
                CityMapper.map(this)
            }

    suspend fun getRegionCity(countryId: String): List<RegionCityModel> =
        dataSource.getRegions(countryId)
            .run { RegionCityMapper.map(this) }

    suspend fun getCountryByAddress(address: SimpleAddress): CountryModel? =
        dataSource.getCountryFromAddress(address)
            ?.let { CountryMapper.map(it) }

    suspend fun getRegionByAddress(address: SimpleAddress): Pair<RegionModel?, CityModel?> {
        return dataSource.getCityByAddress(address)?.let {
            val (region, city) = it
            Pair(
                region?.let { r -> RegionMapper.map(r) },
                city?.let { c -> CityMapper.map(c) }
            )
        } ?: Pair(null, null)
    }
}
