package ru.perevozka24.perevozka24.data.cache

import timber.log.Timber

class CacheProvider(
    private val store: CacheStore,
    private val mapper: CacheMapper
) {

    suspend fun <T> proceed(key: CacheKey, eviction: CacheEviction, block: suspend () -> T): T {
        val json = store.get(key.key)
        val cacheObject: CacheObject? = json?.let { mapper.fromJson(it, CacheObject::class.java) }
        val cacheEvictionType = cacheObject?.evictionType?.let {
            Class.forName(it).kotlin
        }
        val cacheEviction = cacheEvictionType?.let {
            mapper.fromJson(cacheObject.eviction, it.java) as CacheEviction
        }
        val evicted = cacheEviction != null &&
                cacheEviction.isSame(eviction) &&
                !cacheEviction.evicted()
        return if (cacheObject != null && evicted) {
            mapper.fromJson(cacheObject.value, key.type) as T
        } else {
            Timber.e(cacheEviction.toString())
            val v = block()
            store.put(
                key.key,
                mapper.toJson(
                    CacheObject(
                        eviction.javaClass.name,
                        mapper.toJson(eviction.buildToSave()),
                        mapper.toJson(v)
                    )
                )
            )
            v
        }
    }
}
