package ru.perevozka24.perevozka24.data.features.items.entities

import com.google.gson.annotations.SerializedName

data class ItemEntity(
    val id: String?,
    val name: String?,
    val phone: String?,
    @SerializedName("user_name") val userName: String?,
    @SerializedName("user_id") val userId: String,
    @SerializedName("coord_x") val coordX: Double?,
    @SerializedName("coord_y") val coordY: Double?,
    @SerializedName("image") private val imagePath: String?,
    val country: String? = null,
    @SerializedName("country_name") val countryName: String? = null,
    @SerializedName("region_id") val regionId: String? = null,
    @SerializedName("region_name") val regionName: String? = null,
    @SerializedName("default_city") val cityId: String? = null,
    @SerializedName("city_name") val cityName: String?,
    val opts: Array<OptionEntity>?,
    val costs: Array<String>?,
    val text: String?,
    val shipment: Long?,
    @SerializedName("star_rating") val rating: Float,
    @SerializedName("cat_id") val categoryId: Long?,
    val parentCategory: String?,
    @SerializedName("item_time") val itemTime: String?,
    @SerializedName("address") val address: String?,
    @SerializedName("show_offer_button") val showOffer: Number?,
    val favorite: Boolean,
    @SerializedName("cat_image") private val catIcon: String?
) {
    val image: String?
        get() = imagePath?.let { if (it.startsWith("http")) it else "https://perevozka24.ru$it" }
    val categoryIcon: String?
        get() = catIcon?.let { if (it.startsWith("http")) it else "https://perevozka24.ru$it" } ?: {
            print(this)
            ""
        }()

    val showOfferBool: Boolean
        get() = showOffer?.toInt() == 1
}
