package ru.perevozka24.perevozka24.data.features.notification

import com.google.firebase.iid.FirebaseInstanceId
import ru.perevozka24.perevozka24.data.features.notification.entity.NotificationEntity
import ru.perevozka24.perevozka24.data.features.user.UserDataSource

class NotificationRemoteDataSource(
    private val userDataSource: UserDataSource,
    private val api: NotificationApi
) {
    suspend fun getNotification(): NotificationEntity =
        api.getNotificationSettings(userDataSource.authToken)

    suspend fun setNotification(params: NotificationEntity) =
        api.setNotificationSettings(
            token = userDataSource.authToken,
            pushToken = FirebaseInstanceId.getInstance().token,
            parentCategory = params.category,
            categoryId = params.subCategory,
            country = params.country,
            regionId = params.region,
            cityId = params.city,
            notify = params.enabled,
            sound = params.soundEnabled
        )
}
