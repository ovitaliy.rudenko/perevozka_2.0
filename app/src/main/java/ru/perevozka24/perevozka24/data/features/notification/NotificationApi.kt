package ru.perevozka24.perevozka24.data.features.notification

import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST
import ru.perevozka24.perevozka24.data.features.notification.entity.NotificationEntity
import java.util.HashMap

@SuppressWarnings("LongParameterList")
interface NotificationApi {
    @FormUrlEncoded
    @POST("api4/set_settings")
    suspend fun setNotificationSettings(
        @Field("token") token: String?,
        @Field("push_token") pushToken: String?,
        @Field("typeof") parentCategory: String?,
        @Field("cat_id") categoryId: String?,
        @Field("country") country: String?,
        @Field("region_id") regionId: String?,
        @Field("city_id") cityId: String?,
        @Field("notify") notify: Int,
        @Field("sound") sound: Int
    ): HashMap<Any, Any>

    @FormUrlEncoded
    @POST("api4/get_settings")
    suspend fun getNotificationSettings(
        @Field("token") token: String?
    ): NotificationEntity
}
