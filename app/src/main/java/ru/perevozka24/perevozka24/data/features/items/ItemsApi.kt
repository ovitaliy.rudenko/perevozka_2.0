package ru.perevozka24.perevozka24.data.features.items

import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Query
import ru.perevozka24.perevozka24.data.base.Response
import ru.perevozka24.perevozka24.data.base.SimpleResponse
import ru.perevozka24.perevozka24.data.features.items.entities.ItemEntity
import ru.perevozka24.perevozka24.data.features.items.entities.ItemSimpleEntity

@SuppressWarnings("TooManyFunctions")
interface ItemsApi {
    @FormUrlEncoded
    @POST("api3/items")
    @SuppressWarnings("LongParameterList")
    suspend fun getItems(
        @Field("token") token: String?,
        @Field("shipment") shipment: Int?,
        @Field("typeof") parentCategory: String?,
        @Field("ts_type") category: String?,
        @Field("country") countryId: String?,
        @Field("region_id") regionId: String?,
        @Field("city_id") cityId: String?,
        @Field("sale") sale: Int?,
        @Field("blacklist_arr") blacklist: String?
    ): Response<List<ItemEntity>>

    @FormUrlEncoded
    @POST("api3/items")
    suspend fun getMyItems(
        @Field("token") token: String?,
        @Field("user_items") userItems: Int
    ): Response<List<ItemEntity>>

    @FormUrlEncoded
    @POST("api3/items")
    suspend fun getFavorite(
        @Field("token") token: String?,
        @Field("favorites") favorite: Int
    ): Response<List<ItemEntity>>

    @FormUrlEncoded
    @POST("api3/items")
    suspend fun getFavoriteNoAuth(
        @Field("favorites_arr") favorites: String?,
        @Field("favorites") favorite: Int
    ): Response<List<ItemEntity>>

    @FormUrlEncoded
    @POST("api3/abuse")
    suspend fun abuse(
        @Field("token") token: String?,
        @Field("item_id") itemId: String?,
        @Field("text") text: String?,
        @Field("contacts") contacts: String?
    ): SimpleResponse

    @FormUrlEncoded
    @POST("api3/blacklist")
    suspend fun blacklist(
        @Field("token") token: String?,
        @Field("user_id") userId: String?
    ): SimpleResponse

    @FormUrlEncoded
    @POST("api3/favorite")
    suspend fun favoriteAdd(
        @Field("token") token: String?,
        @Field("item_id") itemId: String?
    ): SimpleResponse

    @FormUrlEncoded
    @POST("api3/favorite_remove")
    suspend fun favoriteRemove(
        @Field("token") token: String?,
        @Field("id") itemId: String?
    ): SimpleResponse

    @GET("api3/items/search")
    suspend fun search(
        @Query("token") token: String?,
        @Query("q") query: String
    ): Response<List<ItemEntity>>

    @GET("api3/items")
    @SuppressWarnings("LongParameterList")
    suspend fun getSimpleItem(
        @Query("token") token: String?,
        @Query("shipment") shipment: Int?,
        @Query("typeof") parentCategory: String?,
        @Query("ts_type") category: String?,
        @Query("country") countryId: String?,
        @Query("region_id") regionId: String?,
        @Query("city_id") cityId: String?,
        @Query("sale") sale: Int?,
        @Query("blacklist_arr") blacklist: String?,
        @Query("map") map: String
    ): Response<List<ItemSimpleEntity>>

    @GET("api3/items/search")
    suspend fun searchSimple(
        @Query("token") token: String?,
        @Query("map") map: String,
        @Query("q") query: String
    ): Response<List<ItemSimpleEntity>>

    @GET("api3/items")
    suspend fun getItemByUri(
        @Query("token") token: String?,
        @Query("uri") uri: String
    ): Response<List<ItemEntity>>
}
