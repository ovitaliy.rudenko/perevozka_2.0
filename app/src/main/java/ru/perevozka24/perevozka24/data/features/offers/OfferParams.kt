package ru.perevozka24.perevozka24.data.features.offers

data class OfferParams(
    val shipment: Int?,
    val category: String?,
    val subCategory: String?,
    val country: String?,
    val region: String?,
    val city: String?,
    val countryTo: String?,
    val regionTo: String?,
    val cargoBodyType: String?,
    val status: String?,
    val offset: Int
)

data class OfferTakeParams(
    val offerId: String?,
    val price: String?
)

data class OfferCancelParams(
    val offerId: String?,
    val text: String?
)

data class OfferCreateParams(
    val categoryId: String?,
    val country: String?,
    val regionId: String?,
    val cityId: String?,
    val phone: String?,
    val offer: String?,
    val name: String?,
    val address: String?,
    val countryTo: String?,
    val regionIdTo: String?,
    val cityIdTo: String?,
    val addressTo: String?,
    val toUserId: String?,
    val paymentRate: String?,
    val paymentType: String?,
    val defaultImage: String?,
    val images: List<String>,
    val periodType: String?,
    val periodDate1: String?,
    val periodDate2: String?
)
