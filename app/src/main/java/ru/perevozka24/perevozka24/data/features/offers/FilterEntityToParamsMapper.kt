package ru.perevozka24.perevozka24.data.features.offers

import ru.perevozka24.perevozka24.data.features.filter.FilterPickerDataSource.Companion.CARGO_TYPE
import ru.perevozka24.perevozka24.data.features.filter.entities.FilterCargoEntity
import ru.perevozka24.perevozka24.data.features.filter.entities.FilterEntity
import ru.perevozka24.perevozka24.data.features.filter.entities.FilterOfferEntity
import ru.perevozka24.perevozka24.ui.common.Mapper

object FilterEntityToParamsMapper : Mapper<FilterEntity, OfferParams> {
    override fun map(e: FilterEntity): OfferParams {
        return when (e) {
            is FilterCargoEntity -> CargoMapper.map(e)
            is FilterOfferEntity -> OfferMapper.map(e)
            else -> error("unsupported")
        }
    }

    private object CargoMapper : Mapper<FilterCargoEntity, OfferParams> {
        override fun map(e: FilterCargoEntity): OfferParams {
            return OfferParams(
                shipment = e.shipment,
                category = CARGO_TYPE,
                subCategory = e.subCategory?.id,
                country = e.country?.code,
                region = e.region?.id,
                city = null,
                countryTo = e.countryTo?.code,
                regionTo = e.regionTo?.id,
                cargoBodyType = e.cargoBody?.id,
                status = e.status?.toString(),
                offset = 0
            )
        }
    }

    private object OfferMapper : Mapper<FilterOfferEntity, OfferParams> {
        override fun map(e: FilterOfferEntity): OfferParams {
            return OfferParams(
                shipment = e.shipment,
                category = e.category?.id,
                subCategory = e.subCategory?.id,
                country = e.country?.code,
                region = e.region?.id,
                city = e.city?.id,
                countryTo = null,
                regionTo = null,
                cargoBodyType = null,
                status = e.status?.toString(),
                offset = 0
            )
        }
    }
}
