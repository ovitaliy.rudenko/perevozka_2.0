package ru.perevozka24.perevozka24.data.features.user

data class UserEntity(
    val id: String?,
    val email: String?,
    val name: String?
)
