package ru.perevozka24.perevozka24.data.cache

interface CacheMapper {

    fun <T> toJson(value: T): String

    fun <T> fromJson(value: String, type: Class<T>): T
}
