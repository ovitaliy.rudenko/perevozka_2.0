package ru.perevozka24.perevozka24.data.features.upload.entity

import com.google.gson.annotations.SerializedName
import ru.perevozka24.perevozka24.data.base.BaseResponse

class UploadResponse : BaseResponse() {
    var image: String? = null
    @SerializedName("file_name")
    var fileName: String? = null
}
