package ru.perevozka24.perevozka24.data.cache

data class SimpleCacheKey(
    override val key: String,
    override val type: Class<*>
) : CacheKey
