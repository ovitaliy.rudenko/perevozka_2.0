package ru.perevozka24.perevozka24.data.features.items.blacklist

import android.content.SharedPreferences
import ru.perevozka24.perevozka24.data.base.BaseListPrefSource

class BlacklistItemsLocalDataSource(sharedPreferences: SharedPreferences) :
    BlacklistItemsDataSource, BaseListPrefSource<String>(sharedPreferences) {
    override suspend fun addToBlackList(itemId: String) = addTo(itemId)
    override fun getBlackList(): List<String>? = getList()
}
