package ru.perevozka24.perevozka24.data.features.filter.entities

data class FilterDistanceCalculatorEntity(
    override val country: CountryEntity? = null,
    override val region: RegionEntity? = null,
    val city: CityEntity? = null,
    val countryTo: CountryEntity? = null,
    val regionTo: RegionEntity? = null,
    val cityTo: CityEntity? = null
) : FilterEntity
