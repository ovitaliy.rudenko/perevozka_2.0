package ru.perevozka24.perevozka24.data.cache

interface CacheEviction {
    fun evicted(): Boolean

    fun buildToSave(): CacheEviction = this

    fun isSame(eviction: CacheEviction): Boolean =
        javaClass.simpleName == eviction.javaClass.simpleName
}
