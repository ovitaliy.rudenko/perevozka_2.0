package ru.perevozka24.perevozka24.data.features.offers.entity

import com.google.gson.annotations.SerializedName

enum class OfferStatusEntity {

    @SerializedName("noauth")
    NOAUTH,

    @SerializedName("paid")
    PAID,

    @SerializedName("showlink")
    SHOWLINK,

    @SerializedName("nopaid")
    NOPAID,

    @SerializedName("noviews")
    NOVIEWS,

    @SerializedName("taked")
    TAKED;

    companion object {
        fun valueOfString(status: String?): OfferStatusEntity {
            if (status != null) {
                for (offerStatus in values()) {
                    if (offerStatus.name.equals(status, true)) {
                        return offerStatus
                    }
                }
            }
            return NOAUTH
        }
    }
}
