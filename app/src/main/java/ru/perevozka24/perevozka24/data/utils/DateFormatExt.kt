package ru.perevozka24.perevozka24.data.utils

import org.threeten.bp.Instant
import org.threeten.bp.LocalDateTime
import org.threeten.bp.ZoneId
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.Date

fun SimpleDateFormat.safeParse(source: String): Date? {
    return try {
        parse(source)
    } catch (e: ParseException) {
        null
    }
}

fun longToDateTime(time: Long): LocalDateTime =
    LocalDateTime.ofInstant(
        Instant.ofEpochMilli(time),
        ZoneId.systemDefault()
    )
