package ru.perevozka24.perevozka24.data.features.profile

import ru.perevozka24.perevozka24.data.base.checkIsOk
import ru.perevozka24.perevozka24.data.features.profile.entities.ProfileEntity
import ru.perevozka24.perevozka24.data.features.user.UserDataSource

class ProfileDataSource(
    private val userDataSource: UserDataSource,
    private val api: ProfileApi
) {
    suspend fun getProfile(): ProfileEntity =
        api.getProfile(checkNotNull(userDataSource.authToken))
            .checkIsOk()
            .results

    suspend fun setProfile(params: ProfileSaveParams) {
        api.setProfile(
            token = checkNotNull(userDataSource.authToken),
            category = params.category,
            name = params.name,
            country = params.country,
            region = params.region,
            city = params.city,
            phone = params.phone,
            address = params.address,
            description = params.description,
            mode = params.mode,
            inn = params.inn,
            kpp = params.kpp,
            ogrn = params.orgn
        ).checkIsOk()
    }
}
