package ru.perevozka24.perevozka24.data.features.rate

import ru.perevozka24.perevozka24.R
import ru.perevozka24.perevozka24.data.ResourceProvider
import kotlin.coroutines.resume
import kotlin.coroutines.suspendCoroutine

class RateDataSource(resourceProvider: ResourceProvider) {
    private val types = arrayOf(
        RateTypeEntity(
            "1",
            resourceProvider.getString(R.string.create_order_info_payment_type_cache)
        ),
        RateTypeEntity(
            "2",
            resourceProvider.getString(R.string.create_order_info_payment_type_cashless_with_var)
        ),
        RateTypeEntity(
            "3",
            resourceProvider.getString(R.string.create_order_info_payment_type_cashless_without_var)
        )
    )

    suspend fun getRates(): Array<RateTypeEntity> = suspendCoroutine {
        it.resume(types)
    }
}
