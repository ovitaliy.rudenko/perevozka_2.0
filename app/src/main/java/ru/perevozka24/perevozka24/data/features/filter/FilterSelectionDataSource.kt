package ru.perevozka24.perevozka24.data.features.filter

import android.content.SharedPreferences
import androidx.core.content.edit
import com.google.gson.Gson
import com.yandex.mapkit.geometry.Point
import kotlinx.coroutines.withTimeout
import ru.perevozka24.perevozka24.data.features.filter.entities.FilterEntity
import ru.perevozka24.perevozka24.data.features.filter.entities.FilterTypeEntity
import ru.perevozka24.perevozka24.data.features.location.GeoLocationDataSource
import ru.perevozka24.perevozka24.data.features.location.LocationDataSource
import ru.perevozka24.perevozka24.ui.common.ext.safeSuspended
import timber.log.Timber

class FilterSelectionDataSource(
    private val gson: Gson,
    private val geoLocationDataSource: GeoLocationDataSource,
    private val locationDataSource: LocationDataSource,
    private val filterLocationDataSource: FilterPickerDataSource,
    private val sharedPreferences: SharedPreferences
) {
    @SuppressWarnings("TooGenericExceptionCaught", "NestedBlockDepth")
    suspend fun getFilterItems(type: FilterTypeEntity): FilterEntity {
        return sharedPreferences.getString(type.key, null)?.let {
            gson.fromJson(it, type.type) as FilterEntity
        } ?: type.createNew().run {
            if (type.needDefaultLocation) {
                try {
                    val location = safeSuspended {
                        withTimeout(TIMEOUT) {
                            geoLocationDataSource.getLocation()
                        }
                    } ?: Point(MOSCOW_LAT, MOSCOW_LONG)
                    val address = locationDataSource.getAddress(location).first()
                    val country = filterLocationDataSource.getCountryFromAddress(address)
                    val regionCity = filterLocationDataSource.getCityByAddress(address)
                    val region = regionCity?.first
                    val city = if (type.needDefaultCity) regionCity?.second else null
                    val result = setDefaultLocation(country, region, city)
                    setFilterItems(type, result)
                    result
                } catch (e: Exception) {
                    Timber.e(e)
                    this
                }
            } else {
                this
            }
        }
    }

    fun setFilterItems(type: FilterTypeEntity, v: FilterEntity) = setFilterItem(type.key, v)

    private fun setFilterItem(key: String, value: Any) {
        val json = gson.toJson(value)
        sharedPreferences.edit {
            putString(key, json)
        }
    }

    companion object {
        const val MOSCOW_LAT = 55.751
        const val MOSCOW_LONG = 37.622

        const val TIMEOUT = 3000L
    }
}
