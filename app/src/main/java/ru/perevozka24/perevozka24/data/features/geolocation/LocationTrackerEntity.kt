package ru.perevozka24.perevozka24.data.features.geolocation

data class LocationTrackerEntity(
    val id: String,
    val name: String
) {
    class List : ArrayList<LocationTrackerEntity>()
}
