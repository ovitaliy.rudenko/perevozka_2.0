package ru.perevozka24.perevozka24.data.features.notification

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import ru.perevozka24.perevozka24.data.features.filter.FilterPickerDataSource
import ru.perevozka24.perevozka24.data.features.filter.entities.RegionsEntity
import ru.perevozka24.perevozka24.data.features.notification.entity.NotificationEntity
import ru.perevozka24.perevozka24.data.utils.toInt
import ru.perevozka24.perevozka24.ui.filters.models.CategoryMapper
import ru.perevozka24.perevozka24.ui.filters.models.CategoryModel
import ru.perevozka24.perevozka24.ui.filters.models.CityMapper
import ru.perevozka24.perevozka24.ui.filters.models.CityModel
import ru.perevozka24.perevozka24.ui.filters.models.CountryMapper
import ru.perevozka24.perevozka24.ui.filters.models.CountryModel
import ru.perevozka24.perevozka24.ui.filters.models.Direction
import ru.perevozka24.perevozka24.ui.filters.models.NotificationModel
import ru.perevozka24.perevozka24.ui.filters.models.RegionCityModel
import ru.perevozka24.perevozka24.ui.filters.models.RegionMapper
import ru.perevozka24.perevozka24.ui.filters.models.RegionModel
import ru.perevozka24.perevozka24.ui.filters.models.RegionsMapper
import ru.perevozka24.perevozka24.ui.filters.models.SubCategoryMapper
import ru.perevozka24.perevozka24.ui.filters.models.SubCategoryModel

class NotificationRepository(
    private val notificationDataSource: NotificationDataSource,
    private val pickerDataSource: FilterPickerDataSource
) {
    suspend fun getNotificationSetting(): NotificationModel {
        val entity = notificationDataSource.getNotification()
        val country = withContext(Dispatchers.IO) { getCountryModel(entity) }
        val region = withContext(Dispatchers.IO) { getRegionModel(entity) }
        val city = withContext(Dispatchers.IO) { getCityModel(entity) }
        val category = withContext(Dispatchers.IO) { getCategory(entity) }
        val subCategory = withContext(Dispatchers.IO) { getSubCategory(entity) }

        return NotificationModel(
            enabled = entity.enabled == 1,
            sounds = entity.soundEnabled == 1,
            category = category,
            subCategory = subCategory,
            country = country,
            region = region,
            city = city,
        )
    }

    suspend fun save(params: NotificationModel) {
        notificationDataSource.setNotification(
            NotificationEntity(
                category = params.category.id,
                subCategory = params.subCategory.id,
                country = params.country.id,
                region = params.region.id,
                city = params.city.id,
                enabled = params.enabled.toInt(),
                soundEnabled = params.sounds.toInt()
            )
        )
    }

    suspend fun saveDefaults() {
        withContext(Dispatchers.IO) {
            save(getNotificationSetting())
        }
    }

    private fun getCategory(e: NotificationEntity): CategoryModel {
        val categoryId =
            e.category?.takeIfIdIsOk() ?: return CategoryModel.default()
        return pickerDataSource.getCategories().find { it.id == categoryId }?.let {
            CategoryMapper.map(it)
        } ?: CategoryModel.default()
    }

    private suspend fun getSubCategory(e: NotificationEntity): SubCategoryModel {
        val categoryId =
            e.category?.takeIfIdIsOk() ?: return SubCategoryModel.default()
        val subCategoryId = e.subCategory
        return pickerDataSource.getSubCategories(0, categoryId).find { it.id == subCategoryId }
            ?.let {
                SubCategoryMapper.map(it)
            } ?: SubCategoryModel.default()
    }

    private suspend fun getCountryModel(e: NotificationEntity): CountryModel {
        val code =
            e.country?.takeIfIdIsOk() ?: return CountryModel.default()
        return CountryMapper.map(pickerDataSource.getCountyByCode(code))
    }

    private suspend fun getRegionCityModel(e: NotificationEntity): RegionCityModel {
        val code = e.country?.takeIfIdIsOk()
        val regionId = e.region?.takeIfIdIsOk()
        return if (code == null || regionId == null) {
            RegionCityModel.default(countryId = code)
        } else {
            val region: RegionsEntity = pickerDataSource.getRegionCity(code, regionId)
            RegionCityModel(
                Direction.NONE,
                RegionsMapper.map(region),
                getCityModel(e)
            )
        }
    }

    private suspend fun getRegionModel(e: NotificationEntity): RegionModel {
        val code = e.country?.takeIfIdIsOk()
        val regionId = e.region?.takeIfIdIsOk()
        return if (code == null || regionId == null) {
            RegionModel.default(countryId = code)
        } else {
            RegionMapper.map(pickerDataSource.getRegion(code, regionId))
        }
    }

    private suspend fun getCityModel(e: NotificationEntity): CityModel {
        val code = e.country?.takeIfIdIsOk()
        val regionId = e.region?.takeIfIdIsOk()
        val cityId = e.city?.takeIfIdIsOk()
        return if (code == null || regionId == null || cityId == null) {
            CityModel.default(regionId)
        } else {
            CityMapper.map(pickerDataSource.getCity(code, regionId, cityId))
        }
    }

    private fun String.takeIfIdIsOk() = if (!isNullOrBlank() && this != "0") this else null
}
