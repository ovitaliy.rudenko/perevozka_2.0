package ru.perevozka24.perevozka24.data.cache

import java.io.File

class FileCacheStore(private val dir: File) : CacheStore {
    init {
        check(dir.isDirectory)
    }

    override fun put(key: String, value: String?) {
        if (value == null) {
            File(dir, key).takeIf { it.exists() }?.delete()
        } else {
            File(dir, key).writeText(value)
        }
    }

    override fun get(key: String): String? {
        return File(dir, key).takeIf { it.exists() }?.readText()
    }
}
