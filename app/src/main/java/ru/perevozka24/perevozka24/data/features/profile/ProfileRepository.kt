package ru.perevozka24.perevozka24.data.features.profile

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import ru.perevozka24.perevozka24.data.features.filter.FilterPickerDataSource
import ru.perevozka24.perevozka24.data.features.profile.entities.ProfileEntity
import ru.perevozka24.perevozka24.ui.filters.models.CategoryMapper
import ru.perevozka24.perevozka24.ui.filters.models.CategoryModel
import ru.perevozka24.perevozka24.ui.filters.models.CityMapper
import ru.perevozka24.perevozka24.ui.filters.models.CityModel
import ru.perevozka24.perevozka24.ui.filters.models.CountryMapper
import ru.perevozka24.perevozka24.ui.filters.models.CountryModel
import ru.perevozka24.perevozka24.ui.filters.models.RegionMapper
import ru.perevozka24.perevozka24.ui.filters.models.RegionModel
import ru.perevozka24.perevozka24.ui.profile.models.ProfileModel

class ProfileRepository(
    private val dataSource: ProfileDataSource,
    private val pickerDataSource: FilterPickerDataSource
) {
    suspend fun getProfile(): ProfileModel {
        val entity = withContext(Dispatchers.IO) { dataSource.getProfile() }
        val country = withContext(Dispatchers.IO) { getCountryModel(entity) }
        val region = withContext(Dispatchers.IO) { getRegionModel(entity) }
        val city = withContext(Dispatchers.IO) { getCityModel(entity) }
        val category = withContext(Dispatchers.IO) { getCategory(entity) }
        return ProfileModel(
            name = entity.companyName,
            phone = entity.companyPhone,
            countryModel = country,
            regionModel = region,
            cityModel = city,
            address = entity.companyAddress,
            categoryModel = category,
            description = entity.companyDescription,
            mode = entity.companyMode?.takeIf { it > 0 },
            inn = entity.companyInn,
            kpp = entity.companyKpp,
            orgn = entity.companyOgrn
        )
    }

    suspend fun save(profileData: ProfileModel) {
        withContext(Dispatchers.IO) { dataSource.setProfile(ProfileSaveParams.Mapper.map(profileData)) }
    }

    private fun getCategory(e: ProfileEntity): CategoryModel {
        val regionId = e.companyType?.takeIfIdIsOk() ?: return CategoryModel.default()
        return CategoryMapper.map(pickerDataSource.getCategories().first { it.id == regionId })
    }

    private suspend fun getCountryModel(e: ProfileEntity): CountryModel {
        val code = e.country?.takeIfIdIsOk() ?: return CountryModel.default()
        return CountryMapper.map(pickerDataSource.getCountyByCode(code))
    }

    private suspend fun getRegionModel(e: ProfileEntity): RegionModel {
        val code = e.country?.takeIfIdIsOk()
        val regionId = e.region?.takeIfIdIsOk()
        return if (code == null || regionId == null) {
            RegionModel.default(countryId = code)
        } else {
            RegionMapper.map(pickerDataSource.getRegion(code, regionId))
        }
    }

    private suspend fun getCityModel(e: ProfileEntity): CityModel {
        val code = e.country?.takeIfIdIsOk()
        val regionId = e.region?.takeIfIdIsOk()
        val cityId = e.city?.takeIfIdIsOk()
        return if (code == null || regionId == null || cityId == null) {
            CityModel.default(regionId)
        } else {
            CityMapper.map(pickerDataSource.getCity(code, regionId, cityId))
        }
    }

    private fun String.takeIfIdIsOk() = if (!isNullOrBlank() && this != "0") this else null
}
