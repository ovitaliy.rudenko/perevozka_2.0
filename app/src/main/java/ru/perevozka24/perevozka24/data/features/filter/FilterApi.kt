package ru.perevozka24.perevozka24.data.features.filter

import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query
import ru.perevozka24.perevozka24.data.base.Response
import ru.perevozka24.perevozka24.data.features.filter.entities.CityEntity
import ru.perevozka24.perevozka24.data.features.filter.entities.CountryEntity
import ru.perevozka24.perevozka24.data.features.filter.entities.RegionsEntity
import ru.perevozka24.perevozka24.data.features.filter.entities.SubCategoryEntity

interface FilterApi {
    @GET("api4/regions")
    suspend fun getRegions(@Query("country") country: String?): Response<List<RegionsEntity>>

    @GET("api3/categories")
    suspend fun getCategories(
        @Query("shipment") shipment: Int?,
        @Query("typeof") type: String?
    ): Response<List<SubCategoryEntity>>

    @GET("api3/cities/{regionId}")
    suspend fun getCities(@Path("regionId") regionId: String?): Response<List<CityEntity>>

    @GET("api4/countries")
    suspend fun getCountries(): Response<List<CountryEntity>>

    @GET("api4/gruz_kuzov_type")
    suspend fun getCargoBody(): Response<Map<String, String>>
}
