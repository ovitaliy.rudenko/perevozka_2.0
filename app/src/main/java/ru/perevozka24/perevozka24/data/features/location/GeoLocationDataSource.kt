package ru.perevozka24.perevozka24.data.features.location

class GeoLocationDataSource(
    private val fusedLocationDataSource: FusedLocationDataSource,
    private val ipApiLocationDataSource: IpApiLocationDataSource
) {

    suspend fun getLocation() = when (fusedLocationDataSource.hasPermissions()) {
        true -> fusedLocationDataSource.getNetwork()
        else -> ipApiLocationDataSource.getLocation()
    }

    fun getLocationFlow() = fusedLocationDataSource.getLocation()
}
