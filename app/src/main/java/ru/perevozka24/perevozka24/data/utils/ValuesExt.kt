package ru.perevozka24.perevozka24.data.utils

import androidx.core.text.HtmlCompat

fun Boolean.toInt(): Int = when (this) {
    true -> 1
    false -> 0
}

fun String.escape(): String = HtmlCompat.fromHtml(this, HtmlCompat.FROM_HTML_MODE_LEGACY).toString()
