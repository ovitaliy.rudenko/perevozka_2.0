package ru.perevozka24.perevozka24.data.features.location

import com.yandex.mapkit.directions.driving.DrivingRoute
import com.yandex.mapkit.geometry.Geometry
import com.yandex.mapkit.geometry.Point
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import ru.perevozka24.perevozka24.data.features.filter.FilterSelectionDataSource
import ru.perevozka24.perevozka24.data.features.filter.entities.FilterEntityMapper
import ru.perevozka24.perevozka24.data.features.filter.entities.FilterTypeEntity
import ru.perevozka24.perevozka24.data.features.location.entity.SimpleAddress
import ru.perevozka24.perevozka24.ui.filters.models.FilterDistanceCalculatorModel
import ru.perevozka24.perevozka24.ui.filters.models.FilterModelMapper

class LocationRepository(
    private val selectionDataSource: FilterSelectionDataSource,
    private val locationDataSource: LocationDataSource,
    private val distanceDataSource: DistanceDataSource
) {

    suspend fun getSelection(): FilterDistanceCalculatorModel {
        val e = selectionDataSource.getFilterItems(FilterTypeEntity.DISTANCE)
        val m = FilterModelMapper.map(e)
        return m as FilterDistanceCalculatorModel
    }

    suspend fun setSelection(v: FilterDistanceCalculatorModel) =
        selectionDataSource.setFilterItems(FilterTypeEntity.DISTANCE, FilterEntityMapper.map(v))

    suspend fun getRoute(params: CalculateLocationParams): List<DrivingRoute> {
        val from = checkNotNull(getLocation(params.from))
        val to = checkNotNull(getLocation(params.to))
        return withContext(Dispatchers.Main) { distanceDataSource.calculate(from, to) }
    }

    suspend fun getRoute(from: String, to: String): List<DrivingRoute> {
        val pointFrom = checkNotNull(getLocation(from))
        val pointTo = checkNotNull(getLocation(to))
        return withContext(Dispatchers.Main) {
            distanceDataSource.calculate(pointFrom, pointTo)
        }
    }

    suspend fun getLocation(params: LocationParams): Point? {
        return withContext(Dispatchers.IO) { locationDataSource.getLocation(params) }
    }

    private suspend fun getLocation(query: String): Point? {
        return withContext(Dispatchers.IO) { locationDataSource.getLocation(query) }
    }

    suspend fun getAddress(geometry: Geometry, query: String): List<SimpleAddress> =
        withContext(Dispatchers.Main) { locationDataSource.getAddress(geometry, query) }

    suspend fun getAddress(point: Point, zoom: Float) =
        withContext(Dispatchers.Main) { locationDataSource.getAddress(point, zoom) }
}
