package ru.perevozka24.perevozka24.data.features.auth

data class LoginParams(
    val username: String,
    val password: String
)

data class RegistrationParams(
    val firstname: String,
    val email: String,
    val password: String,
    val passconf: String,
    val agreement: Int
)

data class RestoreParams(
    val email: String
)
