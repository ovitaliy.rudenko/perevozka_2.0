package ru.perevozka24.perevozka24.data.features.profile

import ru.perevozka24.perevozka24.ui.profile.models.ProfileModel

data class ProfileSaveParams(
    val category: String?,
    val name: String?,
    val country: String?,
    val region: String?,
    val city: String?,
    val phone: String?,
    val address: String?,
    val description: String?,
    val mode: Int?,
    val inn: String?,
    val kpp: String?,
    val orgn: String?
) {
    object Mapper : ru.perevozka24.perevozka24.ui.common.Mapper<ProfileModel, ProfileSaveParams> {
        override fun map(e: ProfileModel): ProfileSaveParams {
            return ProfileSaveParams(
                category = e.categoryModel.id,
                name = e.name,
                country = e.countryModel.id,
                region = e.regionModel.id,
                city = e.cityModel.id,
                phone = e.phone,
                address = e.address,
                description = e.description,
                mode = e.mode,
                inn = e.inn,
                kpp = e.kpp,
                orgn = e.orgn
            )
        }
    }
}
