package ru.perevozka24.perevozka24.data.features.items.blacklist

import ru.perevozka24.perevozka24.data.features.user.UserDataSource

class BlacklistItemsDataSourceImpl(
    private val userDataSource: UserDataSource,
    private val localDataSource: BlacklistItemsDataSource,
    private val remoteDataSource: BlacklistItemsDataSource
) : BlacklistItemsDataSource {
    override suspend fun addToBlackList(itemId: String) =
        if (userDataSource.authorized) {
            remoteDataSource.addToBlackList(itemId)
        } else {
            localDataSource.addToBlackList(itemId)
        }

    override fun getBlackList(): List<String>? =
        if (userDataSource.authorized) {
            remoteDataSource.getBlackList()
        } else {
            localDataSource.getBlackList()
        }
}
