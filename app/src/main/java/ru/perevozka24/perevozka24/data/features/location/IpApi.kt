package ru.perevozka24.perevozka24.data.features.location

import retrofit2.http.GET
import ru.perevozka24.perevozka24.data.features.location.entity.IpApiLocationResponse

interface IpApi {
    @GET("/")
    suspend fun getMyLocation(): IpApiLocationResponse
}
