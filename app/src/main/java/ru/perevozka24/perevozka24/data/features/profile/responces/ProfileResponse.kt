package ru.perevozka24.perevozka24.data.features.profile.responces

import ru.perevozka24.perevozka24.data.base.BaseResponse
import ru.perevozka24.perevozka24.data.features.profile.entities.ProfileEntity

class ProfileResponse(val results: ProfileEntity) : BaseResponse()
