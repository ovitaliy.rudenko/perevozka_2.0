package ru.perevozka24.perevozka24.data.features.location

import android.content.Context
import android.location.Geocoder
import com.yandex.mapkit.geometry.Geometry
import com.yandex.mapkit.geometry.Point
import com.yandex.mapkit.search.Response
import com.yandex.mapkit.search.SearchFactory
import com.yandex.mapkit.search.SearchManagerType
import com.yandex.mapkit.search.SearchOptions
import com.yandex.mapkit.search.SearchType
import com.yandex.mapkit.search.Session
import com.yandex.runtime.Error
import ru.perevozka24.perevozka24.data.features.location.entity.SimpleAddress
import ru.perevozka24.perevozka24.ui.picker.map.findBest
import ru.perevozka24.perevozka24.ui.picker.map.toSimple
import java.io.IOException
import java.util.Locale
import kotlin.coroutines.resume
import kotlin.coroutines.resumeWithException
import kotlin.coroutines.suspendCoroutine
import kotlin.math.ceil

class LocationDataSource(context: Context) {
    private val geocoder = Geocoder(context, Locale("ru", "RU"))

    init {
        SearchFactory.initialize(context)
    }

    suspend fun getLocation(params: LocationParams): Point? {
        return getLocation(params.buildSearch())
    }

    suspend fun getLocation(query: String): Point? {
        if (query.isEmpty()) return null

        return suspendCoroutine { cont ->
            var i = 0
            var ex: Exception? = null
            while (i++ < RETRIES) {
                try {
                    val addressList = geocoder.getFromLocationName(query, MAX_RESULTS)
                    val address = addressList.findBest(query)
                    val point = address?.let {
                        Point(it.latitude, it.longitude)
                    }
                    cont.resume(point)
                    ex = null
                    break
                } catch (e: IOException) {
                    ex = e
                }
            }
            ex?.let {
                cont.resumeWithException(it)
            }
        }
    }

    suspend fun getAddress(geometry: Geometry, query: String): List<SimpleAddress> {
        val searchManager =
            SearchFactory.getInstance().createSearchManager(SearchManagerType.COMBINED)

        return suspendCoroutine { cont ->
            val searchListener = object : Session.SearchListener {
                override fun onSearchError(error: Error) {
                    cont.resumeWithException(Exception(error.toString()))
                }

                override fun onSearchResponse(response: Response) {
                    val addresses = response.collection.children
                        .map { it.toSimple() }
                    cont.resume(addresses)
                }
            }
            val options = SearchOptions().setSearchTypes(SearchType.GEO.value)
                .setResultPageSize(YANDEX_MAX_RESULTS)

            searchManager.submit(query, geometry, options, searchListener)
        }
    }

    suspend fun getAddress(point: Point): List<SimpleAddress> {
        return suspendCoroutine { cont ->
            val result = geocoder.getFromLocation(point.latitude, point.longitude, MAX_RESULTS)
                .map { it.toSimple() }
            cont.resume(result)
        }
    }

    suspend fun getAddress(point: Point, zoom: Float): List<SimpleAddress> {
        val searchManager =
            SearchFactory.getInstance().createSearchManager(SearchManagerType.COMBINED)

        return suspendCoroutine { cont ->
            val searchListener = object : Session.SearchListener {
                override fun onSearchError(error: Error) {
                    cont.resumeWithException(Exception(error.toString()))
                }

                override fun onSearchResponse(response: Response) {
                    val addresses = response.collection.children
                        .map { it.toSimple() }
                    cont.resume(addresses)
                }
            }
            val options = SearchOptions().setSearchTypes(SearchType.GEO.value)
                .setResultPageSize(YANDEX_MAX_RESULTS)

            searchManager.submit(point, ceil(zoom).toInt(), options, searchListener)
        }
    }

    private companion object {
        const val MAX_RESULTS = 5
        const val YANDEX_MAX_RESULTS = 50
        const val RETRIES = 3
    }
}
