package ru.perevozka24.perevozka24.data.features.location.entity

import com.yandex.mapkit.geometry.Point

data class IpApiLocationResponse(
    val lat: Double,
    val lon: Double
) {
    fun toPoint() = Point(lat, lon)
}
