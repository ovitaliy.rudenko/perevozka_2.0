package ru.perevozka24.perevozka24.data.features.offers

import org.kodein.di.Kodein
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.singleton
import org.kodein.di.weakReference
import ru.perevozka24.perevozka24.data.features.offers.favorite.favoriteOffers

val offers = Kodein.Module("offers") {
    import(favoriteOffers)

    bind<OffersDataSource>() with singleton(ref = weakReference) {
        OffersDataSource(instance(), instance())
    }
}
