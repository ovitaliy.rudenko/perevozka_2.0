package ru.perevozka24.perevozka24.data.base

class ApiException(message: String) : Exception(message)
