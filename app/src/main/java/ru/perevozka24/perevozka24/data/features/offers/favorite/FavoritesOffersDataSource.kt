package ru.perevozka24.perevozka24.data.features.offers.favorite

import ru.perevozka24.perevozka24.data.features.offers.entity.OffersEntity

interface FavoritesOffersDataSource {

    suspend fun getFavorites(): OffersEntity
    suspend fun addFavorite(itemId: String)
    suspend fun removeFavorite(itemId: String)
}
