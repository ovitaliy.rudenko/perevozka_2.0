package ru.perevozka24.perevozka24.data.features.offers

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import ru.perevozka24.perevozka24.data.features.filter.FilterPickerDataSource
import ru.perevozka24.perevozka24.data.features.offers.entity.OffersEntity
import ru.perevozka24.perevozka24.data.features.offers.favorite.FavoritesOffersDataSource
import ru.perevozka24.perevozka24.ui.main.orders.models.OfferAbuseModel
import ru.perevozka24.perevozka24.ui.main.orders.models.OfferMapper
import ru.perevozka24.perevozka24.ui.main.orders.models.OfferModel
import ru.perevozka24.perevozka24.ui.main.orders.models.OfferStatus

class OfferRepository(
    private val favoritesDataSource: FavoritesOffersDataSource,
    private val dataSource: OffersDataSource,
    private val filterPickerDataSource: FilterPickerDataSource
) {
    suspend fun getOffer(id: String): OfferModel {
        return withContext(Dispatchers.IO) {
            proceedOffer(dataSource.getOffer(id))
        }
    }

    suspend fun getContacts(id: String): OfferModel {
        return withContext(Dispatchers.IO) {
            dataSource.getContacts(id)
            getOffer(id)
        }
    }

    suspend fun takeOffer(params: OfferTakeParams) {
        dataSource.takeOffer(params)
    }

    suspend fun cancelOffer(params: OfferCancelParams) {
        dataSource.cancelOffer(params)
    }

    suspend fun loadAbuses(id: String) =
        OfferAbuseModel.Mapper.map(dataSource.loadAbuses(id))

    suspend fun createOffer(params: OfferCreateParams) =
        withContext(Dispatchers.IO) { dataSource.createOffer(params) }

    private suspend fun proceedOffer(offersEntity: OffersEntity): OfferModel {
        return withContext(Dispatchers.IO) {
            val entity = checkNotNull(offersEntity.data).first().run {
                copy(parentCategory = filterPickerDataSource.getCategory(typeOf, sale))
            }
            val offer = OfferMapper.map(entity)
            val myOffersId = dataSource.unTakeOffer()
            val isMy = myOffersId == offer.id
            val contacts = if (isMy) {
                dataSource.getContacts(entity.id).contacts?.getPhone()
            } else {
                null
            }
            offer.copy(
                contacts = contacts,
                code = OfferStatus.build(entity, isMy)
            ).applyFavorites(favoritesDataSource)
        }
    }

    private fun String.getPhone(): String {
        return Regex("tel:(.*?)'")
            .find(this)
            ?.value
            ?.replace("'", "")
            ?.replace("tel:", "")
            ?: this
    }
}
