package ru.perevozka24.perevozka24.data.features.filter.entities

import com.google.gson.annotations.SerializedName
import ru.perevozka24.perevozka24.ui.filters.models.Direction

data class RegionEntity(
    @SerializedName("region_id") val id: String?,
    val name: String?,
    val countryId: String?,
    val direction: Direction?
)
