package ru.perevozka24.perevozka24.data.features.items.blacklist

import org.kodein.di.Kodein
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.singleton
import org.kodein.di.weakReference

val blacklist = Kodein.Module("blacklist") {

    bind<BlacklistItemsDataSource>("local") with singleton(ref = weakReference) {
        BlacklistItemsLocalDataSource(
            instance()
        )
    }
    bind<BlacklistItemsDataSource>("remote") with singleton(ref = weakReference) {
        BlacklistItemsRemoteDataSource(
            instance(),
            instance()
        )
    }
    bind<BlacklistItemsDataSource>() with singleton(ref = weakReference) {
        BlacklistItemsDataSourceImpl(
            instance(),
            instance("local"),
            instance("remote")
        )
    }
}
