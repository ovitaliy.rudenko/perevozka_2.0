package ru.perevozka24.perevozka24.data.cache

interface CacheKey {
    val key: String
    val type: Class<*>
}
