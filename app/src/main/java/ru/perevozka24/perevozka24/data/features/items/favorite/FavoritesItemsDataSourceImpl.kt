package ru.perevozka24.perevozka24.data.features.items.favorite

import ru.perevozka24.perevozka24.data.features.items.entities.ItemEntity
import ru.perevozka24.perevozka24.data.features.user.UserDataSource

class FavoritesItemsDataSourceImpl(
    private val userDataSource: UserDataSource,
    private val localDataSource: FavoritesItemsDataSource,
    private val remoteDataSource: FavoritesItemsDataSource
) : FavoritesItemsDataSource {

    override suspend fun getFavorites(): List<ItemEntity> =
        if (userDataSource.authorized) {
            remoteDataSource.getFavorites()
        } else {
            localDataSource.getFavorites()
        }

    override suspend fun addFavorite(itemId: String) {
        if (userDataSource.authorized) {
            remoteDataSource.addFavorite(itemId)
        } else {
            localDataSource.addFavorite(itemId)
        }
    }

    override suspend fun removeFavorite(itemId: String) {
        if (userDataSource.authorized) {
            remoteDataSource.removeFavorite(itemId)
        } else {
            localDataSource.removeFavorite(itemId)
        }
    }
}
