package ru.perevozka24.perevozka24.data.features.filter

import ru.perevozka24.perevozka24.data.features.filter.entities.FilterEntityMapper
import ru.perevozka24.perevozka24.data.features.filter.entities.FilterTypeEntity
import ru.perevozka24.perevozka24.data.features.filter.entities.FilterTypeEntityMapper
import ru.perevozka24.perevozka24.ui.filters.filter.FilterType
import ru.perevozka24.perevozka24.ui.filters.models.CountryModel
import ru.perevozka24.perevozka24.ui.filters.models.FilterModel
import ru.perevozka24.perevozka24.ui.filters.models.FilterModelMapper
import ru.perevozka24.perevozka24.ui.filters.models.RegionModel

class FilterRepository(
    private val selectionDataSource: FilterSelectionDataSource
) {
    suspend fun getFilterItems(type: FilterType): FilterModel {
        val entity = selectionDataSource.getFilterItems(
            FilterTypeEntityMapper.map(type)
        )
        return FilterModelMapper.map(entity)
    }

    fun setFilterItems(type: FilterType, v: FilterModel) {
        val entity = FilterEntityMapper.map(v)
        selectionDataSource.setFilterItems(FilterTypeEntityMapper.map(type), entity)
    }

    suspend fun updateCargoRoutes(
        startCountry: CountryModel,
        startRegionCity: RegionModel,
        endCountry: CountryModel,
        endRegion: RegionModel
    ) {
        val entity = selectionDataSource.getFilterItems(FilterTypeEntity.CARGO)
        val m = FilterModelMapper.map(entity)
            .updated(startCountry)
            .updated(startRegionCity)
            .updated(endCountry)
            .updated(endRegion)

        setFilterItems(FilterType.CARGO, m)
    }
}
