package ru.perevozka24.perevozka24.data.cache

import com.google.gson.Gson

object GsonCacheMapper : CacheMapper {
    private val gson = Gson()
    override fun <T> toJson(value: T): String {
        return gson.toJson(value)
    }

    override fun <T> fromJson(value: String, type: Class<T>): T {
        return gson.fromJson(value, type)
    }
}
