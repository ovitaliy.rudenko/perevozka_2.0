package ru.perevozka24.perevozka24.data.features.items

import org.kodein.di.Kodein
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.singleton
import org.kodein.di.weakReference
import ru.perevozka24.perevozka24.data.features.items.blacklist.blacklist
import ru.perevozka24.perevozka24.data.features.items.favorite.favorites

val items = Kodein.Module("items") {
    import(blacklist)
    import(favorites)

    bind<ItemsDataSource>() with singleton(ref = weakReference) {
        ItemsDataSource(instance(), instance())
    }
}
