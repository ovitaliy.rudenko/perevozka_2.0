package ru.perevozka24.perevozka24.data.base

import com.google.gson.annotations.SerializedName

open class BaseResponse {

    @SerializedName("error")
    private var _error: String? = null

    @SerializedName("errors")
    private var _errors: String? = null

    @SerializedName("error_message")
    private var _errorMessage: String? = null

    @SerializedName("message")
    private var _message: String? = null
    val errorMessage: String?
        get() = _errorMessage ?: _errors ?: _message ?: _error

    private val status: String? = null
    private val state: String? = null
    val isOk: Boolean
        get() {
            val result = status ?: state
            return result == "ok" || result == "success"
        }
}

fun <T : BaseResponse> T.checkIsOk(): T {
    if (!isOk) throw ApiException(errorMessage ?: "Sorry, something went wrong")
    return this
}
