package ru.perevozka24.perevozka24.data.features.items

import ru.perevozka24.perevozka24.data.features.items.entities.ItemEntity
import ru.perevozka24.perevozka24.data.features.items.entities.ItemSimpleEntity
import ru.perevozka24.perevozka24.data.features.user.UserDataSource

class ItemsDataSource(
    private val userDataSource: UserDataSource,
    private val api: ItemsApi
) {

    suspend fun getMyItems(): List<ItemEntity> = api.getMyItems(
        userDataSource.authToken,
        1
    ).results

    suspend fun search(query: String): List<ItemEntity> = api.search(
        userDataSource.authToken,
        query
    ).results

    suspend fun getItems(params: ItemsParams): List<ItemEntity> =
        api.getItems(
            token = userDataSource.authToken,
            shipment = params.shipment,
            parentCategory = params.type,
            category = params.category,
            countryId = params.country,
            regionId = params.region,
            cityId = params.cityId,
            sale = params.sale,
            blacklist = params.blackList?.joinToString(",")
        ).results

    suspend fun abuse(params: ItemAbuseParams) =
        api.abuse(
            token = userDataSource.authToken,
            itemId = params.itemId,
            text = params.text,
            contacts = params.contact
        )

    suspend fun getSimpleItems(params: ItemsParams): List<ItemSimpleEntity> {
        return api.getSimpleItem(
            token = userDataSource.authToken,
            shipment = params.shipment,
            parentCategory = params.type,
            category = params.category,
            countryId = params.country,
            regionId = params.region,
            cityId = params.cityId,
            sale = params.sale,
            blacklist = params.blackList?.joinToString(","),
            map = "1"
        ).results
    }

    suspend fun searchSimple(query: String): List<ItemSimpleEntity> = api.searchSimple(
        userDataSource.authToken,
        map = "1",
        query
    ).results

    suspend fun getItemByUri(uri: String): List<ItemEntity> {
        return api.getItemByUri(
            token = userDataSource.authToken,
            uri = uri
        ).results
    }
}
