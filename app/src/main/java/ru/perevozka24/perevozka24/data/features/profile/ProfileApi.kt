package ru.perevozka24.perevozka24.data.features.profile

import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST
import ru.perevozka24.perevozka24.data.base.BaseResponse
import ru.perevozka24.perevozka24.data.features.profile.responces.ProfileResponse

@SuppressWarnings("LongParameterList")
interface ProfileApi {
    @FormUrlEncoded
    @POST("api5/get_profile")
    suspend fun getProfile(
        @Field("token") token: String
    ): ProfileResponse

    @FormUrlEncoded
    @POST("api5/set_profile")
    suspend fun setProfile(
        @Field("token") token: String,
        @Field("company_type") category: String?,
        @Field("company_name") name: String?,
        @Field("country") country: String?,
        @Field("region") region: String?,
        @Field("city") city: String?,
        @Field("company_phone") phone: String?,
        @Field("company_address") address: String?,
        @Field("company_description") description: String?,
        @Field("company_mode") mode: Int?,
        @Field("company_inn") inn: String?,
        @Field("company_kpp") kpp: String?,
        @Field("company_ogrn") ogrn: String?
    ): BaseResponse
}
