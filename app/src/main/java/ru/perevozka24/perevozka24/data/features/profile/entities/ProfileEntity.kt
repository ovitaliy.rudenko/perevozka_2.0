package ru.perevozka24.perevozka24.data.features.profile.entities

import com.google.gson.annotations.SerializedName

data class ProfileEntity(
    @SerializedName("id") val id: String?,
    @SerializedName("company_name") val companyName: String?,
    @SerializedName("company_type") val companyType: String?,
    @SerializedName("country") val country: String?,
    @SerializedName("company_region_id") val region: String?,
    @SerializedName("company_city_id") val city: String?,
    @SerializedName("company_phone") val companyPhone: String?,
    @SerializedName("company_phone2") val companyPhone2: String?,
    @SerializedName("company_address") val companyAddress: String?,
    @SerializedName("company_email") val companyEmail: String?,
    @SerializedName("company_description") val companyDescription: String?,
    @SerializedName("company_mode") val companyMode: Int?,
    @SerializedName("company_inn") val companyInn: String?,
    @SerializedName("company_kpp") val companyKpp: String?,
    @SerializedName("company_ogrn") val companyOgrn: String?,
    @SerializedName("company_image") val companyImage: String?
)
