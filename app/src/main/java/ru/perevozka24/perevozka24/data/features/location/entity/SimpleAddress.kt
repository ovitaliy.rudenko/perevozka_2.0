package ru.perevozka24.perevozka24.data.features.location.entity

import android.os.Parcelable
import com.yandex.mapkit.geometry.Point
import com.yandex.mapkit.map.CameraPosition
import kotlinx.android.parcel.Parcelize
import ru.perevozka24.perevozka24.data.features.location.LocationParams

@Parcelize
data class SimpleAddress(
    val latitude: Double? = null,
    val longitude: Double? = null,
    val country: String? = null,
    val region: String? = null,
    val city: String? = null,
    val line: String? = null
) : Parcelable {
    fun toPoint() = if (latitude != null && longitude != null) Point(latitude, longitude) else null
    fun toCameraPosition(): CameraPosition? {
        val params = LocationParams(
            country = country,
            region = region,
            city = city,
            address = line
        )
        return toPoint()?.let { CameraPosition(it, params.zoomValue(), 0f, 0f) }
    }
}
