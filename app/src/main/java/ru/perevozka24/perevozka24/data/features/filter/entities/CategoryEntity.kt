package ru.perevozka24.perevozka24.data.features.filter.entities

data class CategoryEntity(
    val id: String?,
    val name: String?,
    val sale: Int = 0,
    val shipment: Int = 0,
    val icon: Int = 0
)
