package ru.perevozka24.perevozka24.data.features.items.favorite

import ru.perevozka24.perevozka24.data.features.items.entities.ItemEntity

interface FavoritesItemsDataSource {

    suspend fun getFavorites(): List<ItemEntity>

    suspend fun addFavorite(itemId: String)

    suspend fun removeFavorite(itemId: String)
}
