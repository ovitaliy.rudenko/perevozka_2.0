package ru.perevozka24.perevozka24.data.features.location

import android.Manifest
import android.content.Context
import android.os.Looper
import com.google.android.gms.location.LocationCallback
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationResult
import com.google.android.gms.location.LocationServices
import com.yandex.mapkit.geometry.Point
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.suspendCancellableCoroutine
import ru.perevozka24.perevozka24.ui.common.ext.hasPermission
import kotlin.coroutines.resume
import kotlin.coroutines.resumeWithException

class FusedLocationDataSource(private val context: Context) {
    private val fusedLocationProviderClient =
        LocationServices.getFusedLocationProviderClient(context)
    private val locationHighAccuracy: LocationRequest
        get() = LocationRequest().apply {
            numUpdates = 1
            priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        }
    private val locationNetwork: LocationRequest
        get() = LocationRequest().apply {
            numUpdates = 1
            priority = LocationRequest.PRIORITY_LOW_POWER
        }

    suspend fun getHighAccuracy() = getLocation(locationHighAccuracy)
    suspend fun getNetwork() = getLocation(locationNetwork)

    fun getLocation() = flow {
        emit(getNetwork())
        emit(getHighAccuracy())
    }

    fun hasPermissions() = context.hasPermission(
        Manifest.permission.ACCESS_FINE_LOCATION,
        Manifest.permission.ACCESS_COARSE_LOCATION
    )

    private suspend fun getLocation(request: LocationRequest): Point {
        if (!hasPermissions()) {
            error("no permission")
        }
        return suspendCancellableCoroutine { cont ->
             try {
                  val locationCallback = object : LocationCallback() {
                      override fun onLocationResult(result: LocationResult) {
                          result.lastLocation?.let {
                              val latLng = Point(it.latitude, it.longitude)
                              fusedLocationProviderClient.removeLocationUpdates(this)
                              cont.resume(latLng)
                          }
                      }
                  }

                  fusedLocationProviderClient.requestLocationUpdates(
                      request,
                      locationCallback,
                      Looper.getMainLooper()
                  )
              } catch (e: SecurityException) {
                  cont.resumeWithException(e)
              }
        }
    }
}
