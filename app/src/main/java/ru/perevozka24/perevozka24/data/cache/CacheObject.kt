package ru.perevozka24.perevozka24.data.cache

class CacheObject(
    val evictionType: String,
    val eviction: String,
    val value: String
)
