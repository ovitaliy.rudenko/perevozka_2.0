package ru.perevozka24.perevozka24.data.features.notification.entity

import com.google.gson.annotations.SerializedName

data class NotificationEntity(
    @SerializedName("typeof") val category: String?,
    @SerializedName("cat_id") val subCategory: String?,
    @SerializedName("country") val country: String?,
    @SerializedName("region_id") val region: String?,
    @SerializedName("city_id") val city: String?,
    @SerializedName("notify") val enabled: Int,
    @SerializedName("sound") val soundEnabled: Int
)
