package ru.perevozka24.perevozka24.data.cache

object NeverEvict : CacheEviction {
    override fun evicted() = false
}
