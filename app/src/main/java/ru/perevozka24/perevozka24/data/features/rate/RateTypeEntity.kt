package ru.perevozka24.perevozka24.data.features.rate

data class RateTypeEntity(
    val id: String,
    var name: String
)
