package ru.perevozka24.perevozka24.data.features.user

import android.content.SharedPreferences
import ru.perevozka24.perevozka24.data.prefs.prefInt
import ru.perevozka24.perevozka24.data.prefs.prefString

class UserDataSource(preferences: SharedPreferences) {

    var authToken: String? by prefString(preferences, "authToken")
    var userId: String? by prefString(preferences, "userId")
    var userEmail: String? by prefString(preferences, "userEmail")
    var userName: String? by prefString(preferences, "userName")
    var onBoardingWasShown: Int by prefInt(preferences, "onbording")
    var geolocationEnabled: Int by prefInt(preferences, "geolocationEnabled")
    var geolocationItem: String? by prefString(preferences, "geolocationItem")

    fun getUserEntity() = UserEntity(
        id = userId,
        email = userEmail,
        name = userName
    )

    val authorized: Boolean
        get() = authToken != null
}
