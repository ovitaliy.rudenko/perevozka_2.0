package ru.perevozka24.perevozka24.data.features.offers.favorite

import ru.perevozka24.perevozka24.data.features.offers.OffersApi
import ru.perevozka24.perevozka24.data.features.offers.entity.OffersEntity
import ru.perevozka24.perevozka24.data.features.user.UserDataSource

class FavoritesOffersRemoteDataSource constructor(
    private val userDataSource: UserDataSource,
    private val api: OffersApi
) : FavoritesOffersDataSource {
    override suspend fun getFavorites(): OffersEntity {
        val offers = api.favoriteOfferGet(userDataSource.authToken, "1")
        return offers.copy(
            data = (offers.data ?: emptyList()).map { it.copy(isFavorite = true) }
        )
    }

    override suspend fun addFavorite(itemId: String) {
        api.favoriteOfferAdd(userDataSource.authToken, itemId)
    }

    override suspend fun removeFavorite(itemId: String) {
        api.favoriteOfferRemove(userDataSource.authToken, itemId)
    }
}
