package ru.perevozka24.perevozka24.data.features.items.favorite

import android.content.SharedPreferences
import ru.perevozka24.perevozka24.data.base.BaseListPrefSource
import ru.perevozka24.perevozka24.data.features.items.ItemsApi
import ru.perevozka24.perevozka24.data.features.items.entities.ItemEntity

class FavoritesItemsLocalDataSource constructor(
    private val api: ItemsApi,
    sharedPreferences: SharedPreferences
) : BaseListPrefSource<String>(sharedPreferences),
    FavoritesItemsDataSource {

    override val key: String = "FavoritesItemsLocalDataSource_2"

    override suspend fun getFavorites(): List<ItemEntity> = emptyList()
    /*   api.getFavoriteNoAuth(getList().joinToString(","), 1)
           .results
           .map { it.copy(favorite = true) }*/

    override suspend fun addFavorite(itemId: String) {
        addTo(itemId)
    }

    override suspend fun removeFavorite(itemId: String) {
        remove(itemId)
    }
}
