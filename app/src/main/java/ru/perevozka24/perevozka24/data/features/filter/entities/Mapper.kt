package ru.perevozka24.perevozka24.data.features.filter.entities

import ru.perevozka24.perevozka24.ui.common.Mapper
import ru.perevozka24.perevozka24.ui.filters.models.CategoryModel
import ru.perevozka24.perevozka24.ui.filters.models.CityModel
import ru.perevozka24.perevozka24.ui.filters.models.CountryModel
import ru.perevozka24.perevozka24.ui.filters.models.RegionModel
import ru.perevozka24.perevozka24.ui.filters.models.SubCategoryModel

object CategoryMapper : Mapper<CategoryModel, CategoryEntity> {
    override fun map(e: CategoryModel) =
        CategoryEntity(
            id = e.id,
            name = e.name,
            sale = e.sale,
            shipment = e.shipment
        )
}

object SubCategoryMapper : Mapper<SubCategoryModel, SubCategoryEntity> {
    override fun map(e: SubCategoryModel) =
        SubCategoryEntity(
            id = e.id,
            image = e.icon,
            name = e.name,
            categoryId = e.categoryId,
            shipment = checkNotNull(e.shipment)
        )
}

object CityMapper : Mapper<CityModel, CityEntity> {
    override fun map(e: CityModel) =
        CityEntity(
            id = e.id,
            name = e.name,
            countryId = e.countryId,
            regionId = e.regionId,
            direction = e.direction
        )
}

object RegionMapper : Mapper<RegionModel, RegionEntity> {
    override fun map(e: RegionModel) =
        RegionEntity(
            id = e.id,
            name = e.name,
            countryId = e.countryId,
            direction = e.direction
        )
}

object CountryMapper : Mapper<CountryModel, CountryEntity> {
    override fun map(e: CountryModel) = CountryEntity(
        code = e.id,
        name = e.name,
        direction = e.direction
    )
}
