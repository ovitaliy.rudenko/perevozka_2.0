package ru.perevozka24.perevozka24.data.features.location

import com.yandex.mapkit.geometry.Point
import org.threeten.bp.Duration
import ru.perevozka24.perevozka24.data.cache.CacheProvider
import ru.perevozka24.perevozka24.data.cache.ExpireEvict
import ru.perevozka24.perevozka24.data.cache.SimpleCacheKey

class IpApiLocationDataSource(private val api: IpApi, private val cacheProvider: CacheProvider) {

    suspend fun getLocation(): Point = cacheProvider.proceed(
        SimpleCacheKey("current_location", Point::class.java),
        ExpireEvict.create(Duration.ofMinutes(2))
    ) { api.getMyLocation().toPoint() }
}
