package ru.perevozka24.perevozka24.data.features.location

import com.yandex.mapkit.RequestPoint
import com.yandex.mapkit.RequestPointType
import com.yandex.mapkit.directions.DirectionsFactory
import com.yandex.mapkit.directions.driving.DrivingOptions
import com.yandex.mapkit.directions.driving.DrivingRoute
import com.yandex.mapkit.directions.driving.DrivingRouter
import com.yandex.mapkit.directions.driving.DrivingSession
import com.yandex.mapkit.geometry.Point
import com.yandex.runtime.Error
import kotlin.coroutines.resume
import kotlin.coroutines.resumeWithException
import kotlin.coroutines.suspendCoroutine

class DistanceDataSource {
    suspend fun calculate(from: Point, to: Point): List<DrivingRoute> {
        return suspendCoroutine { cont ->
            val listener = object : DrivingSession.DrivingRouteListener {
                override fun onDrivingRoutesError(error: Error) {
                    cont.resumeWithException(Exception(error.toString()))
                }

                override fun onDrivingRoutes(result: MutableList<DrivingRoute>) {
                    cont.resume(result)
                }
            }

            calculate(from, to, listener)
        }
    }

    private fun calculate(
        from: Point,
        to: Point,
        listener: DrivingSession.DrivingRouteListener
    ) {

        val drivingRouter: DrivingRouter = DirectionsFactory.getInstance().createDrivingRouter()

        val options = DrivingOptions()
        val requestPoints = listOf(
            RequestPoint(from, RequestPointType.WAYPOINT, null),
            RequestPoint(to, RequestPointType.WAYPOINT, null)
        )

        drivingRouter.requestRoutes(
            requestPoints,
            options,
            listener
        )
    }
}
