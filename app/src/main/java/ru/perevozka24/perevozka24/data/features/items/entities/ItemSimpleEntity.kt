package ru.perevozka24.perevozka24.data.features.items.entities

import com.google.gson.annotations.SerializedName

data class ItemSimpleEntity(
    val id: String?,
    val uri: String?,
    @SerializedName("coord_x") val coordX: Double?,
    @SerializedName("coord_y") val coordY: Double?,
    @SerializedName("cat_id") val categoryId: Long?,
    @SerializedName("cat_image") val catIcon: String?
) {
    val categoryIcon: String?
        get() = catIcon?.let { if (it.startsWith("http")) it else "https://perevozka24.ru$it" } ?: {
            print(this)
            ""
        }()
}
