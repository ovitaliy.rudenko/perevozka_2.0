package ru.perevozka24.perevozka24.services.geolocation

import android.annotation.SuppressLint
import android.content.Context
import androidx.work.Constraints
import androidx.work.CoroutineWorker
import androidx.work.ExistingPeriodicWorkPolicy
import androidx.work.NetworkType
import androidx.work.PeriodicWorkRequest
import androidx.work.WorkManager
import androidx.work.WorkerParameters
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.androidXModule
import org.kodein.di.generic.instance
import ru.perevozka24.perevozka24.App
import ru.perevozka24.perevozka24.data.features.geolocation.LocationTrackerRepository
import ru.perevozka24.perevozka24.data.features.location.GeoLocationRepository
import ru.perevozka24.perevozka24.di.apiModule
import ru.perevozka24.perevozka24.di.dataModule
import ru.perevozka24.perevozka24.di.repositoryModule
import ru.perevozka24.perevozka24.ui.viewModelModule
import timber.log.Timber
import java.util.concurrent.TimeUnit

class LocationTrackerWorker(
    private val context: Context,
    workerParams: WorkerParameters
) : CoroutineWorker(context, workerParams), KodeinAware {

    override val kodein: Kodein by Kodein.lazy {
        import(androidXModule(context.applicationContext as App))
        import(apiModule)
        import(dataModule)
        import(repositoryModule)
        import(viewModelModule)
    }

    @SuppressWarnings("TooGenericExceptionCaught")
    override suspend fun doWork(): Result = withContext(Dispatchers.IO) {
        try {
            val locationTrackerRepository by instance<LocationTrackerRepository>()
            val geoLocationRepository by instance<GeoLocationRepository>()

            val id = locationTrackerRepository.getCurrentId()
            if (id != null) {
                val point = geoLocationRepository.getLocationPoint()
                if (point != null) {
                    locationTrackerRepository.postLocation(id, point.latitude, point.longitude)
                }
            } else {
                WorkManager.getInstance(applicationContext).cancelAllWorkByTag(TAG)
            }
            Result.success()
        } catch (e: Exception) {
            Timber.e(e)
            Result.failure()
        }
    }

    companion object {
        const val TAG = "LocationTrackerWorker"

        @SuppressLint("InvalidPeriodicWorkRequestInterval")
        fun start(context: Context) {
            val constraints = Constraints.Builder()
                .setRequiredNetworkType(NetworkType.CONNECTED).build()
            val uploadWorkRequest = PeriodicWorkRequest.Builder(
                LocationTrackerWorker::class.java, 1L, TimeUnit.MINUTES
            )
                .setConstraints(constraints)
                .addTag(TAG)
                .build()
            WorkManager.getInstance(context).enqueueUniquePeriodicWork(
                TAG,
                ExistingPeriodicWorkPolicy.REPLACE,
                uploadWorkRequest
            )
        }

        fun cancel(context: Context) {
            WorkManager.getInstance(context).cancelAllWorkByTag(TAG)
        }
    }
}
