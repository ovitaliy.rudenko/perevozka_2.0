package ru.perevozka24.perevozka24.services

import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.graphics.BitmapFactory
import android.net.Uri
import androidx.core.app.NotificationCompat
import androidx.core.app.TaskStackBuilder
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import com.google.gson.Gson
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance
import ru.perevozka24.perevozka24.MainActivity
import ru.perevozka24.perevozka24.R
import ru.perevozka24.perevozka24.data.features.notification.NotificationRepository
import ru.perevozka24.perevozka24.data.features.user.UserDataSource
import ru.perevozka24.perevozka24.ui.common.ext.safeSuspended
import ru.perevozka24.perevozka24.ui.main.orders.detailed.OfferDetailedActivity
import timber.log.Timber

/**
 * Created by ovi on 27/12/16.
 */
class AppFirebaseMessagingService : FirebaseMessagingService(), KodeinAware {

    override val kodein: Kodein by kodein()

    private val userDataSource: UserDataSource by instance<UserDataSource>()
    private val notificationRepository: NotificationRepository by instance<NotificationRepository>()

    override fun onNewToken(s: String) {
        super.onNewToken(s)
        if (userDataSource.authToken == null) {
            return
        }
        GlobalScope.launch {
            safeSuspended {
                notificationRepository.saveDefaults()
            }
        }
    }

    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        Timber.e(Gson().toJson(remoteMessage.notification))
        Timber.e(Gson().toJson(remoteMessage.data))
        // Check if message contains a data payload.
        if (remoteMessage.notification != null) {
            sendNotification(
                remoteMessage.notification?.clickAction,
                remoteMessage.notification?.title,
                remoteMessage.notification?.body,
                remoteMessage.data["offer_id"]
            )
        }
    }

    /**
     * Create and show a simple notification containing the received FCM message.
     */
    private fun sendNotification(
        type: String?,
        title: String?,
        messageBody: String?,
        offerId: String?
    ) {
        val context = this
        GlobalScope.launch {
            val sounds = withContext(Dispatchers.IO) {
                notificationRepository.getNotificationSetting().sounds
            }
            var stackBuilder = TaskStackBuilder.create(context)
            if (type == "view_offers") {
                stackBuilder = stackBuilder.addParentStack(MainActivity::class.java)
                val intent = Intent(context, OfferDetailedActivity::class.java)
                if (offerId != null) intent.putExtra("offerId", offerId)
                stackBuilder = stackBuilder.addNextIntent(intent)
            } else {
                stackBuilder = stackBuilder.addNextIntent(Intent(context, MainActivity::class.java))
            }
            val pendingIntent = stackBuilder.getPendingIntent(
                0,
                PendingIntent.FLAG_UPDATE_CURRENT or PendingIntent.FLAG_ONE_SHOT
            )
            val notificationBuilder =
                NotificationCompat.Builder(
                    context,
                    CHANNEL_ID
                )
                    .setSmallIcon(R.drawable.notification)
                    .setLargeIcon(BitmapFactory.decodeResource(resources, R.mipmap.ic_launcher))
                    .setContentTitle(title)
                    .setContentText(messageBody)
                    .setAutoCancel(true)
                    .setContentIntent(pendingIntent)
            if (sounds) {
                notificationBuilder.setSound(
                    Uri.parse("android.resource://" + packageName + "/" + R.raw.notification)
                )
            }
            notificationBuilder.setLights(LIGHT_COLOR, ON_DELAY, OFF_DELAY)
            val notificationManager =
                getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

            notificationManager.cancel(0)
            notificationManager.notify(0 /* ID of notification */, notificationBuilder.build())
        }
    }

    companion object {
        const val CHANNEL_ID = "fms_channel"
        private const val LIGHT_COLOR = 16370732
        private const val ON_DELAY = 3000
        private const val OFF_DELAY = 3000
    }
}
