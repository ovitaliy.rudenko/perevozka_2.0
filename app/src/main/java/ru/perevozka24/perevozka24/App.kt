package ru.perevozka24.perevozka24

import android.app.NotificationChannel
import android.app.NotificationManager
import android.os.Build
import androidx.multidex.MultiDexApplication
import androidx.work.Configuration
import androidx.work.DelegatingWorkerFactory
import androidx.work.WorkManager
import com.facebook.stetho.Stetho
import com.google.firebase.FirebaseApp
import com.jakewharton.threetenabp.AndroidThreeTen
import com.yandex.mapkit.MapKitFactory
import com.yandex.metrica.YandexMetrica
import com.yandex.metrica.YandexMetricaConfig
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.androidXModule
import ru.perevozka24.perevozka24.di.apiModule
import ru.perevozka24.perevozka24.di.dataModule
import ru.perevozka24.perevozka24.di.repositoryModule
import ru.perevozka24.perevozka24.services.AppFirebaseMessagingService.Companion.CHANNEL_ID
import ru.perevozka24.perevozka24.ui.viewModelModule
import timber.log.Timber
import timber.log.Timber.DebugTree

class App : MultiDexApplication(), KodeinAware {

    override val kodein = Kodein {
        import(androidXModule(this@App))
        import(apiModule)
        import(dataModule)
        import(repositoryModule)
        import(viewModelModule)
    }

    override fun onCreate() {
        super.onCreate()
        Stetho.initializeWithDefaults(this)
        instance = this
        MapKitFactory.setApiKey(getString(R.string.yandex_key))
        AndroidThreeTen.init(this)
        FirebaseApp.initializeApp(this)
        if (BuildConfig.DEBUG) {
            Timber.plant(DebugTree())
        }
        setupNotificationChannel()

        val config = YandexMetricaConfig.newConfigBuilder(getString(R.string.yandex_metrica_key))
            .withLogs()
            .build()
        YandexMetrica.activate(applicationContext, config)
        YandexMetrica.enableActivityAutoTracking(this)

        initWorkManager()
    }

    private fun setupNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            // Create the NotificationChannel
            val name: CharSequence = "Объявления"
            val description = "Уведомления о новых объявлених"
            val importance = NotificationManager.IMPORTANCE_DEFAULT
            val channel = NotificationChannel(CHANNEL_ID, name, importance)
            channel.description = description
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            val notificationManager = getSystemService(NOTIFICATION_SERVICE) as NotificationManager
            notificationManager.createNotificationChannel(channel)
        }
    }

    @SuppressWarnings("TooGenericExceptionCaught")
    private fun initWorkManager() {
        try {
            val config = Configuration.Builder()
                .setWorkerFactory(DelegatingWorkerFactory()) // Overrides default WorkerFactory
                .build()
            WorkManager.initialize(this, config)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    companion object {
        lateinit var instance: App
    }
}
