package ru.perevozka24.perevozka24

import java.text.SimpleDateFormat
import java.util.Locale

const val API_URL = "https://perevozka24.ru/"
const val ICON_PATH = "$API_URL/img_beta/icons_white/"
const val ICON_DEFAULT = "${ICON_PATH}tehnika1.jpg"
const val DEFAULT_DATE_FORMAT = "yyyy-MM-dd HH:mm"
const val DEFAULT_DATE_FORMAT_2 = "dd.MM.yyyy HH:mm"
const val DEFAULT_DATE_FORMAT_3 = "yyyy-MM-dd HH:mm:ss"
const val DATE_FORMAT = "dd.MM.yyyy"

val MIN_SEC_FORMAT = SimpleDateFormat("mm:ss", Locale.getDefault())

const val IP_API_URL = "http://ip-api.com/json/"

const val DEFAULT_REQUEST_DELAY = 200L
